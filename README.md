smapshot is a project developped at the University of Applied Sciences Western Switzerland. It was supported by the Hasler Foundation, ETHZ, swisstopo, Cultural Percent Migros.
The production server is accessible here: https://smapshot.heig-vd.ch

# Warning
smapshot is still a prototype. The next stage of the project is to create a clean, well documented open source repository. You will get more informations here.

# Requirement

## Postgis database
The connection parameters are found in `app/model/index.js`

## Node server
The server is launched with the command `node server.js`

## Images
The images are stored in the folder `public/data/collections/id_collection/images`

