var math = require('mathjs')

exports.convergenceMeridien = function (y, x) {
		// console.log('y', y)
		// console.log('x', x)
		// y = 679520.05-600000;
		// x = 212273.44-200000;

		// https://www.swisstopo.admin.ch/content/swisstopo-internet/en/online/calculation-services/_jcr_content/contentPar/tabs/items/documents_publicatio/tabPar/downloadlist/downloadItems/20_1467104436749.download/refsyse.pdf
  var p1 = 10.668 * y * Math.pow(10, -6)
  var p2 = 1.788 * y * x * Math.pow(10, -12)
  var p3 = -0.14 * Math.pow(y, 3) * Math.pow(10, -18)
  var p4 = 4.3 * Math.pow(10, -19) * y * Math.pow(x, 2)
		// var muDegre = (p1+p2+p3+p4)*0.9;
		// console.log('muGon', p1+p2+p3)
  var muDegre = (p1 + p2 + p3) * 360 / 400
  return muDegre
}

function computeError (p, gcpArray) {
		// Generate matrix of the parameters
  pMat = math.matrix([p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]])

		// Compute number of observation (GCP)
  var nObs = gcpArray.length

		// Generate matrix for the GCPs
  var XYZarr = []
		// var xyarr = [];
  var x = []
  var y = []
  for (var i = 0; i < nObs; i++) {
    XYZarr.push([gcpArray[i].X, gcpArray[i].Y, gcpArray[i].Z])
			// xyarr.push([data[i].line, data[i].column]); //les entetes de colonnes sont fausses dans pic2map
    x.push(gcpArray[i].x)
    y.push(gcpArray[i].y)
  }
  var XYZ = math.matrix(XYZarr)
		// var xy = math.matrix(xyarr);
  var x = math.matrix(x)
  var y = math.matrix(y)

		// Compute image coordinates of the 3D GCP with computed parameters
  var xy_reproj = project3Dto2D(XYZ, pMat)
  var x_reproj = math.squeeze(xy_reproj[0])
  var y_reproj = math.squeeze(xy_reproj[1])

		// Computation of the residuals
  var dx = math.subtract(x, math.transpose(x_reproj))
  var dy = math.subtract(y, math.transpose(y_reproj))
  var dxy = math.dotPow(math.add(math.dotPow(dx, 2), math.dotPow(dy, 2)), 0.5)// math.dotPow((,0.5)

		// Generate output
  var dxyArray = []
  dxy.forEach(function (value, index, matrix) {
    dxyArray.push(value)
  })

  return [xy_reproj, dxyArray]
};

function LM_to_cesium_angles (az, tilt, roll) {
  var az2 = azLM2cesium(rad2deg(az))
  var tilt2 = tiltLM2cesium(rad2deg(tilt))
  var roll2 = rollLM2cesium(rad2deg(roll))

  return [az2, tilt2, roll2]
};

function rollLM2cesium (roll) {
  return -roll
};

function tiltLM2cesium (tilt) {
  return 90.0 - tilt
};

function azLM2cesium (azimuth) {
  return azimuth + 270
};

function cesium_to_LM_angles (az, tilt, roll) {
  var az2 = deg2rad(azCesium2LM(az))
		// console.log('LM tilt', tiltCesium2LM(tilt))
  var tilt2 = deg2rad(tiltCesium2LM(tilt))
  var roll2 = deg2rad(rollCesium2LM(roll))

  return [az2, tilt2, roll2]
};

function rollCesium2LM (roll) {
		// return roll
  return roll - 360.0
};

function tiltCesium2LM (tilt) {
  return 90 - tilt // TIMI
		// return tilt+90.
};

function azCesium2LM (azimuth) {
		// Modulus operators keeps only the remainder of azimuth/360
  azimuth = azimuth % 360

  switch (true) {
    case (azimuth >= 0.0) && (azimuth <= 90.0):
      var az = 90 + azimuth // TIMI
				// var az = 90-azimuth;
				// console.log('az',1); //OK?
      break
    case (azimuth > 90.0) && (azimuth <= 180.0):
      var az = azimuth + 90.0// 225.;-(azimuth - 90.);
				// console.log('az',2);
      break
    case (azimuth > 180.0) && (azimuth <= 270.0):
      var az = azimuth + 90.0
				// console.log('az',3);
      break
    case (azimuth > 270.0) && (azimuth <= 360.0):
      var az = (azimuth - 270)// 180.-(azimuth-270.);
				// console.log('az',4);
      break
  }
  return az
}
function pic2map_to_LM_angles (az, tilt, roll) {
  var az2 = deg2rad(az2rad(az))
  var tilt2 = deg2rad(cesiumTilt2Tilt(tilt))
  var roll2 = deg2rad(roll)

  return [az2, tilt2, roll2]
}
	// function cesiumTilt2Tilt (tilt){
	// 	tilt2 = (90.+(90.-tilt));
	// 	console.log('tilt2', tilt2);
	// 	return tilt2
	// };
function az2rad (azimuth) {
  switch (true) {
    case (azimuth >= 0.0) && (azimuth <= 90.0):
      var az = -(90 - azimuth)
      console.log('az', 1)
      break
    case (azimuth > 90.0) && (azimuth <= 180.0):
      var az = (azimuth - 90.0)
      console.log('az', 2)
      break
    case (azimuth > 180.0) && (azimuth <= 270.0):
      var az = (azimuth - 90.0)
      console.log('az', 3)
      break
    case (azimuth > 270.0) && (azimuth <= 360.0):
      var az = -90.0 - (360.0 - azimuth)
      console.log('az', 4)
      break
  }
  var az2 = -az
  console.log('az2', az2)
    // var az_rad = -deg2rad(az);
  return az2
}
function rad2deg (alpha) {
			// Translate degrees angles in radians
  var alpha2 = alpha * 180.0 / math.pi
  return alpha2
}
function deg2rad (alpha) {
	    // Translate degrees angles in radians
	    var alpha2 = alpha * math.pi / 180.0
	    return alpha2
}
function compute_Rot2 (alpha) {
		// Around North
	  var Rot2 = math.matrix([[math.cos(alpha), 0.0, math.sin(alpha)], [0.0, 1.0, 0.0], [-math.sin(alpha), 0.0, math.cos(alpha)]])
  return Rot2
}
exports.compute_Rot3 = function (alpha) {
		// Around Z
  var Rot3 = math.matrix([[math.cos(alpha), -math.sin(alpha), 0.0], [math.sin(alpha), math.cos(alpha), 0.0], [0.0, 0.0, 1.0]])
  return Rot3
}
function compute_Rot3 (alpha) {
		// Around Z
  var Rot3 = math.matrix([[math.cos(alpha), -math.sin(alpha), 0.0], [math.sin(alpha), math.cos(alpha), 0.0], [0.0, 0.0, 1.0]])
  return Rot3
}
function compute_R (R1, R2, R3) {
	  var R = math.multiply(R3, math.multiply(R2, R1))
	  return R
}
exports.camera2world = function (xyz, x) {
		// console.log('size', math.size(xyz))
		// Compute ZXZ rotation matrix
  var RotZ2 = compute_Rot3(math.subset(x, math.index([5])))
  var RotN = compute_Rot2(math.subset(x, math.index([4])))
  var RotZ = compute_Rot3(math.subset(x, math.index([3])))

		// Combine rotations
  var R = compute_R(RotZ, RotN, RotZ2)

		// Vector camera DEM
  var T = math.matrix([[math.subset(x, math.index([0])), math.subset(x, math.index([1])), math.subset(x, math.index([2]))]])
  var nObs = math.subset(math.size(xyz), math.index([0]))
		// console.log('nobs', nObs);
  var Tarr = []
  for (var i = 0; i < nObs; i++) {
    Tarr.push(math.squeeze(T))
  }
  var Tmat = math.transpose(math.matrix(Tarr))
		// console.log('Tmat', Tmat);

		// var rotT = math.multiply(math.transpose(R),Tmat);
		// console.log('rotT', rotT);

		// var xyzC = math.subtract(math.transpose(xyz),rotT);
		// var XYZ = math.multiply(math.transpose(R),xyzC);
  var rotXyz = math.multiply(math.transpose(R), math.transpose(xyz))
		// console.log('rotXyz', rotXyz);
  var XYZ = math.add(rotXyz, Tmat)
		// console.log('rotXyz', rotXyz);

		// var XYZ = math.subtract(rotXyz,rotT);
		// console.log('XYZ', XYZ);

  return XYZ
}

function world2camera (XYZ, x) {
	  // Rigid body transformation

		// Compute ZXZ rotation matrix
  var RotZ2 = compute_Rot3(math.subset(x, math.index([5])))
  var RotN = compute_Rot2(math.subset(x, math.index([4])))
  var RotZ = compute_Rot3(math.subset(x, math.index([3])))

		// Combine rotations
  var R = compute_R(RotZ, RotN, RotZ2)

	  // Vector camera DEM
  var T = math.matrix([[math.subset(x, math.index([0])), math.subset(x, math.index([1])), math.subset(x, math.index([2]))]])

  var nObs = math.subset(math.size(XYZ), math.index([0]))

  var Tarr = []
  for (var i = 0; i < nObs; i++) {
    Tarr.push(math.squeeze(T))
  }

  var Tmat = math.matrix(Tarr)

	  var XYZc = math.subtract(XYZ, Tmat)

	  // Rotate according to rotation matrix
	  xyz = math.multiply(R, math.transpose(XYZc))

	  return xyz
}
function camera2image (xyz, p) {
		// Get internal parameters
  var f = math.subset(p, math.index(6))
  var cx = math.subset(p, math.index(7))
  var cy = math.subset(p, math.index(8))

  var nObs = math.subset(math.size(xyz), math.index([1]))

		// Perspective transform
  var xs = math.subset(xyz, math.index(0, math.range(0, nObs)))
  var ys = math.subset(xyz, math.index(1, math.range(0, nObs)))
  var zs = math.subset(xyz, math.index(2, math.range(0, nObs)))

  var x = math.multiply(f, math.dotDivide(ys, zs))// line -f*xyz[1,:]/xyz[2,:]
  var y = math.multiply(-f, math.dotDivide(xs, zs))// column f*xyz[0,:]/xyz[2,:]

  var xoff = math.add(x, cx)
  var yoff = math.add(y, cy)

	  return [xoff, yoff]
}
function project3Dto2D (XYZ, p) {
    // Project Xi points in the camera with the pose xp and the focal foc_pix"""

    // Rigid body transfomation world -> camera
  var xyz = world2camera(XYZ, p)// x[0:3]);

		// perspective transform
  var xy = camera2image(xyz, p)

  return xy
}
function estimatePoseLM (p, p_bool, gcpArray) {
		// x = column = horizontal coordinate first
		// y = line = vertical coordinate second
		// X = East, projected coordinate
		// Y = North, projected coordinate
		// Z = Altitude

		// heading: 0 / 360 in the west direction increasing in the North (90),
		// South = 270, East(180),

		// Tilt = 0 is pointing up, 90 is horizontal, 180 is pointing down

		// Roll = 0 if the camera is flat, positive in the right direction, negative
		// in the left direction

		// focal in pixel
  var f = p[6]

		// Principal point
  var cx = p[7] // half column size
  var cy = p[8] // half height size

		// location of the camera
  var X0 = p[0]
  var Y0 = p[1]
  var Z0 = p[2]

		// rotation of the camera in degree
  var az = p[3]
  var tilt = p[4]
  var roll = p[5]

		// Convert map/pic2map angles in radian angles
		// var angles = pic2map_to_LM_angles(az, tilt, roll);

		// Generate vector p of the apriori, known values
  p = math.matrix([X0, Y0, Z0, az, tilt, roll, f, cx, cy])

		// Get parameters to optimize. How many parameters to optimize
  var n_unknowns = p_bool.filter(function (value) { return value === true }).length

		// Compute number of observation (GCP)
  var nObs = gcpArray.length

		// Create mathjs matrix
  var XYZarr = []
  var x = []
  var y = []
  for (var i = 0; i < nObs; i++) {
    XYZarr.push([gcpArray[i].X, gcpArray[i].Y, gcpArray[i].Z])
    x.push(gcpArray[i].x)
    y.push(gcpArray[i].y)
  }
  var XYZ = math.matrix(XYZarr)
  var x = math.matrix(x)
  var y = math.matrix(y)

		// Model fitting with Levenberg-Marquardt,
	  // p: the a priori pose
	  // foc_pix: the camera focal [pixel]
	  // xy: location of the features
	  // XYZ: 3d corresponding features
	  // correspondences: matrix of the the l, XYZ correspondences"""

	  // Levenberg Marquardt
	  // -------------------

		// Initial parameters of the LM algorithms
	  var lamba = 0.01
	  var seuil = 1
  var conv_threshold = 0.0001
  var perturbation = math.matrix([0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001]) // for numerical derivative
		// var perturbation = math.matrix([0.1,0.1.,1.,0.1, 0.1, 0.1, 0.1, 0.1, 0.1]);

		// iteration counter
	  var jj = 1

		// iteration
	  while ((math.max(math.abs(seuil)) > conv_threshold) && (lamba != Infinity) && (jj < 100)) {
		  // Evaluate function at current p values
    var xy_bulle = project3Dto2D(XYZ, p)
    var x_bulle = math.squeeze(xy_bulle[0])
    var y_bulle = math.squeeze(xy_bulle[1])

			// Computation of the residuals
    var dx = math.subtract(x, math.transpose(x_bulle))
    var dy = math.subtract(y, math.transpose(y_bulle))
    var dxy = math.dotPow(math.add(math.dotPow(dx, 2), math.dotPow(dy, 2)), 0.5)// math.dotPow((,0.5)

			// 1D vector of the residual
    var r = []
    for (var i = 0; i < nObs; i++) {
      r.push(math.subset(dx, math.index(i)))
      r.push(math.subset(dy, math.index(i)))
    }
    var r = math.matrix(r)

			// Compute current error metric
    var error = math.multiply(0.5, math.multiply(math.transpose(r), r))

		  // Compute Jacobian
    var J = math.zeros(2 * nObs, n_unknowns)

			// Loop over the observation
		  for (var i = 0; i < nObs; i++) {
				// Numerical derivatives
				// ---------------------
    var idJac = 0
    for (var j = 0; j < 9; j++) {
      if (p_bool[j] == true) {
						// This parameter must be optimized
        var nul_perturb = math.zeros(9)
        var cur_perturb = math.subset(nul_perturb, math.index(j), math.subset(perturbation, math.index(j)))// ICI la faute!

						// f(p+dp)
        var xy_forward = project3Dto2D(math.subset(XYZ, math.index(i, [0, 1, 2])), math.add(p, cur_perturb))
						// console.log('xy_forward[0]', xy_forward[0]);
						// console.log('xy_forward[1]', xy_forward[1]);

						// f(p) is already computed in xy_bulle
						// console.log('x_bulle[0]',math.subset(x_bulle, math.index(i)))
						// console.log('y_bulle[0]',math.subset(y_bulle, math.index(i)))
						// console.log('pert',math.subset(perturbation, math.index(j)))

						// Compute numerical derivative
        var df1_dpi = (xy_forward[0] - math.subset(x_bulle, math.index(i))) / math.subset(perturbation, math.index(j))
        var df2_dpi = (xy_forward[1] - math.subset(y_bulle, math.index(i))) / math.subset(perturbation, math.index(j))

						// fill Jacobian
        var J = math.subset(J, math.index(2 * i, idJac), df1_dpi)
        var J = math.subset(J, math.index(2 * i + 1, idJac), df2_dpi)
        idJac += 1
						// console.log('J',J);
      } else {
						// This parameter is fixed
      }

					// var nul_perturb = math.zeros(9);
					// var cur_perturb = math.subset(nul_perturb, math.index(j), math.subset(perturbation, math.index(j)));//ICI la faute!
					//
					// //f(p+dp)
					// xy_forward = project3Dto2D(math.subset(XYZ, math.index(i,[0,1,2])), math.add(p, cur_perturb));
					// //console.log('xy_forward[0]', xy_forward[0]);
					// //console.log('xy_forward[1]', xy_forward[1]);
					//
					// //f(p) is already computed in xy_bulle
					// //console.log('x_bulle[0]',math.subset(x_bulle, math.index(i)))
					// //console.log('y_bulle[0]',math.subset(y_bulle, math.index(i)))
					// //console.log('pert',math.subset(perturbation, math.index(j)))
					//
					// //Compute numerical derivative
					// df1_dpi = (xy_forward[0]-math.subset(x_bulle, math.index(i)))/math.subset(perturbation, math.index(j));
					// df2_dpi = (xy_forward[1]-math.subset(y_bulle, math.index(i)))/math.subset(perturbation, math.index(j));
					//
					// //fill Jacobian
					// var J = math.subset(J, math.index(2*i,j),df1_dpi);
					// var J = math.subset(J, math.index(2*i+1,j),df2_dpi);
					// //console.log('J',J);
    }
  }
			// Compute Hessian
			// console.log('J',J);
		  var H = math.multiply(math.transpose(J), J)

		  // if (lamba != 0){
		  //	cont = true
			//  while ((cont == true) && (lamba != Infinity)){

		  // Compute step
    var dH = math.multiply(lamba, math.diag((math.diag(H))))
    var Jr = math.multiply(math.transpose(J), r)
		  var dp = math.squeeze(math.lusolve(math.add(H, dH), Jr))

		  // Update
			// Generate dp vector
    var dp_all = math.zeros(9)
    var j = 0
    for (var i = 0; i < 9; i++) {
				// console.log('i',i);
				// console.log('j',j);
      if (p_bool[i] === true) {
					// console.log('true');
        var dp_all = math.subset(dp_all, math.index(i), math.subset(dp, math.index(j)))
        j++
      }
    }

			// console.log('dp_all', dp_all);
		  var pdp = math.add(p, dp_all)

		  // Evaluate function at next x values
		  var xy_bulle_new = project3Dto2D(XYZ, pdp)
    var x_bulle_new = math.squeeze(xy_bulle_new[0])
    var y_bulle_new = math.squeeze(xy_bulle_new[1])

			// console.log('xy_bulle_new ',xy_bulle_new);

		  // Compute residuals
    var dx_new = math.subtract(x, math.transpose(x_bulle_new))
    var dy_new = math.subtract(y, math.transpose(y_bulle_new))

    var r_new = []
    for (var i = 0; i < nObs; i++) {
      r_new.push(math.subset(dx_new, math.index(i)))
      r_new.push(math.subset(dy_new, math.index(i)))
    }
    var r_new = math.matrix(r_new)

			// Compute new error
    var error_new = math.multiply(0.5, math.multiply(math.transpose(r_new), r_new))

			// console.log('error_new',error_new);
			// console.log('error',error);

	    if (error_new >= error) {
					// console.log('Plus grand')
	        var lamba = lamba * 10
	        var cont = true
    } else if (error_new < error) {
					// console.log('Plus petit, update')
	        var lamba = lamba / 10
	        var p = math.add(p, dp_all)
					// console.log('p',p);
      var error = error_new
	        // var cont = false;
    }
			//	dgh}//end while

		  // }else{
			//		console.log('else')
					// Compute step
			//		var dH = math.diag((math.multiply(lamba,math.diag(H))));
		//			var Jr = math.multiply(math.transpose(J),r);
		//			var dp = math.lusolve(H, Jr);

 		//			var p = math.add(p,dp_all);
					//
					//
					//

    	jj++
    	var seuil = dp_all
			// console.log('seuil', seuil)
  }// end while
		// console.log('p',p);

  var result = []
  p.forEach(function (value, index, matrix) {
  		// console.log('value:', value, 'index:', index);
    result.push(value)
  })
  return result
}

		// function parseData(url, callBack) {
		// 	var results = Papa.parse(url, {
		//  		header: true,
		// 		download: true,
		// 		dynamicTyping: true,
		// 		skipEmptyLines: true,
		// 		comments: true,
		//  		complete: function(results) {
		//  			//data = results;
		// 			callBack(results.data);
		//  			//console.log("parse:", results);
		//  		}
		// 	});
		// }
		//
		// parseData("http://localhost/tproduit/public_html/3Dgeoref/images/07861v_GCPs.csv", estimatePoseLM);
