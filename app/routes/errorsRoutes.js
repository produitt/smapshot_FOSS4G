var models = require('../model')

var express = require('express')
var router = new express.Router()

// Get all errors
router.post('/errors/getErrorsList/', function (req, res) {

  // var imageId = req.body.imageId;
  models.errors.findAll(
    {
      raw: true,
      attributes: ['id', 'title', 'translations'],
      order: [['id', 'ASC']],
      // where: {}
    })
  .then(function (result) {
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Errors not sent'})
  })
})

// Get errors from list
router.post('/errors/getErrors/', function (req, res) {
  var errors = req.body.list

  // var imageId = req.body.imageId;
  models.errors.findAll(
    {
      raw: true,
      where: {
        id: errors
      }
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Errors not sent'})
  })
})

// Get one errors
router.post('/errors/getError/', function (req, res) {
  var errorId = req.body.errorId

  // var imageId = req.body.imageId;
  models.errors.findAll(
    {
      raw: true,
      where: {
        id: errorId
      }
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Error not sent'})
  })
})

module.exports = router
