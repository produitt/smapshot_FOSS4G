var models = require('../model')
var bcrypt = require('bcrypt')
var generatePassword = require('password-generator')
var nodemailer = require('nodemailer')
var validator = require('email-validator')
var Sequelize = require('sequelize')
var reCAPTCHA = require('recaptcha2')
var recaptcha = new reCAPTCHA({
  siteKey: '6LdkIwwUAAAAAJg2L0W6Fd6-37CzxCM5_TCFp9VQ',
  secretKey: '6LdkIwwUAAAAANHNGJ1zdcbDSJ8jkHDH4AcvEYJG'
})
var jwt = require('jwt-simple')
var passport = require('passport')
var fs = require('fs')
var tok = require('../authentication/token.js')
var request = require('request')

var express = require('express')
var router = new express.Router()

router.post('/auth/resetPassword', function (req, res) {
  var emailClient = req.body.email
  models.volunteers.findOne({
    attributes: ['username'],
    where: {
      email: emailClient
    }
  }).then(function (volunteer) {
    if (volunteer) {
      var generatedPwd = generatePassword()
      // Encrypt password
      bcrypt.genSalt(8, function (err, salt) {
        if (err) {
          res.json({success: false, message: 'Error during the generation of the password'})
        }
        bcrypt.hash(generatedPwd, salt, function (err, hash) {
          if (err) {
            res.json({success: false, message: 'Error during the generation of the password'})
          }
          new Promise(function (resolve, reject) {
            // Send the email
            var transporter = nodemailer.createTransport({
              host: 'smtp.gmail.com',
              port: 465,
              secure: true, // use SSL
              auth: {
                user: 'smapshot.ch@gmail.com',
                pass: 'L9np9BGE'
              }
            })
            var text = 'This is your new smapshot passord: ' + generatedPwd// req.body.name;
            var mailOptions = {
              from: 'smapshot.ch@gmail.com', // sender address
              to: emailClient, // list of receivers
              subject: 'New smapshot password', // Subject line
              text: text //, // plaintext body
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
            }
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                reject()
              } else {
                resolve()
              }
            })
          }).then(function () {
            // email is sent: change the password
            // Create the user
            models.volunteers.update({
              password: hash
            },
              {
                where: {
                  email: emailClient
                }
              }).then(function () {
                res.json({success: true, message: 'Server send the email'})
              })
              .catch(function (error){
                res.json({success: false, message: 'Server fail to send the email'})
              })
          })
          .catch(function () {
            // email is not sent
            res.json({success: false, message: 'Server fail to send the email'})
          })
        })// end bcrypt
      })// end genSalt
    } else {
      res.json({success: false, message: 'email doesnt exists'})
    }
  }, function (error) {
    res.json({success: false, message: 'Database error at reset password'})
  })
})
// create a new user account
router.post('/auth/register', function (req, res) {
  // Verify captcha
  recaptcha.validate(req.body.gRecaptchaResponse)
  .then(function () {
    // Validate email form
    var isValid = validator.validate(req.body.email)
    if (isValid === false) {
      res.json({success: false, msg: 'Email is not valid'})
    } else {
      if (!req.body.username || !req.body.password) {
        res.json({success: false, msg: 'Please pass id and password.'})
      } else {
        // Encrypt password
        bcrypt.genSalt(10, function (err, salt) {
          if (err) {
            // winston.log('error', err)
            return
          }
          bcrypt.hash(req.body.password, salt, function (err, hash) {
            if (err) {
              // winston.log('error', err)
              return
            }
            // Create the user
            models.volunteers.create({
              // id: req.body.id,
              username: req.body.username,
              letter: req.body.letter,
              email: req.body.email,
              password: hash, // req.body.password
              is_validator: false,
              date_registr: Sequelize.literal('current_timestamp'),
              super_admin: false
            }).then(function (volunteer) {
              // winston.log('info', 'create new user' + req.body.username)
              // Send token to the client
              var token = jwt.encode(volunteer, '5map5h0tKey')
              res.json({success: true, token: 'JWT ' + token})
            })
            .catch(function (err) {
              return res.json({success: false, msg: 'Email already exists.'})
            })
          })
        })
      }
    }
  })
  .catch(function (err) {
    console.log('captcha is refused', err)
  })
})

// route to authenticate a user
router.post('/auth/authenticate', function (req, res) {
  models.volunteers.findOne({
    where: {
      // username: req.body.username
      email: req.body.email
    }
  }).then(function (volunteer) {
    if (!volunteer) {
      res.send({success: false, msg: 'Authentication failed. User not found.'})
    } else {
        // check if password matches
      bcrypt.compare(req.body.password, volunteer.password, function (err, isMatch) {
        if (err) {
          // winston.log('error', err)
          return
        }
        if (isMatch === true) {
          var token = jwt.encode(volunteer, '5map5h0tKey')
          res.json({success: true, token: 'JWT ' + token})
        } else {
          res.json({success: false, msg: 'Authentication failed. Wrong password.'})
        };
      })
    }
  }).catch(function (err) {
    res.send({success: false, msg: 'Authentication failed.'})
  })
})

// Protected get volunteer info
router.get('/volunteers/memberinfo', function (req, res) { // , passport.authenticate('jwt', {session: false})
  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['id', 'username', 'email', 'first_name', 'last_name',
      'is_validator', 'letter', 'has_one_validated', 'notifications',
      'super_admin', 'owner_admin', 'owner_validator', 'lang',
      [Sequelize.literal('CASE WHEN (googleid is null AND facebookid is null) THEN TRUE ELSE FALSE END'), 'local_login']],
      where: {
        id: decoded.id
      }
    }).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      }
      return res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'})
    }).catch(function (err) {
      return res.status(403).send({success: false, msg: 'Error while geting the volunteer.'})
    })
  }else{
    res.send({success: false, msg: 'Token not provided'})
  }
})

// GOOGLE
router.post('/auth/google', function (req, res) {
  var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token'
  var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect'
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: 'zAHxice4hOuoGIGo6QgjEKoK',
    redirect_uri: req.body.redirectUri,
    grant_type: 'authorization_code'
  }
  // winston.log('info', 'logIn 0')

  // Step 1. Exchange authorization code for access token.
  request.post(accessTokenUrl, { json: true, form: params }, function (err, response, token) {
    // winston.log('info', 'logIn 1')
    var accessToken = token.access_token
    var headers = { Authorization: 'Bearer ' + accessToken }

    // Step 2. Retrieve profile information about the current user.
    request.get({ url: peopleApiUrl, headers: headers, json: true }, function (err, response, profile) {
      // winston.log('info', 'logIn 2')
      if (profile.error) {
        return res.status(500).send({message: profile.error.message})
      }

      // Step 3a. Link user accounts. the token is found
      if (req.header('Authorization')) {
        // winston.log('info', 'logIn 3a')
        models.volunteers.findOne({
          where: {
            googleid: profile.sub
          }
        }).then(function (volunteer) {
          // winston.log('info', 'logIn 3aa')
          if (!volunteer) {
            // user is not found: create a google user
            // winston.log('info', 'this user is not existing' + profile.name + profile.email)
            models.volunteers.create({
              googleid: profile.sub,
              username: profile.name,
              email: profile.email,
              date_registr: Sequelize.literal('current_timestamp'),
              is_validator: false,
              super_admin: false
            }).then(function (volunteer) {
              // res.json({success: true, msg: 'Successful created new user.'});
              // winston.log('info', 'create new google user' + profile.name)
              var token = jwt.encode(volunteer, '5map5h0tKey')
              return res.json({success: true, token: 'JWT ' + token})
            }).catch(function (err) {
              return res.json({success: false, msg: 'Username already exists.'})
            })
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
          } else {
            // User exists, token is send to the client
            var token = jwt.encode(volunteer, '5map5h0tKey')
            return res.json({success: true, token: 'JWT ' + token})
          }
        }).catch(function (err) {
          // winston.log('error', err + 'error in find One')
        })
      } else {
        // Step 3b. Create a new user account or return an existing one.
        models.volunteers.findOne({
          where: {
            googleid: profile.sub
          }
        }).then(function (volunteer) {
          if (!volunteer) {
            // user is not found: create a google user

            models.volunteers.create({
              googleid: profile.sub,
              username: profile.name,
              email: profile.email,
              date_registr: Sequelize.literal('current_timestamp'),
              is_validator: false,
              super_admin: false
            }).then(function (volunteer) {
              // winston.log('info', 'create new google user' + profile.name)
              // res.json({success: true, msg: 'Successful created new user.'});
              var token = jwt.encode(volunteer, '5map5h0tKey')
              res.json({success: true, token: 'JWT ' + token})
            }).catch(function (err) {
              return res.json({success: false, msg: 'Username already exists.'})
            })
          } else {
            // User exists, token is send to the client
            var token = jwt.encode(volunteer, '5map5h0tKey')
            return res.json({success: true, token: 'JWT ' + token})
          }
        }).catch(function (err) {
          winston.log('error', err + 'error in find One')
        })
      }
    })
  })
})
// FACEBOOK
router.post('/auth/facebook', function (req, res) {
  var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name']
  var accessTokenUrl = 'https://graph.facebook.com/v2.7/oauth/access_token'
  var graphApiUrl = 'https://graph.facebook.com/v2.7/me?fields=' + fields.join(',')
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: '5e2435aa976822b3a6514e8c1cd03be7',
    redirect_uri: req.body.redirectUri
  }

  // Step 1. Exchange authorization code for access token.
  request.get({ url: accessTokenUrl, qs: params, json: true }, function (err, response, accessToken) {
    if (response.statusCode !== 200) {
      return res.status(500).send({success: false, message: accessToken.error.message })
    }

    // Step 2. Retrieve profile information about the current user.
    request.get({ url: graphApiUrl, qs: accessToken, json: true }, function (err, response, profile) {
      if (response.statusCode !== 200) {
        return res.status(500).send({success: false, message: profile.error.message })
      }
      if (req.header('Authorization')) {
        models.volunteers.findOne({
          where: {
            facebookid: profile.id
          }
        }).then(function (volunteer) {
          if (!volunteer) {
            // user is not found: create a facebook user
            models.volunteers.create({
              facebookid: profile.id,
              username: profile.name,
              email: profile.email,
              date_registr: Sequelize.literal('current_timestamp'),
              is_validator: false,
              super_admin: false
            }).then(function (volunteer) {
              // winston.log('info', 'create new facebook user' + profile.name)
              // res.json({success: true, msg: 'Successful created new user.'});
              var token = jwt.encode(volunteer, '5map5h0tKey')
              return res.json({success: true, token: 'JWT ' + token})
            })
            .catch(function (err) {
              // winston.log('error', err + 'creating facebook user')
              return res.json({success: false, msg: 'Username already exists.'});
            })
            // return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {
            // res.json({success: true, msg: 'Welcome in the member area ' + volunteer.id + '!', data: volunteer});
            var token = jwt.encode(volunteer, '5map5h0tKey')
            return res.json({success: true, token: 'JWT ' + token})
          }
        })
        .catch(function (err) {
          console.log('error in find One', err)
        })
      } else {
        // Step 3. Create a new user account or return an existing one.
        models.volunteers.findOne({
          where: {
            facebookid: profile.id
          }
        }).then(function (volunteer) {
          if (!volunteer) {
            // user is not found: create a google user
            models.volunteers.create({
              facebookid: profile.id,
              username: profile.name,
              email: profile.email,
              date_registr: Sequelize.literal('current_timestamp'),
              is_validator: false,
              super_admin: false
            }).then(function (volunteer) {
              // winston.log('info', 'create new facebook user' + profile.name)
              var token = jwt.encode(volunteer, '5map5h0tKey')
              return res.json({success: true, token: 'JWT ' + token})
            })
            .catch(function (err) {
              // winston.log('error', err + ' creating facebook user')
              return res.json({success: false, msg: 'Username already exists.'})
            })
            // return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {
            // res.json({success: true, msg: 'Welcome in the member area ' + volunteer.id + '!', data: volunteer});
            var token = jwt.encode(volunteer, '5map5h0tKey')
            return res.json({success: true, token: 'JWT ' + token})
          }
        })
        .catch(function (err) {
          console.log('error in find One', err)
        })
      }
    })
  })
})

// Reward
// ------
router.get('/reward/:imageId/:volunteerId/', passport.authenticate('jwt', {session: false}), function (req, res) {
  var imageId = req.params.imageId
  var clientId = req.params.volunteerId

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['id'],
      where: {
        id: decoded.id
      }
    }).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      } else {
        // res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'});
        // Get image id from request
        var clientId = volunteer.id
        // Get image in the database
        models.images.findOne({
          raw: true,
          attributes: ['volunteer_id', 'collection_id'],
          where: {
            id: imageId,
            georef_3d_bool: true
          }
        }).then(function (image) {
          var georeferencerId = image.volunteer_id
          if (georeferencerId === clientId) {
            // Read image
            var img = fs.readFileSync('./public/data/collections/'+image.collection_id+'/images/1500/' + imageId + '.jpg')
            var img = new Buffer(img, 'binary').toString('base64')
            res.end(img) //, img
          } else {
            res.status(404).send('asdf brip brip!')
          }
        })
      }
    }).catch(function (err) {
      console.log('error in find One', err)
    })
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'})
  }
})

module.exports = router
