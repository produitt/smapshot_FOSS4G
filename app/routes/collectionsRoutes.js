var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op

var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

router.post('/search/getCurrentChallenge', function (req, res) {
  models.collections.findAll({
    raw: true,
    attributes: ['id', 'name', 'link', 'owner_id', 'short_descr', 'long_descr', 'date_publi',
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id" AND "images"."is_published" = true)'), 'nImages'],
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id" AND "images"."georef_3d_bool" = true)'), 'nGeoref']
    ],
    where: {
      is_challenge: true
    }
  }).then(function (Collections) {
    var results = parseDB(Collections)
    return res.json(results)
  }).catch(function (err){
    res.json({success: 'false', msg: 'error while getting the challenge'})
  })
})

router.post('/collections/getMy', function (req, res) {
  var volunteerId = req.body.volunteerId
  var sql =
  `
  select max(date_georef) as max_date, collection_id, collections.name from images
  left join collections on collections.id = images.collection_id
  where images.volunteer_id = ` + volunteerId +
  `
  group by collection_id, collections.name
  order by max_date desc
  `
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (Collections) {
    return res.json(Collections)
  })
  .catch(function (err){
    res.json({success: 'false', msg: 'error while getting my collections'})
  })
})

router.post('/search/getCollections', function (req, res) {
  models.collections.findAll({
    raw: true,
    attributes: ['id', 'name', 'link', 'owner_id', 'short_descr', 'long_descr', 'date_publi',
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id")'), 'nImages'],
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id" AND "images"."georef_3d_bool" = true)'), 'nGeoref']
   ],
    where: {
      date_publi: {
            [Op.not]: null
          }
        }
  }).then(function (Collections) {
    var results = parseDB(Collections)
    return res.json(results)
  })
  .catch(function (err){
    res.json({success: 'false', msg: 'error while getting the collections'})
  })
})

router.post('/collections/getDescription', function (req, res) {
  id = req.body.collectionId
  models.collections.findAll({
    raw: true,
    attributes: ['id', 'name', 'link', 'owner_id', 'short_descr', 'long_descr', 'date_publi',
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id")'), 'nImages'],
     [Sequelize.literal('(SELECT count("images") FROM "images" WHERE "images"."collection_id" = "collections"."id" AND "images"."georef_3d_bool" = true)'), 'nGeoref']
   ],
   where:{
     id: id
   }
  }).then(function (Collections) {
    // Get three best volunteers
    models.images.findAll({
      raw: true,
      attributes: ['volunteer_id',
        [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
        [Sequelize.literal('count("volunteer_id")'), 'nImages']
      ],
      where: {
        collection_id: id,
        is_validated: true
      },
      group: 'volunteer_id',
      order: Sequelize.literal('"nImages" DESC'),//'"nImages" DESC',
      limit: 3
    }).then(function(best3){
      Collections[0].best3 = best3
      return res.json(Collections)
    })
    .catch(function (err){
      res.json({success: 'false', msg: 'error while getting the 3 bests'})
    })
  })
  .catch(function (err){
    res.json({success: 'false', msg: 'error while getting the collection description'})
  })
})

router.post('/search/getCollectionLink', function (req, res) {
  var collection_id = req.body.collection_id
  models.collections.findAll({
    attributes: ['link'],
    where: {
      id: collection_id
    },
    raw: true
  }).then(function (collection) {
    return res.json(collection[0])
  })
  .catch(function (err){
    res.json({success: 'false', msg: 'error while getting the collection link'})
  })
})

router.post('/collections/getList', function (req, res) {
  var ownerId = req.body.ownerId
  if (ownerId){
    where = {
      owner_id: ownerId,
      date_publi: {
        [Op.not]: null
      }
    }
  }else{
    where = {
      date_publi: {
        [Op.not]: null
      }
    }
  }
  models.collections.findAll({
    attributes: ['name', 'id'],
    where: where
  }).then(function (collections) {
    return res.json(collections)
  }).catch(function (err) {
    console.log('error get collections list', err)
  })
})

router.post('/collections/getCollections', function (req, res) {
  var ownerId = req.body.ownerId
  if (ownerId){
    where = {
      owner_id: ownerId,
      date_publi: {
        [Op.not]: null
      }
    }
  }else{
    where = {
      date_publi: {
        [Op.not]: null
      }
    }
  }
  var sql = `
  select collections.id, collections.name as cname, owner_id, owners.name as oname,
  collections.long_descr, n_georef, n_images, url_name, date_publi,
  round(cast(n_georef as numeric)/n_images*100, 1) as ratio from collections
  left join (select count(*) as n_georef, collection_id from images where georef_3d_bool = true and is_published = true group by collection_id) as f1 on collections.id = f1.collection_id
  left join (select count(*) as n_images, collection_id from images where is_published = true group by collection_id) as f2 on collections.id = f2.collection_id
  left join owners on collections.owner_id = owners.id
  `
  sql = sql + ' where date_publi is not null'
  if (ownerId){
    sql = sql + ' and owner_id = ' + ownerId
  }

  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (collections) {
    return res.json(collections)
  })
  .catch(function (err) {
    console.log('error get collections list', err)
  })
})

module.exports = router
