var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op

var emailsSender = require('../controllers/emails')
var owner = require('../controllers/owner')

var express = require('express')
var router = new express.Router()

// For the check of the validator status
var volunteer = require('../controllers/volunteer')
var jwt = require('jwt-simple')
var tok = require('../authentication/token')
var passport = require('passport')

// Record validation
router.post('/validation/createValidation/', function (req, res) {
  console.log('create validation')
  var imageId = req.body.imageId
  var volunteerId = req.body.volunteerId
  models.validations.findAll({
    where: {
      image_id: imageId,
      volunteer_id: volunteerId
    }
  }).then(function(result){
    if (result.length === 0){
      models.validations.create(
        {
          image_id: imageId,
          volunteer_id: volunteerId,
          date_georef: Sequelize.literal('current_timestamp'),
          start: Sequelize.literal('current_time')
        }
      )
      // .then(function (result) {
      //   models.images.update({
      //     last_start: Sequelize.literal('current_timestamp')
      //   },{
      //     where: {
      //       id: imageId,
      //     }
      //   })
        .then(function(result){
          // return result
          return res.json({success: true, message: 'Validation is created'})
        }
      //   , function (error) {
      //     return res.json({success: false, message: 'Validation is not created'})
      //   }
      // )
      //}
      , function (error) {
        return res.json({success: false, message: 'Validation is not created'})
      })
    } else {
      models.validations.update(
        {
          image_id: imageId,
          volunteer_id: volunteerId,
          validator_id: null,
          validated: null,
          seen: false,
          date_georef: Sequelize.literal('current_timestamp'),
          start: Sequelize.literal('current_time'),
          date_seen: null,
          date_validation: null,
          stop: null
        },{
          where: {
            image_id: imageId,
            volunteer_id: volunteerId
          }
        }
      )
      // .then(function (result) {
      //   // Lock the image
      //   models.images.update({
      //     last_start: Sequelize.literal('current_timestamp')
      //   },{
      //     where: {
      //       id: imageId,
      //     }
      //   })
        .then(function(result){
          // return result
          return res.json({success: true, message: 'Validation is created'})
        }
      //   , function (error) {
      //     return res.json({success: false, message: 'Validation is not created'})
      //   }
      // )
      //}
      , function (error) {
        return res.json({success: false, message: 'Validation is not updated'})
      })
    }
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

// Record stop time
router.post('/validation/recordStopTime/', function (req, res) {
  var imageId = req.body.imageId
  var volunteerId = req.body.volunteerId

  models.validations.update(
    {
      stop: Sequelize.literal('current_time')
    }, {
      where: {
        image_id: imageId,
        volunteer_id: volunteerId
      }
    }
  ).then(function (result) {
    // return result
    return res.json({success: true, message: 'Validation is created'})
  }, function (error) {
    return res.json({success: false, message: 'Validation is not created'})
  })
})


// Record validation
router.post('/validation/submitValidation/', passport.authenticate('jwt', {session: false}), function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function(vol){
      var imageId = req.body.imageId
      var volunteerId = req.body.volunteerId
      var validatorId = req.body.validatorId
      var validation = req.body.validation
      var errors = validation.errors
      var remark = validation.remark
      var status = validation.status

      //Check status
      var validated = false
      if (status === 'validated') {
        validated = true
      }
      var improved = false
      if (status === 'improved') {
        improved = true
      }
      var rejected = false
      if (status === 'rejected') {
        rejected = true
      }

      if (!validated) {
        // The image is rejected or improved
        // Create error list and other error text
        var otherText = ''
        var other = false
        var listErrors = '{'
        var listE = []
        for (var i = 0; i < Object.keys(errors).length; i++) {
          if (errors[i].is_checked) {
            listErrors = listErrors + errors[i].id + ', '
            listE.push(errors[i].id)
            if (errors[i].title === 'Other') {
              other = true
              otherText = errors[i].text
            }
          }
        }
        if (listErrors != '{'){
          listErrors = listErrors.slice(0,-2)
        }
        listErrors = listErrors + '}'
          models.validations.update(
            {
              validator_id: validatorId,
              errors_list: listE,
              other: other,
              other_desc: otherText,
              remark: remark.text,
              validated: validated,
              improved: improved,
              rejected: rejected,
              date_validation: Sequelize.literal('current_timestamp'),
              seen: true,
              date_seen: Sequelize.literal('current_timestamp'),
            },
            {
              where: {
                image_id: imageId,
                volunteer_id: volunteerId
              }
            }
          ).then(function (result) {
            // return result
            if (rejected) {
              models.images.update({
                georef_3d_bool: false,
                is_validated: false,
                validated: false

              }, {
                where: {
                  id: imageId
                }
              })
              .then(function () {
                // Check if volunteers want notifications
                models.volunteers.findOne({
                  attributes: ['notifications', 'email', 'username'],
                  where: {
                    id: volunteerId
                  }
                }).then(function (vol) {
                  // Send email
                  vol.notifications = false // avoid the email
                  if (vol.notifications) {
                    owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                      emailsSender.sendImageRejection(vol, imageId, ownerName)
                      .then(function (error, info) {
                        return res.json({success: true, message: 'Validation is saved, geoloc rejected'})
                      }).catch(function (err) {
                        return res.json({success: false, message: 'Error while sending email'})
                      })
                    })
                  } else {
                    return res.json({success: true, message: 'Validation is saved, geoloc rejected'})
                  }
                })
                .catch(function (err) {
                  return res.json({success: false, msg: 'error'})
                })
              })
              .catch(function (err) {
                return res.json({success: false, msg: 'error'})
              })
            } else {
              // The geolocalisation is improved
              models.images.update({
                is_validated: true,
                validated: true,
                validation_date: Sequelize.literal('current_timestamp')
              }, {
                where: {
                  id: imageId
                }
              })
              .then(function () {
                // Check if volunteers want notifications
                models.volunteers.findOne({
                  attributes: ['notifications', 'email', 'username'],
                  where: {
                    id: volunteerId
                  }
                }).then(function (vol) {
                  // Send email
                  vol.notifications = false // avoid the email
                  if (vol.notifications) {
                    // Get signature according to the validator
                    owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                      emailsSender.sendImageImprovement(vol, imageId, ownerName)
                      .then(function (error, info) {
                        return res.json({success: true, message: 'Validation is saved'})
                      }).catch(function(err){
                        return res.json({success: false, message: 'Error while sending email'})
                      })
                    })
                    .catch(function (err) {
                      return res.json({success: false, msg: 'error'})
                    })

                  } else {
                    return res.json({success: true, message: 'Validation is saved'})
                  }
                })
                .catch(function (err) {
                  return res.json({success: false, msg: 'error'})
                })
              })
              .catch(function (err) {
                return res.json({success: false, msg: 'error'})
              })
            }
          })
          .catch(function (err) {
            return res.json({success: false, msg: 'error'})
          })
      } else {
        // The image is validated

        // Push in database
        models.validations.update(
          {
            image_id: imageId,
            volunteer_id: volunteerId,
            validator_id: validatorId,
            validated: validated,
            seen: true,
            date_seen: Sequelize.literal('current_timestamp'),
            date_validation: Sequelize.literal('current_timestamp')
          },
          {
            where: {
              image_id: imageId,
              volunteer_id: volunteerId
            }
          }
        ).then(function (result) {
          // Update volunteer
          models.volunteers.update({
            has_one_validated: true
          }, {
            where: {
              id: volunteerId
            }
          }).then(function (result) {
            // Update validation status of the image
            models.images.update({
              is_validated: true,
              validated: true,
              validation_date: Sequelize.literal('current_timestamp')
            }, {
              where: {
                id: imageId
              }
            }).then(function (result) {

              // Check if volunteers want notifications
              models.volunteers.findOne({
                attributes: ['notifications', 'email', 'username'],
                where: {
                  id: volunteerId
                }
              }).then(function (vol) {
                // Send email
                vol.notifications = false // avoid the email
                if (vol.notifications) {
                  owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                    emailsSender.sendImageValidation(vol, imageId, ownerName)
                    .then(function (error, info) {
                      return res.json({success: true, message: 'Validation validated is saved'})
                    })
                    .catch(function (err) {
                      return res.json({success: false, msg: 'error'})
                    })
                  })
                  .catch(function (err) {
                    return res.json({success: false, msg: 'error'})
                  })
                } else {
                  return res.json({success: true, message: 'Validation validated is saved'})
                }
              })
              .catch(function (err) {
                return res.json({success: false, msg: 'error'})
              })
            })
            .catch(function (err) {
              return res.json({success: false, msg: 'error'})
            })
          })
          .catch(function (err) {
            return res.json({success: false, msg: 'error'})
          })
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
    })
    .catch(function(err){
      return res.json({success: false, message: 'You arent a validator'})
    })
  }
})

router.post('/validations/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  // validations.date_georef + interval '1 day' as date_georef, validations.stop + interval '1 hour' as stop_time,

  var sql = `
  select images.id as image_id, images.id, images.original_id, images.georef_3d_bool, images.title,
  images.caption, images.owner_id, images.collection_id,
  images.surface_ratio_volunteer, images.score_volunteer, images.n_gcp_volunteer,
  validations.date_georef as date_georef, validations.stop as stop_time,
  validations.validator_id, validations.seen as processed,
  validations.volunteer_id, validations.improved, validations.rejected, validations.validated,
  owners.name as ownername, collections.name as collectionname,
  v1.username as username, is_published,
  v2.username as validator_name
  from validations
  left join volunteers as v1 on validations.volunteer_id = v1.id
  left join volunteers as v2 on validations.validator_id = v2.id
  left join images on validations.image_id = images.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  // if (originalId) {
  //   sql = sql + "WHERE original_id = '" + originalId + "'"
  // }else if (imageId) {
  //   sql = sql + "WHERE images.id = '" + imageId + "'"
  // }else{
  var wheres = []
  if (originalId) {
    wheres.push("original_id = '" + originalId + "'")
  }
  if (imageId) {
    wheres.push("images.id = '" + imageId + "'")
  }
  // Generate where
  // Owner constraint
  if (ownerId) {
    wheres.push('images.owner_id = ' + ownerId)
  }
  // From date constraint
  if (startDate) {
    wheres.push("validations.date_georef >= to_date('" + startDate + "','DD/MM/YYYY')")
  }
  // To date constraint
  if (stopDate) {
    wheres.push("validations.date_georef <= to_date('" + stopDate + "','DD/MM/YYYY')")
  }
  // Collection constraint
  if (collectionId) {
    wheres.push('collection_id = ' + collectionId)
  }
  if (validatorId){
    wheres.push('validations.validator_id = ' + validatorId)
  }
  if ((volunteerUsername != null) && (volunteerUsername != 'All')){
    wheres.push("v1.username ~* '" + volunteerUsername + "'")
  }
  if (processed != null){
    if ((processed === 'false') || (processed === false)) {
      wheres.push('(seen = false or seen is NULL)')
    }else if ((processed === 'true') || (processed === true)) {
      wheres.push('seen = true')
    }
  }
  wheres.push('stop is not null')
  // Concatenate where clauses
  if (wheres.length != 0) {
    for (var i = 0; i < wheres.length; i++){
      if (i === 0){
        sql = sql + ' WHERE'
      }
      sql = sql + ' ' + wheres[i]
      if (i !== wheres.length-1){
        sql = sql + ' AND'
      }
    }
  }
  //}
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
