var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

router.post('/badGeoloc/submit', function (req, res) {
  //var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId

  models.bad_geoloc.findOne({
    where: {
      image_id: imageId
    }
  })
  .then(function(badGeoloc){
    if (badGeoloc === null){
      // This geolocalisation is not submited
      models.bad_geoloc.create({
        image_id: imageId,
        counter: 1,
        createdat: Sequelize.literal('current_timestamp')
      })
      .then(function (result) {
        // return result
        return res.json({success: true, message: 'bad geoloc is created'})
      })
      .catch(function (result){
        return res.json({success: false, message: 'bad geoloc is not created'})
      })
    }else{
      // This geolocalisation was already submited
      models.bad_geoloc.increment('counter', {
        where:{
          image_id: imageId,
          updatedat: Sequelize.literal('current_timestamp')
        }
      })
      .then(function (result) {
        // return result
        return res.json({success: true, message: 'bad geoloc is incremented'})
      })
      .catch(function (result){
        return res.json({success: false, message: 'bad geoloc is not incremented'})
      })
    }
  })
  .catch(function(){
    return res.json({success: false, message: 'find geoloc error'})
  })

  // models.badGeoloc.create({
  //   id: Sequelize.literal("nextval('bad_geoloc_id_seq'::regclass)"),
  //   image_id: imageId,
  //   volunteer_id: volunteerId,
  // }).then(function (result) {
  //     // return result
  //     return res.json({success: true, message: 'bad geoloc is created'})
  //   })
  //   .catch(function (result){
  //     return res.json({success: false, message: 'bad geoloc is not created'})
  //   })
})

router.post('/badGeoloc/remove', function (req, res) {

  var imageId = req.body.imageId

  models.bad_geoloc.destroy({
    where: {
      image_id: imageId
    }
  })
  .then(function (problem) {
    return res.json({success: true, msg: 'bad geoloc deleted'})
  })
  .catch(function (result){
    return res.json({success: false, message: 'bad geoloc is not deleted'})
  })
})

router.post('/badGeoloc/validate', function (req, res) {

  var imageId = req.body.imageId
  var validatorId = req.body.validatorId

  models.bad_geoloc.update({
    processed: true,
    result: 'validated',
    validator_id: validatorId
  },
  {
    where: {
      image_id: imageId
    }
  })
  .then(function (badGeoloc) {
    models.images.update({
      georef_3d_bool: false,
      volunteer_id: null,
      is_validated: false,
      validation_date: null
    },
    {
      where: {
        id: imageId
      }
    })
    .then(function (problem) {
      return res.json({success: true, msg: 'georef canceled'})
    })
    .catch(function (result){
      return res.json({success: false, message: 'georef not canceled'})
    })
  })
  .catch(function (result){
    return res.json({success: false, message: 'bad geoloc is not deleted'})
  })
})

router.post('/badGeoloc/reject', function (req, res) {

  var imageId = req.body.imageId
  var validatorId = req.body.validatorId

  models.bad_geoloc.update({
    processed: true,
    result: 'rejected',
    validator_id: validatorId
  },
  {
    where: {
      image_id: imageId
    }
  })
  .then(function (problem) {
    return res.json({success: true, msg: 'bad geolo'})
  })
  .catch(function (result){
    return res.json({success: false, message: 'georef not canceled'})
  })
})

router.post('/badGeoloc/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  // var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select bad_geoloc.image_id, counter, substring(updatedat::TEXT,0,11) as update,
  substring(createdat::TEXT, 0, 11) as creation,
  processed, result,
  images.original_id, images.is_published,
  owners.name as ownername, collections.name as collectionname,
  is_published,
  v2.username as validator_name
  from bad_geoloc
  left join volunteers as v2 on bad_geoloc.validator_id = v2.id
  left join images on bad_geoloc.image_id = images.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  if (originalId) {
    sql = sql + "WHERE original_id = '" + originalId + "'"
  }else if (imageId) {
    sql = sql + "WHERE images.id = '" + imageId + "'"
  }else{
    var wheres = []
    // Generate where
    // Owner constraint
    if (ownerId) {
      wheres.push('images.owner_id = '+ ownerId)
    }
    // From date constraint
    // if (startDate) {
    //   wheres.push("problems.date_problem >= date('" + startDate + "')")
    // }
    // // To date constraint
    // if (stopDate) {
    //   wheres.push("problems.date_problem <= date('" + stopDate + "')")
    // }
    // Collection constraint
    if (collectionId) {
      wheres.push('collection_id = ' + collectionId)
    }
    if (validatorId){
      wheres.push('bad_geoloc.validator_id = ' + validatorId)
    }
    // if ((volunteerUsername != null) && (volunteerUsername != 'All')){
    //   wheres.push("v1.username ~* '" + volunteerUsername + "'")
    // }
    if (processed != null){
      if ((processed === 'false') || (processed === false)) {
        wheres.push('processed = false')
      }else if ((processed === 'true') || (processed === true)) {
        wheres.push('processed = true')
      }
    }
    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++){
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1){
          sql = sql + ' AND'
        }
      }
    }
  }
  sql = sql + ' order by counter'
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (problems) {
    return res.json(problems)
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
