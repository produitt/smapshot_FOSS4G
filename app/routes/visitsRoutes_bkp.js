var models = require('../model')

var express = require('express')
var router = new express.Router()

// Get the last new
router.post('/search/getVisits/', function (req, res) {
  models.visits.findAll(
    {
      raw: true,
      attributes: [
        'id', 'title_fr', 'title_en', 'title_de', 'title_it', 'text_fr', 'text_en', 'text_de', 'text_it', 'path', 'timestamp', 'author', 'theme'
      ],
      order: models.sequelize.literal('timestamp DESC')
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Visits are not sent'})
  })
})

module.exports = router
