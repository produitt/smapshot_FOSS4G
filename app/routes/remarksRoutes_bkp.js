var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

router.post('/remarks/submit', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var remark = req.body.remark

  models.remarks.create({
    id: Sequelize.literal("nextval('remarks_id_seq'::regclass)"),
    image_id: imageId,
    volunteer_id: volunteerId,
    remark: remark,
    date_remark: Sequelize.literal('current_timestamp')
  }).then(function (result) {
      // return result
      return res.json({success: true, message: 'Remark is saved'})
    }, function (rejectedPromiseError) {
      return res.json({success: false, message: 'Remark is not saved'})
    })
})

// Get images which arent validated  for mysMapShot
router.post('/remarks/getToValidateRemarks', function (req, res) {
  var query = `
  select remarks.id as rem_id, image_id, remarks.volunteer_id, remark, date_remark,
  processed, owner_id, collection_id,
  (SELECT collections.name FROM collections WHERE collections.id = images.collection_id) as collectionName,
  (SELECT owners.name FROM owners WHERE owners.id = images.owner_id) as ownerName,
  (SELECT volunteers.username FROM volunteers WHERE volunteers.id = remarks.volunteer_id) as username
  from remarks, images
  where images.id = remarks.image_id
  and processed = false
  order by date_remark desc
  `
  // and owner_id = 0
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
  .then(function (remarks) {
    // We don't need spread here, since only the results will be returned for select queries
    return res.json(remarks)
  })
})

// Get images which arent validated  for mysMapShot
router.post('/remarks/process', function (req, res) {

  var remark = req.body.remark

  models.remarks.update({
    reply: remark.reply,
    processed: true
  }, {
    where: {
      id: remark.rem_id
    }
  }).then(function () {
    return res.json({success: true, message: 'remark is processed'})
  })
})

module.exports = router
