var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

// Get markers
router.post('/markers/getMarkersImage/', function (req, res) {
  var imageId = req.body.imageId

  models.markers.findAll(
    {
      raw: true,
      attributes: [
      [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
      [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
      [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
        'title', 'text', 'date_in', 'date_marker',
      [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "markers"."volunteer_id")'), 'username']
      ],
      where: {
        image_id: imageId
      }
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Marker is not sent'})
  })
})

// Create a story
router.post('/markers/createMarker/', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var title = req.body.marker.title
  var text = req.body.marker.text
  var lat = req.body.marker.latitude
  var long = req.body.marker.longitude
  var height = req.body.marker.height
  var date_marker = req.body.marker.date

  models.markers.create(
    {
      title: title,
      location: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', long, lat, height), '4326'),
      volunteer_id: volunteerId,
      text: text,
      image_id: imageId,
      date_in: Sequelize.literal('current_timestamp'),
      date_marker: date_marker
    }
  ).then(function (result) {
    // return result
    return res.json({success: true, message: 'Marker is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Marker is not saved'})
  })
})

module.exports = router
