var models = require('../model')
var Sequelize = require('sequelize')
var jwt = require('jwt-simple')
var token = require('../authentication/token')
var passport = require('passport')
var fs = require('fs')
var math = require('mathjs')
var poseEst = require('../poseEstimation/optimisation')
var collada2gltf = require('collada2gltf') // Convert .dae to .gltf

var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

// Save map azimuth
router.post('/georef/saveMapAzimuth/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var azimuth = data.azimuth
  // var volunteerId = data.volunteerId;
  // Create query
  models.images.update(
    {
    // location_lat: orientation.lat,
    // location_long: orientation.long,
    // location_height: orientation.Z,
    // location: Sequelize.fn('ST_SetSRID',Sequelize.fn('ST_MakePoint', location.longitude, location.latitude, 0),'4326'), //C EST NOUVEAU
      azimuth: azimuth,
    // volunteer_id: volunteerId,
      date_georef: Sequelize.literal('current_timestamp'),
      georef_azi_bool: true
    // gcp_json: gcpArray,
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    return res.json(result)
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'Error save azimuth'})
  })

  // logger.log('info', 'Georeferencing map azimuth', {
  //   image_id: imageId,
  //   ip: req.connection.remoteAddress,
  //   user_agent: req.headers['user-agent'],
  //   referer: req.headers.referer
  // })
})

// Save camera and GCP
router.post('/georef/updateOrientation/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var orientation = data.orientation
  var volunteerId = data.volunteerId
  var gcpArray = data.gcpArray
  var nGCP = Object.keys(JSON.parse(gcpArray)).length
  var score = data.score
  var surfaceRatio = parseInt(data.surfaceRatio)
  var validationMode = data.validationMode
  var validatorId = data.validatorId

  if (validationMode) {
    // The validator update the georeferencing
    // Create query
    models.images.update(
      {
        location_volunteer: Sequelize.literal('location'),
        location: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', orientation.long, orientation.lat, orientation.Z), '4326'),
        roll: orientation.roll,
        tilt: orientation.tilt,
        azimuth: orientation.azimuth,
        focal: orientation.f,
        cx: orientation.cx,
        cy: orientation.cy,
        georef_3d_bool: true,
        gcp_json: gcpArray,
        is_validated: true,
        score_validator: score,
        surface_ratio_validator: surfaceRatio,
        validator_id: validatorId,
        n_gcp_validator: nGCP
      },
      {
        where: {id: imageId}
      })
    .then(function (result) {
      // return result
      return res.json({success: true, message: 'Orientation is updated'})
    }, function (rejectedPromiseError) {
      return res.json({success: false, message: 'Orientation is not updated'})
    })
  } else {
    // The volunteer update the georeferencing
    // Create query
    models.images.update(
      {
        location_volunteer: Sequelize.literal('location'),
        location: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', orientation.long, orientation.lat, orientation.Z), '4326'), // C EST NOUVEAU
        roll: orientation.roll,
        tilt: orientation.tilt,
        azimuth: orientation.azimuth,
        focal: orientation.f,
        cx: orientation.cx,
        cy: orientation.cy,
        georef_3d_bool: true,
        gcp_json: gcpArray,
        score_volunteer: score,
        surface_ratio_volunteer: surfaceRatio,
        n_gcp_volunteer: nGCP,
        date_georef: Sequelize.literal('current_timestamp')
      },
      {
        where: {id: imageId}
      })
    .then(function (result) {
      models.validations.update({
        date_georef: Sequelize.literal('current_timestamp')
      },
      {
        where: {
          image_id: imageId,
          volunteer_id: volunteerId
        }
      })
      // models.forum.create(
      //   {
      //     title: "J'ai modifié la position de cette image.",
      //     volunteer_id: volunteerId,
      //     text: "J'ai modifié la position de cette image.",
      //     image_id: imageId,
      //     timestamp: Sequelize.literal('current_timestamp')
      //   }
      // )
      // return result
      return res.json({success: true, message: 'Orientation is updated'})
    }, function (rejectedPromiseError) {
      return res.json({success: false, message: 'Orientation is not updated'})
    })
  }
})

// Save camera and GCP
router.post('/georef/saveVolunteerId/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var volunteerId = data.volunteerId

  // Create query
  models.images.update(
    {
      volunteer_id: volunteerId // Default volunteer
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    models.forum.update(
      {
        volunteer_id: volunteerId
      },
      {
        where: {image_id: imageId}
      }
    )
    // return result
    return res.json({success: true, message: 'Volunteer is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Volunteer is not saved'})
  })
})

// Save camera and GCP
router.post('/georef/saveOrientation/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var orientation = data.orientation
  var volunteerId = data.volunteerId // Default volunteer = 14
  var gcpArray = data.gcpArray
  var nGCP = Object.keys(JSON.parse(gcpArray)).length
  var score = data.score
  var surfaceRatio = data.surfaceRatio

  // Create query
  models.images.update(
    {
    // location_lat: orientation.lat,
    // location_long: orientation.long,
    // location_height: orientation.Z,
      location: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', orientation.long, orientation.lat, orientation.Z), '4326'), // C EST NOUVEAU
      roll: orientation.roll,
      tilt: orientation.tilt,
      azimuth: orientation.azimuth,
      focal: orientation.f,
      cx: orientation.cx,
      cy: orientation.cy,
      volunteer_id: volunteerId,
      date_georef: Sequelize.literal('current_timestamp'),
      georef_3d_bool: true,
      gcp_json: gcpArray,
      score_volunteer: score,
      n_gcp_volunteer: nGCP,
      surface_ratio_volunteer: surfaceRatio
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    models.forum.create(
      {
        title: "J'ai retrouvé la position de cette image.",
        volunteer_id: volunteerId,
        text: "J'ai retrouvé la position de cette image.",
        image_id: imageId,
        timestamp: Sequelize.literal('current_timestamp')
      }
    )
    // return result
    return res.json({success: true, message: 'Orientation is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Orientation is not saved'})
  })
})

// Get collada file, transform and save the gltf
router.get('/threed/template3dModel/', function (req, res, next) {
  fs.readFile('public/3Dmodel/collada_template_mix.dae', 'utf8', function (err, data) {
    if (err) {
      console.log('error reading dae')
    } else {
      return res.json({xml: data})
    }
  })
})

// Get collada file, transform and save the gltf
router.post('/georef/saveGltf/', function (req, res) {

  var p = req.body.cameraParameters
  var imageId = req.body.imageId
  var collectionId = req.body.collectionId
  // change the roll (sign + 90): there is a problem with the roll definition
  var p5 = math.subset(p, math.index(5)) + math.pi / 2.0
  var p = math.subset(p, math.index(5), p5)
  // Compute image coordinate in world coordinates
  // ---------------------------------------------
  var urCorner = [p[7], p[8], p[6]]
  var ulCorner = [-p[7], p[8], p[6]]
  var lrCorner = [p[7], -p[8], p[6]]
  var llCorner = [-p[7], -p[8], p[6]]
  var xyzCam = math.matrix([urCorner, ulCorner, lrCorner, llCorner])
  // Transform the image plan from camera coordinates to world coordinates
  var XYZCam = poseEst.camera2world(xyzCam, p)
  // Substract the camera location to get a 3D model close to the origin
  // -------------------------------------------------------------------
  // Vector camera DEM
  var T = math.matrix([[math.subset(p, math.index([0])), math.subset(p, math.index([1])), math.subset(p, math.index([2]))]])
  var nObs = math.subset(math.size(xyzCam), math.index([0]))
  var Tarr = []
  for (var i = 0; i < nObs; i++) {
    Tarr.push(math.squeeze(T))
  }
  var Tmat = math.transpose(math.matrix(Tarr))
  var XYZCamOff = math.subtract(XYZCam, Tmat)
  // Rotate around Z vector to take into account meridian convergence
  // ----------------------------------------------------------------
  var muDegre = poseEst.convergenceMeridien(p[0] - 2600000, p[1] - 1200000) // In civil coordinates (centered in bern)
  var muRadian = muDegre * Math.PI / 180
  var R = poseEst.compute_Rot3(-muRadian)
  var XYZCamOff = math.multiply(R, XYZCamOff)
  // Scale the model
  // ---------------
  var urCorner = math.squeeze(math.subset(XYZCamOff, math.index([0, 1, 2], 0)))
  var ulCorner = math.squeeze(math.subset(XYZCamOff, math.index([0, 1, 2], 1)))
  var dif = math.subtract(urCorner, ulCorner)
  var dist = math.norm(dif)

  var maxSize = 100 // max size of the bigest side of the image
  var ratio = maxSize / dist
  var scaledXYZ = math.multiply(XYZCamOff, ratio)
  // Insert the computed coordinates in the collada template file
  // ------------------------------------------------------------
  // create the coordinate string for collada
  var coordString = ''
  var dim0 = math.subset(math.size(XYZCamOff), math.index([0]))
  var dim1 = math.subset(math.size(XYZCamOff), math.index([1]))
  for (var i = 0; i < dim1; i++) {
    for (var j = 0; j < dim0; j++) {
      var str = math.subset(scaledXYZ, math.index(j, i)).toFixed(1)// XYZCamOff
      var coordString = coordString.concat(str)
      var coordString = coordString.concat(' ')
    };
  };
  // var xml = fs.readFile('public/3Dmodel/collada_template_mix.dae', 'utf8')
  fs.readFile('public/3Dmodel/collada_template_mix.dae', 'utf8', function (err, data) {
    if (err) {
      return res.json({success: false, message: err})
    } else {
      var xml = data
      var newXml = xml.replace('#IMAGECOORDINATES#', coordString)
      var path = '../images/1024/' + imageId + '.jpg'
      var newXml = newXml.replace('#PATH2IMAGE#', path ) // dds png

      //Save in the temp folder
      var path = './public/data/collections/'+collectionId+'/temp_collada/' + imageId + '.dae'

      fs.writeFile(path, newXml, function (err) {
        if (err) {
          return res.json({success: false, message: err})
        } else {
          collada2gltf(path, {path: '/bin'}, function (err) {
            if (err) {
              return res.json({success: false, message: err})
            } else {
              return res.json({success: true, message: 'GLTF is generated'})
            }
          })
        }
      })
    }// else
  })// read file
})

// Copy gltf in the collada folder
router.post('/georef/copyGltf/', function (req, res) {
  var imageId = req.body.imageId
  var collectionId = req.body.collectionId

  // Generate the path of the files to be copied
  var rootTemp = './public/data/collections/'+collectionId+'/temp_collada/'
  var rootGltf = './public/data/collections/'+collectionId+'/gltf/'
  // bin
  var binTemp = rootTemp + imageId + '.bin'
  var bin = rootGltf + imageId + '.bin'
  fs.createReadStream(binTemp).pipe(fs.createWriteStream(bin))

  // gltf
  var gltfTemp = rootTemp + imageId + '.gltf'
  var gltf = rootGltf + imageId + '.gltf'
  fs.createReadStream(gltfTemp).pipe(fs.createWriteStream(gltf))

  // glsl
  var glsl1Temp = rootTemp + imageId + '0FS.glsl'
  var glsl1 = rootGltf + imageId + '0FS.glsl'
  fs.createReadStream(glsl1Temp).pipe(fs.createWriteStream(glsl1))

  var glsl2Temp = rootTemp + imageId + '0VS.glsl'
  var glsl2 = rootGltf + imageId + '0VS.glsl'
  fs.createReadStream(glsl2Temp).pipe(fs.createWriteStream(glsl2))

  return res.json({success: true, message: 'GLTF is copied'})
})

// Save the footprint in the database
router.post('/georef/saveFootprint/', function (req, res) {
  var footprintGeojson = req.body.footprintGeojson
  var imageId = req.body.imageId
  var longitude = req.body.longitude
  var latitude = req.body.latitude

  models.images.update(
    {
    // footprint: Sequelize.fn('ST_Simplify',Sequelize.fn('ST_Buffer',Sequelize.fn('ST_MakePolygon',Sequelize.fn('ST_GeomFromGeoJSON', footprintGeojson)), 0.0045),0.0001)
      footprint: Sequelize.cast(Sequelize.fn('ST_Intersection',
      // GeometryA
      Sequelize.fn('ST_Simplify', Sequelize.fn('ST_Buffer', Sequelize.fn('ST_MakePolygon', Sequelize.fn('ST_GeomFromGeoJSON', footprintGeojson)), 0.0045), 0.0001),
      // GeometryB
      Sequelize.fn('ST_buffer', Sequelize.fn('ST_SetSRID', Sequelize.cast(Sequelize.fn('ST_MakePoint', longitude, latitude), 'geography'), '4326'), 60000)), 'geometry')
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    // return result
    return res.json({success: true, message: 'Footprint is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Footprint is not saved'})
  })
})

module.exports = router
