var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op
var GeoJSON = require('geojson')

var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

router.post('/search/getToponymsLike', function (req, res) {
  var searchString = '%' + req.body.input + '%'
  models.toponyms.findAll({
    raw: true,
    attributes: ['name',
      [Sequelize.fn('ST_X', Sequelize.col('geom')), 'lng'],
      [Sequelize.fn('ST_Y', Sequelize.col('geom')), 'lat']
    ],
    where: {
      name: {
        [Op.iLike]: searchString
      }
    },
    limit: 4
  }).then(function (Toponyms) {
    var results = parseDB(Toponyms)
    return res.json(results)
  })
})

router.post('/search/getToponymLocation', function (req, res) {
  var searchString = req.body.input
  models.toponyms.findAll({
    raw: true,
    attributes: ['name',
       [Sequelize.fn('ST_X', Sequelize.col('geom')), 'lng'],
       [Sequelize.fn('ST_Y', Sequelize.col('geom')), 'lat']
    ],
    where: {
      name: searchString
    },
    limit: 1
  }).then(function (Toponyms) {
    var results = parseDB(Toponyms)
    return res.json(results)
  })
})

router.post('/search/getToponymsSoundLike', function (req, res) {
  var searchString = req.body.input
  models.toponyms.findAll({
    raw: true,
    attributes: ['name',
       [Sequelize.fn('ST_X', Sequelize.col('geom')), 'lng'],
       [Sequelize.fn('ST_Y', Sequelize.col('geom')), 'lat']
    ],
    limit: 2,
    order: [[Sequelize.fn('levenshtein', Sequelize.col('name'), searchString), 'ASC']]
  }).then(function (Toponyms) {
    var results = parseDB(Toponyms)
    return res.json(results)
  })
})

// Get toponyms from the database
router.post('/topo/getToponyms/', function (req, res, next) {
  var footprintGeojson = req.body.footprint
  // var latitude = req.body.latitude
  // var longitude = req.body.longitude
  var fpJson = JSON.parse(footprintGeojson)
  if (fpJson.type === 'Polygon') {
    var sql = "st_intersects(toponyms.geom,st_buffer(st_geomfromgeojson('" + footprintGeojson + "'), 0.0045))"// " and st_Distance(toponyms.geom, ST_MakePoint("+latitude+","+longitude+",4326)) < 0.06";
  } else {
    var sql = "st_intersects(toponyms.geom,st_buffer(st_makePolygon(st_geomfromgeojson('" + footprintGeojson + "')), 0.0045))"// " and st_Distance(toponyms.geom, ST_MakePoint("+latitude+","+longitude+",4326)) < 0.06";
  }

  models.toponyms.findAll({
    attributes: ['name', 'type', [Sequelize.fn('ST_X', Sequelize.col('geom')), 'longitude'], [Sequelize.fn('ST_Y', Sequelize.col('geom')), 'latitude']],
    where: {
      geom: Sequelize.literal(sql)
    },
    raw: true
  }).then(function (toponyms) {
    var results = parseDB(toponyms)
    var geoResult = GeoJSON.parse(results.table, {Point: ['latitude', 'longitude'], include: ['name', 'type']})
    return res.json(geoResult)
  }).catch(function (err) {
    console.log('error getting toponyms in footprint')
  })
})

// Save the geotags in the database
router.post('/topo/saveToponyms/', function (req, res) {
  var geotags_array = req.body.geotags_array
  var imageId = req.body.imageId
  models.images.update(
    {
      geotags_array: geotags_array
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    return res.json({success: true, message: 'Toponyms are saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Toponyms are not saved'})
  })
})

// Create a new toponyms
router.post('/topo/createToponym/', function (req, res) {
  var toponym = req.body.toponym
  var imageId = req.body.imageId
  var volunteerId = req.body.volunteerId
  models.toponyms.create(
    {
      id: Sequelize.literal("nextval('toponyms_id_seq'::regclass)"),
      name: toponym.name,
      geom: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', toponym.longitude, toponym.latitude), '4326'),
      volunteer_id: volunteerId,
      type: 'test',
      date_in: Sequelize.literal('current_timestamp')
    },
    {
      where: {id: imageId}
    })
  .then(function (result) {
    // return result
    return res.json({success: true, message: 'Toponym is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Toponym is not saved'})
  })
})

module.exports = router
