var models = require('../model')
var Sequelize = require('sequelize')
var jwt = require('jwt-simple')
var tok = require('../authentication/token')
var passport = require('passport')

var express = require('express')
var router = new express.Router()

var volunteer = require('../controllers/volunteer')

// Protected add to game
router.post('/game/add/', passport.authenticate('jwt', {session: false}), function (req, res) {
  var imageId = req.body.imageId
  var token = tok.getToken(req.headers)
  if (token) {
    // var decoded = jwt.decode(token, '5map5h0tKey')
    models.images.update({
      in_game: true
    }, {
      where: {
        id: imageId
      }
    }).then(function () {
      return res.json({success: true, message: 'Image is added to game'})
    },
    function () {
      return res.json({success: false, message: 'Image is not added to game'})
    }
    )
  }else{
    return res.json({success: false, message: 'You didnt provided a token'})
  }
})

// Protected remove from game
router.post('/game/remove/', passport.authenticate('jwt', {session: false}), function (req, res) {
  var imageId = req.body.imageId
  var token = tok.getToken(req.headers)
  if (token) {
    // var decoded = jwt.decode(token, '5map5h0tKey')
    models.images.update({
      in_game: false
    }, {
      where: {
        id: imageId
      }
    }).then(function () {
      return res.json({success: true, message: 'Image is removed from game'})
    },
    function () {
      return res.json({success: false, message: 'Image is not removed from game'})
    }
    )
  }else{
    return res.json({success: false, message: 'You are not logged'})
  }
})

// Protected validate geolocation
router.post('/georef/validate/', passport.authenticate('jwt', {session: false}), function (req, res) {
  var imageId = req.body.imageId
  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')

    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function (vol) {
      models.images.update(
        {
          is_validated: true,
          validator_id: decoded.id
        }
        , {
          where: {
            id: imageId
          }
        }).then(function () {
          return res.json({success: true, message: 'Image is validated'})
        })
        .catch(function (err) {
          return res.json({success: false, message: 'Image is not validated'})
        })
    })
    .catch(function(){
      return res.json({success: false, message: 'You arent a validator'})
    })
  } else {
    return res.json({success: false, message: 'You didnt provide a token'})
  }
})

// Protected reject geolocation
router.post('/georef/reject/', passport.authenticate('jwt', {session: false}), function (req, res) {
  var imageId = req.body.imageId
  var tok = token.getToken(req.headers)
  if (tok) {

    var decoded = jwt.decode(tok, '5map5h0tKey')
    volunteer.checkValidator(decoded.id)
    .then(function (vol) {
      models.images.update({
        location: Sequelize.literal('images.location_apriori'),
        roll: null,
        tilt: null,
        azimuth: null,
        focal: null,
        cx: null,
        cy: null,
        volunteer_id: null,
        date_georef: null,
        georef_3d_bool: false,
        georef_azi_bool: false,
        georef_map_bool: false,
        gcp_json: null
      }, {
        where: {
          id: imageId
        }
      }).then(function () {
        return res.json({success: true, message: 'Image is rejected'})
      },
      function () {
        return res.json({success: false, message: 'Image is not rejected'})
      }
      )
    })
    .catch(function (err){
      return res.json({success: false, message: 'You arent a validator'})
    })
  } else {
    return res.json({success: false, message: 'You didnt provide a token'})
  }
})

module.exports = router
