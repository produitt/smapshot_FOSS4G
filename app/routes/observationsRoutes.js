var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op

var express = require('express')
var router = new express.Router()

var emailsSender = require('../controllers/emails')
var owner = require('../controllers/owner')
var volunteer = require('../controllers/volunteer')

// For the check of the validator status
var jwt = require('jwt-simple')
var tok = require('../authentication/token')
var passport = require('passport')

router.post('/observations/submit', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var observation = req.body.observation

  models.observations.create({
    id: Sequelize.literal("nextval('observations_id_seq'::regclass)"),
    image_id: imageId,
    volunteer_id: volunteerId,
    observation: observation.observation,
    coord_x: observation.coord_x,
    coord_y: observation.coord_y,
    height: observation.height,
    width: observation.width,
    date_observation: Sequelize.literal('current_timestamp')
  }).then(function (result) {
      // return result
      return res.json({success: true, message: 'Observation is saved'})
    }
    , function (rejectedPromiseError) {
      return res.json({success: false, message: 'Observation is not saved'})
    }
  )
})

router.post('/observations/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select images.id, images.original_id, images.georef_3d_bool, images.title,
  images.owner_id, images.collection_id,
  observations.id as obs_id, image_id, observations.volunteer_id, date_observation,
  observation, coord_x, coord_y, observations.validated, observations.processed,
  observations.height, observations.width, observations.remark,
  owners.name as ownername, collections.name as collectionname,
  v1.username as username, is_published, v1.lang,
  v2.username as validator_name
  from observations
  left join volunteers as v1 on observations.volunteer_id = v1.id
  left join volunteers as v2 on observations.validator_id = v2.id
  left join images on observations.image_id = images.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  if (originalId) {
    sql = sql + "WHERE images.original_id = '" + originalId + "'"
  }else if (imageId) {
    sql = sql + "WHERE images.id = '" + imageId + "'"
  }else{
    var wheres = []
    // Generate where
    // Owner constraint
    if (ownerId) {
      wheres.push('images.owner_id = '+ ownerId)
    }
    // From date constraint
    if (startDate) {
      wheres.push("observations.date_observation >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("observations.date_observation <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
    // Collection constraint
    if (collectionId) {
      wheres.push('images.collection_id = ' + collectionId)
    }
    if (validatorId){
      wheres.push('observations.validator_id = ' + validatorId)
    }
    if ((volunteerUsername != null) && (volunteerUsername != 'All')){
      wheres.push("v1.username ~* '" + volunteerUsername + "'")
    }
    if (processed != null){
      if ((processed === 'false') || (processed === false)) {
        wheres.push('observations.processed = ' + false)
      }else if ((processed === 'true') || (processed === true)) {
        wheres.push('observations.processed = ' + true)
      }
    }
    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++){
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1){
          sql = sql + ' AND'
        }
      }
    }
  }
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/observations/getForDownload/', function (req, res) {
  // Grab data from http request
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var timeType = req.body.dates.type
  var ids = req.body.ids
  var download = req.body.download
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select observations.id as obs_id, original_id, image_id, username, replace(observation,E'\n',' '),
  date_observation::date::TEXT, round(coord_x,4) as coord_x, round(coord_y, 4) as coord_y,
  round(observations.width, 4) as width, round(observations.height, 4) as height
  from observations
  inner join images on images.id = observations.image_id
  inner join volunteers on observations.volunteer_id = volunteers.id
  where observations.validated  = true
  `
  // Generate where
  var wheres = []

  if (timeType === 'download'){
    // From date constraint
    if (startDate) {
      wheres.push("observations.download_timestamp  >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("observations.download_timestamp  <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
  } else if (timeType === 'creation'){
    // From date constraint
    if (startDate) {
      wheres.push("observations.validation_date >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("observations.validation_date <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
  }
  // owner contstraint
  if (ownerId){
    wheres.push('images.owner_id = ' + ownerId)
  }
  // Collection constraint
  if (collectionId) {
    wheres.push('images.collection_id = ' + collectionId)
  }
  if (download == 'false'){
    wheres.push("observations.downloaded = False")
  }else if (download == 'true'){
    wheres.push("observations.downloaded = true")
  }
  if (ids){
    for (var i=0; i<ids.length; i++){
      ids[i] = "'"+ids[i]+"'"
    }
    wheres.push("images.original_id IN (" + ids.toString() + ")")
  }

  if (wheres.length != 0){
    sql = sql + ' AND '
    var wheresString = wheres.join(' AND ')
    sql = sql + wheresString
  }



  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (observations) {
    var listIds = []
    for (var i=0; i<observations.length; i++){
      listIds.push(observations[i].obs_id)
    }
    if (listIds.length !== 0){
      models.observations.update(
        {
          download_timestamp: Sequelize.literal('CURRENT_TIMESTAMP'),
          downloaded: true
        },{
          where:{
           id: {
             [Op.in]: listIds
           }
         }
      }).then(function (){
        return res.json(observations)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }
    else{
      return res.json(observations)
    }
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})


// Get my observations
router.post('/observations/getMy', function (req, res) {
  var volunteerId = req.body.volunteerId
  var query = `
  select observations.id as obs_id, image_id, observations.volunteer_id, date_observation,
  observation, coord_x, coord_y, observations.validated, images.collection_id,
  images.original_id, images.title,
  remark, observations.processed, observations.height, observations.width,
  (SELECT collections.name FROM collections WHERE collections.id = images.collection_id) as collectionName,
  (SELECT owners.name FROM owners WHERE owners.id = images.owner_id) as ownerName
  from observations, images
  where images.id = observations.image_id
  and observations.volunteer_id = ` + volunteerId +
  `
  order by date_observation desc
  `
  // and owner_id = 0
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
  .then(function (observations) {
    // We don't need spread here, since only the results will be returned for select queries
    return res.json(observations)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

// Get observations which are validated
router.post('/observations/getImageObservations', function (req, res) {
  var imageId = req.body.imageId
  var query = `
  select observations.id as obs_id, image_id, observations.volunteer_id, date_observation,
  observation, coord_x, coord_y, observations.validated, observations.height, observations.width,
  (SELECT volunteers.username FROM volunteers WHERE volunteers.id = observations.volunteer_id) as username
  from observations
  where observations.image_id =` + imageId +
  ` and observations.validated = true
  `
  // and owner_id = 0
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
  .then(function (corrections) {
    // We don't need spread here, since only the results will be returned for select queries
    return res.json(corrections)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

// Accept the observation
router.post('/observations/accept/', passport.authenticate('jwt', {session: false}), function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function(vol){
      var observation = req.body.observation
      var validatorId = req.body.validatorId
      var volunteerId = req.body.observation.volunteer_id
      var imageId = req.body.observation.image_id
      var remark = req.body.observation.remark

      models.observations.update({
        validated: true,
        processed: true,
        validator_id: validatorId,
        remark: remark,
        validation_date: Sequelize.literal('current_timestamp')
      }, {
        where: {
          id: observation.obs_id
        }
      }).then(function () {
        volunteer.checkNotification(volunteerId).then(function (volunteer){
          volunteer.notifications = false // avoid the email
          if (volunteer.notifications){
            owner.getValidatorOwnerName(validatorId)
            .then(function (ownerName) {
              emailsSender.sendObservationValidate(volunteer, imageId, ownerName, remark, observation.observation)
              .then(function() {
                return res.json({success: true, message: 'Observation is accepted'})
              })
              .catch(function (error){
                return res.json({success: false, message: 'Error while sending observation accpet email'})
              })
            })
            .catch(function(err){
              return res.json({success: false, msg: 'error'})
            })
          }else{
            return res.json({success: true, message: 'Observation is accepted'})
          }
        })
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    })
    .catch(function(err){
      return res.json({success: false, message: 'You arent a validator'})
    })
  }else{
    return res.json({success: false, msg: 'no token'})
  }
})

// Reject the observation based on volunteers corrections
router.post('/observations/reject/', passport.authenticate('jwt', {session: false}), function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function(vol){
      var observation = req.body.observation
      var validatorId = req.body.validatorId
      var volunteerId = req.body.observation.volunteer_id
      var imageId = req.body.observation.image_id
      var remark = req.body.observation.remark

      models.observations.update({
        validated: false,
        processed: true,
        validator_id: validatorId,
        remark: remark,
        validation_date: Sequelize.literal('current_timestamp')
      }, {
        where: {
          id: observation.obs_id
        }
      }).then(function () {
        volunteer.checkNotification(volunteerId).then(function (volunteer){
          volunteer.notifications = false // avoid the email
          if (volunteer.notifications){
            owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
              emailsSender.sendObservationReject(volunteer, imageId, ownerName, remark, observation.observation)
              .then(function() {
                return res.json({success: true, message: 'Observation is rejected'})
              })
              .catch(function (error){
                return res.json({success: false, message: 'Error while sending observation reject email'})
              })
            })
          }else{
            return res.json({success: true, message: 'Observation is rejected'})
          }
        })
      })
      .catch(function(err){
        return res.json({success: false, message: 'You arent a validator'})
      })
    })
    .catch(function(err){
      return res.json({success: false, message: 'You arent a validator'})
    })
   }
   else{
     return res.json({success: false, msg: 'no token'})
   }
})

module.exports = router
