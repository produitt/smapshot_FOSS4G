var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

// Get stories
router.post('/story/getStory/', function (req, res) {
  var imageId = req.body.imageId

  // var imageId = req.body.imageId;
  models.stories.findAll(
    {
      raw: true,
      attributes: [
      [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
      [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
      [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
        'title', 'text', 'timestamp', 'date_story',
      [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "stories"."volunteer_id")'), 'username']
      ],
      where: {
        image_id: imageId
      }
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Story is not sent'})
  })
})

// Create a story
router.post('/story/createStory/', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var title = req.body.story.title
  var text = req.body.story.text
  var lat = req.body.story.latitude
  var long = req.body.story.longitude
  var height = req.body.story.height
  var date_story = req.body.story.date

  // var imageId = req.body.imageId;
  models.stories.create(
    {
    // id: Sequelize.literal("nextval('toponyms_id_seq'::regclass)"),
      title: title,
      location: Sequelize.fn('ST_SetSRID', Sequelize.fn('ST_MakePoint', long, lat, height), '4326'),
      volunteer_id: volunteerId,
      text: text,
      image_id: imageId,
      timestamp: Sequelize.literal('current_timestamp'),
      date_story: date_story
    }
  ).then(function (result) {
    // return result
    return res.json({success: true, message: 'Story is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Story is not saved'})
  })
})

module.exports = router
