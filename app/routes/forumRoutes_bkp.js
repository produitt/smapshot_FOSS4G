var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

// Create a forum
router.post('/forum/createComment/', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var title = req.body.comment.title
  var text = req.body.comment.text

  models.forum.create(
    {
      title: title,
      volunteer_id: volunteerId,
      text: text,
      image_id: imageId,
      timestamp: Sequelize.literal('current_timestamp')
    }
  ).then(function (result) {
    // return result
    return res.json({success: true, message: 'Forum is saved'})
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Forum is not saved'})
  })
})

// Get the forum
router.post('/forum/getForum/', function (req, res) {
  var imageId = req.body.imageId
  models.forum.findAll(
    {
      raw: true,
      attributes: [
        'title', 'text', 'timestamp',
      [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "forum"."volunteer_id" AND "forum"."image_id" =' + imageId + ')'), 'username']
      ],
      order: 'timestamp', //, 'ASC']
      where: {
        image_id: imageId
      }
    })
  .then(function (result) {
    // var results = parseDB(result)
    // return result
    return res.json(result)
  }, function (rejectedPromiseError) {
    return res.json({success: false, message: 'Forum is not sent'})
  })
})

module.exports = router
