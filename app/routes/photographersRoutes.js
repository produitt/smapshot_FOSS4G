var models = require('../model')

var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

router.post('/search/getPhotographerLink', function (req, res) {
  var photographer_id = req.body.photographer_id
  models.photographers.findAll({
    attributes: ['link'],
    where: {
      id: photographer_id
    },
    raw: true
  }).then(function (photographer) {
    return res.json(photographer[0])
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
