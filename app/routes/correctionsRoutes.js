var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op

var express = require('express')
var router = new express.Router()

var emailsSender = require('../controllers/emails')
var owner = require('../controllers/owner')
var volunteer = require('../controllers/volunteer')

// For the check of the validator status
var jwt = require('jwt-simple')
var tok = require('../authentication/token')
var passport = require('passport')

router.post('/corrections/submit', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var dateShot = req.body.correction.dateShot
  var caption = req.body.correction.caption
  var title = req.body.correction.title
  var title_processed = false
  if (title == null) {
    title_processed = true
  }
  var caption_processed = false
  if (caption == null) {
    caption_processed = true
  }

  models.corrections.create({
    id: Sequelize.literal("nextval('corrections_id_seq'::regclass)"),
    image_id: imageId,
    volunteer_id: volunteerId,
    date_shot: dateShot,
    caption: caption,
    title: title,
    date_correction: Sequelize.literal('current_timestamp'),
    caption_processed: caption_processed,
    title_processed: title_processed
  }).then(function (result) {
      // return result
      return res.json({success: true, message: 'Correction is saved'})
    })
    .catch(function (rejectedPromiseError) {
      return res.json({success: false, message: 'Correction is not saved'})
    })
})

// Get my corrections
router.post('/corrections/getMy/', function (req, res) {

  var volunteerId = req.body.volunteerId
  var query = `
  select corrections.id as corr_id, image_id,
  corrections.title_processed, corrections.caption_processed,
  corrections.title as sub_title, corrections.caption as sub_caption,
  images.title as orig_title, images.caption as orig_caption,
  images.original_id,
  corrections.title_update, corrections.caption_update,
  corrections.title_remark, corrections.caption_remark,
  corrections.title_result, corrections.caption_result,
  date_correction,
  owner_id, collection_id,
  (SELECT collections.name FROM collections WHERE collections.id = images.collection_id) as collectionName,
  (SELECT owners.name FROM owners WHERE owners.id = images.owner_id) as ownerName
  from corrections, images
  where images.id = corrections.image_id
  and corrections.volunteer_id = ` + volunteerId +
  `
  order by date_correction desc
  `
  // and owner_id = 0
  models.sequelize.query(query, {type: models.sequelize.QueryTypes.SELECT})
  .then(function (corrections) {
    // We don't need spread here, since only the results will be returned for select queries
    return res.json(corrections)
  })
  .catch(function (rejectedPromiseError) {
    return res.json({success: false, message: 'get my correction error'})
  })
})


router.post('/corrections/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select images.original_id, images.georef_3d_bool, images.orig_title as orig_title,
  images.orig_caption as orig_caption, images.owner_id, images.collection_id,
  corrections.id as corr_id, image_id,
  corrections.title as sub_title, corrections.caption as sub_caption,
  corrections.title_result, corrections.caption_result,
  corrections.title_update, corrections.caption_update,
  corrections.title_processed, corrections.caption_processed,
  corrections.title_remark, corrections.caption_remark,
  corrections.volunteer_id, date_correction,
  owners.name as ownername, collections.name as collectionname,
  v1.username as username, is_published, v1.lang,
  v2.username as validator_name
  from corrections
  left join volunteers as v1 on corrections.volunteer_id = v1.id
  left join volunteers as v2 on corrections.validator_id = v2.id
  left join images on corrections.image_id = images.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  if (originalId) {
    sql = sql + "WHERE images.original_id = '" + originalId + "'"
  }else if (imageId) {
    sql = sql + "WHERE images.id = '" + imageId + "'"
  }else{
    var wheres = []
    // Generate where
    // Owner constraint
    if (ownerId) {
      wheres.push('images.owner_id = '+ ownerId)
    }
    // From date constraint
    if (startDate) {
      wheres.push("corrections.date_correction >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("corrections.date_correction <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }

    // Collection constraint
    if (collectionId) {
      wheres.push('images.collection_id = ' + collectionId)
    }
    if (validatorId){
      wheres.push('corrections.validator_id = ' + validatorId)
    }
    if ((volunteerUsername != null) && (volunteerUsername != 'All')){
      wheres.push("v1.username ~* '" + volunteerUsername + "'")
    }
    if (processed != null){
      if ((processed === 'false') || (processed === false)) {
        wheres.push('(corrections.title_processed = false or corrections.caption_processed = false)')
      }else if ((processed === 'true') || (processed === true)) {
        wheres.push('corrections.title_processed = true and corrections.caption_processed = true')
      }
    }
    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++){
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1){
          sql = sql + ' AND'
        }
      }
    }
  }
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function (err) {
    return res.json({success: false, message: 'error getting correction dashboard'})
  })
})

  // Accept the title based on volunteers corrections
  router.post('/corrections/acceptTitle/', passport.authenticate('jwt', {session: false}), function (req, res) {

    var token = tok.getToken(req.headers)
    if (token) {
      var decoded = jwt.decode(token, '5map5h0tKey')
      // Check that the volunteer is validator_id
      volunteer.checkValidator(decoded.id)
      .then(function(vol){
        var newTitle = req.body.newTitle
        var corrId = req.body.corrId
        var imageId = req.body.imageId
        var validatorId = req.body.validatorId
        var volunteerId = req.body.volunteerId
        var titleRemark = req.body.titleRemark

        models.corrections.update({
          title_result: 'accepted',
          title_processed: true,
          title_remark: titleRemark,
          validator_id: validatorId,
          title_validation_date: Sequelize.literal('current_timestamp')
        }, {
          where: {
            id: corrId
          }
        }).then(function () {
          models.images.update({
            title: newTitle
          }, {
            where: {
              id: imageId
            }
          }).then(function () {
            volunteer.checkNotification(volunteerId).then(function (volunteer){
              volunteer.notifications = false // avoid the email
              if (volunteer.notifications){
                owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                  emailsSender.sendCorrectionTitleValidation(volunteer, imageId, ownerName, newTitle, titleRemark)
                  .then(function() {
                    return res.json({success: true, message: 'Title correction is accepted'})
                  })
                  .catch(function (error){
                    return res.json({success: false, message: 'Error while sending title validated email'})
                  })
                })
              }else{
                return res.json({success: true, message: 'Title correction is accepted'})
              }
            })
          })
          .catch(function(err){
            return res.json({success: false, message: 'Error check notifications'})
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'Error update title'})
        })
      })
      .catch(function(err){
        return res.json({success: false, message: 'You arent a validator'})
      })
    }else{
      return res.json({success: false, message: 'No token provided'})
    }

  })

  // Update the caption based on volunteers corrections
  router.post('/corrections/acceptCaption/', passport.authenticate('jwt', {session: false}), function (req, res) {

      var token = tok.getToken(req.headers)
      if (token) {
        var decoded = jwt.decode(token, '5map5h0tKey')
        // Check that the volunteer is validator_id
        volunteer.checkValidator(decoded.id)
        .then(function(vol){
          var newCaption = req.body.newCaption
          var corrId = req.body.corrId
          var imageId = req.body.imageId
          var validatorId = req.body.validatorId
          var volunteerId = req.body.volunteerId
          var captionRemark = req.body.captionRemark

          models.corrections.update({
            caption_result: 'accepted',
            caption_processed: true,
            validator_id: validatorId,
            caption_validation_date: Sequelize.literal('current_timestamp')
          }, {
            where: {
              id: corrId
            }
          }).then(function () {
            models.images.update({
              caption: newCaption
            }, {
              where: {
                id: imageId
              }
            }).then(function () {
              volunteer.checkNotification(volunteerId).then(function (volunteer){
                volunteer.notifications = false // avoid the email
                if (volunteer.notifications){
                  owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                    emailsSender.sendCorrectionCaptionValidation(volunteer, imageId, ownerName, newCaption, captionRemark)
                    .then(function() {
                      return res.json({success: true, message: 'Caption correction is accepted'})
                    })
                    .catch(function (error){
                      return res.json({success: false, message: 'Error while sending caption validated email'})
                    })
                  })
                }else{
                  return res.json({success: true, message: 'Caption correction is accepted'})
                }
              })
              .catch(function(err){
                return res.json({success: false, message: 'Error check notification'})
              })
            })
            .catch(function(err){
              return res.json({success: false, message: 'Error update images'})
            })
          })
          .catch(function(err){
            return res.json({success: false, message: 'Error update caption'})
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'You arent a validator'})
        })
       }
       else {
         return res.json({success: false, message: 'You didnt provided a token'})
       }
  })

  // Update the title based on volunteers corrections
  router.post('/corrections/updateTitle/'  , passport.authenticate('jwt', {session: false}), function (req, res) {

      var token = tok.getToken(req.headers)
      if (token) {
        var decoded = jwt.decode(token, '5map5h0tKey')
        // Check that the volunteer is validator_id
        volunteer.checkValidator(decoded.id)
        .then(function(vol){
          var valTitle = req.body.valTitle // submited by the validator
          var volTitle = req.body.volTitle // submited by the volunteer
          var corrId = req.body.corrId
          var imageId = req.body.imageId
          var validatorId = req.body.validatorId
          var volunteerId = req.body.volunteerId
          var titleRemark = req.body.titleRemark

          models.corrections.update({
            title_result: 'updated',
            title_processed: true,
            title_update: valTitle,
            title_remark: titleRemark,
            validator_id: validatorId,
            title_validation_date: Sequelize.literal('current_timestamp')
          }, {
            where: {
              id: corrId
            }
          }).then(function () {
            models.images.update({
              title: valTitle
            }, {
              where: {
                id: imageId
              }
            }).then(function () {
              volunteer.checkNotification(volunteerId).then(function (volunteer){
                volunteer.notifications = false // avoid the email
                if (volunteer.notifications){
                  owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                    emailsSender.sendCorrectionTitleUpdate(volunteer, imageId, ownerName, valTitle, volTitle, titleRemark)
                    .then(function() {
                      return res.json({success: true, message: 'Title correction is updated'})
                    })
                    .catch(function (error){
                      return res.json({success: false, message: 'Error while sending update title email'})
                    })
                  })
                }else{
                  return res.json({success: true, message: 'Title correction is updated'})
                }
              })
              //return res.json({success: true, message: 'Title correction is updated'})
            })
            .catch(function(err){
              return res.json({success: false, message: 'error update title'})
            })
          })
          .catch(function(err){
            return res.json({success: false, message: 'error update correction'})
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'You arent a validator'})
        })
       }
       else {
        return res.json({success: false, message: 'You didnt provided a token'})
      }
  })

  // Update the caption based on volunteers corrections
  router.post('/corrections/updateCaption/', passport.authenticate('jwt', {session: false}), function (req, res) {

      var token = tok.getToken(req.headers)
      if (token) {
        var decoded = jwt.decode(token, '5map5h0tKey')
        // Check that the volunteer is validator_id
        volunteer.checkValidator(decoded.id)
        .then(function(vol){
          var valCaption = req.body.valCaption
          var volCaption = req.body.volCaption
          var corrId = req.body.corrId
          var imageId = req.body.imageId
          var validatorId = req.body.validatorId
          var volunteerId = req.body.volunteerId
          var captionRemark = req.body.captionRemark

          models.corrections.update({
            caption_result: 'updated',
            caption_processed: true,
            caption_update: volCaption,
            caption_remark: captionRemark,
            validator_id: validatorId,
            caption_validation_date: Sequelize.literal('current_timestamp')
          }, {
            where: {
              id: corrId
            }
          }).then(function () {
            models.images.update({
              caption: volCaption
            }, {
              where: {
                id: imageId
              }
            }).then(function () {
              volunteer.checkNotification(volunteerId).then(function (volunteer){
                volunteer.notifications = false // avoid the email
                if (volunteer.notifications){
                  owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                    emailsSender.sendCorrectionCaptionUpdate(volunteer, imageId, ownerName, valCaption, volCaption, captionRemark)
                    .then(function() {
                      return res.json({success: true, message: 'Caption correction is updated'})
                    })
                    .catch(function (error){
                      return res.json({success: false, message: 'Error while sending caption updated email'})
                    })
                  })
                }else{
                  return res.json({success: true, message: 'Caption correction is updated'})
                }
              })
              //return res.json({success: true, message: 'Caption correction is updated'})
            })
            .catch(function(err){
              return res.json({success: false, message: 'Error upate image'})
            })
          })
          .catch(function(err){
            return res.json({success: false, message: 'Error upate correction'})
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'You arent a validator'})
        })
       }
       else {
        return res.json({success: false, message: 'You didnt provided a token'})
      }
  })

  // Reject the caption based on volunteers corrections
  router.post('/corrections/rejectCaption/', passport.authenticate('jwt', {session: false}), function (req, res) {

      var token = tok.getToken(req.headers)
      if (token) {
        var decoded = jwt.decode(token, '5map5h0tKey')
        // Check that the volunteer is validator_id
        volunteer.checkValidator(decoded.id)
        .then(function(vol){
          var corrId = req.body.corrId
          var validatorId = req.body.validatorId
          var volunteerId = req.body.volunteerId
          var volCaption = req.body.volCaption
          var imageId = req.body.imageId
          var captionRemark = req.body.captionRemark

          models.corrections.update({
            caption_result: 'rejected',
            caption_processed: true,
            caption_remark: captionRemark,
            validator_id: validatorId,
            caption_validation_date: Sequelize.literal('current_timestamp')
          }, {
            where: {
              id: corrId
            }
          }).then(function () {
            volunteer.checkNotification(volunteerId).then(function (volunteer){
              volunteer.notifications = false // avoid the email
              if (volunteer.notifications){
                owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                  emailsSender.sendCorrectionCaptionReject(volunteer, imageId, ownerName, volCaption, captionRemark)
                  .then(function() {
                    return res.json({success: true, message: 'Caption correction is rejected'})
                  })
                  .catch(function (error){
                    return res.json({success: false, message: 'Error while sending caption rejected email'})
                  })
                })
              }else{
                return res.json({success: true, message: 'Caption correction is rejected'})
              }
            })
            //return res.json({success: true, message: 'Caption correction is rejected'})
          })
          .catch(function(err){
            return res.json({success: false, message: 'error in the update of the correction' })
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'You arent a validator'})
        })
       }
       else {
        return res.json({success: false, message: 'You didnt provided a token'})
      }

  })

  // Reject the title based on volunteers corrections
  router.post('/corrections/rejectTitle/'  , passport.authenticate('jwt', {session: false}), function (req, res) {

      var token = tok.getToken(req.headers)
      if (token) {
        var decoded = jwt.decode(token, '5map5h0tKey')
        // Check that the volunteer is validator_id
        volunteer.checkValidator(decoded.id)
        .then(function(vol){
          var corrId = req.body.corrId
          var validatorId = req.body.validatorId
          var volunteerId = req.body.volunteerId
          var volTitle = req.body.volTitle
          var imageId = req.body.imageId
          var titleRemark = req.body.titleRemark

          models.corrections.update({
            title_result: 'rejected',
            title_processed: true,
            title_remark: titleRemark,
            validator_id: validatorId,
            title_validation_date: Sequelize.literal('current_timestamp')
          }, {
            where: {
              id: corrId
            }
          }).then(function () {
            volunteer.checkNotification(volunteerId).then(function (volunteer){
              volunteer.notifications = false // avoid the email
              if (volunteer.notifications){
                owner.getValidatorOwnerName(validatorId).then(function (ownerName) {
                  emailsSender.sendCorrectionTitleReject(volunteer, imageId, ownerName, volTitle, titleRemark)
                  .then(function() {
                    return res.json({success: true, message: 'Title correction is rejected'})
                  })
                  .catch(function (error){
                    return res.json({success: false, message: 'Error while sending caption rejected email'})
                  })
                })
              }else{
                return res.json({success: true, message: 'Title correction is rejected'})
              }
            })
            //return res.json({success: true, message: 'Title correction is rejected'})
          })
          .catch(function(err){
            return res.json({success: false, message: 'Error in the update of the correction'})
          })
        })
        .catch(function(err){
          return res.json({success: false, message: 'You arent a validator'})
        })
       }
       else {
        return res.json({success: false, message: 'You didnt provided a token'})
      }
  })

  router.post('/corrections/getTitleForDownload/', function (req, res) {
    // Grab data from http request
    var collectionId = req.body.collection.id
    var startDate = req.body.dates.min
    var stopDate = req.body.dates.max
    var timeType = req.body.dates.type
    var ids = req.body.ids
    var download = req.body.download
    var ownerId = req.body.owner.id
    console.log('body', req.body)

// select corrections.id as corr_id, original_id, corrections.image_id, username,
// date_correction, title_result as result,
// case when title_result like 'updated' THEN title_update ELSE corrections.title  END as title
// from corrections
// inner join images on images.id = corrections.image_id
// inner join volunteers on corrections.volunteer_id = volunteers.id
// inner join (select image_id, max(date_correction) as m from corrections group by image_id) c on corrections.image_id = c.image_id and corrections.date_correction = c.m
// where corrections.title_processed  = true
// and corrections.title_result not like 'rejected'

    // Generate query
    // var sql = `
    // select corrections.id as corr_id, original_id, image_id, username,
    // date_correction, title_result as result,
    // case when title_result like 'updated' THEN title_update ELSE corrections.title  END as title
    // from corrections
    // inner join images on images.id = corrections.image_id
    // inner join volunteers on corrections.volunteer_id = volunteers.id
    // where corrections.title_processed  = true
    // and corrections.title_result not like 'rejected'
    // `
    // date_correction::date::TEXT,
    var sql = `
    select distinct corrections.id as corr_id, original_id, corrections.image_id, username,
    title_validation_date::date::TEXT, title_result as result,
    case when title_result like 'updated' THEN replace(title_update,E'\n',' ')  ELSE replace(corrections.title,E'\n',' ') END as title
    from corrections
    inner join images on images.id = corrections.image_id
    inner join volunteers on corrections.volunteer_id = volunteers.id
    inner join (select image_id, max(title_validation_date) as m from corrections group by image_id) c
    on corrections.image_id = c.image_id and corrections.title_validation_date = c.m
    where corrections.title_processed  = true
    and corrections.title_result not like 'rejected'
    `
    // Generate where
    var wheres = []

    if (timeType === 'download'){
      // From date constraint
      if (startDate) {
        wheres.push("corrections.download_timestamp  >= to_date('" + startDate + "','DD/MM/YYYY')")
      }
      // To date constraint
      if (stopDate) {
        wheres.push("corrections.download_timestamp  <= to_date('" + stopDate + "','DD/MM/YYYY')")
      }
    } else if (timeType === 'creation'){
      // From date constraint
      if (startDate) {
        wheres.push("corrections.title_validation_date >= to_date('" + startDate + "','DD/MM/YYYY')")
      }
      // To date constraint
      if (stopDate) {
        wheres.push("corrections.title_validation_date <= to_date('" + stopDate + "','DD/MM/YYYY')")
      }
    }
    // owner contstraint
    if (ownerId){
      wheres.push('images.owner_id = ' + ownerId)
    }
    // Collection constraint
    if (collectionId) {
      wheres.push('images.collection_id = ' + collectionId)
    }
    if (download == 'false'){
      wheres.push("corrections.downloaded = False")
    }else if (download == 'true'){
      wheres.push("corrections.downloaded = true")
    }
    if (ids){
      for (var i=0; i<ids.length; i++){
        ids[i] = "'"+ids[i]+"'"
      }
      wheres.push("images.original_id IN (" + ids.toString() + ")")
    }

    if (wheres.length != 0){
      sql = sql + ' AND '
      var wheresString = wheres.join(' AND ')
      sql = sql + wheresString
    }



    // Submit request
    models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
    .then(function (corrections) {
      var listIds = []
      for (var i=0; i<corrections.length; i++){
        listIds.push(corrections[i].corr_id)
      }
      if (listIds.length !== 0){
        models.corrections.update(
          {
            download_timestamp: Sequelize.literal('CURRENT_TIMESTAMP'),
            downloaded: true
          },{
            where:{
             id: {
               [Op.in]: listIds
             }
           }
        }).then(function (){
          return res.json(corrections)
        })
        .catch(function(err){
          return res.json({success: false, msg: 'error'})
        })
      }else{
        return res.json(corrections)
      }
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  })

  router.post('/corrections/getCaptionForDownload/', function (req, res) {
    // Grab data from http request
    var collectionId = req.body.collection.id
    var startDate = req.body.dates.min
    var stopDate = req.body.dates.max
    var timeType = req.body.dates.type
    var ids = req.body.ids
    var download = req.body.download
    var ownerId = req.body.owner.id

    // Generate query
    // var sql = `
    // select corrections.id as corr_id, original_id, image_id, username,
    // date_correction, caption_result as result,
    // case when caption_result like 'updated' THEN caption_update ELSE corrections.caption END as caption
    // from corrections
    // inner join images on images.id = corrections.image_id
    // inner join volunteers on corrections.volunteer_id = volunteers.id
    // where corrections.caption_processed  = true
    // and corrections.caption_result not like 'rejected'
    // `
    var sql = `
    select distinct corrections.id as corr_id, original_id, corrections.image_id, username,
    caption_validation_date::date::TEXT, caption_result as result,
    case when caption_result like 'updated' THEN replace(caption_update,E'\n',' ') ELSE replace(corrections.caption,E'\n',' ')  END as caption
    from corrections
    inner join images on images.id = corrections.image_id
    inner join volunteers on corrections.volunteer_id = volunteers.id
    inner join (select image_id, max(caption_validation_date) as m from corrections group by image_id) c
    on corrections.image_id = c.image_id and corrections.caption_validation_date = c.m
    where corrections.caption_processed  = true
    and corrections.caption_result not like 'rejected'
    `
    // Generate where
    var wheres = []

    if (timeType === 'download'){
      // From date constraint
      if (startDate) {
        wheres.push("corrections.download_timestamp  >= to_date('" + startDate + "','DD/MM/YYYY')")
      }
      // To date constraint
      if (stopDate) {
        wheres.push("corrections.download_timestamp  <= to_date('" + stopDate + "','DD/MM/YYYY')")
      }
    } else if (timeType === 'creation'){
      // From date constraint
      if (startDate) {
        wheres.push("corrections.caption_validation_date >= to_date('" + startDate + "','DD/MM/YYYY')")
      }
      // To date constraint
      if (stopDate) {
        wheres.push("corrections.caption_validation_date <= to_date('" + stopDate + "','DD/MM/YYYY')")
      }
    }
    // owner contstraint
    if (ownerId){
      wheres.push('images.owner_id = ' + ownerId)
    }
    // Collection constraint
    if (collectionId) {
      wheres.push('images.collection_id = ' + collectionId)
    }
    if (download == 'false'){
      wheres.push("corrections.downloaded = False")
    }else if (download == 'true'){
      wheres.push("corrections.downloaded = true")
    }
    if (ids){
      for (var i=0; i<ids.length; i++){
        ids[i] = "'"+ids[i]+"'"
      }
      wheres.push("images.original_id IN (" + ids.toString() + ")")
    }

    if (wheres.length != 0){
      sql = sql + ' AND '
      var wheresString = wheres.join(' AND ')
      sql = sql + wheresString
    }



    // Submit request
    models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
    .then(function (corrections) {
      var listIds = []
      for (var i=0; i<corrections.length; i++){
        listIds.push(corrections[i].corr_id)
      }
      if (listIds.length !== 0){
        models.corrections.update(
          {
            download_timestamp: Sequelize.literal('CURRENT_TIMESTAMP'),
            downloaded: true
          },{
            where:{
             id: {
               [Op.in]: listIds
             }
           }
        }).then(function (){
          return res.json(corrections)
        })
        .catch(function(err){
          return res.json({success: false, msg: 'error'})
        })
      }else{
        return res.json(corrections)
      }
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  })

module.exports = router
