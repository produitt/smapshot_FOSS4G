var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op


var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

router.post('/search/getOwnerLink', function (req, res) {
  var ownerId = req.body.owner_id
  models.owners.findAll({
    attributes: ['link'],
    where: {
      id: ownerId
    },
    raw: true
  }).then(function (owner) {
    return res.json(owner[0])
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getCustomParameters', function (req, res) {
  var ownerUrlName = req.body.ownerUrlName
  models.owners.findOne({
    attributes: ['link', 'id', 'challenge_collection', 'name', 'description'],
    where: {
      url_name: ownerUrlName
    },
    raw: true
  }).then(function (owner) {
    return res.json(owner)
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getCurrent', function (req, res) {
  models.owners.findAll({
    attributes: ['id', 'name', 'link', 'description', 'challenge_collection', 'url_name' ],
    where: {
      is_current: true
    },
    raw: true
  }).then(function (owner) {
    return res.json(owner[0])
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getCollections', function (req, res) {
  var ownerId = req.body.ownerId
  models.collections.findAll({
    attributes: ['name', 'id'],
    where: {
      owner_id: ownerId
    }
  }).then(function (collections) {
    return res.json(collections)
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getOwnersCollections', function (req, res) {
  var listId = req.body.listOwnersId
  models.collections.findAll({
    attributes: ['name', 'id'],
    where: {
      owner_id: {[Op.in]: listId}
    }
  }).then(function (collections) {
    return res.json(collections)
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getList', function (req, res) {
  models.owners.findAll({
    attributes: ['name', 'id'],
    where: {
    }
  }).then(function (owners) {
    return res.json(owners)
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/owners/getStats', function (req, res) {
  var ownerId = req.body.ownerId

  var sql = `select (select count(*) from images where owner_id = :ownerId and georef_3d_bool = true and is_published = true) as nGeoref,
  (select count(*) from images where owner_id = :ownerId  and is_published = true) as nImages,
  (select count(distinct volunteer_id) from images where owner_id = :ownerId  and is_published = true) as nVolunteers`

  models.sequelize.query(sql, {
    replacements: {
      ownerId : ownerId,
    },
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (stats) {
    return res.json(stats)
  }).catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
