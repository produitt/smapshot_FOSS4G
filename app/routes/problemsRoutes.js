var models = require('../model')
var Sequelize = require('sequelize')

var express = require('express')
var router = new express.Router()

router.post('/problems/submit', function (req, res) {
  var volunteerId = req.body.volunteerId
  var imageId = req.body.imageId
  var problem = req.body.problem

  models.problems.create({
    id: Sequelize.literal("nextval('problems_id_seq'::regclass)"),
    image_id: imageId,
    volunteer_id: volunteerId,
    title: problem.title,
    description: problem.description,
    date_problem: Sequelize.literal('current_timestamp')
  }).then(function (result) {
      // return result
      return res.json({success: true, message: 'Problem is saved'})
    })
    .catch(function (result){
      return res.json({success: false, message: 'Problem is not saved'})
    })
})

router.post('/problems/delete', function (req, res) {

  var problemId = req.body.problemId

  sql = 'delete from problems where id = ' + problemId
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.DELETE})
  .then(function (problem) {
    return res.json({success: true, msg: 'problem deleted'})
  })
  .catch(function (result){
    return res.json({success: false, message: 'Problem is not deleted'})
  })
})

router.post('/problems/ownerProcessed', function (req, res) {
  var problemId = req.body.problemId

  models.problems.update({
    owner_processed: true
  },
  {
    where: {
      id: problemId
    }
  }).then(function (result) {
        // return result
        return res.json({success: true, message: 'Problem is processed'})
      }, function (rejectedPromiseError) {
        return res.json({success: false, message: 'Problem is not processed'})
      })
})

router.post('/problems/smapshotProcessed', function (req, res) {
  var problemId = req.body.problemId

  models.problems.update({
    smapshot_processed: true
  },
  {
    where: {
      id: problemId
    }
  }).then(function (result) {
        // return result
        return res.json({success: true, message: 'Problem is processed'})
      }, function (rejectedPromiseError) {
        return res.json({success: false, message: 'Problem is not processed'})
      })
})

router.post('/problems/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select problems.id, problems.date_problem as date, problems.title,
  problems.description, problems.validated, problems.owner_processed,
  problems.smapshot_processed,
  images.id as image_id, images.original_id, images.is_published,
  owners.name as ownername, collections.name as collectionname,
  v1.username as username, is_published,
  v2.username as validator_name
  from problems
  left join volunteers as v1 on problems.volunteer_id = v1.id
  left join volunteers as v2 on problems.validator_id = v2.id
  left join images on problems.image_id = images.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  if (originalId) {
    sql = sql + "WHERE original_id = '" + originalId + "'"
  }else if (imageId) {
    sql = sql + "WHERE images.id = '" + imageId + "'"
  }else{
    var wheres = []
    // Generate where
    // Owner constraint
    if (ownerId) {
      wheres.push('images.owner_id = '+ ownerId)
    }
    // From date constraint
    if (startDate) {
      wheres.push("problems.date_problem >= date('" + startDate + "')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("problems.date_problem <= date('" + stopDate + "')")
    }
    // Collection constraint
    if (collectionId) {
      wheres.push('collection_id = ' + collectionId)
    }
    if (validatorId){
      wheres.push('validations.validator_id = ' + validatorId)
    }
    if ((volunteerUsername != null) && (volunteerUsername != 'All')){
      wheres.push("v1.username ~* '" + volunteerUsername + "'")
    }
    if (processed != null){
      if ((processed === 'false') || (processed === false)) {
        wheres.push('(problems.owner_processed = false or smapshot_processed = false)')
      }else if ((processed === 'true') || (processed === true)) {
        wheres.push('(problems.owner_processed = true and smapshot_processed = true)')
      }
    }
    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++){
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1){
          sql = sql + ' AND'
        }
      }
    }
  }
  sql = sql + ' order by problems.id'
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (problems) {
    return res.json(problems)
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
