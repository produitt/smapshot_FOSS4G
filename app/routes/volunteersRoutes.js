var models = require('../model')

var bcrypt = require('bcrypt')
var generatePassword = require('password-generator')
var nodemailer = require('nodemailer')
var validator = require('email-validator')
var Sequelize = require('sequelize')
var Op = Sequelize.Op

var reCAPTCHA = require('recaptcha2')
var recaptcha = new reCAPTCHA({
  siteKey: '6LdkIwwUAAAAAJg2L0W6Fd6-37CzxCM5_TCFp9VQ',
  secretKey: '6LdkIwwUAAAAANHNGJ1zdcbDSJ8jkHDH4AcvEYJG'
})
var jwt = require('jwt-simple')
var passport = require('passport')
var fs = require('fs')
var tok = require('../authentication/token.js')
var request = require('request')

var express = require('express')
var router = new express.Router()

function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}


// AUTHTENTICATION
// ---------------
router.post('/volunteers/updatePwd', function (req, res) {
  var token = tok.getToken(req.headers)
  if (token) {
    var oldPwd = req.body.oldPwd
    var newPwd1 = req.body.newPwd1
    var newPwd2 = req.body.newPwd2

    if (newPwd2 === newPwd1){
      var decoded = jwt.decode(token, '5map5h0tKey')
      // check if password matches
      bcrypt.compare(req.body.oldPwd, decoded.password, function (err, isMatch) {
        if (err) {
          // winston.log('error', err)
          return
        }
        if (isMatch === true) {
          // Encrypt password
          bcrypt.genSalt(10, function (err, salt) {
            if (err) {
              // winston.log('error', err)
              return
            }
            bcrypt.hash(req.body.newPwd1, salt, function (err, hash) {
              if (err) {
                // winston.log('error', err)
                return
              }
              // Create the user
              models.volunteers.update({
                password: hash
              },{
                where: {
                  id: decoded.id
                }
              }).then(function (volunteer) {
               // Create the user
                models.volunteers.findOne({
                  where: {
                    id: decoded.id
                  }
                }).then(function (volunteer) {
                  // Send token to the client
                  var token = jwt.encode(volunteer, '5map5h0tKey')
                  return res.json({success: true, token: 'JWT ' + token})
                })
                .catch(function (err) {
                  return res.json({success: false, msg: 'cant update.'})
                })
              .catch(function (err) {
                return res.json({success: false, msg: 'cant update.'})
              })
            })
          })
        })
        } else {
          res.json({success: false, msg: 'Authentication failed. Wrong password.'})
        }
      })
    }else{
      return res.json({success: false, msg: 'The password are not the same'})
    }
  }else{
    return res.json({success: false, msg: 'You are not logged'})
  }
})

router.post('/volunteers/submitUsername', function (req, res) {
  var token = tok.getToken(req.headers)
  if (token) {
    var username = req.body.username
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.update({
      username: username
    }, {
      where: {
        id: decoded.id
      }
    }
    ).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      }
      // else {
      //   return res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'})
      // }
      return res.json({success: true, volunteer: volunteer, msg: 'Newsletter is updated'})
    }).catch(function (err) {
      return res.status(403).send({success: false, msg: 'Error while geting the volunteer.'})
    })
  }else{
    return res.json({success: false, msg: 'error'})
  }
})

router.post('/volunteers/submitNewsletters', function (req, res) {
  var token = tok.getToken(req.headers)
  if (token) {
    var newsletter = req.body.newsletters
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.update({
      letter: newsletter
    }, {
      where: {
        id: decoded.id
      }
    }
    ).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      }
      // else {
      //   return res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'})
      // }
      return res.json({success: true, volunteer: volunteer, msg: 'Newsletter is updated'})
    }).catch(function (err) {
      return res.status(403).send({success: false, msg: 'Error while geting the volunteer.'})
    })
  }
  else{
    return res.json({success: false, msg: 'error'})
  }
})

router.post('/volunteers/submitNotifications', function (req, res) {
  var token = tok.getToken(req.headers)
  if (token) {
    var notifications = req.body.notifications
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.update({
      notifications: notifications
    }, {
      where: {
        id: decoded.id
      }
    }
    ).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      }
      // else {
      //   return res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'})
      // }
      return res.json({success: true, volunteer: volunteer, msg: 'Notifications is updated'})
    }).catch(function (err) {
      return res.status(403).send({success: false, msg: 'Error while geting the volunteer.'})
    })
  }
  else{
    return res.json({success: false, msg: 'error'})
  }
})

router.post('/volunteers/submitLang', function (req, res) {
  var token = tok.getToken(req.headers)
  if (token) {
    var lang = req.body.lang
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.update({
      lang: lang
    }, {
      where: {
        id: decoded.id
      }
    }
    ).then(function (volunteer) {
      if (!volunteer) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'})
      }
      // else {
      //   return res.json({success: true, volunteer: volunteer, msg: ' Volunteer is sent'})
      // }
      return res.json({success: true, volunteer: volunteer, msg: 'Lang is updated'})
    }).catch(function (err) {
      return res.status(403).send({success: false, msg: 'Error while geting the volunteer.'})
    })
  }
  else{
    return res.json({success: false, msg: 'error'})
  }
})

router.post('/search/getRanking', function (req, res) {
  // Grab data from http request
  var type = req.body.type
  var data = req.body
  var ownerId = data.ownerId
  var startDate = data.startDate
  var stopDate = data.stopDate
  var collectionId = data.collectionId
  var photographerId = data.photographerId
  console.log('collectionId', collectionId)

  if (type === 'geoloc'){
    // create where clause
    var where = {
      georef_3d_bool: true,
      is_published: true,
      is_validated: true
    }
    // Owner constraint
    if (ownerId) {
      where.owner_id = ownerId
    }
    // From date constraint
    if (startDate) {
      where.date_georef = {
        [Op.gte]: Sequelize.fn('to_date', startDate, 'YYYY-MM-DD')
      }
    }
    // To date constraint
    if (stopDate) {
      if (startDate) {
        where.date_georef[Op.lte] = Sequelize.fn('to_date', stopDate, 'YYYY-MM-DD')
      } else {
        where.date_georef = {
          [Op.lte]: Sequelize.fn('to_date', stopDate, 'YYYY-MM-DD')
        }
      }
    }
    // Collection constraint
    if (collectionId) {
      where.collection_id = collectionId
    }
    // Photographer constraint
    if (photographerId) {
      where.photographer_id = photographerId
    }

    models.images.findAll({
      raw: true,
      attributes: ['volunteer_id',
        [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
        [Sequelize.literal('count("volunteer_id")'), 'nImages']
      ],
      where: where,
      group: 'volunteer_id',
      order: Sequelize.literal('"nImages" DESC')
    }).then(function (Volunteers) {
      var results = parseDB(Volunteers)
      return res.json(results)
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }else {
    if (type === 'obs'){
      var sql = `
      select count(*) as nobs, volunteer_id, username
      from observations, volunteers,
      (select id from images
      `
      var sqlEnd = `
      ) as im
      where validated = true
      and observations.image_id = im.id
      and volunteers.id = volunteer_id
      group by volunteer_id, username
      `
    }
    if (type === 'modif'){
      var sql = `
      select count(*) as ncorr, volunteer_id, username
      from corrections, volunteers,
      (select id from images
      `
      var sqlEnd = `
      ) as im
      where (title_result in ('accepted', 'updated') or caption_result in ('accepted', 'updated'))
      and corrections.image_id = im.id
      and volunteers.id = volunteer_id
      group by volunteer_id, username
      `
    }
    var wheres = []
    if (ownerId) {
      wheres.push('owner_id = ' + ownerId)
    }
    if (startDate) {
      wheres.push("date_georef >= to_date(" + startDate + ", 'YYYY-MM-DD'")
    }
    if (stopDate) {
      wheres.push("date_georef <= to_date(" + stopDate + ", 'YYYY-MM-DD'")
    }
    if (collectionId) {
      wheres.push('collection_id = ' + collectionId)
    }
    if (photographerId) {
      wheres.push('photographer_id = ' + photographerId)
    }
    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++) {
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1) {
          sql = sql + ' AND'
        }
      }
    }
    sql = sql + sqlEnd
    // Submit request
    models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
    .then(function (Volunteers) {
      var results = parseDB(Volunteers)
      return res.json(results)
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.get('/volunteers/validatorsListByOwner', function (req, res) {
  var ownerId = req.ownerId
  if (ownerId){
    where = {
      owner_validator: ownerId
    }
  }else {
    where = {
      owner_validator: {
        [Op.not]: null
      }
    }
  }
  models.volunteers.findAll({
    attributes: ['id', 'username'],
    where: where,
  }).then(function (Volunteers) {
    return res.json(Volunteers)
  })
  .catch(function (err) {
    return res.json({success: false, msg: 'error'})
  })

})

router.post('/volunteers/getEmails', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['owner_admin', 'super_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      if (volunteer.super_admin || volunteer.owner_admin){

        if (volunteer.owner_admin !== null){
          var ownerId = volunteer.owner_admin
        }

        // Grab data from http request
        var collectionId = req.body.collection.id
        var ownerId = req.body.owner.id
        var startDate = req.body.dates.min
        var stopDate = req.body.dates.max
        var geolocalizations = req.body.geolocalizations
        var metadata = req.body.metadata
        var observations = req.body.observations
        if (!geolocalizations && !metadata && !observations){
          geolocalizations = true
          metadata = true
          observations = true
        }

        // Generate query
        var nQuery = 0
        var subQueries = []
        if (geolocalizations) {
          nQuery+=1
          var sqlGeoloc = `
          select distinct username, email
          from volunteers
          left join validations on validations.volunteer_id = volunteers.id
          left join images on validations.image_id = images.id
          left join collections on images.collection_id = collections.id
          left join owners on images.owner_id = owners.id
          `
          var wheres = []
          // Generate where
          // Owner constraint
          if (ownerId) {
            wheres.push('images.owner_id = '+ ownerId)
          }
          // Collection constraint
          if (collectionId) {
            wheres.push('collection_id = ' + collectionId)
          }
          // From date constraint
          if (startDate) {
            wheres.push("validations.date_georef >= to_date('" + startDate + "', 'DD/MM/YYYY')")
          }
          // To date constraint
          if (stopDate) {
            wheres.push("validations.date_georef <= to_date('" + stopDate + "', 'DD/MM/YYYY')")
          }
          if (wheres.length != 0) {
            for (var i = 0; i < wheres.length; i++){
              if (i === 0){
                sqlGeoloc = sqlGeoloc + ' WHERE'
              }
              sqlGeoloc = sqlGeoloc + ' ' + wheres[i]
              if (i !== wheres.length-1){
                sqlGeoloc = sqlGeoloc + ' AND'
              }
            }
          }
          subQueries.push(sqlGeoloc)
        }
        if (observations) {
          nQuery+=1
          var sqlObservations = `
          select distinct username, email
          from volunteers
          left join observations on observations.volunteer_id = volunteers.id
          left join images on observations.image_id = images.id
          left join collections on images.collection_id = collections.id
          left join owners on images.owner_id = owners.id
          `
          var wheres = []
          // Generate where
          // Owner constraint
          if (ownerId) {
            wheres.push('images.owner_id = '+ ownerId)
          }
          // Collection constraint
          if (collectionId) {
            wheres.push('collection_id = ' + collectionId)
          }
          // From date constraint
          if (startDate) {
            wheres.push("observations.date_observation >= to_date('" + startDate + "', 'DD/MM/YYYY')")
          }
          // To date constraint
          if (stopDate) {
            wheres.push("observations.date_observation <= to_date('" + stopDate + "', 'DD/MM/YYYY')")
          }
          if (wheres.length != 0) {
            for (var i = 0; i < wheres.length; i++){
              if (i === 0){
                sqlObservations = sqlObservations + ' WHERE'
              }
              sqlObservations = sqlObservations + ' ' + wheres[i]
              if (i !== wheres.length-1){
                sqlObservations = sqlObservations + ' AND'
              }
            }
          }
          subQueries.push(sqlObservations)
        }
        if (metadata) {
          nQuery+=1
          var sqlMetadata = `
          select distinct username, email
          from volunteers
          left join corrections on corrections.volunteer_id = volunteers.id
          left join images on corrections.image_id = images.id
          left join collections on images.collection_id = collections.id
          left join owners on images.owner_id = owners.id
          `
          var wheres = []
          // Generate where
          // Owner constraint
          if (ownerId) {
            wheres.push('images.owner_id = '+ ownerId)
          }
          // Collection constraint
          if (collectionId) {
            wheres.push('collection_id = ' + collectionId)
          }
          // From date constraint
          if (startDate) {
            wheres.push("corrections.date_correction >= to_date('" + startDate + "', 'DD/MM/YYYY')")
          }
          // To date constraint
          if (stopDate) {
            wheres.push("corrections.date_correction <= to_date('" + stopDate + "', 'DD/MM/YYYY')")
          }
          if (wheres.length != 0) {
            for (var i = 0; i < wheres.length; i++){
              if (i === 0){
                sqlMetadata = sqlMetadata + ' WHERE'
              }
              sqlMetadata = sqlMetadata + ' ' + wheres[i]
              if (i !== wheres.length-1) {
                sqlMetadata = sqlMetadata + ' AND'
              }
            }
          }
          subQueries.push(sqlMetadata)
        }
        var sql = 'select distinct username, email from ('
        for (var i=0; i<subQueries.length; i++){
          sql = sql + subQueries[i]
          if (i !== subQueries.length-1){
            sql = sql + ' UNION'
          }
        }
        sql = sql + ') as foo'
        models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
        .then(function (Volunteers) {
          var results = parseDB(Volunteers)
          return res.json(results)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/volunteers/getByEmail', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['owner_admin', 'super_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      if (volunteer.super_admin || volunteer.owner_admin){
        models.volunteers.findAll({
          attributes: ['id', 'email', 'username', 'is_validator', 'super_admin', 'owner_admin', 'owner_validator'],
          where:{
            email: {
              [Op.like]:'%'+req.body.email+'%'
            }
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else {
        return res.json({success: false, msg: 'error'})
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/volunteers/setSuperAdmin', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['super_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      var volunteerId = req.body.volunteerId
      if (volunteer.super_admin){
        models.volunteers.update({
          is_validator: true,
          super_admin: true
        },{
          where:{
            id: volunteerId
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else {
        return res.json({success: false, msg: 'error'})
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/volunteers/setOwnerAdmin', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['super_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      var volunteerId = req.body.volunteerId
      var ownerId = req.body.ownerId
      if (volunteer.super_admin){
        models.volunteers.update({
          is_validator: true,
          owner_admin: ownerId,
          owner_validator: ownerId
        },{
          where:{
            id: volunteerId
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else {
        return res.json({success: false, msg: 'error'})
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/volunteers/setValidator', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['super_admin', 'owner_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      var volunteerId = req.body.volunteerId
      var ownerId = req.body.ownerId
      if (volunteer.super_admin || volunteer.owner_admin){
        models.volunteers.update({
          is_validator: true,
          owner_validator: ownerId
        },{
          where:{
            id: volunteerId
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else {
        return res.json({success: false, msg: 'error'})
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/volunteers/removeRights', function (req, res) {

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    models.volunteers.findOne({
      attributes: ['super_admin', 'owner_admin'],
      where:{
        id: decoded.id
      }
    }).then(function (volunteer) {
      var volunteerId = req.body.volunteerId
      var ownerId = req.body.ownerId
      if (volunteer.super_admin){
        models.volunteers.update({
          is_validator: false,
          owner_validator: null,
          owner_admin: null,
          super_admin: false
        },{
          where:{
            id: volunteerId
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else if (volunteer.owner_admin){
        // The current user is owner admin he can only update validators of his team
        models.volunteers.update({
          is_validator: false,
          owner_validator: null,
          owner_admin: null
        },{
          where:{
            id: volunteerId,
            owner_validator: volunteer.owner_admin
          }
        }).then(function (volunteer) {
          return res.json(volunteer)
        })
        .catch(function (err) {
          return res.json({success: false, msg: 'error'})
        })
      }
      else {
        return res.json({success: false, msg: 'error'})
      }
    })
    .catch(function (err) {
      return res.json({success: false, msg: 'error'})
    })
  }
})


module.exports = router
