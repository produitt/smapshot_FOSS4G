var path = require('path')

var express = require('express')
var router = new express.Router()

router.get('/ETHZ/', function (req, res) {
  // Send web page and owner home page parameters
  res.sendFile(path.join(__dirname, '../../public/views', 'ownerHome.html'))
})

// Get the customized map
router.get('/ETHZ/map/', function (req, res) {
  res.sendFile(path.join(__dirname, '../../public/views', 'map.html'))
})

// Get the customized globe
router.get('/ETHZ/geolocalization/', function (req, res) {
  res.sendFile(path.join(__dirname, '../../public/views', '3Dgeoref.html'))
})

module.exports = router
