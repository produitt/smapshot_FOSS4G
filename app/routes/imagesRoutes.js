var models = require('../model')
var Sequelize = require('sequelize')
var Op = Sequelize.Op
var GeoJSON = require('geojson')

// For the check of the validator status
var volunteer = require('../controllers/volunteer')
var jwt = require('jwt-simple')
var tok = require('../authentication/token')
var passport = require('passport')

// For full download
var obsCtrl = require('../controllers/observations')
var corrCtrl = require('../controllers/corrections')
var imgCtrl = require('../controllers/images')

var express = require('express')
var router = new express.Router()

// Parse database
function parseDB (Images) {
  var results = {}
  results.table = []

  // Get column names
  var head = []
  var firstRow = Images[0]
  for (var columnName in firstRow) {
    head.push(columnName)
  };
  results.columnName = head

  // Fill the table
  for (var idR in Images) {
    var curRow = Images[idR]
    var r = {}
    for (var idH in head) {
      var cName = head[idH]
      r[cName] = curRow[cName]
    }
    results.table.push(r)
  };
  return results
}

// Query all images
router.post('/map/allImages', function (req, res) {
  var collectionId = req.body.collectionId
  var ownerId = req.body.ownerId
  var geolocalized = req.body.geolocalized
  var filterValues = req.body.filter

  new Promise(function(resolve, reject) {
    if (filterValues) {
      // Get ids according to the filter
      filter(filterValues)
      .then(function(listIds){
        resolve(listIds)
      })
    }else {
      listIds = null
      resolve(listIds)
    }
  }).then(function (listIds) {


    if (geolocalized) {
      var wAttributes = {}
      wAttributes.is_validated = true
      // Geolocalization
      wAttributes.georef_3d_bool = true

      // Collection
      if (collectionId != null) {
        wAttributes.collection_id = collectionId
      }
      // Owner
      if (ownerId != null) {
        wAttributes.owner_id = ownerId
      }
      // Publication
      wAttributes.is_published = true

      if (listIds){
        wAttributes.id = {
          [Op.in]: listIds
        }
      }
      models.images.findAll({
        raw: true,
        attributes: ['azimuth', 'name', 'id', 'georef_3d_bool',
        [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
        [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
        [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
          'title', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id',
          'link', 'caption', 'azimuth', 'tilt', 'roll', 'original_id',
          'observation_enabled', 'correction_enabled', 'orig_date', 'download_link', 'shop_link',
        [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
        [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
        [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
        [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
        [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner']
        ],
        where: wAttributes,
        limit: 1000
      }).then(function (Images) {
        var results = parseDB(Images)
        var geoResult = GeoJSON.parse(results.table, {
          Point: ['latitude', 'longitude'],
          include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title',
            'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
            'username', 'collection', 'photographer', 'owner', 'caption', 'height',
          'azimuth', 'tilt', 'roll', 'original_id', 'download_link', 'shop_link']})
        return res.json(geoResult)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }else{

      var query = `
      select images.id,
      (case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end) as date_shot,
      images.name, images.id,  images.georef_3d_bool, images.title,
      images.license, images.link, images.caption, images.original_id,
      images.observation_enabled, images.correction_enabled, orig_date, download_link, shop_link,
      ST_X(apriori_locations.geom) as longitude, ST_Y(apriori_locations.geom) as latitude,
      collections.name as collection, collections.id as collection_id,
      concat(photographers.first_name,' ',photographers.last_name) as photographer, photographers.id as photographer_id,
      owners.name as owner, owners.id as owner_id
      from images
      RIGHT JOIN apriori_locations ON (images.id = apriori_locations.image_id)
      LEFT JOIN collections ON (collections.id = images.collection_id)
      LEFT JOIN owners ON (owners.id = images.owner_id)
      LEFT JOIN photographers ON (photographers.id = images.photographer_id)
      where images.georef_3d_bool = false
      and images.is_published = true
      `
            // and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)
      // Collection
      if (collectionId != null) {
        query = query + ' AND  images.collection_id = ' + collectionId
      }
      // Owner
      if (ownerId != null) {
        query = query + ' AND  images.owner_id = ' + ownerId
      }
      if (listIds){
        query = query + ' AND  images.id in (' + listIds.toString()+')'
      }else{
        query = query + ' order by random() limit 1000'
      }

      // Right join because i want all the entries of apriori_locations

      models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
        var results = parseDB(images)
        var geoResult = GeoJSON.parse(results.table, {
          Point: ['latitude', 'longitude'],
          include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title',
            'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
            'username', 'collection', 'photographer', 'owner', 'caption', 'height',
          'azimuth', 'tilt', 'roll', 'original_id', 'observation_enabled', 'correction_enabled',
        'download_link', 'shop_link']})
        return res.json(geoResult)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }
  })
})

// Query images in bbox
router.post('/map/noApriori', function (req, res) {
  var collectionId = req.body.collectionId
  var ownerId = req.body.ownerId

  var query = `
  select images.id,
  (case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end) as date_shot,
  images.name, images.id,  images.georef_3d_bool, images.title,
  images.download_link, images.shop_link,
  images.license, images.link, images.caption, images.original_id, orig_date,
  ST_X(apriori_locations.geom) as longitude, ST_Y(apriori_locations.geom) as latitude,
  collections.name as collection, collections.id as collection_id,
  concat(photographers.first_name,' ',photographers.last_name) as photographer, photographers.id as photographer_id,
  owners.name as owner, owners.id as owner_id
  from images
  LEFT JOIN apriori_locations ON (images.id = apriori_locations.image_id)
  LEFT JOIN collections ON (collections.id = images.collection_id)
  LEFT JOIN owners ON (owners.id = images.owner_id)
  LEFT JOIN photographers ON (photographers.id = images.photographer_id)
  where images.georef_3d_bool = false
  and images.is_published = true
  and apriori_locations.image_id is null
  `
  //  and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)
  // Collection
  if (collectionId != null) {
    query = query + ' AND  images.collection_id = ' + collectionId
  }
  // Owner
  if (ownerId != null) {
    query = query + ' AND  images.owner_id = ' + ownerId
  }
  // limit to 30 images sorted at random
  query = query + " order by random()"
  query = query + " limit 30"

  // Right join because i want all the entries of apriori_locations
  models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
    return res.json(images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

var filter = function (searchParams) {
   var promise = new Promise(function(resolve, reject) {
    if (searchParams.georef.georef_3d_bool === true){
      // Create query
      // ------------
      var query = {
        attributes: ['id'],
        where: {
          is_published: true,
          is_validated: true
        }
      }
      // Process keyword
      // -------------------
      if (searchParams.keyword) {
        sql = "(original_id = '" + searchParams.keyword +"' OR lower(title) like lower('% " + searchParams.keyword + "%') OR lower(caption) like lower('%" + searchParams.keyword + "%'))"
        query.where.title = Sequelize.literal(sql)
      }

      // Process collections
      // -------------------
      if (searchParams.listCollectionId.length > 0) {
        query.where.collection_id = {
          [Op.in]: searchParams.listCollectionId
        }
      }

      // Process owners
      // -------------------
      if (searchParams.listOwnersId.length > 0) {
        query.where.owner_id = {
          [Op.in]: searchParams.listOwnersId
        }
      }
      // Process georef
      // --------------
      query.where.georef_3d_bool = true

      // Process dates
      // -------------------
      if (searchParams.dates) {
        var sqlDate = "case when exact_date = true then date_shot <= '" + searchParams.dates.to + "' and date_shot >= '" + searchParams.dates.from + "' else (max_date <= '" + searchParams.dates.to + "' and max_date > '" + searchParams.dates.from + "') or (min_date <= '" + searchParams.dates.to + "' and min_date > '" + searchParams.dates.from + "') end "
        query.where.date_shot = Sequelize.literal(sqlDate)
      }
      // Process roi
      // -----------
      if (searchParams.roi.shapeString != null) {
        if ((searchParams.roi.locationBool === true) && (searchParams.roi.footprintBool === false)) {
          var sqlLocation = "st_intersects(images.location,st_geomfromgeojson('" + searchParams.roi.shapeString + "'))"
        }
        if ((searchParams.roi.locationBool === false) && (searchParams.roi.footprintBool === true)) {
          var sqlLocation = "(st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) AND viewshed_created = TRUE)"
        }
        if ((searchParams.roi.locationBool === true) && (searchParams.roi.footprintBool === true)) {
          var sqlLocation = "(st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) OR st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) AND images.vieshed_create = TRUE)"
        }
        query.where.location = Sequelize.literal(sqlLocation)
      }
      // Process toponyms
      // ----------------
      if (searchParams.toponyms.length > 0) {
        var sqlGeotagArray = ' geotags_array @> ARRAY['// +"]";
        for (var id in searchParams.toponyms) {
          sqlGeotagArray = sqlGeotagArray + "'" + searchParams.toponyms[id] + "',"
        }
            // Suppress last comma
        sqlGeotagArray = sqlGeotagArray.slice(0, -1)
        sqlGeotagArray = sqlGeotagArray + ']'
        query.where.geotags = Sequelize.literal(sqlGeotagArray)
      }
      models.images.findAll(query)
        .then(function (imagesId) {
          var listIds = []
          for (var i=0; i<imagesId.length; i++){
            listIds.push(imagesId[i].id)
          }
          //imagesId = parseDB(imagesId)
          resolve (listIds)
        })
        .catch(function(err){
          return res.json({success: false, msg: 'error'})
        })

    }else{
      var query = `
      select images.id
      from images
      where images.georef_3d_bool = false
      and images.is_published = true
      `
      //       and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)
      // Collection
      if (searchParams.listCollectionId.length > 0) {
        query = query + ' AND  images.collection_id in (' + searchParams.listCollectionId.toString() + ')'
      }

      // Owner
      if (searchParams.listOwnersId.length > 0) {
        query = query + ' AND  images.owner_id in (' + searchParams.listOwnersId.toString() + ')'
      }
      // dates
      if (searchParams.dates) {
        query = query + " AND case when exact_date = true then date_shot <= '" + searchParams.dates.to + "' and date_shot >= '" + searchParams.dates.from + "' else (max_date <= '" + searchParams.dates.to + "' and max_date >= '" + searchParams.dates.from + "') or (min_date <= '" + searchParams.dates.to + "' and min_date > '" + searchParams.dates.from + "') END "
      }

      // keyword
      if (searchParams.keyword) {
        query = query + " AND (images.original_id = '" + searchParams.keyword +"' OR lower(title) like lower('%" + searchParams.keyword + "%') OR lower(caption) like lower('%" + searchParams.keyword + "%')) "
      }
      // Right join because i want all the entries of apriori_locations

      models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (imagesId) {
        var listIds = []
        for (var i=0; i<imagesId.length; i++){
          listIds.push(imagesId[i].id)
        }
        //imagesId = parseDB(imagesId)
        resolve (listIds)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }
  })
  return promise
}
// Query nearest images
router.post('/images/getNearest', function(req, res) {
  // var sqlLocation = 'st_intersects(ST_Buffer(ST_SetSRID(ST_MakePoint(' + req.body.lng + ',' + req.body.lat + '),4326)::geography, 10000), images.location)'
  var collectionId = req.body.collectionId
  var ownerId = req.body.ownerId
  var geolocalized = req.body.geolocalized
  var imageId = req.body.imageId
  var filterValues = req.body.filter

  new Promise(function(resolve, reject) {
    if (filterValues) {
      // Get ids according to the filter
      filter(filterValues)
      .then(function(listIds){
        resolve(listIds)
      })
    }else {
      listIds = null
      resolve(listIds)
    }
  }).then(function (listIds) {
    if (geolocalized) {

      var wAttributes = {}
      wAttributes.is_validated = true
      // Geolocalization
      if (geolocalized === false) {
        wAttributes.georef_3d_bool = false
      } else {
        wAttributes.georef_3d_bool = true
      }
      // Collection
      if (collectionId != null) {
        wAttributes.collection_id = collectionId
      }
      // Owner
      if (ownerId != null) {
        wAttributes.owner_id = ownerId
      }
      // Publication
      wAttributes.is_published = true
      // filter
      if (listIds){
        wAttributes.id = {
          [Op.in]: listIds
        }
      }

      models.images.findAll({
        raw: true,
        attributes: ['azimuth', 'name', 'id', 'georef_3d_bool',
        [Sequelize.literal('ST_Distance(location, (SELECT location FROM images where id ='+imageId+'), false)'), 'distance'],
        [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
        [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
        [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
          'title', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
          'azimuth', 'tilt', 'roll', 'caption', 'original_id', 'download_link', 'shop_link',
          'observation_enabled', 'correction_enabled', 'orig_date',
        [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
        [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
        [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
        [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
        [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner']
        ],
        where: wAttributes,
        limit: 30,
        order: Sequelize.literal('distance ASC')
      }).then(function (Images) {
        return res.json(Images)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })

    } else {
      var query = `
      select
      images.id,
      (case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end) as date_shot,
      images.name, images.id,  images.georef_3d_bool, images.title,
      images.license, images.link, images.caption, images.original_id,
      images.observation_enabled, images.correction_enabled, orig_date,
      images.download_link, images.shop_link,
      collections.name as collection, collections.id as collection_id,
      concat(photographers.first_name,' ',photographers.last_name) as photographer, photographers.id as photographer_id,
      owners.name as owner, owners.id as owner_id,
      ST_X(apriori_locations.geom) as longitude, ST_Y(apriori_locations.geom) as latitude
      from images
      RIGHT JOIN apriori_locations ON (images.id = apriori_locations.image_id)
      LEFT JOIN collections ON (collections.id = images.collection_id)
      LEFT JOIN owners ON (owners.id = images.owner_id)
      LEFT JOIN photographers ON (photographers.id = images.photographer_id)
      where images.id in (
      	select distinct(a2.image_id) from apriori_locations a2
      	where st_intersects(
      		(select ST_Union(ST_Buffer(geom, 1)) as buf from apriori_locations a1
      		where image_id = `+ imageId +`),
      		a2.geom)
      	)
      and images.georef_3d_bool = false
      and images.is_published = true
      `
      //       and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)
      // Collection
      if (collectionId != null) {
        query = query + ' AND  images.collection_id = ' + collectionId
      }
      // Owner
      if (ownerId != null) {
        query = query + ' AND  images.owner_id = ' + ownerId
      }
      if (listIds){
        query = query + ' AND  images.id in (' + listIds.toString()+')'
      }
      query = query + ' limit 30'
      // Right join because i want all the entries of apriori_locations

      models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
        return res.json(images)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }
  })

})
//Get number of not georeferenced images
router.post('/images/numberNotGeoref', function (req, res) {
  var collectionId = req.body.collectionId
  var ownerId = req.body.ownerId
  var wAttributes = {}
  // Collection
  if (collectionId != null) {
    wAttributes.collection_id = collectionId
  }
  // Owner
  if (ownerId != null) {
    wAttributes.owner_id = ownerId
  }
  wAttributes.is_published = true
  wAttributes.georef_3d_bool = false
  models.images.count({
    where: wAttributes
  }).then(function(response) {
    return res.json(response)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

 // Query images in bbox
router.post('/map/imagesInBbox', function (req, res) {

  var collectionId = req.body.collectionId
  var ownerId = req.body.ownerId
  var geolocalized = req.body.geolocalized
  var filterValues = req.body.filter

  new Promise(function(resolve, reject) {
    if (filterValues) {
      // Get ids according to the filter
      filter(filterValues)
      .then(function(listIds){
        resolve(listIds)
      })
    }else {
      listIds = null
      resolve(listIds)
    }
  }).then(function (listIds) {


    if (geolocalized) {
      var wAttributes = {}
      // Geolocalization
      wAttributes.georef_3d_bool = true
      wAttributes.is_published = true
      wAttributes.is_validated = true
      // Collection
      if (collectionId != null) {
        wAttributes.collection_id = collectionId
      }
      // Owner
      if (ownerId != null) {
        wAttributes.owner_id = ownerId
      }
      if (listIds){
        wAttributes.id = {
          [Op.in]: listIds
        }
      }
      // Publication
      wAttributes.is_published = true
      // Location
      var sql = 'location && ST_MakeEnvelope('.concat(+req.body.bbox.left).concat(',').concat(req.body.bbox.bottom).concat(',').concat(req.body.bbox.right).concat(',').concat(req.body.bbox.top).concat(', 4326)')
      wAttributes.location = Sequelize.literal(sql)

      models.images.findAll({
        raw: true,
        attributes: ['azimuth', 'name', 'id', 'georef_3d_bool',
        [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
        [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
        [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
          'title', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
          'azimuth', 'tilt', 'roll', 'caption', 'original_id', 'download_link', 'shop_link',
          'observation_enabled', 'correction_enabled', 'orig_date',
        [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
        [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
        [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
        [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
        [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner']
        ],
        where: wAttributes//,
        // limit: 100
      }).then(function (Images) {
        var results = parseDB(Images)
        var geoResult = GeoJSON.parse(results.table, {
          Point: ['latitude', 'longitude'],
          include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title',
          'owner_id', 'volunteer_id', 'photographer_id', 'license',
          'collection_id', 'link', 'username', 'collection', 'photographer', 'owner',
          'azimuth', 'tilt', 'roll', 'caption', 'height', 'original_id',
          'observation_enabled', 'correction_enabled', 'download_link', 'shop_link']})
        return res.json(geoResult)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })

    } else {
      var query = `
      select images.id,
      (case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end) as date_shot,
      images.name, images.id,  images.georef_3d_bool, images.title,
      images.license, images.link, images.caption, images.original_id,
      images.observation_enabled, images.correction_enabled, orig_date,
      download_link, shop_link,
      ST_X(apriori_locations.geom) as longitude, ST_Y(apriori_locations.geom) as latitude,
      collections.name as collection, collections.id as collection_id,
      concat(photographers.first_name,' ',photographers.last_name) as photographer, photographers.id as photographer_id,
      owners.name as owner, owners.id as owner_id
      from images
      RIGHT JOIN apriori_locations ON (images.id = apriori_locations.image_id)
      LEFT JOIN collections ON (collections.id = images.collection_id)
      LEFT JOIN owners ON (owners.id = images.owner_id)
      LEFT JOIN photographers ON (photographers.id = images.photographer_id)
      where images.georef_3d_bool = false
      and images.is_published = true
      `
      //       and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)
      // Collection
      if (collectionId != null) {
        query = query + ' AND  images.collection_id = ' + collectionId
      }
      // Owner
      if (ownerId != null) {
        query = query + ' AND  images.owner_id = ' + ownerId
      }

      var within = 'apriori_locations.geom && ST_MakeEnvelope('.concat(+req.body.bbox.left).concat(',').concat(req.body.bbox.bottom).concat(',').concat(req.body.bbox.right).concat(',').concat(req.body.bbox.top).concat(', 4326)')
      query = query + ' AND ' + within
      if (listIds){
        query = query + ' AND  images.id in (' + listIds.toString()+')'
      }
      // query = query + ' order by random() limit 100'
      // Right join because i want all the entries of apriori_locations

      models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
        var results = parseDB(images)
        var geoResult = GeoJSON.parse(results.table, {
          Point: ['latitude', 'longitude'],
          include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title',
            'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
            'username', 'collection', 'photographer', 'owner', 'caption', 'height',
          'azimuth', 'tilt', 'roll', 'original_id', 'observation_enabled', 'correction_enabled',
        'download_link', 'shop_link']})
        return res.json(geoResult)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }

  })
})

  router.post('/search/getMyImages', function (req, res) {
  var volunteerId = req.body.volunteerId

  var query = `select images.id, images.georef_3d_bool, images.name, title,
  images.original_id,
  collection_id, validations.date_georef, images.owner_id, is_validated, images.download_link, images.shop_link,
  validations.validated, rejected, improved, collections.name as collectionName,
  owners.name as ownerName, errors_list, remark, other_desc
  from validations
  LEFT JOIN images ON (images.volunteer_id = validations.volunteer_id and images.id = validations.image_id)
  LEFT JOIN collections ON (collections.id = images.collection_id)
  LEFT JOIN owners ON (owners.id = images.owner_id)
  where images.volunteer_id = ` + volunteerId

  models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
    var results = parseDB(images)
    return res.json(results)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

// Get images which arent validated  for mysMapShot
router.post('/search/getToValidateImages', function (req, res) {
  var ownerId = req.body.ownerId
  var where = {
    georef_3d_bool: true,
    is_validated: false,
    is_published: true
  }
  if (ownerId !== null) {
    where.owner_id = ownerId
  }
  models.images.findAll({
    raw: true,
    attributes: ['id', 'name', 'title', 'collection_id', 'date_georef', 'in_game',
    'owner_id', 'score_volunteer', 'surface_ratio_volunteer', 'volunteer_id',
    'original_id',
    [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collectionName'],
    [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
    [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'ownerName']
    ],
    where: where,
    order: Sequelize.literal('date_georef ASC')
  }).then(function (images) {
    var results = parseDB(images)
    return res.json(results)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getVolunteersImages', function (req, res) {
  models.images.findAll({
    raw: true,
    attributes: ['volunteer_id',
      [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
      [Sequelize.literal('count("volunteer_id")'), 'nImages']
    ],
    where: {
      georef_3d_bool: true,
      is_published: true
    },
    group: 'volunteer_id',
    order: Sequelize.literal('"nImages" DESC')
  }).then(function (Volunteers) {
    var results = parseDB(Volunteers)
    return res.json(results)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getImagesFromCollection', function (req, res) {
  var collectionId = req.body.collectionId
  // var georef = req.body.georef

  if (collectionId != null) {
    models.images.findAll({
      raw: true,
      attributes: ['id', 'title', 'link', 'collection_id',
        [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
        [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude']
      ],
      where: {
        collection_id: collectionId,
        georef_3d_bool: false,
        is_published: true
      }
    }).then(function (Images) {
      var results = parseDB(Images)
      return res.json(results)
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  } else {
    models.images.findAll({
      raw: true,
      attributes: ['id', 'title', 'link', 'collection_id',
        [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
        [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude']
      ],
      where: {
        georef_3d_bool: false,
        is_published: true
      }
    }).then(function (Images) {
      var results = parseDB(Images)
      return res.json(results)
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/images/unlock', function (req, res) {
  var imageId = req.body.imageId
  models.images.update({
    last_start: null},
    {
      where:{
        id: imageId
      }
    }
  ).then(function (image) {
    return res.json(image)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})
router.post('/images/lock', function (req, res) {
  var imageId = req.body.imageId
  models.images.update({
    last_start: Sequelize.literal('current_timestamp')},
    {
      where:{
        id: imageId
      }
    }
  ).then(function (image) {
    return res.json(image)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getImage', function (req, res) {
  var imageId = req.body.imageId
  models.images.findAll({
    raw: true,
    attributes: ['azimuth', 'tilt', 'roll', 'focal', 'px', 'py',
      'name', 'id', 'georef_3d_bool', 'gcp_json', 'georef_map_bool', 'georef_azi_bool', 'id',
      'title', 'caption', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
      'geoloc_rate', 'image_rate', 'apriori_height', 'view_type', 'original_id',
      'observation_enabled', 'correction_enabled', 'download_link', 'shop_link', 'orig_date',
      'is_published',
     [Sequelize.literal('(DATE_PART(\'day\', now()::timestamp - last_start::timestamp) * 24 + DATE_PART(\'hour\', now()::timestamp - last_start::timestamp)) * 60 + DATE_PART(\'minute\', now()::timestamp - last_start::timestamp)'), 'delta_last_start'],
     [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
     [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
     [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
     [Sequelize.fn('ST_AsGeoJson', Sequelize.col('footprint'), 5, 2), 'footprint'],
     [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
     [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
     [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
     [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
     [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner'],
     [Sequelize.literal('(SELECT "owners"."url_name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'url_name']
    ],
    where: {
      id: imageId
    }
  }).then(function (Images) {
    return res.json(Images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getImagesForSlider', function (req, res) {
    // Get images in the neighborood order by distance
    // A raw query is used because a bug occurs with the usual way.
  var collectionId = req.body.collectionId
  var listImageId = req.body.listImageId
  // var imageId = req.body.imageId
  // var visitId = req.body.visitId
  var latitude = req.body.latitude
  var longitude = req.body.longitude

  var whereQuery = '"images"."georef_3d_bool" = true AND "images"."is_published" = true'

  if (listImageId != null) {
    var strList = '('
    for (var id in listImageId) {
      strList = strList + listImageId[id] + ','
    }
    strList = strList.slice(0, strList.length - 1) + ')'
    whereQuery = whereQuery + ' AND id IN ' + strList
  } else if (collectionId != null) {
    whereQuery = whereQuery + ' AND collection_id=' + collectionId
  }
  var q = 'SELECT "id", ST_Distance("location", ST_SetSRID(ST_MakePoint(:longitude,:latitude),4326)) AS "dist" FROM "images" AS "images" WHERE'
  q = q + whereQuery
  q = q + ' ORDER BY "dist" ASC LIMIT 50;'

    // var q = 'select id from images'
  models.sequelize.query(q, {
    replacements: {
      latitude: parseFloat(latitude),
      longitude: parseFloat(longitude)
    },
    type: models.sequelize.QueryTypes.SELECT
  }).then(function (images) {
    return res.json(images)// parseDB(res)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getImagesForMono', function (req, res) {
  var collectionId = req.body.collectionId
  var listImageId = req.body.listImageId
  // var imageId = req.body.imageId
  var visitId = req.body.visitId

  if (visitId != null) {
    var q = 'SELECT ST_X(location) as longitude, ' +
      'ST_Y(location) as latitude, ' +
      'ST_Z(location) as height, ' +
      'azimuth, tilt, roll, px, py, focal, name, id, ' +
      'georef_3d_bool, georef_map_bool, georef_azi_bool, title, owner_id, ' +
      'photographer_id, license, collection_id, link, volunteer_id, link, ' +
      'observation_enabled, correction_enabled, orig_date,' +
      "case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end as date_shot, " +
      '(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id") as username, ' +
      '(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id") as collection, ' +
      '(SELECT concat("photographers"."first_name", \' \'  ,"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id") as photographer, ' +
      '(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id") as owner,' +
      'ST_AsGeoJson(footprint, 5, 2) as footprint, ' +
      'geoloc_rate, image_rate ' +
      'FROM images, (select id_image, rank from visit_image where id_visit = ' + visitId + ' order by rank ASC) as visit ' +
      'where images.id = visit.id_image and georef_3d_bool = true and is_published = true ' +
      'order by visit.rank'
    models.sequelize.query(q, {
      type: models.sequelize.QueryTypes.SELECT
    }).then(function (Images) {
      var results = parseDB(Images)
      var geoResult = GeoJSON.parse(results.table,
        {Point: ['latitude', 'longitude'],
          include: ['height', 'azimuth', 'tilt', 'roll', 'px', 'py', 'focal', 'date_shot',
            'name', 'id', 'georef_3d_bool', 'georef_map_bool', 'georef_azi_bool', 'title', 'caption',
            'owner_id', 'photographer_id', 'date_shot', 'orig_date', 'license', 'collection_id', 'link', 'volunteer_id',
            'username', 'collection', 'photographer', 'owner', 'link', 'footprint', 'geoloc_rate', 'image_rate',
            'observation_enabled', 'correction_enabled'
          ]})
      return res.json(geoResult)
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  } else {
    if (listImageId != null) {
      var where = {
        id: {
          [Op.in]: listImageId
        },
        georef_3d_bool: true,
        is_published: true
      }
    } else if (collectionId != null) {
      var where = {
        collection_id: collectionId,
        georef_3d_bool: true,
        is_published: true
      }
    } else {
      var where = {
        georef_3d_bool: true,
        is_published: true
      }
    }
    models.images.findAll({
      raw: true,
      attributes: [
          [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
          [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
          [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
        'azimuth', 'tilt', 'roll', 'px', 'py', 'focal', 'name', 'id',
        'georef_3d_bool', 'georef_map_bool', 'georef_azi_bool', 'title', 'owner_id',
        'photographer_id', 'license', 'collection_id', 'link', 'volunteer_id', 'link',
        'observation_enabled', 'correction_enabled', 'orig_date',
          [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
          [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
          [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
          [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
          [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner'],
          [Sequelize.fn('ST_AsGeoJson', Sequelize.col('footprint'), 5, 2), 'footprint'],
        'geoloc_rate', 'image_rate'
      ],
      where: where
    }).then(function (Images) {
      var results = parseDB(Images)
      var geoResult = GeoJSON.parse(results.table,
        {Point: ['latitude', 'longitude'],
          include: ['height', 'azimuth', 'tilt', 'roll', 'px', 'py', 'focal',
            'name', 'id', 'georef_3d_bool', 'georef_map_bool', 'georef_azi_bool', 'title', 'caption',
            'owner_id', 'photographer_id', 'date_shot', 'orig_date', 'license', 'collection_id', 'link', 'volunteer_id',
            'username', 'collection', 'photographer', 'owner', 'link', 'footprint', 'geoloc_rate', 'image_rate',
            'observation_enabled', 'correction_enabled'
          ]})
      return res.json(geoResult)
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
})

router.post('/search/getNotGeolocatedBbox', function (req, res) {
  var sqlLocation = 'location && ST_MakeEnvelope('.concat(+req.body.left).concat(',').concat(req.body.bottom).concat(',').concat(req.body.right).concat(',').concat(req.body.top).concat(', 4326)')

  models.images.findAll({
    raw: true,
    attributes: [
         [Sequelize.literal('count("id")'), 'nImages']
    ],
    where: {
      georef_3d_bool: false,
      is_published: true,
      location: Sequelize.literal(sqlLocation)
    },
    limit: 1
  }).then(function (result) {
    var results = parseDB(result)
    return res.json(results)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getNotGeolocatedNearby', function (req, res) {
  if (req.body.left) {
    var sqlLocation = 'location && ST_MakeEnvelope('.concat(+req.body.left).concat(',').concat(req.body.bottom).concat(',').concat(req.body.right).concat(',').concat(req.body.top).concat(', 4326)')
  } else {
    var sqlLocation = 'st_intersects(ST_Buffer(ST_SetSRID(ST_MakePoint(' + req.body.lng + ',' + req.body.lat + '),4326)::geography, 10000), images.location)'
  }
  models.images.findAll({
    raw: true,
    attributes: [
         [Sequelize.literal('count("id")'), 'nImages']
    ],
    where: {
      georef_3d_bool: false,
      location: Sequelize.literal(sqlLocation),
      is_published: true
    },
    limit: 1
  }).then(function (result) {
    var results = parseDB(result)
    return res.json(results)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getFootprint', function (req, res) {
  var imageId = req.body.imageId
  models.images.findAll({
    attributes: [[
      Sequelize.literal('CASE WHEN viewshed IS NULL THEN ST_AsGeoJson(footprint,5,2) ELSE ST_AsGeoJson(viewshed,5,2) END'), 'footprint'
      // Sequelize.fn('ST_AsGeoJson', Sequelize.col('footprint'), 5, 2), 'footprint'
    ]],
    where: {
      id: imageId
    },
    raw: true
  }).then(function (image) {
    return res.json(image[0])
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/submitForm', function (req, res) {
  // Get searchParameters
  var searchParams = req.body

  if (searchParams.georef.georef_3d_bool === true){
    // Create query
    // ------------
    var query = {
      raw: true,
      attributes: ['azimuth', 'name', 'id', 'georef_3d_bool', 'original_id',
          [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
          [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
          [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
        'title', 'caption', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
        'observation_enabled', 'correction_enabled', 'orig_date', 'tilt', 'roll',
          [Sequelize.literal("case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end"), 'date_shot'],
          [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
          [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collection'],
          [Sequelize.literal('(SELECT concat("photographers"."first_name",' + "' '" + ',"photographers"."last_name") FROM "photographers" WHERE "photographers"."id" = "images"."photographer_id")'), 'photographer'],
          [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'owner']
      ],
          // raw: true,
          // attributes: ['azimuth', 'date_shot', 'name', 'id', 'georef_3d_bool',[Sequelize.fn('ST_X', Sequelize.col('location')),'longitude'], [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'], 'title', 'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link'],
      where: {
        is_published: true,
        is_validated: true
      }
    }
    // Process keyword
    // -------------------
    if (searchParams.keyword) {
      sql = "(original_id = '" + searchParams.keyword +"' OR lower(title) like lower('%" + searchParams.keyword + "%') OR lower(caption) like lower('%" + searchParams.keyword + "%'))"
      query.where.title = Sequelize.literal(sql)
    }

    // Process collections
    // -------------------
    if (searchParams.listCollectionId.length > 0) {
      query.where.collection_id = {
        [Op.in]: searchParams.listCollectionId
      }
    }

    // Process owners
    // -------------------
    if (searchParams.listOwnersId.length > 0) {
      query.where.owner_id = {
        [Op.in]: searchParams.listOwnersId
      }
    }
    // Process georef
    // --------------
    query.where.georef_3d_bool = true
    // if (searchParams.georef.georef_3d_bool != null) {
    //   if (searchParams.georef.georef_3d_bool === true) {
    //
    //   }
    //   if (searchParams.georef.georef_3d_bool === false) {
    //     query.where.georef_3d_bool = false
    //   }
    // }

    // Process dates
    // -------------------
    if (searchParams.dates) {
      var sqlDate = "case when exact_date = true then date_shot <= '" + searchParams.dates.to + "' and date_shot >= '" + searchParams.dates.from + "' else (max_date <= '" + searchParams.dates.to + "' and max_date > '" + searchParams.dates.from + "') or (min_date <= '" + searchParams.dates.to + "' and min_date > '" + searchParams.dates.from + "') end"
      query.where.date_shot = Sequelize.literal(sqlDate)
    }
    // Process roi
    // -----------
    if (searchParams.roi.shapeString != null) {
      if ((searchParams.roi.locationBool === true) && (searchParams.roi.footprintBool === false)) {
        var sqlLocation = "st_intersects(images.location,st_geomfromgeojson('" + searchParams.roi.shapeString + "'))"
      }
      if ((searchParams.roi.locationBool === false) && (searchParams.roi.footprintBool === true)) {
        var sqlLocation = "(st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) AND viewshed_created = TRUE)"
      }
      if ((searchParams.roi.locationBool === true) && (searchParams.roi.footprintBool === true)) {
        var sqlLocation = "(st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) OR st_intersects(images.viewshed,st_geomfromgeojson('" + searchParams.roi.shapeString + "')) AND images.vieshed_create = TRUE)"
      }
      query.where.location = Sequelize.literal(sqlLocation)
    }
    // Process toponyms
    // ----------------
    if (searchParams.toponyms.length > 0) {
      var sqlGeotagArray = 'geotags_array @> ARRAY['// +"]";
      for (var id in searchParams.toponyms) {
        sqlGeotagArray = sqlGeotagArray + "'" + searchParams.toponyms[id] + "',"
      }
          // Suppress last comma
      sqlGeotagArray = sqlGeotagArray.slice(0, -1)
      sqlGeotagArray = sqlGeotagArray + ']'
      query.where.geotags = Sequelize.literal(sqlGeotagArray)
    }
    models.images.findAll(query)
        .then(function (images) {
          var results = parseDB(images)
          var geoResult = GeoJSON.parse(results.table, {
            Point: ['latitude', 'longitude'],
            include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title', 'caption',
              'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
              'username', 'collection', 'photographer', 'owner', 'original_id',
              'observation_enabled', 'correction_enabled', 'height', 'tilt', 'roll']})
          return res.json(geoResult)
        })
        .catch(function(err){
          return res.json({success: false, msg: 'error'})
        })

  }else{
    var query = `
    select images.id,
    (case when exact_date = true THEN date(date_shot)::TEXT else concat(EXTRACT(YEAR FROM (min_date)), ' - ', EXTRACT(YEAR FROM (max_date))) end) as date_shot,
    images.name, images.id,  images.georef_3d_bool, images.title,
    images.license, images.link, images.caption, images.original_id,
    images.observation_enabled, images.correction_enabled, orig_date,
    download_link, shop_link,
    ST_X(apriori_locations.geom) as longitude, ST_Y(apriori_locations.geom) as latitude,
    collections.name as collection, collections.id as collection_id,
    concat(photographers.first_name,' ',photographers.last_name) as photographer, photographers.id as photographer_id,
    owners.name as owner, owners.id as owner_id
    from images
    RIGHT JOIN apriori_locations ON (images.id = apriori_locations.image_id)
    LEFT JOIN collections ON (collections.id = images.collection_id)
    LEFT JOIN owners ON (owners.id = images.owner_id)
    LEFT JOIN photographers ON (photographers.id = images.photographer_id)
    where images.georef_3d_bool = false
    and images.is_published = true
    `
    // and (DATE_PART('minutes', now()::timestamp - last_start::timestamp) is null or DATE_PART('minutes', now()::timestamp - last_start::timestamp) >= 240)

    // Collection
    if (searchParams.listCollectionId.length > 0) {
      query = query + ' AND  images.collection_id in (' + searchParams.listCollectionId.toString() + ')'
    }

    // Owner
    if (searchParams.listOwnersId.length > 0) {
      query = query + ' AND  images.owner_id in (' + searchParams.listOwnersId.toString() + ')'
    }
    // dates
    if (searchParams.dates) {
      query = query + " AND case when exact_date = true then date_shot <= '" + searchParams.dates.to + "' and date_shot >= '" + searchParams.dates.from + "' else (max_date <= '" + searchParams.dates.to + "' and max_date >= '" + searchParams.dates.from + "') or (min_date <= '" + searchParams.dates.to + "' and min_date > '" + searchParams.dates.from + "') END"
    }

    // keyword
    if (searchParams.keyword) {
      query = query + " AND (images.original_id = '" + searchParams.keyword +"' OR lower(title) like lower('%" + searchParams.keyword + "%') OR lower(caption) like lower('%" + searchParams.keyword + "%'))"
    }
    // Right join because i want all the entries of apriori_locations

    models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (images) {
      var results = parseDB(images)
      var geoResult = GeoJSON.parse(results.table, {
        Point: ['latitude', 'longitude'],
        include: ['azimuth', 'date_shot', 'orig_date', 'name', 'id', 'georef_3d_bool', 'title',
          'owner_id', 'volunteer_id', 'photographer_id', 'license', 'collection_id', 'link',
          'username', 'collection', 'photographer', 'owner', 'caption', 'height',
        'azimuth', 'tilt', 'roll', 'original_id', 'observation_enabled', 'correction_enabled',
      'download_link', 'shop_link']})
      return res.json(geoResult)
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }

})

// Get image info from the database
router.get('/georef/readImage/:idpicture/', function (req, res, next) {
  models.images.findAll({
    attributes: ['name', [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'],
    [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
    [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'],
      'gcp_json', 'azimuth', 'tilt', 'roll', 'focal', 'px', 'py', 'georef_3d_bool',
      'georef_map_bool', 'georef_azi_bool', 'id', 'apriori_height', 'view_type',
    [Sequelize.fn('ST_AsGeoJson', Sequelize.col('footprint'), 5, 2), 'footprint'],
      'title', 'owner_id', 'photographer_id', 'date_shot', 'orig_date', 'license', 'collection_id',
      'link', 'volunteer_id', 'caption', 'original_id', 'download_link', 'shop_link'],
    where: {
      id: req.params.idpicture
    },
    raw: true
  }).then(function (image) {
    return res.json(image[0])
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/game/randomImage/', function (req, res, next) {
  var request = {
    attributes: ['name', [Sequelize.fn('ST_X', Sequelize.col('location')), 'longitude'], [Sequelize.fn('ST_Y', Sequelize.col('location')), 'latitude'],
    [Sequelize.fn('ST_Z', Sequelize.col('location')), 'height'], 'gcp_json',
    'azimuth', 'tilt', 'roll', 'focal', 'px', 'py', 'georef_3d_bool', 'id',
    'title', 'owner_id', 'photographer_id', 'date_shot', 'license',
    'collection_id', 'link', 'volunteer_id'],
    // order: 'random()',
    limit: 1,
    raw: true
  }

  if (req.body.imageId != null) {
      // Image is provided get image info
    request.where = {
      id: req.body.imageId
    }
  } else if (req.body.collectionId != null) {
    // Collection is provided, get random image in the collection
    request.where = {
      georef_3d_bool: true,
      is_published: true,
      in_game: true,
      collection_id: req.body.collectionId
    }
    request.order = Sequelize.literal('random()')
  } else {
    // Get a random image
    request.where = {
      georef_3d_bool: true,
      in_game: true,
      is_published: true
    }
    request.order = Sequelize.literal('random()')
  }

  models.images.findAll(request).then(function (image) {
    return res.json(image[0])
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/images/incrementViews/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var type = data.type

  // Create query
  models.images_views.create(
    {
      id: Sequelize.literal("nextval('images_views_id_seq'::regclass)"),
      date: Sequelize.literal('current_timestamp'),
      type: type,
      image_id: imageId
    }
  )
  .then(function (result) {
      // return result
      return res.json({success: true, message: 'images_views is incremented'})
    }
  )
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/images/incrementDownloads/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var imageId = data.imageId
  var type = data.type

  // Create query
  models.images_downloads.create(
    {
      id: Sequelize.literal("nextval('images_views_id_seq'::regclass)"),
      date: Sequelize.literal('current_timestamp'),
      type: type,
      image_id: imageId
    }
  )
  .then(function (result) {
      // return result
      return res.json({success: true, message: 'images_downloads is incremented'})
    }
  )
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/search/getRatio/', function (req, res) {
  // Grab data from http request
  var data = req.body
  var ownerId = data.ownerId
  var startDate = data.startDate
  var stopDate = data.stopDate
  var collectionId = data.collectionId
  var photographerId = data.photographerId

  // Generate where
  var where = {
    is_published: 'true'
  }
  // Owner constraint
  if (ownerId) {
    where.owner_id = ownerId
  }
  // From date constraint
  if (startDate) {
    where.date_georef = {
      gt: startDate
    }
  }
  // To date constraint
  if (stopDate) {
    where.date_georef = {
      lt: stopDate
    }
  }
  // Collection constraint
  if (collectionId) {
    where.collection_id = collectionId
  }
  // Photographer constraint
  if (photographerId) {
    where.photographer_id = photographerId
  }

  // Create empty result
  var result = {
    nGeoref: null,
    nNotGeoref: null,
    ratioNotGeoref: null,
    ratioGeoref: null
  }
  where.georef_3d_bool = 'true'
  // Count geolocalized images
  models.images.count(
    {
      where: where
    })
  .then(function (count) {
    // store the number of images
    result.nGeoref = count
    where.georef_3d_bool = 'false'
    // count not geolocalized images
    models.images.count(
      {
        where: where
      }).then(function (count) {
        result.nNotGeoref = count
        result.ratioNotGeoref = count / (count + result.nGeoref)
        result.ratioGeoref = 1 - result.ratioNotGeoref
        return res.json(result)
      }).catch(function (err) {
        return res.json({success: false, msg: 'error'})
      })
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})


router.post('/images/getDashboard/', function (req, res) {
  // Grab data from http request
  var originalId = req.body.originalId
  var imageId = req.body.Id
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var validatorId = req.body.validator.id
  var volunteerUsername = req.body.volunteer.username
  var processed = req.body.processed
  var ownerId = req.body.owner.id

  // Generate query
  var sql = `
  select * from (
  select distinct images.id, original_id, images.georef_3d_bool, images.title,
  images.caption, v1.username, is_published, images.date_georef,
  images.n_gcp_volunteer, v1.lang,
  validator_id,  v2.username as validator_name,
  owners.name as ownername, collections.name as collectionname
  from images
  left join volunteers as v1 on images.volunteer_id = v1.id
  left join volunteers as v2 on images.validator_id = v2.id
  left join collections on images.collection_id = collections.id
  left join owners on images.owner_id = owners.id
  `
  if (originalId) {
    sql = sql + "WHERE original_id = '" + originalId + "'"
  }else if (imageId){
    sql = sql + "WHERE images.id = '" + imageId + "'"
  }else{
    var wheres = []
    // Generate where
    // Owner constraint
    if (ownerId) {
      wheres.push('images.owner_id = '+ ownerId)
    }
    // From date constraint
    if (startDate) {
      wheres.push("date_georef >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("date_georef <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
    // Collection constraint
    if (collectionId) {
      wheres.push('collection_id =' + collectionId)
    }
    if (validatorId) {
      wheres.push('corrections.validator_id = ' + validatorId)
    }
    if ((volunteerUsername != null) && (volunteerUsername != 'All')) {
      wheres.push("v1.username ~* '" + volunteerUsername + "'")
    }

    // Concatenate where clauses
    if (wheres.length != 0) {
      for (var i = 0; i < wheres.length; i++){
        if (i === 0){
          sql = sql + ' WHERE'
        }
        sql = sql + ' ' + wheres[i]
        if (i !== wheres.length-1){
          sql = sql + ' AND'
        }
      }
    }
  }
  // Add views counter
  sql = sql + `
  ) as ims,
  (select images.id, count(*) from images left join images_views on images.id = images_views.image_id
  group by images.id) as counter
  where ims.id = counter.id
  `
  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/images/publish/', passport.authenticate('jwt', {session: false}), function (req, res) {
  // Grab data from http request
  var imageId = req.body.imageId

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function (){
      models.images.update(
        {
          is_published: true
        },
        {
          where: {id: imageId}
        })
      .then(function () {
        var message = {
          success: true,
          msg: 'Image is published'
        }
        return res.json(message)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
  else {
    var message = {
      success: false,
      msg: 'User is not logged'
    }
    return res.json(message)
  }
})

router.post('/images/unpublish/', passport.authenticate('jwt', {session: false}), function (req, res) {
  // Grab data from http request
  var imageId = req.body.imageId

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function (){
      models.images.update(
        {
          is_published: false
        },
        {
          where: {id: imageId}
        })
      .then(function () {
        var message = {
          success: true,
          msg: 'Image is unpublished'
        }
        return res.json(message)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
  else {
    var message = {
      success: false,
      msg: 'User is not logged'
    }
    return res.json(message)
  }
})

router.post('/images/publishList/', passport.authenticate('jwt', {session: false}), function (req, res) {
  // Grab data from http request
  var imageIds = req.body.imageIds

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function (){
      models.images.update(
        {
          is_published: true
        },
        {
          where: {
            id: {
              [Op.in]: imageIds
            }
          }
        })
      .then(function () {
        var message = {
          success: true,
          msg: 'Images are published'
        }
        return res.json(message)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
  else {
    var message = {
      success: false,
      msg: 'User is not logged'
    }
    return res.json(message)
  }
})

router.post('/images/unpublishList/', passport.authenticate('jwt', {session: false}), function (req, res) {
  // Grab data from http request
  var imageIds = req.body.imageIds

  var token = tok.getToken(req.headers)
  if (token) {
    var decoded = jwt.decode(token, '5map5h0tKey')
    // Check that the volunteer is validator_id
    volunteer.checkValidator(decoded.id)
    .then(function (){
      models.images.update(
        {
          is_published: false
        },
        {
          where: {
            id: {
              [Op.in]: imageIds
            }
          }
        })
      .then(function () {
        var message = {
          success: true,
          msg: 'Image is unpublished'
        }
        return res.json(message)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    })
    .catch(function(err){
      return res.json({success: false, msg: 'error'})
    })
  }
  else {
    var message = {
      success: false,
      msg: 'User is not logged'
    }
    return res.json(message)
  }
})

router.post('/images/fullDownload/', function (req, res) {
  // Grab data from http request
  var collectionId = req.body.collection.id
  // var startDate = req.body.dates.min
  // var stopDate = req.body.dates.max
  // var timeType = req.body.dates.type
  var ids = req.body.ids
  // var download = req.body.download
  var format = req.body.format
  var ownerId = req.body.owner.id

  var titleSql = corrCtrl.generateFullDownloadSqlTitle()
  var captionSql = corrCtrl.generateFullDownloadSqlCaption()
  var observationSql = obsCtrl.generateFullDownloadSql()
  var imagesSql = imgCtrl.generateFullDownloadSql(format)

  var sql = `
  select images.id, original_id,
  concat('https://smapshot.heig-vd.ch/map/?imageId=',images.id) as address,
  is_published, georef_3d_bool as is_georeferenced,
  imgData.*, titleData.*, captionData.*, observationData.*
  from images
  `
  sql = sql + 'left outer join('+imagesSql+') as imgData on imgData.image_id = images.id '
  sql = sql + 'left outer join('+titleSql+') as titleData on titleData.image_id = images.id  '
  sql = sql + 'left outer join('+captionSql+') as captionData on captionData.image_id = images.id  '
  sql = sql + 'left outer join('+observationSql+') as observationData on observationData.image_id = images.id  '
  sql = sql + ' WHERE '

  // Generate where
  var wheres = []

  // owner contstraint
  if (ownerId){
    wheres.push('images.owner_id = ' + ownerId)
  }
  // Collection constraint
  if (collectionId) {
    wheres.push('images.collection_id = ' + collectionId)
  }
  if (ids){
    for (var i=0; i<ids.length; i++){
      ids[i] = "'"+ids[i]+"'"
    }
    wheres.push("images.original_id IN (" + ids.toString() + ")")
  }

  if (wheres.length != 0){
    var wheresString = wheres.join(' AND ')
    sql = sql + wheresString
  }
  console.log('sql', sql)

  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/images/getForDownload/', function (req, res) {
  // Grab data from http request
  var collectionId = req.body.collection.id
  var startDate = req.body.dates.min
  var stopDate = req.body.dates.max
  var timeType = req.body.dates.type
  var ids = req.body.ids
  var download = req.body.download
  var format = req.body.format
  var ownerId = req.body.owner.id

  // Generate query
  if (format === 'geojson'){
    var sql = `
    select images.id as image_id, original_id,
    round(focal/greatest(width, height),2) as focal_ratio,
    round(azimuth%360,2) as azimuth, round(tilt,2) as tilt,
    CASE
    WHEN roll%360 > 180 THEN round(roll%360-360,2)
    ELSE round(roll%360,2)
    END as roll,
    round(st_y(location)::numeric,6) as latitude, round(st_x(location)::numeric,6) as longitude, round(st_z(location)::numeric, 0) as altitude,
    replace(ST_AsGeoJson(location), '"', '''') as point,
    replace(ST_AsGeoJson(viewshed), '"', '''') as footprint,
    replace(geotags_array::TEXT, '"', '') as geotags
    from images
    where geotag_created  = true
    `
  } else if (format === 'kml') {
    var sql = `
    select images.id as image_id, original_id,
    round(focal/greatest(width, height),2) as focal_ratio,
    round(azimuth%360,2) as azimuth, round(tilt,2) as tilt,
    CASE
    WHEN roll%360 > 180 THEN round(roll%360-360,2)
    ELSE round(roll%360,2)
    END as roll,
    round(st_y(location)::numeric,6) as latitude, round(st_x(location)::numeric,6) as longitude, round(st_z(location)::numeric, 0) as altitude,
    ST_AsKml(location) as point,
    ST_AsKml(viewshed) as footprint,
    replace(geotags_array::TEXT, '"', '') as geotags
    from images
    where geotag_created  = true
    `
  }else{
    var sql = `
    select images.id as image_id, original_id,
    round(focal/greatest(width, height),2) as focal_ratio,
    round(azimuth%360,2) as azimuth, round(tilt,2) as tilt,
    CASE
    WHEN roll%360 > 180 THEN round(roll%360-360,2)
    ELSE round(roll%360,2)
    END as roll,
    round(st_y(location)::numeric,6) as latitude, round(st_x(location)::numeric,6) as longitude, round(st_z(location)::numeric, 0) as altitude,
    st_astext(location) as point,
    st_astext(viewshed) as footprint,
    replace(geotags_array::TEXT, '"', '') as geotags
    from images
    where geotag_created  = true
    `
  }
  // Generate where
  var wheres = []

  if (timeType === 'download'){
    // From date constraint
    if (startDate) {
      wheres.push("images.download_timestamp  >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("images.download_timestamp  <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
  } else if (timeType === 'creation'){
    // From date constraint
    if (startDate) {
      wheres.push("images.validation_date >= to_date('" + startDate + "','DD/MM/YYYY')")
    }
    // To date constraint
    if (stopDate) {
      wheres.push("images.validation_date <= to_date('" + stopDate + "','DD/MM/YYYY')")
    }
  }
  // owner contstraint
  if (ownerId){
    wheres.push('images.owner_id = ' + ownerId)
  }
  // Collection constraint
  if (collectionId) {
    wheres.push('images.collection_id = ' + collectionId)
  }
  if (download == 'false'){
    wheres.push("images.downloaded = False")
  }else if (download == 'true'){
    wheres.push("images.downloaded = true")
  }
  if (ids){
    for (var i=0; i<ids.length; i++){
      ids[i] = "'"+ids[i]+"'"
    }
    wheres.push("images.original_id IN (" + ids.toString() + ")")
  }

  if (wheres.length != 0){
    sql = sql + ' AND '
    var wheresString = wheres.join(' AND ')
    sql = sql + wheresString
  }



  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    var listIds = []
    for (var i=0; i<images.length; i++){
      listIds.push(images[i].id)
    }
    if (listIds.length !== 0){
      models.images.update(
        {
          download_timestamp: Sequelize.literal('CURRENT_TIMESTAMP'),
          downloaded: true
        },{
          where:{
           id: {
             [Op.in]: listIds
           }
         }
      }).then(function (){
        return res.json(images)
      })
      .catch(function(err){
        return res.json({success: false, msg: 'error'})
      })
    }
    else{
      return res.json(images)
    }

  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

router.post('/images/getSmapshotAddresses/', function (req, res) {
  // Grab data from http request
  var collectionId = req.body.collection.id
  var ownerId = req.body.owner.id
  var ids = req.body.ids

  // Generate query
  var sql = `
  select concat('https://smapshot.heig-vd.ch/map/?imageId=',images.id) as address,
  original_id, is_published
  from images
  `
  var wheres = []
  // Owner constraint
  if (ownerId) {
    wheres.push('images.owner_id = ' + ownerId)
  }
  // Collection constraint
  if (collectionId) {
    wheres.push('images.collection_id = ' + collectionId)
  }
  if (ids){
    for (var i=0; i<ids.length; i++){
      ids[i] = "'"+ids[i]+"'"
    }
    wheres.push("images.original_id IN (" + ids.toString() + ")")
  }
  if (wheres.length !== 0){
    var wString = wheres.join(' AND ')
    sql = sql + ' where ' + wString
  }

  // Submit request
  models.sequelize.query(sql, {type: Sequelize.QueryTypes.SELECT})
  .then(function (images) {
    return res.json(images)
  })
  .catch(function(err){
    return res.json({success: false, msg: 'error'})
  })
})

module.exports = router
