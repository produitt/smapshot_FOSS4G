"use strict";
//Settings for the stories
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Validations = sequelize.define('validations', {
    // id: { // Create an identifier which is automatically incremented
    //   type: Sequelize.INTEGER,
    //   primaryKey: true,
    //   autoIncrement: true // Automatically gets converted to SERIAL for postgres
    // },
    image_id: { // Reference image
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    volunteer_id: {// Reference volunteer
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    validator_id: {// Reference volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    errors_list: { //
      type: Sequelize.ARRAY(Sequelize.INTEGER),
      allowNull: true
    },
    remark: { //
      type: Sequelize.TEXT,
      allowNull: true
    },
    seen: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    date_validation: { // Date creation
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    date_seen: { // Date viewing
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    other: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    other_desc: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    validated: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    improved: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    rejected: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    date_georef: { // Date viewing
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    start: {
      type: Sequelize.TIME,
      allowNull: true
    },
    stop: {
      type: Sequelize.TIME,
      allowNull: true
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return Validations
};
