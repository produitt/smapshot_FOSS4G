"use strict";
//Settings for the forum table
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Corrections = sequelize.define('corrections', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: { // id of the volunteer
      type: Sequelize.TEXT,
      allowNull: false
    },
    date_shot: { // modification of the image date
      type: Sequelize.TEXT,
      allowNull: true
    },
    caption: { // modification ot the image caption
      type: Sequelize.TEXT,
      allowNull: true
    },
    title: { // modification of the image title
      type: Sequelize.TEXT,
      allowNull: true
    },
    date_correction: { // date of the submission
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    title_processed: { // if the title is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    caption_processed: { // if the caption is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    title_result: { // 'accepted', 'rejected', 'updated'
      type: Sequelize.TEXT,
      allowNull: true
    },
    caption_result: { // 'accepted', 'rejected', 'updated'
      type: Sequelize.TEXT,
      allowNull: true
    },
    caption_update: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    title_update: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    title_remark: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    caption_remark: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    validator_id: { // references volunteers.id
      type: Sequelize.INTEGER,
      allowNull: true
    },
    downloaded: { // if the caption is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    download_timestamp: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    },
    title_validation_date: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    },
    caption_validation_date: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return Corrections
}
