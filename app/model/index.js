'use strict'
// Connection to the database settings
var fs = require('fs')
var path = require('path')
var Sequelize = require('sequelize')
var env = process.env.NODE_ENV || 'development' //
if (env === 'development') {
  var dbConn = 'postgres://postgres:postgres@localhost:5432/dbName' // for development local DB
  var logBool = true
} else if (env === 'production') {
  var dbConn = 'postgres://postgres:YouProductionPassword@localhost:5432/dbName' // for production
  var logBool = false
} else if (env === 'staging') {
  var dbConn = 'postgres://postgres:YouProductionPassword@localhost:5432/dbName'
  var logBool = false
}

var sequelize = new Sequelize(dbConn, {
  define: {
    timestamps: false, // Must be false, else it want to fill the fields createdAt, updatedAt which doesnt exists
  },
  logging: logBool,
  timezone: 'Europe/Zurich'
})

var db = {}
// read the files within the folder and add the tables
fs
  .readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js')
  })
  .forEach(function (file) {
    var model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(function (modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
