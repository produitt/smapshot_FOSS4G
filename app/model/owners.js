'use strict'
// Settings for the image owners
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var Owners = sequelize.define('owners', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true
    },
    name: {
      type: Sequelize.STRING
    },
    link: {
      type: Sequelize.STRING
    },
    // bg_image: {
    //   type: Sequelize.STRING
    // },
    // description: {
    //   type: Sequelize.STRING
    // },
    url_name: {
      type: Sequelize.STRING
    },
    challenge_collection: {
      type: Sequelize.INTEGER
    },
    is_current: {
      type: Sequelize.BOOLEAN
    }
  },
    {
      freezeTableName: true // Model tableName will be the same as the model name
    })
  return Owners
}
