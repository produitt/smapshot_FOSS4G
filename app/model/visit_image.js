"use strict";
//Settings for the forum table
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Visit_image = sequelize.define('news', {
    id_visit: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      allowNull: false
    },
    id_image: { //tags
      type: Sequelize.INTEGER,
      allowNull: false
    },
    text_fr: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_en: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_it: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_de: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    rank:{
      type: Sequelize.INTEGER,
      allowNull: false
    }
  },
  {
    freezeTableName: true
  });
  return Visit_image;
};
