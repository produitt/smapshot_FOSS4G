"use strict";
//Settings for the photographers
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Photographers = sequelize.define('photographers', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true,
    },
    first_name: {
      type: Sequelize.STRING,
    },
    last_name: {
      type: Sequelize.STRING,
    },
    link: {
      type: Sequelize.STRING,
    },
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return  Photographers;
};
