//Setting for the collection table
"use strict";
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Collections = sequelize.define('collections', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true,
    },
    name: {
      type: Sequelize.STRING,
    },
    owner: {
      type: Sequelize.STRING,
    },
    link: {
      type: Sequelize.STRING,
    },
    date_publi: {
      type: Sequelize.DATEONLY,
    },
    is_challenge : { //Collection is in highlight
      type: Sequelize.BOOLEAN
    },
    short_descr : {//Collection description
      type: Sequelize.TEXT
    },
    long_descr : {//Collection description
      type: Sequelize.TEXT
    }

  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return  Collections;
};
