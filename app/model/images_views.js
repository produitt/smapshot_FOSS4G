"use strict";
// This table record each view of the image (osd or globe)
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var ImagesViews = sequelize.define('images_views', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    date: { // date of visualisation
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    type: { // 2D or 3D
      type: Sequelize.TEXT,
      allowNull: false
    }
  },
    {
      freezeTableName: true
    })
  return ImagesViews
}
