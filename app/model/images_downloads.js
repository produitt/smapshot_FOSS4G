"use strict";
// This table record each view of the image (osd or globe)
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var ImagesDownloads = sequelize.define('images_downloads', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    date: { // date of visualisation
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return ImagesDownloads
}
