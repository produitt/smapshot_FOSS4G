"use strict";
//Settings for the toponyms
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Toponyms = sequelize.define('toponyms', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false
    },
    geom :{
      type: 'geometry(Point,4326,2)',
      allowNull: false
    },
    //Not suposed to be used (toponyms are no more created in V1.0)
    date_in :{
      type: Sequelize.DATE,
    },
    volunteer_id :{
      type: Sequelize.INTEGER,
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return  Toponyms;
};
