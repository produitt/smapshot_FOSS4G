"use strict";
//Settings for the forum table
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Forum = sequelize.define('forum', {
    id: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { //Which image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    title: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text: { //tags
      type: Sequelize.TEXT,
      allowNull: false
    },
    timestamp:{
      type: Sequelize.DATE,
      allowNull: false
    }
  },
  {
    freezeTableName: true
  });
  return Forum;
};
