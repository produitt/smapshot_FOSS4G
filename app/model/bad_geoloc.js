"use strict";
// Settings for the forum table
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var badGeoloc = sequelize.define('bad_geoloc', {
    // id: { // Create an identifier which is automatically incremented
    //   type: Sequelize.INTEGER,
    //   primaryKey: true,
    //   autoIncrement: true // Automatically gets converted to SERIAL for postgres
    // },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    // volunteer_id: { // id of the volunteer
    //   type: Sequelize.INTEGER,
    //   allowNull: false
    // },
    validator_id: { // id of the validator
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdat: {
      type: Sequelize.TIME,
      allowNull: true
    },
    updatedat: {
      type: Sequelize.TIME,
      allowNull: true
    },
    processed: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    counter: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    result:{
      type: Sequelize.TEXT,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return badGeoloc
}
