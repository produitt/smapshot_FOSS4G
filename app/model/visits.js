"use strict";
//Settings for the forum table
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Visits = sequelize.define('visits', {
    id: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    title_fr: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    title_en: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    title_it: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    title_de: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_fr: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_en: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_it: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    text_de: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    timestamp:{
      type: Sequelize.DATE,
      allowNull: false
    },
    author:{
      type: Sequelize.TEXT,
      allowNull: true
    },
    theme:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },
  {
    freezeTableName: true
  });
  return Visits;
};
