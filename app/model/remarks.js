"use strict";
// Settings for the forum table
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var Remarks = sequelize.define('remarks', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: { // id of the volunteer
      type: Sequelize.TEXT,
      allowNull: false
    },
    date_remark: { // modification of the image date
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    remark: { // modification ot the image caption
      type: Sequelize.TEXT,
      allowNull: false
    },
    processed: { // if the remark is answerd
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    reply: {
      type: Sequelize.TEXT,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return Remarks
}
