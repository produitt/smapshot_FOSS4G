"use strict";
//Settings for the stories
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Stories = sequelize.define('stories', {
    id: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { //Reference image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: {//Reference volunteer
      type: Sequelize.INTEGER,
      allowNull: false
    },
    title: { //
      type: Sequelize.TEXT,
      allowNull: false
    },
    text: { //
      type: Sequelize.TEXT,
      allowNull: false
    },
    location:{
      type: 'geometry(Point,4326,3)',
      allowNull: false
    }
    ,
    timestamp:{ //Date creation
      type: Sequelize.DATE,
      allowNull: false
    }
    ,
    date_story:{ //Date story
      type: Sequelize.DATE,
      allowNull: false
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return Stories;
};
