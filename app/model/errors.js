"use strict";
//Settings for the stories
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Errors = sequelize.define('errors', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    title: { // title in english
      type: Sequelize.TEXT,
      allowNull: false
    },
    translations: {// Reference volunteer
      type: Sequelize.TEXT,
      allowNull: false
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  })
  return Errors
};
