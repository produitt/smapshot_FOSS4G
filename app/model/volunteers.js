"use strict";
//Settings for the volunteers
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Volunteers = sequelize.define('volunteers', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      // allowNull: false,
      autoIncrement: true,
      unique: true
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        isEmail: true
      },
      unique: true
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    username: {
      type: Sequelize.STRING,
      unique: false
    },
    googleid: { // Provided by Goolge
      type: Sequelize.STRING,
      unique: true
    },
    facebookid: {// Provided by Facebook
      type: Sequelize.STRING,
      unique: true
    },
    letter: { // If they want the newsletter
      type: Sequelize.BOOLEAN
    },
    nimages: { // Number of images (not in use)
      type: Sequelize.INTEGER
    },
    roi: { // Region of interest (not in use)
      type: 'geometry(Multipolygon,4326,3)', // Z
      allowNull: true
    },
    is_validator: { // can modify and validate geolocation
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    date_registr: {
      type: Sequelize.DATE
    },
    notifications: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    has_one_validated: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    lang: {
      type: Sequelize.STRING,
      allownNull: true
    },
    super_admin: { // can modify and validate geolocation
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    owner_admin: { // can modify and validate geolocation
      type: Sequelize.INTEGER,
      allowNull: true
    },
    owner_validator: { // can modify and validate geolocation
      type: Sequelize.INTEGER,
      allowNull: true
    }
  },
    {
      freezeTableName: true // Model tableName will be the same as the model name
    })
  return Volunteers
}
