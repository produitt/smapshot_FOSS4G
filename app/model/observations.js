"use strict";
// Settings for the forum table
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var Observations = sequelize.define('observations', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: { // id of the volunteer
      type: Sequelize.INTEGER,
      allowNull: false
    },
    validator_id: { // id of the volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    date_observation: { // date of submission
      type: Sequelize.DATE,
      allowNull: false
    },
    observation: { // modification ot the image caption
      type: Sequelize.TEXT,
      allowNull: false
    },
    remark: { // modification ot the image caption
      type: Sequelize.TEXT,
      allowNull: true
    },
    coord_x: { // image coordinate of the observation
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    coord_y: { // image coordinate of the observation
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    validated: { // validation
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    processed: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    height: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    width: {
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    downloaded: { // if the caption is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    download_timestamp: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    },
    validation_date: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return Observations
}
