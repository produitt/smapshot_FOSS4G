"use strict";
//Settings for the image table
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Images = sequelize.define('images', {
    id: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    photographer_id: { //Name of the photographer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    owner_id: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    collection_id: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    volunteer_id: { //Volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    validator_id: { //Volunteer who validate the image
      type: Sequelize.INTEGER,
      allowNull: true
    },
    name: { //Name of the image
      type: Sequelize.STRING,
      allowNull: false
    },
    date_in: { //date of insertion
      type: Sequelize.DATE,
      allowNull: true
    },
    date_shot: { //Date of the photography
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    orig_date: { //Date of the photography in text format provided by the owner
      type: Sequelize.TEXT,
      allowNull: true
    },
    date_georef: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    tags: { //tags
      type: Sequelize.TEXT,
      allowNull: true
    },
    title: { //Image title
      type: Sequelize.TEXT,
      allowNull: true
    },
    caption: { //original identifier which links the metadata with the image file
      type: Sequelize.TEXT,
      allowNull: true
    },
    orig_title: { //Image title
      type: Sequelize.TEXT,
      allowNull: true
    },
    orig_caption: { //original identifier which links the metadata with the image file
      type: Sequelize.TEXT,
      allowNull: true
    },
    link: { //image link
      type: Sequelize.TEXT,
      allowNull: true
    },
    license: { //which license
      type: Sequelize.TEXT,
      allowNull: true
    },
    toponyms: { //toponyms
      type: Sequelize.TEXT,
      allowNull: true
    },
    gcp_json: { //Stores GCP as json
      type: Sequelize.TEXT,
      allowNull: true
    },
    azimuth: { //azimuth
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    tilt: { //tilt
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    roll: { //roll
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    focal: { //focal
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    px: { //Principal point
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    py: { //Principal point
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    georef_3d_bool: { //is georeferenced
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    georef_map_bool: { //is georeferenced on the map
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    georef_azi_bool: { //has a provided azimuth
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    in_game: { //has a provided azimuth
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    is_validated: { //has a provided azimuth
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    validation_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    view_type : { //type of view (terrestrial, nadir, low_oblique, high_hoblique)
      type: Sequelize.TEXT,
      allowNull: true
    },
    apriori_height : { //Apriori height above ground?
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    location_apriori:{ //Stores the image location computed from the place name
      type: 'geometry(Point,4326,3)',
      allowNull: true
    },
    location:{
      type: 'geometry(Point,4326,3)',
      allowNull: true
    },
    footprint:{
      type: 'geometry(Polygon,4326,3)',//Z
      allowNull: true
    },
    geotags_array:{ //Only one of those is used to strore the place name and search
      type: 'text[]'
    },
    geotags_jsonb:{
      type: 'jsonb'
    },
    geoloc_rate_array:{
      type: 'integer[]'
    },
    image_rate_array:{
      type: 'integer[]'
    },
    geoloc_rate:{
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    image_rate:{
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    is_published: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    exact_date: { // date is exact or is an interval
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    min_date: { // if interval: minimum date
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    max_date: { // if interval: maximum date
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    original_id: { // identifier provided by the owner
      type: Sequelize.TEXT,
      allowNull: true
    },
    link_id: { // original identifier which links the metadata with the image file
      type: Sequelize.TEXT,
      allowNull: true
    },
    location_volunteer: { // location computed by the volunteer
      type: 'geometry(Point,4326,3)',
      allowNull: true
    },
    score_validator: { // gcp_score validator
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    score_volunteer: { // gcp_score volunteer
      type: Sequelize.DOUBLE,
      allowNull: true
    },
    surface_ratio_volunteer: { // surface_score validator
      type: Sequelize.INTEGER,
      allowNull: true
    },
    surface_ratio_validator: { // surface_score volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    n_gcp_validator: { // surface_score volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    n_gcp_volunteer: { // surface_score volunteer
      type: Sequelize.INTEGER,
      allowNull: true
    },
    download_link: { // link for the downlaod
      type: Sequelize.TEXT,
      allowNull: true
    },
    shop_link: { // link for the shop
      type: Sequelize.TEXT,
      allowNull: true
    },
    has_download_link: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    categories: { // surface_score volunteer
      type: 'text[]'
    },
    observation_enabled: { // surface_score volunteer
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    correction_enabled: { // surface_score volunteer
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    downloaded: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    download_timestamp: {
      type: Sequelize.DATE,
      allowNull: true
    },
    viewshed:{
      type: 'geometry(Multipolygon,4326,2)',//Z
      allowNull: true
    },
    viewshed_precise:{
      type: 'geometry(Multipolygon,4326,2)',//Z
      allowNull: true
    },
    viewshed_created: { // if the caption is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    geotag_created: { // if the caption is checked
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    geotag_timestamp: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    },
    viewshed_timestamp: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    },
    geotags_json: { // link for the downlaod
      type: Sequelize.TEXT,
      allowNull: true
    },
    last_start: { // date of the submission
      type: Sequelize.DATE,
      allowNull: true
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return Images;
};
