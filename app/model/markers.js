"use strict";
//Settings for the marker table (not used in V1.0)
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  var Markers = sequelize.define('markers', {
    id: { //Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    text: { 
      type: Sequelize.TEXT,
      allowNull: true
    },
    location:{
      type: 'geometry(Point,4326,3)',
      allowNull: false
    }
    ,
    date_in:{
      type: Sequelize.DATE,
      allowNull: false
    }
    ,
    date_marker:{
      type: Sequelize.DATE,
      allowNull: false
    }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  });
  return Markers;
};
