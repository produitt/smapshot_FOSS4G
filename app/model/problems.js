"use strict";
// Settings for the forum table
var Sequelize = require('sequelize')
module.exports = function (sequelize, DataTypes) {
  var Problems = sequelize.define('problems', {
    id: { // Create an identifier which is automatically incremented
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    image_id: { // id of the image
      type: Sequelize.INTEGER,
      allowNull: false
    },
    volunteer_id: { // id of the volunteer
      type: Sequelize.TEXT,
      allowNull: false
    },
    validator_id: { // id of the validator
      type: Sequelize.TEXT,
      allowNull: true
    },
    date_problem: { // date of submission
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    title: { // validation
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: { // validation
      type: Sequelize.TEXT,
      allowNull: true
    },
    validated: { // validation
      type: Sequelize.BOOLEAN,
      allowNull: true
    }
    ,
    owner_processed: { // validation
      type: Sequelize.BOOLEAN,
      allowNull: true
    }
    ,
    smapshot_processed: { // validation
      type: Sequelize.BOOLEAN,
      allowNull: true
    }
  },
    {
      freezeTableName: true
    })
  return Problems
}
