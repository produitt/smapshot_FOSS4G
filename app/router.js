var fs = require('fs')
var models = require('./model')
var Sequelize = require('sequelize')

var express = require('express')
var path = require('path')
var router = new express.Router()

var clientIp = require('client-ip');

// Home page
router.get('/', function (req, res) {
  try {
    var ip = clientIp(req)
  } catch (error) {
    var ip = null
  }

  global.logger.log('info', 'HOME', {
    ip: ip,
    user_agent: req.headers['user-agent'],
    referer: req.headers.referer
  })

  res.setHeader('Content-Type', 'text/html')
  res.sendFile(path.join(__dirname, '../public/views', 'home.html'))
})

// home page
router.get('/home/', function (req, res) {
  try {
    var ip = clientIp(req)
  } catch (error) {
    var ip = null
  }

  global.logger.log('info', 'HOME', {
    ip: ip,
    user_agent: req.headers['user-agent'],
    referer: req.headers.referer
  })
  res.sendFile(path.join(__dirname, '../public/views', 'home.html'))
})

// about page
router.get('/about/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'about.html'))
})

// contact page
router.get('/contact/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'contact.html'))
})

// login page
router.get('/login/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'login.html'))
})

// help page
router.get('/help/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'help.html'))
})

// monoplotting page
router.get('/monoplotting/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'monoplotting.html'))
})

// mysmapshot
router.get('/mysmapshot/', function (req, res) {
  try {
    var ip = clientIp(req)
  } catch (error) {
    var ip = null
  }

  global.logger.log('info', 'MY', {
    ip: ip,
    user_agent: req.headers['user-agent'],
    referer: req.headers.referer,
  })
  res.sendFile(path.join(__dirname, '../public/views', 'mysmapshot.html'))
})

// dashboard
router.get('/dashboard/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/views', 'dashboard.html'))
})

// map page
router.get('/map/:ownerId', function (req, res) {
  try {
    var ip = clientIp(req)
  } catch (error) {
    var ip = null
  }

  if (req.query.imageId){
    global.logger.log('info', 'MAP OWNER IMAGE', {
      ip: ip,
      user_agent: req.headers['user-agent'],
      referer: req.headers.referer,
      image_id: req.query.imageId
    })
  }else{
    global.logger.log('info', 'MAP OWNER', {
      ip: ip,
      user_agent: req.headers['user-agent'],
      referer: req.headers.referer,
      owner_id: req.params.ownerId
    })
  }

  res.sendFile(path.join(__dirname, '../public/views', 'map.html'))
})
// map page
router.get('/map/', function (req, res) {
  if (req.query.imageId){

    var userAgent = req.headers['user-agent']
    if (userAgent.startsWith('facebookexternalhit/1.1') || userAgent === 'Facebot') {
        // Read html monplotter template for the bots
      fs.readFile('public/views/imageOpenGraph.html', 'utf8', function (err, htmlFile) {
        if (err) {
          console.log('error reading georefOpenGraph.html')
          res.json({msg: 'error', success: false})
        } else {
          models.images.findAll({
            attributes: [
              'volunteer_id', 'georef_3d_bool', 'collection_id',
                [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
                [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collectionname'],
                [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'collectionowner']
            ],
            where: {
              id: req.query.imageId// req.params.idpicture
            },
            raw: true
          }).then(function (image) {
            if (image[0].georef_3d_bool){
              var og = {
                title: "Discover archives images in 3D in the time machine smapshot",
                description: '' + image[0].username + ' helped ' + image[0].collectionowner + ' to recover the location of this image. You can also contribute to the smapshot time machine.',
                imageUrl: 'https://smapshot.heig-vd.ch/data/collections/'+image[0].collection_id+'/images/500/' + req.query.imageId + '.jpg',
                link: 'https://smapshot.heig-vd.ch/map/?imageId=' + req.query.imageId,
                sitename: 'https://smapshot.heig-vd.ch/'
              }
              // var newHtml = htmlFile.replace('{{og.title}}', og.title)
              var newHtml = htmlFile.replace(/{{og.title}}/g, og.title)
              var newHtml = newHtml.replace(/{{og.description}}/g, og.description)
              var newHtml = newHtml.replace(/{{og.imageUrl}}/g, og.imageUrl)
              var newHtml = newHtml.replace('{{og.sitename}}', og.sitename)
              var newHtml = newHtml.replace('{{og.link}}', og.link)
              res.send(newHtml)
            }else{
              var og = {
                title: "Discover archives images in 3D in the time machine smapshot",
                description: 'Help ' + image[0].collectionowner + ' to recover the location of this image.',
                imageUrl: 'https://smapshot.heig-vd.ch/data/collections/'+image[0].collection_id+'/images/500/' + req.query.imageId + '.jpg',
                link: 'https://smapshot.heig-vd.ch/map/?imageId=' + req.query.imageId,
                sitename: 'https://smapshot.heig-vd.ch/'
              }
              //var newHtml = htmlFile.replace('{{og.title}}', og.title)
              var newHtml = htmlFile.replace(/{{og.title}}/g, og.title)
              var newHtml = newHtml.replace(/{{og.description}}/g, og.description)
              var newHtml = newHtml.replace(/{{og.imageUrl}}/g, og.imageUrl)
              var newHtml = newHtml.replace('{{og.sitename}}', og.sitename)
              var newHtml = newHtml.replace('{{og.link}}', og.link)
              res.send(newHtml)
            }
          })
        }
      })
    }else{
      try {
        var ip = clientIp(req)
      } catch (error) {
        var ip = null
      }
      global.logger.log('info', 'MAP IMAGE', {
        ip: ip,
        user_agent: req.headers['user-agent'],
        referer: req.headers.referer,
        image_id: req.query.imageId
      })
      res.sendFile(path.join(__dirname, '../public/views', 'map.html'))
    }
  }else{
    try {
      var ip = clientIp(req)
    } catch (error) {
      var ip = null
    }
    global.logger.log('info', 'MAP', {
      ip: ip,
      user_agent: req.headers['user-agent'],
      referer: req.headers.referer
    })
    res.sendFile(path.join(__dirname, '../public/views', 'map.html'))
  }
})

// georeferencing page
router.get('/georef/', function (req, res) {
  // var params = req.body
  // var image_id = req.params.idpicture
  var userAgent = req.headers['user-agent']
  if (userAgent.startsWith('facebookexternalhit/1.1') || userAgent === 'Facebot') {
      // Read html monplotter template for the bots
    fs.readFile('public/views/georefOpenGraph.html', 'utf8', function (err, htmlFile) {
      if (err) {
        console.log('error reading georefOpenGraph.html')
      } else {
        models.images.findAll({
          attributes: [
            'volunteer_id',
              [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
              [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collectionname'],
              [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'collectionowner']
          ],
          where: {
            id: req.query.imageId// req.params.idpicture
          },
          raw: true
        }).then(function (image) {
          var og = {
            title: image[0].username + ' a trouvé le lieu de cette image sur smapshot',
            description: '"' + image[0].username + ' a aidé ' + image[0].collectionowner + ' à retrouver le lieu de cette image. Entraine-toi avec le jeu et viens nous aider à reconstruire un globe virtuel du temps passé."',
            imageUrl: 'https://smapshot.heig-vd.ch/images/thumbnails/' + req.query.imageId + '.jpg',
            link: 'https://smapshot.heig-vd.ch/map/?imageId=' + req.query.imageId,
            sitename: 'https://smapshot.heig-vd.ch/'
          }
          var newHtml = htmlFile.replace('{{og.title}}', og.title)
          var newHtml = newHtml.replace(/{{og.description}}/g, og.description)
          var newHtml = newHtml.replace(/{{og.imageUrl}}/g, og.imageUrl)
          var newHtml = newHtml.replace('{{og.sitename}}', og.sitename)
          var newHtml = newHtml.replace('{{og.link}}', og.link)

          res.send(newHtml)
            // return res.json(image[0]);
        })
      }
    })
  } else {
    try {
      var ip = clientIp(req)
    } catch (error) {
      var ip = null
    }

    global.logger.log('info', 'GEOREFERENCER', {
      ip: ip,
      user_agent: req.headers['user-agent'],
      referer: req.headers.referer,
      image_id: req.query.imageId
    })
    res.sendFile(path.join(__dirname, '../public/views', 'geolocalisation.html'))//, req.body
  }
})

// Game page
router.get('/game/', function (req, res) {
  var userAgent = req.headers['user-agent']

  if (userAgent.startsWith('facebookexternalhit/1.1') || userAgent === 'Facebot') {
      // Read html monplotter template for the bots
    fs.readFile('public/views/gameOpenGraph.html', 'utf8', function (err, htmlFile) {
      if (err) {
        console.log('error reading gameOpenGraph.html')
      } else {
        models.images.findAll({
          attributes: [
            'volunteer_id',
              [Sequelize.literal('(SELECT "volunteers"."username" FROM "volunteers" WHERE "volunteers"."id" = "images"."volunteer_id")'), 'username'],
              [Sequelize.literal('(SELECT "collections"."name" FROM "collections" WHERE "collections"."id" = "images"."collection_id")'), 'collectionname'],
              [Sequelize.literal('(SELECT "owners"."name" FROM "owners" WHERE "owners"."id" = "images"."owner_id")'), 'collectionowner']
          ],
          where: {
            id: req.query.imageId// req.params.idpicture
          },
          raw: true
        }).then(function (image) {
          var og = {
            title: 'Tu reconnais cet endroit?',
            description: 'Tu as reconnu le lieu de cette photo? Viens tester tes connaissances sur le jeu smapshot et navigue dans le paysage tu temps passé.',
            imageUrl: 'http://smapshot.heig-vd.ch/icons/perrochetSample.jpg',
            link: 'http://smapshot.heig-vd.ch/game/?collectionId=2',
            sitename: 'http://smapshot.heig-vd.ch/'
          }
          var newHtml = htmlFile.replace('{{og.title}}', og.title)
          var newHtml = newHtml.replace(/{{og.description}}/g, og.description)
          var newHtml = newHtml.replace(/{{og.imageUrl}}/g, og.imageUrl)
          var newHtml = newHtml.replace('{{og.sitename}}', og.sitename)
          var newHtml = newHtml.replace('{{og.link}}', og.link)
          res.send(newHtml)
        })
      }
    })
  } else {
    res.sendFile(path.join(__dirname, '../public/views', 'game.html'))
    // logger.log('info', 'Game', {
    //   ip: req.connection.remoteAddress,
    //   user_agent: req.headers['user-agent'],
    //   referer: req.headers.referer
    // })
  }
  // res.end('Voici la photo ' + req.params.idpicture);
})

// Authentication
router.use(require('./routes/authenticationRoutes.js'))

// Search images
router.use(require('./routes/imagesRoutes.js'))

// Search Toponyms
router.use(require('./routes/toponymsRoutes.js'))

// search Owners
router.use(require('./routes/ownersRoutes.js'))

// search Photographers
router.use(require('./routes/photographersRoutes.js'))

// search Visits
// router.use(require('./routes/visitsRoutes.js'))

// search Collections
router.use(require('./routes/collectionsRoutes.js'))

// georeferencing
router.use(require('./routes/georefRoutes.js'))

// volunteers
router.use(require('./routes/volunteersRoutes.js'))

// stories
// router.use(require('./routes/storiesRoutes.js'))

// forum
// router.use(require('./routes/forumRoutes.js'))

// admin
router.use(require('./routes/adminRoutes.js'))

// markers
// router.use(require('./routes/markersRoutes.js'))

// upload
// router.use(require('./routes/uploadRoutes.js'))

// corrections
router.use(require('./routes/correctionsRoutes.js'))

// remarks
// router.use(require('./routes/remarksRoutes.js'))

// Observations
router.use(require('./routes/observationsRoutes.js'))

// Problems
router.use(require('./routes/problemsRoutes.js'))

// Errors
router.use(require('./routes/errorsRoutes.js'))

// Validations
router.use(require('./routes/validationsRoutes.js'))

// Volunteers
router.use(require('./routes/volunteersRoutes.js'))

// Corrections
router.use(require('./routes/correctionsRoutes.js'))

// bad Geolocalizations
router.use(require('./routes/badGeolocRoutes.js'))

// customized routes for ethz
// router.use(require('./routes/ethzRoutes.js'))
// map page
router.get('/:ownerId/', function (req, res) {
  // Check if ownerId exist
  var ownerId = req.params.ownerId
  models.owners.findOne({
    where: {
      url_name: ownerId
    },
    raw: true
  }).then(function (owner) {
    if (owner === null){
        res.setHeader('Content-Type', 'text/plain')
        res.status(404).send("This page doesnt exist")
    }else{
      try {
        var ip = clientIp(req)
      } catch (error) {
        var ip = null
      }

      global.logger.log('info', 'OWNER HOME', {
        ip: ip,
        user_agent: req.headers['user-agent'],
        referer: req.headers.referer,
        owner_id: req.params.ownerId
      })
      res.sendFile(path.join(__dirname, '../public/views', 'ownerHome.html'))
    }
  }).catch(function (err) {
    res.status(404).send('Not found')
  })

  // app.use(function (req, res, next) {
  //   res.setHeader('Content-Type', 'text/plain')
  //   res.status(404).send('Page introuvable !')
  // })

  // else return 404

  // logger.log('info', 'Map', {
  //   ip: req.connection.remoteAddress,
  //   user_agent: req.headers['user-agent'],
  //   referer: req.headers.referer
  // })
  // res.sendFile(path.join(__dirname, '../public/views', 'ownerHome.html'))
})

module.exports = router
