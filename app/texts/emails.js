exports.imageReject = `
<p>
Dear username,
</p>
<p>
The geolocalization of this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was rejected.
</p>
<p>
You will find more info about the reason of the rejection by login to your <a href = 'http://smapshot.heig-vd.ch/mysmapshot'>account</a>.
</p>
<p>
If you don't want notification email you can unsubscribe in your <a href = 'http://smapshot.heig-vd.ch/mysmapshot'>account</a>.
</p>
<p>
ownerName's validation team
</p>
`

exports.imageImprove = `
<p>
Dear username,
</p>
<p>
The geolocalization of this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was improved.
</p>
<p>
You will find more info about the reason of the improvement by login to your <a href = 'http://smapshot.heig-vd.ch/mysmapshot'>account</a>.
</p>
<p>
If you don't want notification email you can unsubscribe in your <a href = 'http://smapshot.heig-vd.ch/mysmapshot'>account</a>.
</p>
<p>
ownerName's validation team
</p>
`

exports.imageValidate = `
<p>
Dear username,
</p>
<p>
The geolocalization of this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was validated.
</p>
<p>
ownerName's validation team
</p>
`

exports.titleValidation = `
<p>
Dear username,
</p>
<p>
The title 'newTitle' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was validated.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.captionValidation = `
<p>
Dear username,
</p>
<p>
The caption 'newCaption' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was validated.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.titleUpdate = `
<p>
Dear username,
</p>
<p>
The title 'volTitle' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was updated as 'valTitle'.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.captionUpdate = `
<p>
Dear username,
</p>
<p>
The caption 'volCaption' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was updated as 'valCaption'.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.captionReject = `
<p>
Dear username,
</p>
<p>
The caption 'volCaption' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was rejected.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.titleReject = `
<p>
Dear username,
</p>
<p>
The caption 'volTitle' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was rejected.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.observationReject = `
<p>
Dear username,
</p>
<p>
The observation 'subObservation' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was rejected.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`

exports.observationValidate = `
<p>
Dear username,
</p>
<p>
The observation 'subObservation' that you submited for this <a href = 'http://smapshot.heig-vd.ch/map/?imageId=imageIdentifier'>image</a> was validated.
</p>
<p>
Validator's remark: 'valRemark'.
</p>
<p>
ownerName's validation team
</p>
`
