// var models = require('../model')
// var Sequelize = require('sequelize')

exports.generateFullDownloadSqlTitle = function () {

  // Generate query
  var sql = `
  select distinct corrections.image_id,
  title_validation_date::date::TEXT, title_result,
  case when title_result like 'updated' THEN replace(title_update,E'\n',' ')  ELSE replace(corrections.title,E'\n',' ')  END as title
  from corrections
  inner join (select image_id, max(title_validation_date) as m from corrections group by image_id) c
  on corrections.image_id = c.image_id and corrections.title_validation_date = c.m
  where corrections.title_processed  = true
  and corrections.title_result not like 'rejected'
  `
  return sql
}

exports.generateFullDownloadSqlCaption = function () {

  // Generate query
  var sql = `
  select distinct corrections.image_id,
  caption_validation_date::date::TEXT, caption_result,
  case when caption_result like 'updated' THEN replace(caption_update,E'\n',' ') ELSE replace(corrections.caption,E'\n',' ')  END as caption
  from corrections
  inner join (select image_id, max(caption_validation_date) as m from corrections group by image_id) c
  on corrections.image_id = c.image_id and corrections.caption_validation_date = c.m
  where corrections.caption_processed  = true
  and corrections.caption_result not like 'rejected'
  `
  return sql
}
