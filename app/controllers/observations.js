// var models = require('../model')
// var Sequelize = require('sequelize')

exports.generateFullDownloadSql = function () {

  // Generate query
  var sql = `
  select image_id, string_agg (concat_ws(',', username, replace(observation,E'\n',' '),
	date_observation::date::TEXT, round(coord_x,4), round(coord_y, 4),
	round(observations.width, 4), round(observations.height, 4)), ';') as obs_list
	from observations
	left join volunteers on volunteers.id = observations.volunteer_id
  where observations.validated  = true
	group by image_id
  `
  return sql
}
