var models = require('../model')
var Sequelize = require('sequelize')

exports.checkNotification = function (volunteerId) {
  return new Promise(function (resolve, reject) {
    // Check if volunteers want notifications
    models.volunteers.findOne({
      attributes: ['notifications', 'email', 'username'],
      where: {
        id: volunteerId
      }
    }).then(function (vol) {
      resolve(vol)
    })
  })
}

exports.checkValidator = function (volunteerId) {
  return new Promise(function (resolve, reject) {
    // Check if volunteers want notifications
    models.volunteers.findOne({
      attributes: ['is_validator'],
      where:{
        id: volunteerId
      }
    }).then(function (vol) {
      if (vol.is_validator){
        resolve(vol)
      }
      else {
        reject()
      }

    })
  })
}
