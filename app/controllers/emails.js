var nodemailer = require('nodemailer')
var emailsTexts = require('../texts/emails')

var transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: '',
    pass: ''
  }
})



exports.sendImageRejection = function(volunteer, imageId, ownerName) {
  return new Promise(function (resolve, reject) {
    // Get email text
    var htmlText = emailsTexts.imageReject
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)

    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      //text: text
      html: htmlText // You can choose to send an HTML body instead
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      };
    })
  })
}

exports.sendImageImprovement = function(volunteer, imageId, ownerName) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.imageImprove
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)

    var text = 'Dear  ' + volunteer.username + ', the image: '+ imageId + ' geolocalization was improved.'
    var mailOptions = {
      from: 'smapshot.ch@gmail.com', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendImageValidation = function(volunteer, imageId, ownerName) {
  return new Promise(function (resolve, reject) {
    console.log('vio', volunteer, imageId, ownerName)
    // Get email text
    var htmlText = emailsTexts.imageValidate
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)

    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendCorrectionTitleValidation = function(volunteer, imageId, ownerName, newTitle, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.titleValidation
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('newTitle', newTitle)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: 'smapshot.ch@gmail.com', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}


exports.sendCorrectionCaptionValidation = function(volunteer, imageId, ownerName, newCaption, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.captionValidation
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('newCaption', newCaption)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: 'smapshot.ch@gmail.com', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendCorrectionTitleUpdate = function(volunteer, imageId, ownerName, valTitle, volTitle, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.titleUpdate
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('valTitle', valTitle)
    htmlText = htmlText.replace('valTitle', volTitle)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendCorrectionCaptionUpdate = function(volunteer, imageId, ownerName, valCaption, volCaption, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.captionUpdate
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('valCaption', valCaption)
    htmlText = htmlText.replace('volCaption', volCaption)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: 'smapshot.ch@gmail.com', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendCorrectionCaptionReject = function(volunteer, imageId, ownerName, volCaption, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.captionReject
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('volCaption', volCaption)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendCorrectionTitleReject = function(volunteer, imageId, ownerName, volTitle, remark) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.titleReject
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('volTitle', volTitle)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }

    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendObservationReject = function(volunteer, imageId, ownerName, remark, observation) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.observationReject
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('subObservation', observation)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }


    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}

exports.sendObservationValidate = function(volunteer, imageId, ownerName, remark, observation) {
  return new Promise(function (resolve, reject) {

    // Get email text
    var htmlText = emailsTexts.observationValidate
    // Replace values
    htmlText = htmlText.replace('username', volunteer.username)
    htmlText = htmlText.replace('imageIdentifier', imageId)
    htmlText = htmlText.replace('ownerName', ownerName)
    htmlText = htmlText.replace('subObservation', observation)
    if (remark != null){
      htmlText = htmlText.replace('valRemark', remark)
    } else {
      htmlText = htmlText.replace('valRemark', '-')
    }
    var mailOptions = {
      from: '', // sender address
      to: volunteer.email, // list of receivers
      subject: 'smapshot notification', // Subject line
      html: htmlText
    }
    transporter.sendMail(mailOptions, function (error, info) {
      console.log('error trans', error)
      console.log('error info', info)
      if (error) {
        reject(error)
      } else {
        resolve(info)
      }
    })
  })
}
