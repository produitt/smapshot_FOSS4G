// var models = require('../model')
// var Sequelize = require('sequelize')

exports.generateFullDownloadSql = function (format) {

  // Generate query
  var sql = `
  select id as image_id,
  round(focal/greatest(width, height),2) as focal_ratio,
  round(azimuth%360,2) as azimuth, round(tilt,2) as tilt,
  CASE
  WHEN roll%360 > 180 THEN round(roll%360-360,2)
  ELSE round(roll%360,2)
  END as roll,
  round(st_y(location)::numeric,6) as latitude, round(st_x(location)::numeric,6) as longitude, round(st_z(location)::numeric, 0) as altitude,
  `
  if (format === 'geojson'){
    var sql = sql + `
    replace(ST_AsGeoJson(location), '"', '''') as point,
    replace(ST_AsGeoJson(viewshed), '"', '''') as footprint,
    replace(geotags_array::TEXT, '"', '') as geotags
    from images
    `
  } else if (format === 'kml') {
    var sql = sql + `
    ST_AsKml(location) as point,
    ST_AsKml(viewshed) as footprint,
    `
  }else{
    var sql = sql + `
    st_astext(location) as point,
    st_astext(viewshed) as footprint,
    `
  }
  sql = sql + `replace(geotags_array::TEXT, '"', '') as geotags
  FROM images`
  return sql
}
