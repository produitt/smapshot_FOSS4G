var models = require('../model')
var Sequelize = require('sequelize')

exports.getValidatorOwnerName = function (validatorId) {
  return new Promise(function (resolve, reject) {
    var query = `
      select owners.name
      from owners, volunteers
      where owners.id = volunteers.owner_validator
      and volunteers.id =
    ` + validatorId
    models.sequelize.query(query, {type: Sequelize.QueryTypes.SELECT}).then(function (owner) {
      var ownerName = owner[0].name
      resolve(ownerName)
    })
  })
}
