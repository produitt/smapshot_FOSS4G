// set up
// -------
var express = require('express')
var favicon = require('serve-favicon')
var path = require('path')
var bodyParser = require('body-parser')
var models = require('./app/model') // get sequelize models

var helmet = require('helmet') // security

var winston = require('winston') // Logging
var morgan = require('morgan')

// for https
var https = require('https')
var http = require('http')
var fs = require('fs')

// Login dependancies
var passport = require('passport')

var app = express()

process.env.TZ = 'Europe/Amsterdam' 

// configuration
// --------------
// var connectionString = require(path.join(__dirname, 'config/', 'database'));
app.use(express.static(path.join(__dirname, '/public')))
// app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json())                                     // parse application/json
app.use(helmet()) // for security

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')// http://localhost
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST')
  next()
})
// Use the passport package in our application
app.use(passport.initialize())
// pass passport for configuration
require('./config/passport')(passport)

// Logging
// -------
winston.level = 'debug'
global.logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: 'log/filelog-info.log',
      handleExceptions: true,
      json: true,
      eol: '\n',
      maxsize: 5242880, //5MB Max size in bytes of the logfile, if the size is exceeded then a new file is created, a counter will become a suffix of the log file.
      // maxFiles:         5, //Limit the number of files created when the size of the logfile is exceeded.
      colorize: false,
      timestamp: true
    })
    // ,
    // new winston.transports.File({
    //   level: 'debug',
    //   filename: 'log/debug.log',
    //   handleExceptions: true,
    //   json: true,
    //   eol: '\n',
    //   maxsize: 5242880, //5MB Max size in bytes of the logfile, if the size is exceeded then a new file is created, a counter will become a suffix of the log file.
    //   // maxFiles:         5, //Limit the number of files created when the size of the logfile is exceeded.
    //   colorize: false,
    //   timestamp: true
    // })
    ,
    new winston.transports.Console({
      level: 'info',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  ],
  exitOnError: false
})

logger.stream = {
    write: function(message, encoding){
        logger.debug(message);
    }
}

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream('log/access.log', {flags: 'a'})
app.use(morgan(':date[iso] :method :url :status :res[content-length] - :response-time ms', {stream: accessLogStream}))
// app.use(morgan(':method :url :status :res[content-length] - :response-time ms', {stream: logger.stream}))

logger.info('Server restarted')

// ROUTES
// ------

// Routes
app.use(require('./app/router'))

app.use(favicon(path.join(__dirname, '/public/icons/favicon.png')))

// Error 404
app.use(function (req, res, next) {
  res.setHeader('Content-Type', 'text/plain')
  res.status(404).send('Page introuvable !')
})

// listen (start app with node server.js)
// -------
// var httpServer = https.createServer(app)

var env = process.env.NODE_ENV || 'development'

// Create redirection server
// -------------------------
if (env === 'development') {
  http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://localhost:8443"})
    res.end()
  }).listen(8081)
}
else if (env === 'production') {
  //app.listen(8080, '193.134.216.122') // for production
  http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://smapshot.heig-vd.ch:443"})
    res.end();
  }).listen(80, '193.134.216.122')
} else if (env === 'staging') {
  //app.listen(8080, '193.134.216.124') // for staging
  http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://dev-smapshot.heig-vd.ch:443"})
    res.end();
  }).listen(80, '193.134.216.124')
} else {
  console.log('NODE_ENV is not set')
}


// Create https server
// -------------------
if (env === 'development') {
  var server_options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
    // cert : fs.readFileSync(keys_dir + 'certificate.pem')
  }
  var httpsServer = https.createServer(server_options, app)
  httpsServer.listen(8080) // for local development
  // app.listen(8080) // for local development
} else if (env === 'production') {
    var server_options = {
      key  : fs.readFileSync('./ssl/smapshot.key'),
      cert : fs.readFileSync('./ssl/smapshot.heig-vd.ch.crt.pem'),
      ca   : fs.readFileSync('./ssl/ca_certificate.pem')
      // ca   : fs.readFileSync(keys_dir + 'certauthority.pem'),
      // cert : fs.readFileSync(keys_dir + 'certificate.pem')
    }
    var httpsServer = https.createServer(server_options, app)
    httpsServer.listen(443, '193.134.216.122') // for production
} else if (env === 'staging') {
    var server_options = {
      key  : fs.readFileSync('./ssl/devsmapshot.key'),
      cert : fs.readFileSync('./ssl/dev-smapshot.heig-vd.ch.crt.pem'),
      ca   : fs.readFileSync('./ssl/ca_certificate.pem')
      // ca   : fs.readFileSync(keys_dir + 'certauthority.pem'),
    }
    var httpsServer = https.createServer(server_options, app)
    httpsServer.listen(443, '193.134.216.124') // for staging
} else {
  console.log('NODE_ENV is not set')
}


//
// var server_options = {
//   key  : fs.readFileSync(keys_dir + 'privatekey.pem'),
//   ca   : fs.readFileSync(keys_dir + 'certauthority.pem'),
//   cert : fs.readFileSync(keys_dir + 'certificate.pem')
// }
// httpsServer.listen(server_options, 8443)
// httpsServer.listen(8443) // to replace by 443 in production SSl cerificate to be bought to avoid browser error

models.sequelize.sync({force: false}).then(function () {
})
