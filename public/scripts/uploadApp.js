var dbApp = angular.module('dbApp', ['ngFileUpload'])

dbApp.directive('fileChange', function () {
  return {
    restrict: 'A',
    scope: {
      handler: '&'
    },
    link: function (scope, element) {
      element.on('change', function (event) {
        scope.$apply(function () {
          scope.handler({files: event.target.files})
        })
      })
    }
  }
})

dbApp.controller('dbCtrl', function ($scope, $http, Upload, $window, $timeout) {
  $scope.uploadFiles = function (files) {
    $scope.files = files
    console.log('files', files)
    if (files && files.length) {
      Upload.upload({
        url: 'http://localhost:8080/admin/uploads',
        data: {
          files: files
        }
      }).then(function (response) {
        $timeout(function () {
          $scope.result = response.data
        })
      }, function (response) {
        if (response.status > 0) {
          $scope.errorMsg = response.status + ': ' + response.data
        }
      }, function (evt) {
        $scope.progress =
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
      })
    }
  }
    // var vm = this;
  $scope.submit = function () { // function to call on form submit
    console.log('submit')
    if ($scope.file) { // check if from is valid $scope.upload_form.file.$valid &&
      $scope.upload($scope.file) // call upload function
    }
  }

  $scope.upload = function (file) {
    Upload.upload({
      url: 'http://localhost:8080/admin/upload', // webAPI exposed to upload the file
      data: {file: file} // pass file as data, should be user ng-model
    }).then(function (resp) { // upload function returns a promise
      if (resp.data.error_code === 0) { // validate success
        $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ')
      } else {
        $window.alert('an error occured')
      }
    }, function (resp) { // catch error
      console.log('Error status: ' + resp.status)
      $window.alert('Error status: ' + resp.status)
    }, function (evt) {
      console.log(evt)
      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total)
      console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name)
      $scope.progress = 'progress: ' + progressPercentage + '% ' // capture upload progress
    })
  }

  $scope.imageTable = {}
    // $scope.todoData = {};

    // Get all todos
  $http.get('/admin/readDB')
        .success(function (data) {
          $scope.imageTable = data.table
          $scope.columnName = data.columnName
          console.log(data)
        })
        .error(function (error) {
          console.log('Error: ' + error)
        })
  $scope.uploadImages = function (files) {
    for (var id in files) {
      console.log('files', files[id])
    }
    $http.post('/admin/upload', files)
        .success(function (data) {
          console.log('Success', data)
        })
        .error(function (error) {
          console.log('Error: ' + error)
        })
  }
  $scope.uploadCsv = function (files) {
      // $scope.content = $fileContent;
    console.log('dans load csv')
    var results = Papa.parse(files[0], {
      header: true,
      download: true,
      dynamicTyping: true,
      skipEmptyLines: true,
      comments: true,
      complete: function (results) {
        $scope.updateDB(results)
           // $scope.readDB(results);
      }
    })
  }
  $scope.updateDB = function (results) {
    console.log('results', results)
      // results.replace("'", "\'")
      // console.log('results', results)
      // Escape single quotes
    $scope.csvTable = results
    $http.post('/admin/updateDB', $scope.csvTable)
        .success(function (data) {
          // $scope.imageTable = data.table;
          // $scope.columnName = data.columnName;
          $scope.imageTable = data.table
          $scope.columnName = data.columnName
          // console.log(data);
          console.log('Success', data)
        })
        .error(function (error) {
          console.log('Error: ' + error)
        })
      // $http.get('/admin/readDB')
      //   .success(function(data) {
      //     $scope.imageTable = data.table;
      //     $scope.columnName = data.columnName;
      //     console.log(data);
      //   })
      //   .error(function(error) {
      //     console.log('Error: ' + error);
      //   });
  }
  $scope.reinitializeDB = function () {
    console.log('reinitialize')
      // results.replace("'", "\'")
      // console.log('results', results)
      // Escape single quotes
      // $scope.csvTable = results;
    $http.get('/admin/initializeDB/')
        .success(function (data) {
          console.log('reinitialize1')
          $scope.imageTable = {}
          // $scope.columnName = data.columnName;
          console.log('Success', data)
        })
        .error(function (error) {
          console.log('reinitialize error')
          console.log('Error: ' + error)
        })
    console.log('reinitialize fin')
  }

    // Create a new todo
    // $http.post('/admin/uploadmany', $scope.csvTable)
    //     .success(function(data) {
    //         //$scope.formData = {};
    //         //$scope.todoData = data;
    //         console.log(data);
    //     })
    //     .error(function(error) {
    //         console.log('Error: ' + error);
    //     });

    // Delete a todo
    // $http.delete('/api/v1/todos/' + todoID)
    //     .success(function(data) {
    //         $scope.todoData = data;
    //         console.log(data);
    //     })
    //     .error(function(data) {
    //         console.log('Error: ' + data);
    //     });
})
