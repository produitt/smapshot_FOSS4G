var authApp = angular.module('authApp', [ 'satellizer', 'ui.bootstrap', 'vcRecaptcha'])// "ui.router",noCAPTCHA, , 'vcRecaptcha'
.config(function ($authProvider) {
  $authProvider.google({
    clientId: '',
    url: '/auth/google',
    authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
    // redirectUri: 'http://smapshot.heig-vd.ch/login',//window.location.origin,
    redirectUri: window.location.origin + '/login', // window.location.origin,
    // redirectUri: 'http://localhost/login',
    requiredUrlParams: ['scope'],
    optionalUrlParams: ['display'],
    scope: ['profile', 'email'],
    scopePrefix: 'openid',
    scopeDelimiter: ' ',
    display: 'popup',
    oauthType: '2.0',
    popupOptions: { width: 452, height: 633 },
    tokenName: 'token',
    tokenPrefix: 'auth'
  })
  $authProvider.facebook({
    clientId: '',
    url: '/auth/facebook',
    authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
    // redirectUri: 'http://smapshot.heig-vd.ch/login',//window.location.origin,
    redirectUri: window.location.origin + '/login', // window.location.origin,
    // redirectUri: 'http://localhost/login',
    requiredUrlParams: ['display', 'scope'],
    // optionalUrlParams: ['display'],
    scope: ['public_profile', 'email'],
    // scopePrefix: 'openid',
    scopeDelimiter: ', ',
    display: 'popup',
    oauthType: '2.0',
    popupOptions: { width: 580, height: 400 },
    tokenName: 'token',
    tokenPrefix: 'auth'
  })
})
