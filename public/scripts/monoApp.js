var monoApp = angular.module('monoApp', ['angularCesium', 'authApp', 'topoApp',
  'cesiumApp', 'imageQueryApp', 'storyApp', 'forumApp', 'markerApp', 'moveApp', 'imageSliderApp'])

monoApp.config(function ($logProvider, $locationProvider) {
  // $logProvider.debugEnabled(false);
  $locationProvider.html5Mode(true)
})
