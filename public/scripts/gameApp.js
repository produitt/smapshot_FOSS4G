var gameApp = angular.module('gameApp', ['leaflet-directive', 'ui.openseadragon',
  'angularCesium', 'ui.bootstrap', 'ngAnimate', 'ngSanitize', 'cesiumApp', 'osdApp', 'dbApp', 'imageQueryApp', 'moveApp', 'gameStepsApp'])
gameApp.config(function ($logProvider, $locationProvider) {
  $logProvider.debugEnabled(false)
  $locationProvider.html5Mode(true)
})
