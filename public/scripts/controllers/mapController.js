mapApp.controller('mapCtrl',
  [ '$scope', '$http', '$window', '$location', '$q', 'AuthService', 'searchService', 'leafService',
    'leafletData', 'leafletBoundsHelpers', 'leafletMarkerEvents', 'langService',
    'textService', 'imageQueryService', 'dateService', 'uibDateParser',
    'ownerQueryService', 'collectionsQueryService', 'cesiumService', 'correctionsService',
    'osdService', 'observationsService', 'volunteersService', 'badGeolocService',
    function ($scope, $http, $window, $location, $q, AuthService, searchService, leafService,
     leafletData, leafletBoundsHelpers, leafletMarkerEvents, langService, textService,
   imageQueryService, dateService, uibDateParser, ownerQueryService, collectionsQueryService,
   cesiumService, correctionsService, osdService, observationsService, volunteersService,
   badGeolocService) {

     // Avoid path error on staging server
     $window['CESIUM_BASE_URL'] = '/bower_components/cesium/Build/Cesium/'

      // Menu modes
      // ----------
      $scope.menuMode = 'visit' // 'geoloc', 'noapriori'
      $scope.switchToGeoloc = function () {
        $scope.visitMode = 'map'
        $scope.menuMode = 'geoloc'
        $scope.geolocalized = false
        $scope.switch.geolocEnabled = true

        $scope.search.georef.with = false
        $scope.search.georef.without = true
        $scope.search.georef.all = false

        $scope.getAllImages(true)
        // Change max zoom
        leafletData.getMap().then(function (map) {
          map.setZoom(17)
          map.options.maxZoom = 23
        })
      }
      $scope.switchToNoApriori = function () {
        $scope.markersRed = null
        $scope.markersGreen = null
        $scope.menuMode = 'noapriori'
        $scope.visitMode = 'noapriori'
        $scope.getNoApriori()
      }
      $scope.switchToVisit = function () {
        $scope.visitMode = 'map'
        $scope.menuMode = 'visit'
        $scope.geolocalized = true
        $scope.switch.geolocEnabled = false

        $scope.search.georef.with = true
        $scope.search.georef.without = false
        $scope.search.georef.all = false

        $scope.getAllImages(true)
        // Change max zoom
        leafletData.getMap().then(function (map) {
          map.options.maxZoom = 26
        })
      }
      // function for the check box at the bottom of the map
      $scope.switch = {
        geolocEnabled: false
      }
      $scope.switchVisitGeoloc = function (){
        if ($scope.switch.geolocEnabled) {
          $scope.switchToGeoloc()
        }else {
          $scope.switchToVisit()
        }
      }
      // Open picture in OSD
      $scope.switchToPicture = function () {
        $scope.menuMode = 'visit'
        $scope.geolocalized = true
        $scope.visitMode = 'picture'
        // Open image in osd
        $scope.switchOsdPicture($scope.imageSelected.id)
      }
      // Open picture in globe
      $scope.switchToGlobe = function () {
        // $location.url("/map")
        if (!$scope.showInfo){
          //hide the observations (visible only in ppicture mode)
          $scope.showInfo = true
        }
        $scope.menuMode = 'visit'
        $scope.geolocalized = true
        $scope.visitMode = 'globe'
      }
      //Functions for the parameter box
      $scope.showParameters = false
      $scope.openParameters = function () {
        if ($scope.menuMode === 'noapriori') {
          $scope.switchToGeoloc()
        }
        if ($scope.showMeta){
          $scope.showMeta = false
        }else{
          $scope.showMeta = true
        }
        //$scope.showMeta ^= false
        $scope.showParameters ^= true

      }
      $scope.closeParameters = function () {
        $scope.showParameters = false
        $scope.showMeta = true
      }
      // Change OSD picture
      $scope.switchOsdPicture = function () {
        try {
          // if the viewer already exists
          $scope.osd.mysd.viewer.open('../data/collections/' + $scope.imageSelected.collection_id + '/images/tiles/'.concat($scope.imageSelected.id).concat('.dzi'))
        } catch (err){
        }
        try {
          $scope.o.optionsViewer.tileSources = ["../data/collections/" + $scope.imageSelected.collection_id + "/images/tiles/"+ $scope.imageSelected.id + '.dzi']
        } catch (err){
        }
      }
      // Select an image
      $scope.selectImage = function (id) {
        $scope.showBadGeolocButton = true
        for (var i=0; i< $scope.images.length; i++) {
          if ($scope.images[i].id === id) {
            // For the metadata
            $scope.imageSelected = $scope.images[i]
            // For comparison in the image slider
            $scope.imageOver = $scope.images[i]
          }
        }
      }
      $scope.selectOverImage = function (id) {
        for (var i=0; i< $scope.images.length; i++) {
          if ($scope.images[i].id === id) {
            // For comparison in the image slider
            $scope.imageOver = $scope.images[i]
          }
        }
      }
      // Mouse over the image slider and marker
      $scope.mouseOverSlider = function (id) {
        // An action occurs only in map mode
        if ($scope.visitMode === 'map') {
          //Select the image
          $scope.selectImage(id)
          //$scope.imageSelected = $scope.images[id]
          //Change the marker
          $scope.selectedMarkers.selectedMarker.lat = $scope.imageSelected.latitude
          $scope.selectedMarkers.selectedMarker.lng = $scope.imageSelected.longitude
          // Change marker color
          if ($scope.imageSelected.georef_3d_bool) {
            $scope.selectedMarkers.selectedMarker.icon = local_icons.green_icon
          } else {
            $scope.selectedMarkers.selectedMarker.icon = local_icons.red_icon
          }
          // Display footprint
          if ($scope.imageSelected.georef_3d_bool == true) {
            // Display footprint
            imageQueryService.getFootprint($scope.imageSelected.id)
            .then(function (response) {
              var geojson = JSON.parse(response.footprint)
              $scope.geojson.footprint = {
                name: 'Footprint',
                type: 'geoJSONShape',
                data: geojson,
                visible: true,
                style: {
                  color: 'black',
                  fillColor: '#FFB028',//'#20CDAD',
                  weight: 4.0,
                  opacity: 1,
                  fillOpacity: 0.5
                }
              }

            })
          } else {
            $scope.geojson = {}
          }
        } else if ($scope.visitMode === 'globe') {
          //Select the image without showing the metadata
          $scope.selectOverImage(id)
        } else if ($scope.visitMode === 'picture') {
          //Select the image without showing the metadata
          $scope.selectOverImage(id)
        }
      }
      // Click in the image slider or marker
      $scope.clickSlider = function (id) {
        $location.url('/map/?imageId='+id)
        // Disable edition
        $scope.editing = false
        // Hide filter
        $scope.filterBoxBool = false
        // User click on a slider image
        if ($scope.visitMode === 'map') {
          //Hide parameters
          $scope.showParameters = false
          //Show Meta information
          $scope.showMeta = true
          //Select the image
          $scope.selectImage(id)

          // $scope.imageSelected = $scope.images[id]
          if ($scope.imageSelected.georef_3d_bool) {
            //Open the globe
            $scope.visitMode = 'globe'
            $scope.showGlobe = true
            $scope.getNearestImages().then(function() {
              // Increment the number of views
              imageQueryService.incrementViews($scope.imageSelected.id, '3D')
            })
          }else {
            //Open the picture
            $scope.visitMode = 'picture'
            $scope.menuMode = 'geoloc'
            $scope.switchOsdPicture()
            // $scope.getNearestImages().then(function(){
              // Increment the number of views
            imageQueryService.incrementViews($scope.imageSelected.id, '2D')
            // })
          }
        }
        else if ($scope.visitMode === 'picture') {
          // Select the image
          $scope.selectImage(id)
          // Open another picture
          $scope.switchOsdPicture()
          // $scope.getNearestImages().then(function(){
            // Increment the number of views
            imageQueryService.incrementViews($scope.imageSelected.id, '2D')
          // })
          // Hide observations
          $scope.showInfo = true

        } else if ($scope.visitMode === 'globe') {
          // erase the current image
          $scope.deleteModel()
          //Select the image
          $scope.selectImage(id)
          $scope.drawModel()
          cesiumService.flyToImage($scope.imageSelected, $scope.globe.cesium)
          $scope.getNearestImages().then(function(){
            // Increment the number of views
            imageQueryService.incrementViews($scope.imageSelected.id, '3D')
          })
        }else if ($scope.visitMode === 'noapriori') {
          //Select the image
          $scope.selectImage(id)
          // Open another picture
          $scope.switchOsdPicture()
          imageQueryService.incrementViews($scope.imageSelected.id, '3D')
        }
      }
      // If the page is opened with an image id as argument
      $scope.openPicture = function (id, loaded) {
        // Loaded is a parameter to see if the page is open in picture mode
        $scope.showParameters = false
        //$scope.showMeta = true
        imageQueryService.getImage(id).then(function(res){
          $scope.imageSelected = res
          if ($scope.imageSelected.is_published){
            if ($scope.imageSelected.georef_3d_bool){
              $scope.visitMode = 'globe'
              $scope.showGlobe = true
              $scope.showMeta = true
              // $scope.showInfoVisitBool = false
              // $scope.showInfoGeolocBool = false
              imageQueryService.incrementViews($scope.imageSelected.id, '3D')
            } else {
              $scope.visitMode = 'picture'
              $scope.showMeta = true
              // $scope.showInfoVisitBool = false
              // $scope.showInfoGeolocBool = false
              $scope.switchOsdPicture()
              imageQueryService.incrementViews($scope.imageSelected.id, '2D')
            }
            $scope.getNearestImages()
          }else{
            alert($scope.texts.notPublished[$scope.lang])
            $scope.switchToVisit()
          }
        })
      }

      $scope.selectedCollection = {
        name: 'Alle Sammlungen'
      }
      // Function which select a collection
      $scope.selectCollection = function () {
        var findAMatch = false // if the selected collection match the owner's collections
        if ($scope.collectionId){
          $location.search('collectionId='+$scope.collectionId)
        }else{
          $location.search('')
        }
        for (var i=0; i<$scope.collections.length; i++){
          if ($scope.collections[i].id == $scope.collectionId){
            $scope.collections[i].selected = true
            $scope.selectedCollection.name = $scope.collections[i].cname
            findAMatch = true
            console.log('select a colection')
          }
          else{
            $scope.collections[i].selected = false
          }
        }
        if (!findAMatch){
          $scope.collectionId = null
        }
      }
      // Get the url parameters
      // ----------------------
      // Complex loop to process the url parameters

      //Detect image id
      $scope.initMode = 'map'
      if ($location.search().imageId) {
          $scope.imageId = $location.search().imageId
          $scope.showMeta = true
          // $scope.showInfoVisitBool = false
          // $scope.showInfoGeolocBool = false
          $scope.openPicture($scope.imageId, false)
          $scope.initMode = 'picture'
      }
      // else there is no image id
      else {
        // Check if an owner is passed
        $scope.ownerId = null
        var urlParts = $window.location.href.split('/')
        // Detect map
        var idMap = urlParts.indexOf('map')
        var detecOwner = urlParts[idMap + 1]
        // if there is some more text after /map
        if (!detecOwner){//(detecOwner === 'localhost:8080') || (detecOwner === 'smapshot.heig-vd.ch') || (detecOwner === 'dev-smapshot.heig-vd.ch:8080') || (detecOwner === '10.0.2.2:8080')) {
          // There is no owner provided
          $scope.ownerMode = false
          $scope.ownerId = null
          // Check if a collection is provided
          $scope.collectionId = null
          if ($location.search().collectionId) {
            $scope.collectionId = $location.search().collectionId
          }
          collectionsQueryService.getCollections($scope.ownerId).then(function (res){
            $scope.collections = res
            $scope.search.collections.all = res
            $scope.selectCollection()

            $scope.getAllImages(true)
          })
        } else {
          // There is some texts after map
          // Detect if the string contain search arguments ?
          if (detecOwner.indexOf('?') !== -1){
            // No owner is passed but a collection is passed
            $scope.ownerId = null
            $scope.collectionId = null
            if ($location.search().collectionId) {
              $scope.collectionId = $location.search().collectionId
            }
            collectionsQueryService.getCollections($scope.ownerId).then(function (res){
              $scope.collections = res
              $scope.search.collections.all = res
              // $scope.selectCollection()
              $scope.selectGetCollection($scope.collectionId)

              $scope.getAllImages(true)
            })
          }else{
            // an owner is passed
            // Check if a collection is passed
            $scope.collectionId = null
            if ($location.search().collectionId) {
              $scope.collectionId = $location.search().collectionId
            }

            $scope.ownerMode = true
            var ownerPromise = ownerQueryService.getCustomParameters(detecOwner)
            .then(function (res) {
              $scope.ownerId = res.id
              // Select the owners for the filter
              $scope.search.owners.selected.push(res)
              // Get owner collections
              collectionsQueryService.getCollections($scope.ownerId).then(function (res){
                $scope.collections = res
                $scope.search.collections.all = res
                $scope.selectCollection()

                $scope.getAllImages(true)
              })
          })
        }
      }
    }
          // if (detecOwner.search('imageId') === -1){
          //   $scope.ownerMode = true
          //   var ownerPromise = ownerQueryService.getCustomParameters(detecOwner)
          //   .then(function (res) {
          //     $scope.ownerId = res.id
          //     collectionsQueryService.getCollections($scope.ownerId).then(function (res){
          //       $scope.collections = res
          //       // Collection id
          //       $scope.collectionId = null
          //       if ($location.search().collectionId) {
          //         $scope.collectionId = $location.search().collectionId
          //         if ($scope.collectionId){
          //           for (var i=0; i<$scope.collections.length; i++){
          //             if ($scope.collections[i].id === $scope.collectionId){
          //               $scope.collections[i].selected = true
          //               $scope.selectedCollection = {
          //                 name: $scope.collections[i].cname
          //               }
          //             }
          //           }
          //         }
          //         $scope.getAllImages(true)
          //       }
          //       // $scope.getAllImages(true)
          //     })
          //   })
          // }

        //}

      //}


      // //Detect image id
      // $scope.initMode = 'map'
      // if ($location.search().imageId) {
      //     $scope.imageId = $location.search().imageId
      //     $scope.showMeta = true
      //     // $scope.showInfoVisitBool = false
      //     // $scope.showInfoGeolocBool = false
      //     $scope.openPicture($scope.imageId, false)
      //     $scope.initMode = 'picture'
      // }
      // Bounding box
      $scope.bboxProvided = {}
      $scope.bboxProvided.left = $location.search().left
      $scope.bboxProvided.right = $location.search().right
      $scope.bboxProvided.bottom = $location.search().bottom
      $scope.bboxProvided.top = $location.search().top
      // // Collection id
      // $scope.collectionId = null
      // if ($location.search().collectionId) {
      //   $scope.collectionId = $location.search().collectionId
      //   if ($scope.collectionId){
      //     for (var i=0; i<$scope.collections.length; i++){
      //       if ($scope.collections[i].id === $scope.collectionId){
      //         $scope.collections[i].selected = true
      //         $scope.selectedCollection = {
      //           name: $scope.collections[i].cname
      //         }
      //       }
      //     }
      //   }
      // }
      // Owner id
      // if ($location.search().ownerId) {
      //   $scope.ownerId = $location.search().ownerId
      // }
      // Mode
      $scope.geolocalized = true
      $scope.maxZoom = 26
      if ($location.search().geolocalized) {
        if ($location.search().geolocalized !== 'true') {
          $scope.visitMode = 'map'
          $scope.menuMode = 'geoloc'
          $scope.maxZoom = 23
          $scope.geolocalized = false
          $scope.switch.geolocEnabled = true
        }
      }

      $scope.osd = {
        mysd: {}
      }
      // $scope.notGeoref = 0
      // $scope.showAddTopo = false
      $scope.showDrawROI = true // for the button to draw roi in filter
      // $scope.showSearch = false
      // $scope.isLeft = false
      // $scope.showGoTo3DList = false
      // $scope.isCollapsedMeta = true

      // if filter is activated or not
      $scope.searchMode = false

      // lang
      // ----
      // Set initial language
      $scope.lang = langService.getBrowserLang()
      // Switch the lang
      $scope.switchLang = function (lang) {
        $scope.lang = lang
      }
      $scope.changeVolunteerLang = function (lang) {
        if (AuthService.isAuthenticated()){
          volunteersService.submitLang(lang)
        }
        $scope.lang = lang
      }
      // Get languages model
      $scope.langs = langService.getLangs()

      // texts
      // -----
      // Initialize texts
      $scope.hTexts = {
        'login': {
          'fr': "M'identifier"
        }
      }
      $scope.myName = $scope.hTexts.login[$scope.lang]
  // Load text for the app
      $scope.loadTextPromise = textService.loadText('map').then(function (texts) {
        $scope.texts = texts
      })
  // for the header
      $scope.loadHeaderTextPromise = textService.loadText('header').then(function (texts) {
        $scope.hTexts = texts
        $scope.myName = $scope.hTexts.login[$scope.lang]
      })

  // Functions for the navbar (require AuthService)
  // ------------------------
      $scope.isLogged = false
      $scope.getMyName = function () {
        return $q(function (resolve, reject) {
          if (AuthService.isAuthenticated()) {
            $http.get('/volunteers/memberinfo').then(function (result) {
              $scope.volunteer = result.data.volunteer
              $scope.myName = $scope.volunteer.username
              $scope.isLogged = true
              if ($scope.volunteer.lang){
                $scope.lang = $scope.volunteer.lang
              }
              resolve()
            })
          } else {
            $scope.myName = "M'identifier"
            resolve()
          }
        })
      }
      $scope.getMyName()

      $scope.clickMy = function () {
        if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
          location.href = '/mysmapshot/'
        } else {
      // Go to login
          var win = $window.open('/login', '_blank')
          var timer = setInterval(checkChild, 500)
          function checkChild () {
            if (win.closed) {
              clearInterval(timer)
              AuthService.loadUserCredentials()
              $scope.getMyName()
              //$window.location.reload()
            }
          }
        }
      }
      $scope.logout = function () {
        AuthService.logout()
        $scope.isLogged = false
      }
      $scope.login = function () {
        var win = $window.open('/login', '_blank')
        var timer = setInterval(checkChild, 500)
        function checkChild () {
          if (win.closed) {
            clearInterval(timer)
            AuthService.loadUserCredentials()
            $scope.getMyName()
            //$window.location.reload()
          }
        }
      }
  // Get current challenge status
  // ----------------------------
  //     $scope.challengeCollection = {
  //       nImages: 100,
  //       nGeoref: 0
  //     }
  //     searchService.getCurrentChallenge().then(
  //   function (response) {
  //     $scope.challengeCollection = response[0]
  //   }
  // )

      // Filters model
      // ------------
      // Filter box
      $scope.filterBoxBool = false
      $scope.nResults = 0 // Number of results
      $scope.showHideFilter = function () {
        $scope.filterBoxBool ^= true
      }
      // Model
      $scope.search = {
        keyword: null,
        owners: {
          selected: [],
          all: []
        },
        collections: {
          input: [],
          selected: [],
          all: []
        },
        dates: {
          // modified: false,
          from: new Date(1800, 1, 1),
          to: new Date(),
          fromMin: new Date(1800, 1, 1),
          fromMax: new Date(),
          toMin: new Date(1800, 1, 1),
          toMax: new Date(),
          toMonth: new Date().getMonth() + 1,
          toYear: new Date().getFullYear(),
          fromMonth: 1,
          fromYear: 1800
        },
        roi: {
          shape: null,
          footprintBool: true,
          locationBool: false
        },
        toponyms: {
          selected: [],
      // selected: undefined,
          input: '',
          propositions: []
        },
    // tags: [],
        georef: {
          with: true,
          without: false,
          all: false
        },
        listGeorefSelected: []
      }
      // Owners
      ownerQueryService.getList().then(function (ownersList) {
        $scope.search.owners.all = ownersList
      })
      $scope.selectOwner = function ($item, $model, $label, $event) {
        $scope.search.owners.selected.push($item)
        ownerQueryService.getOwnersCollections($scope.search.owners.selected).then(function (collectionList) {
          $scope.search.collections.all = collectionList
        })
      }
      $scope.removeOwner = function (owner) {
        var index = $scope.search.owners.selected.indexOf(owner)
        $scope.search.owners.selected.splice(index, 1)
        ownerQueryService.getOwnersCollections($scope.search.owners.selected).then(function (collectionList) {
          $scope.search.collections.all = collectionList
        })
      }
      // Collections
      // collectionsQueryService.getList().then(function (collectionsList) {
      // collectionsQueryService.getCollections($scope.ownerId).then(function (collectionsList) {
      //   $scope.search.collections.all = collectionsList
      // })
      $scope.selectCollectionFilter = function ($item, $model, $label, $event) {
        $scope.search.collections.selected.push($item)
      }
      $scope.removeCollection = function (collection) {
        var index = $scope.search.collections.selected.indexOf(collection)
        $scope.search.collections.selected.splice(index, 1)
      }
      // Dates
      $scope.listMonths = dateService.listMonths()
      $scope.showToDate = true
      $scope.showFromDate = true
      $scope.search.dates.fromDateStr = '01.01.1800'
      $scope.processFrom = function(){
        var fromDate = uibDateParser.parse($scope.search.dates.fromDateStr, 'dd.MM.yyyy')
        if (fromDate) {
          $scope.search.dates.fromMonth = fromDate.getMonth() + 1
          $scope.search.dates.fromYear = fromDate.getFullYear()
          $scope.search.dates.from = fromDate
        }
      }
      $scope.processTo = function () {
        var toDate = uibDateParser.parse($scope.search.dates.toDateStr, 'dd.MM.yyyy')
        if (toDate) {
          $scope.search.dates.toMonth = toDate.getMonth() + 1
          $scope.search.dates.toYear = toDate.getFullYear()
          $scope.search.dates.to = toDate
        }
      }
      // $scope.$watch('toDateStr', function () {
      //   var toDate = uibDateParser.parse($scope.toDateStr, 'dd.MM.yyyy')
      //   if (toDate) {
      //     $scope.search.dates.toMonth = toDate.getMonth() + 1
      //     $scope.search.dates.toYear = toDate.getFullYear()
      //     $scope.search.dates.to = toDate
      //   }
      // })
      // Toponyms
      $scope.selectToponym = function ($item, $model, $label, $event) {
        $scope.search.toponyms.selected.push($item.name)
      }
      $scope.removeToponym = function (topo) {
        var id = $scope.search.toponyms.selected.indexOf(topo)
        $scope.search.toponyms.selected.splice(id, 1)
      }
      $scope.searchToponyms = function (input){
        return $q(function (resolve, reject) {
          var propositions = []
          searchService.getToponymsLike(input)
          .then(function (response) {
            propositions = propositions.concat(response)
            if (input.length >= 4) {
              searchService.getToponymsSoundLike(input)
              .then(function (response) {
                propositions = propositions.concat(response)
                resolve(propositions)
              })
            }else {
              resolve(propositions)
            }
          })
        })
      }
      // roi
      $scope.startDrawROI = function () {
        $scope.showDrawROI = false
        leafletData.getMap().then(function (map) {
          $scope.roi = new L.Draw.Polygon(map).enable()
        })
      }
      $scope.eraseROI = function () {
        $scope.showDrawROI = true
        $scope.search.roi.shape = null
        $scope.search.roi.shapeString = null

        leafletData.getMap().then(function (map) {
          map.removeLayer($scope.createdROI)
          new L.Draw.Polygon(map).disable()
          // leafletData.getLayers().then(function (baselayers) {
          //   if ($scope.createdROI) {
          //     var drawnItems = baselayers.overlays.draw
          //     drawnItems.removeLayer($scope.createdROI)
          //   }
          // })
        })
      }

      // Submit and erase
      // ----------------
      $scope.submitRequest = function () {
        $scope.markers = {}
        // Georeferencing
        if($scope.geolocalized){
          $scope.search.georef.with = true
          $scope.search.georef.without = false
          $scope.search.georef.all = false
        }
        else {
          $scope.search.georef.with = false
          $scope.search.georef.without = true
          $scope.search.georef.all = false
        }
        imageQueryService.submitForm($scope.search).then(
          function (response) {
            $scope.search.response = response.geojson //.data
            $scope.nResults = response.geojson.features.length
            if (response.geojson.features.length != 0) {
              var bbox = turf.envelope(response.geojson)
              var bound = [[bbox.geometry.coordinates[0][0][1], bbox.geometry.coordinates[0][0][0]], [bbox.geometry.coordinates[0][2][1], bbox.geometry.coordinates[0][2][0]]]
              leafletData.getMap().then(function (map) {
                map.fitBounds(bound)
              })

              // Fill the list of selected georeferenced images for the 3d viz
              var listIds = []
              for (var id in response.geojson.features) {
                var feature = response.geojson.features[id]
                // Create markers
                //if (feature.properties.georef_3d_bool == true) {
                listIds.push(feature.properties.id)
                //}
              }
              $scope.search.listGeorefSelected = listIds

              // Create marker cluster layers and images
              $scope.images = response.images//.data.images

              leafletData.getMap().then(function (map) {
                map.eachLayer(function (layer) {
                  if ((!layer._url)  && (!layer.dragging)) {
                    map.removeLayer(layer)
                  }
                })
                try {
                  map.removeLayer($scope.markersRed)
                }catch (err){

                }
                try {
                  map.removeLayer($scope.markersGreen)
                }catch (err){

                }
                if ($scope.georeferenced){
                  $scope.markersGreen = leafService.drawImages(response.geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                  //map.addLayer($scope.markersRed)
                  map.addLayer($scope.markersGreen)
                }else{
                  $scope.markersRed = leafService.drawImages(response.geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                  //map.addLayer($scope.markersRed)
                  map.addLayer($scope.markersRed)
                }

                 // Remove footprint
                $scope.geojson.footprint = {}
                $scope.geojson.footprintBool = false
              })
              // Fill the slider
              //$scope.imagesInSlider = response.images
              $scope.imagesInSlider = {
                ids: [],
                images: []
              }
              for (var i=0; i<$scope.images.length; i++) { //
                if ($scope.imagesInSlider.ids.indexOf($scope.images[i].id) === -1){
                  $scope.imagesInSlider.ids.push($scope.images[i].id)
                  $scope.imagesInSlider.images.push($scope.images[i])
                }
              }
              $scope.imagesInSlider.ids = $scope.imagesInSlider.ids.slice(0, $scope.nImagesSlider)
              $scope.imagesInSlider.images = $scope.imagesInSlider.images.slice(0, $scope.nImagesSlider)
              // $scope.imagesInSlider.images = response.images
              // $scope.imagesInSlider.ids = listIds
              // $scope.imagesInSlider.ids = $scope.imagesInSlider.ids.slice(0, $scope.nImagesSlider)
              // $scope.imagesInSlider.images = $scope.imagesInSlider.images.slice(0, $scope.nImagesSlider)


              // Change selected image
              $scope.imageSelected = $scope.images[0]
              $scope.switchOsdPicture()
            } else {
              alert($scope.texts.noImagesSelected[$scope.lang])
            }
            // Hide footprintBool
            // $scope.geojson.footprint = {}
            // $scope.select($scope.imageSelected.id)
          }
    )
        // Update views
        $scope.showSearch = false
        $scope.searchMode = true
    // window.scrollTo(window.innerWidth, 0);
      }

      $scope.eraseRequest = function () {
        // Hide footprint
        $scope.geojson.footprint = {}

        // Hide erase ROI button
        $scope.showDrawROI = true

        // Reinitialise search parameter
        $scope.search.keyword = null
        $scope.search.collections.input = []
        $scope.search.collections.selected = []
        $scope.search.collections.all = []

        $scope.search.owners.input = []
        $scope.search.owners.selected = []
        $scope.search.owners.all = []

        // $scope.search.dates.modified = false
        // $scope.search.dates.from = new Date(1850, 1, 1)
        // $scope.search.dates.to = new Date()
        // $scope.search.dates.fromMin = new Date(1850, 1, 1)
        // $scope.search.dates.fromMax = new Date()
        // $scope.search.dates.toMin = new Date(1850, 1, 1)
        // $scope.search.dates.toMax = new Date()

        // modified: false,
        $scope.search.dates.from = new Date(1800, 1, 1),
        $scope.search.dates.to = new Date(),
        $scope.search.dates.fromMin = new Date(1800, 1, 1),
        $scope.search.dates.fromMax = new Date(),
        $scope.search.dates.toMin = new Date(1800, 1, 1),
        $scope.search.dates.toMax = new Date(),
        $scope.search.dates.toMonth = new Date().getMonth() + 1,
        $scope.search.dates.toYear = new Date().getFullYear(),
        $scope.search.dates.fromMonth =  1,
        $scope.search.dates.fromYear = 1800

        $scope.search.roi.shape = null
        $scope.search.roi.footprintBool = true
        $scope.search.roi.locationBool = false

        $scope.search.toponyms.selected = []
        $scope.search.toponyms.input = ''
        $scope.search.toponyms.propositions = []

        $scope.search.georef.with = true
        $scope.search.georef.without = false
        $scope.search.georef.all = false

        $scope.getAllImages(true)
        // Update views
        $scope.showSearch = true
        $scope.searchMode = false
        // Delete roi in leaflet
        leafletData.getMap().then(function (map) {
          leafletData.getLayers().then(function (baselayers) {
            if ($scope.createdROI) {
              var drawnItems = baselayers.overlays.draw
              drawnItems.removeLayer($scope.createdROI)
            }
          })
        })
      }

  // $scope.$watch('search.collections', function(){
  //   $scope.search.collections.selected = []
  //   for (var index in $scope.search.collections.dict){
  //     var collection = $scope.search.collections.dict[index]
  //     if (collection.selected == true){
  //       $scope.search.collections.selected.push(collection)
  //     }
  //   }
  // }, true)
  //
  // $scope.$watch('search.listGeorefSelected', function(){
  //   if ($scope.search.listGeorefSelected.length > 0){
  //     $scope.showGoTo3DList = true
  //   }else{
  //     $scope.showGoTo3DList = false
  //   }
  // })
  // $scope.$watch('search.dates.to', function(){
  //   $scope.search.dates.modified = true
  //   $scope.search.dates.fromMax = $scope.search.dates.to
  // })
  // $scope.$watch('search.dates.from', function(){
  //   $scope.search.dates.modified = true
  //   $scope.search.dates.toMin = $scope.search.dates.from
  // })
  //
  // $scope.$watch('search.georef.with', function(){
  //   if ($scope.search.georef.with == true){
  //     $scope.search.georef.without = false;
  //     $scope.search.georef.all = false;
  //   }
  //   if (($scope.search.georef.with == false) && ($scope.search.georef.without == false) && ($scope.search.georef.all == false)) {
  //     $scope.search.georef.with = true
  //   }
  // })
  // $scope.$watch('search.georef.without', function(){
  //   if ($scope.search.georef.without == true){
  //     $scope.search.georef.with = false;
  //     $scope.search.georef.all = false;
  //   }
  //   if (($scope.search.georef.with == false) && ($scope.search.georef.without == false) && ($scope.search.georef.all == false)) {
  //     $scope.search.georef.with = true
  //   }
  // })
  // $scope.$watch('search.georef.all', function(){
  //   if ($scope.search.georef.all == true){
  //     $scope.search.georef.with = false;
  //     $scope.search.georef.without = false;
  //   }
  //   if (($scope.search.georef.with == false) && ($scope.search.georef.without == false) && ($scope.search.georef.all == false)) {
  //     $scope.search.georef.with = true
  //   }
  // })

  // searchService.getCollections()
  // .then(function(response){
  //   for (id in response){
  //     $scope.search.collections.all.push(response[id].name)
  //   }
  //   $scope.search.collections.dict = response
  //
  //   var imTot = 0
  //   var imGeorefTot = 0
  //   var nCol = $scope.search.collections.dict.length
  //   //for (var index in $scope.search.collections.dict){
  //   for (index = 0; index<nCol; index++){
  //     var collection = $scope.search.collections.dict[index]
  //     var ratio = (parseInt(collection.nGeoref)/parseInt(collection.nImages)*100).toFixed(1)
  //     $scope.search.collections.dict[index].ratio = ratio
  //     imTot = imTot + parseInt(collection.nImages)
  //     imGeorefTot = imGeorefTot + parseInt(collection.nGeoref)
  //   }
  //   $scope.ratioGeoloc = (imGeorefTot/imTot*100).toFixed(1)
  //   $scope.ratioNotGeoloc = (100-$scope.ratioGeoloc).toFixed(1)
  // })

      // Leaflet parameters
      // ------------------
      $scope.visitMode = 'map'
      // Swiss gravity center
      $scope.center = {
        lat: 46.80112,
        lng: 8.22670,
        zoom: 17
      }
      // var tripodIcon = {
      //   iconUrl: '../icons/tripod.png',
      //   shadowUrl: '../icons/tripod_shadow.png',
      //   iconSize: [51, 100], // size of the icon
      //   shadowSize: [50, 62], // size of the shadow
      //   iconAnchor: [25, 100], // point of the icon which will correspond to marker's location
      //   shadowAnchor: [0, 62]  // the same for the shadow
      //   // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      // }
      var local_icons = {
        default_icon: {},
        red_icon: {
          iconUrl: '../icons/camera_yellow.png',
          iconSize: [48, 48], // size of the icon
          iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
        },
        green_icon: {
          iconUrl: '../icons/camera_green.png',
          iconSize: [48, 48], // size of the icon
          iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
        },
        red_sel_icon: {
          iconUrl: '../icons/camera_red_sel.png',
          iconSize: [48, 48], // size of the icon
          iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
        },
        green_sel_icon: {
          iconUrl: '../icons/camera_green_sel.png',
          iconSize: [48, 48], // size of the icon
          iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
        }
      }
      $scope.selectedMarkers = {
        selectedMarker: {
          lat: 46.48496,
          lng: 7.06748,
          // message: "I want to travel here!",
          focus: false,
          draggable: false,
          icon: local_icons.green_icon
        }
      }
      $scope.geojson = {}
      $scope.markers = {}
      $scope.map = {
        defaults: {
          crs: new L.Proj.CRS('EPSG:21781', '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 ' + '+k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs', {
            resolutions: [4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 250, 100, 50, 20, 10, 5, 2.5, 2, 1.5, 1, 0.5],
            origin: [420000, 350000]
          }),
          continuousWorld: true,
          zoomControl: false,
          searchControl: false
        },
        controls: {
        },
        layers: {
          baselayers: {
            swisstopo: {
              name: 'Swisstopo',
              type: 'xyz',
              url: 'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/21781/{z}/{y}/{x}.jpeg',
                // visible: true,
              layerOptions: {
                attribution: 'Map data &copy; 2015 swisstopo',
                maxZoom: $scope.maxZoom,
                minZoom: 16,
                showOnSelector: false
              }
            }
          },
          overlays: {
            draw: {
              name: 'draw',
              type: 'group',
              visible: true,
              layerParams: {
                showOnSelector: false
              }
            }
          }
        }
      }


      $scope.events = {
        markers: {
          enable: leafletMarkerEvents.getAvailableEvents()
        }
      }
      var clickEvent = 'leafletDirectiveMarker.click'
      $scope.$on(clickEvent, function (event, args) {
        $scope.selectImage(args.model.id)
      })
      var overEvent = 'leafletDirectiveMarker.mouseover'
      $scope.$on(overEvent, function (event, args) {
        $scope.selectImage(args.model.id)
      })

      leafletData.getMap().then(function (map) {
        leafletData.getLayers().then(function (baselayers) {
          var drawnItems = baselayers.overlays.draw
          map.on('draw:created', function (e) {
            var layer = e.layer
            $scope.createdROI = e.layer
            //drawnItems.addLayer(layer)
            map.addLayer(layer)
            $scope.search.roi.shape = layer.toGeoJSON()
          })

          if ($scope.bboxProvided.left) {
            var bound = [[$scope.bboxProvided.left, $scope.bboxProvided.bottom], [$scope.bboxProvided.right, $scope.bboxProvided.top]]
            map.fitBounds(bound)
          }

          $scope.bounds = map.getBounds()
          $scope.bbox = {
            top: $scope.bounds._northEast.lat,
            bottom: $scope.bounds._southWest.lat,
            left: $scope.bounds._southWest.lng,
            right: $scope.bounds._northEast.lng
          }
          // imageQueryService.getNotGeolocatedBbox($scope.bbox)
          //  .then(function (res) {
          //    $scope.notGeoref = res.nImages
          //  })
        })
      })


      // Image slider parameters
      // -----------------------
      $scope.nImagesSlider = 25
      $scope.images = [] //{}
      $scope.imageSelected = {}
      $scope.imagesInSlider = {}
      $scope.$watch('images', function () {
        $scope.imagesInSlider = {
          ids: [],
          images: []
        }
        for (var i=0; i<$scope.images.length; i++) { //
          if ($scope.imagesInSlider.ids.indexOf($scope.images[i].id) === -1){
            $scope.imagesInSlider.ids.push($scope.images[i].id)
            $scope.imagesInSlider.images.push($scope.images[i])
          }
        }
        $scope.imagesInSlider.ids = $scope.imagesInSlider.ids.slice(0, $scope.nImagesSlider)
        $scope.imagesInSlider.images = $scope.imagesInSlider.images.slice(0, $scope.nImagesSlider)
      })

      // Fake image to instanciate openseadragon
      $scope.o = {
        optionsViewer: {
          // tileSources: ["../images/tiles/1.dzi"],
          tileSources: [],
          prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
          gestureSettingsMouse: {
            clickToZoom: false,
            dblClickToZoom: false
          },
          showNavigator: false,
          showHomeControl: false,
          showFullPageControl: false,
          showZoomControl: false,
          overlays: []
        }
      }

      // Picture parameters
      // ------------------


      $scope.closePicture = function () {
        // $scope.showMap = true
        // $scope.showPicture = false
        // Change url
        $location.url('/map/')

        $scope.getAllImages(true)
        $scope.searchMode = false
        $scope.visitMode = 'map'
      }

      // Metainfo parameters
      // -------------------
      // $scope.metaBoxBool = true
      //
      $scope.showMeta = false
      $scope.showHideMeta = function () {
        $scope.showMeta ^= true
      }
      $scope.showInfo = true
      $scope.noObs = true
      $scope.switchInfoObs = function () {
        if ($scope.imageSelected.observation_enabled) {
          $scope.showInfo ^= true
          if (!$scope.showInfo){
            // Switch to the observation
            if ($scope.visitMode == 'globe'){
              $scope.switchToPicture()
            }

            // Get observations in the image
            observationsService.getImageObservations($scope.imageSelected.id).then(function (observations) {
              if (observations.length == 0){
                $scope.noObs = true
              } else {
                $scope.noObs = false
              }
              $scope.observations = []

              var overlays = []
              for (var i=0; i<observations.length; i++){
                var observation = observations[i]
                observation.id = i
                if (observation.coord_x){
                  if (i === observations.length-1){
                    osdService.drawRectangleObservation($scope.osd.mysd, observation, true)
                  }else{
                    osdService.drawRectangleObservation($scope.osd.mysd, observation, false)
                  }
                  $scope.observations.push(observation)
                }
              }
            })
          } else {
            $scope.createObsBool = false
            osdService.deleteAllObservations($scope.osd.mysd, $scope.observations)
          }
        }
      }

      $scope.createObsBool = false
      $scope.obsLocProvided = false
      $scope.newObservationId = 0
      $scope.obsProvided = false // to avoid to start multiple selections
      $scope.createObservation = function () {
        //$scope.createObsBool = true
        $scope.newObservationId += 1
        $scope.newObservation = {
          observation: null,
          volunteerId: null,
          coord_x: null,
          coord_y: null,
          height: null,
          width: null,
          id: $scope.newObservationId
        }
        // Hide observations
        osdService.deleteAllObservations($scope.osd.mysd, $scope.observations)

        // enable selection
        if ($scope.obsProvided){
          osdService.enableSelection($scope.osd.mysd, $scope.selectionValidation)
        }else{
          osdService.initializeSelection($scope.osd.mysd, $scope.selectionValidation)
          $scope.obsProvided = true
        }
      }

      $scope.selectionValidation = function (rect){
        // Stores rectangle
        $scope.newObservation.coord_x = rect.x
        $scope.newObservation.coord_y = rect.y
        $scope.newObservation.height = rect.height
        $scope.newObservation.width = rect.width
        // Draw rectangle
        osdService.disableSelection($scope.osd.mysd)
        osdService.drawRectangleInput($scope.osd.mysd, $scope.newObservation, $scope.submitObservation, $scope.cancelObservation)
        osdService.disableMouseNav($scope.osd.mysd)

        // Change the view
        $scope.obsLocProvided = true

        osdService.disableSelection($scope.osd.mysd)
      }
      $scope.cancelObservation = function () {
        $scope.createObsBool = false
        osdService.enableMouseNav($scope.osd.mysd)
      }
      $scope.obsSubmitted = false
      $scope.submitObservation = function (observation) {
        $scope.newObservation = observation
        // Change the view
        $scope.obsLocProvided = false
        // enable selection
        osdService.disableSelection($scope.osd.mysd)
        osdService.enableMouseNav($scope.osd.mysd)

        if (AuthService.isAuthenticated()){
          $scope.newObservation.username = '' //$scope.volunteer.username
          $scope.newObservation.date_string = ''
          $scope.observations.push($scope.newObservation)
          // Suppress old observation
          osdService.deleteObservation($scope.osd.mysd, $scope.newObservation)
          // Add new observation with text
          osdService.drawRectangleObservation($scope.osd.mysd, JSON.parse(JSON.stringify($scope.newObservation)), true)
          observationsService.submitObservation($scope.imageSelected.id, $scope.volunteer.id, $scope.newObservation).then(function(){
            // $scope.newObservation.observation = null
            // $scope.newObservation.volunteerId = null
            // $scope.newObservation.coord_x = null
            // $scope.newObservation.coord_y = null
            // $scope.newObservation.width = null
            // $scope.newObservation.height = null
            // $scope.newObservation.id = $scope.newObservation.id + 1
            $scope.obsSubmitted = true
          })
          $scope.createObsBool = false
        }else{
          var win = $window.open('/login', '_blank')
          var timer = setInterval(checkChild, 500)
          function checkChild () {
            if (win.closed) {
                // alert("Child window closed");
              clearInterval(timer)
              AuthService.loadUserCredentials()
              $scope.getMyName().then(function(){
                osdService.deleteObservation($scope.osd.mysd, $scope.newObservation)
                if ($scope.newObservation.coord_x != null){
                  // osdService.drawImageObservation($scope.osd.mysd, JSON.parse(JSON.stringify($scope.newObservation)))
                  osdService.drawRectangleObservation($scope.osd.mysd, JSON.parse(JSON.stringify($scope.newObservation)), true)
                }
                //osdService.drawImageObservation($scope.osd.mysd, $scope.newObservation)
                $scope.submitObservation()
              })
            }
          }
        }

      }
      $scope.editing = false
      $scope.startEdition = function () {
        $scope.edition = {
          title: $scope.imageSelected.title,
          caption: $scope.imageSelected.caption,
        }
        $scope.editing = true
      }
      $scope.cancelEdition = function () {
        $scope.editing = false
        $scope.edition = {
          title: $scope.imageSelected.title,
          caption: $scope.imageSelected.caption,
        }
      }
      $scope.submitEdition = function () {
        $scope.editing = false
        if ($scope.imageSelected.title === $scope.edition.title){
          $scope.edition.title = null // The title is not edited
        }
        if ($scope.imageSelected.caption === $scope.edition.caption){
          $scope.edition.caption = null // The caption is not edited
        }
        $scope.imageSelected.title = $scope.edition.title
        $scope.imageSelected.caption = $scope.edition.caption
        if (AuthService.isAuthenticated()){
          console.log('$scope.edition', $scope.edition)
          correctionsService.submitCorrection($scope.imageSelected.id, $scope.volunteer.id, $scope.edition)
        }else{
          var win = $window.open('/login', '_blank')
          var timer = setInterval(checkChild, 500)
          function checkChild () {
            if (win.closed) {
              clearInterval(timer)
              AuthService.loadUserCredentials()
              $scope.getMyName().then(function(){
                $scope.submitEdition()
              })

              //$window.location.reload()
            }
          }
        }
      }

      // Globe parameters
      // ---------------
      // Initialise the globe
      $scope.showGlobe = true
      $scope.globe = {
        cesium: null
      }

      $scope.closeGlobe = function () {
        // Change url
        $location.url('/map/')
        $scope.visitMode = 'map'
        // $scope.searchMode = false

        $scope.getAllImages(true)
      }
      $scope.scales = {
        linear: 1,
        log: 0
      }
      try {
        $scope.scales.log = Math.log10($scope.scales.linear)
      } catch (exception) {
        $scope.scales.log = Math.log($scope.scales.linear)/Math.log(10)
      }
      $scope.changeScaleLog = function () {
        $scope.scales.linear = Math.pow(10, $scope.scales.log)
        $scope.imageSelected.gltfModel.scale = $scope.scales.linear
      }
      $scope.scaleIn = function () {
        // Get scale
        var scale = $scope.imageSelected.gltfModel.scale
        try {
          var sLog = Math.log10(scale)
        } catch (exception) {
          var sLog = Math.log(scale)/Math.log(10)
        }

        sLog = sLog+0.1
        scale = Math.pow(10, sLog)
        $scope.imageSelected.gltfModel.scale = scale
      }
      $scope.scaleOut = function () {
        // Get scale
        var scale = $scope.imageSelected.gltfModel.scale
        try {
          var sLog = Math.log10(scale)
        } catch (exception) {
          var sLog = Math.log(scale)/Math.log(10)
        }
        sLog = sLog-0.1
        scale = Math.pow(10, sLog)
        $scope.imageSelected.gltfModel.scale = scale
      }
      $scope.zoom = function (direction) {
      // Hide toponyms and stories
        // var topoWasTrue = false
        // if ($scope.toponyms.show) {
        //   topoWasTrue = true
        //   $scope.toponyms.show = false
        //   topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)
        // }
        // var storyWasTrue = false
        // if ($scope.stories.show) {
        //   storyWasTrue = true
        //   $scope.stories.show = false
        //   storyService.deleteStories($scope.stories.dicStory, $scope.cesium)
        // }
        cesiumService.zoom($scope.globe.cesium, direction)
      // // Redraw
      //   if (topoWasTrue) {
      //     topoService.drawToponyms($scope.toponyms.geoJsonData, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
      //       $scope.toponyms.list = response.icons
      //       $scope.toponyms.geoJsonData = response.geoJsonData
      //       $scope.toponyms.show = true
      //     })
      //   }
      // // Redraw
      //   if (storyWasTrue) {
      //     storyService.drawStories($scope.stories.dicStory, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
      //       $scope.stories.dicStoryReady = response
      //       $scope.stories.show = true
      //     })
      //   }
      }

      // imageryProvider
      var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741)
      // terrainProvider
      var terrainProvider = new Cesium.CesiumTerrainProvider({
        url: '//3d.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/default/20160115/4326/',
        availableLevels: [8, 9, 10, 12, 14, 16, 17],
        rectangle: rectangle
      })
      $scope.terrainProvider = terrainProvider

      // $scope.drawImage = function(image){
      //   if (image.id != $scope.images.selectedImage.id){
      //     var tempBool = false
      //     var model = cesiumService.drawModel(image, $scope.cesium, 0.95, null, tempBool)
      //     $scope.images.dicImages[image.id].gltfModel = model
      //     $scope.images.dicImages[image.id].gltfLoaded = true
      //   }
      // }
      $scope.deleteModel = function(){
        cesiumService.deleteModel($scope.globe.cesium, $scope.imageSelected.gltfModel)
      }
      $scope.drawModel = function(){
        var tempBool = false
        var model = cesiumService.drawModel($scope.imageSelected, $scope.globe.cesium, 1, $scope.imageSelected.gltfModel, tempBool)
        $scope.imageSelected.gltfModel = model;
        $scope.imageSelected.gltfLoaded = true;
      }
      $scope.transparency = {
        value: 1
      }
      $scope.setTransparency = function (){
        cesiumService.changeModelTransparency($scope.imageSelected.gltfModel, $scope.transparency.value)
      }

      $scope.showInfoVisit = function(){
        if ($scope.visitMode === 'map'){
          $scope.showInfoVisitBool = true;
          $scope.showInfoGeolocBool = false;
          $scope.showInfoNoAprioriBool = false;
        }

      }
      $scope.hideInfoVisit = function(){
        $scope.showInfoVisitBool = false;
      }
      $scope.showInfoGeoloc = function(){
        if ($scope.visitMode === 'map'){
          $scope.showInfoGeolocBool = true;
          $scope.showInfoVisitBool = false;
          $scope.showInfoNoAprioriBool = false;
        }
      }
      $scope.hideInfoGeoloc = function(){
        $scope.showInfoGeolocBool = false;
      }
      $scope.showInfoNoApriori = function(){
        if ($scope.visitMode === 'map'){
          $scope.showInfoNoAprioriBool = true;
          $scope.showInfoGeolocBool = false;
          $scope.showInfoVisitBool = false;
        }
      }
      $scope.hideInfoNoApriori = function(){
        $scope.showInfoNoAprioriBool = false;
      }

      // Links buttons
      $scope.goHome = function () {
        url = '/'
        $window.open(url, '_blank')
      }
      $scope.goToDownload = function () {
        imageQueryService.incrementDownloads($scope.imageSelected.id).then(function () {
          // $window.open($scope.imageSelected.download_link, '_blank')
          var win = $window.open('', '_blank')
          win.location = $scope.imageSelected.download_link
        })
      }
      $scope.goToShop = function () {
        $window.open($scope.imageSelected.shop_link, '_blank')
      }
      $scope.goToGeoref = function () {
        url = '/georef/?imageId=' + $scope.imageSelected.id + '&lat=' + $scope.imageSelected.latitude + '&long=' + $scope.imageSelected.longitude
        $window.open(url, '_blank')
      }
      $scope.goToGeorefNoApriori = function () {
        url = '/georef/?imageId=' + $scope.imageSelected.id
        $window.open(url, '_blank')
      }
      $scope.goToHelp = function () {
        url = '/help/'
        $window.open(url, '_blank')
      }
      $scope.goToMy = function () {
        url = '/mysmapshot/'
        $window.open(url, '_blank')
      }
      $scope.goToLink = function () {
        url = $scope.imageSelected.link
        $window.open(url, '_blank')
      }
      $scope.goToCollectionLink = function () {
        var promise = imageQueryService.getCollectionLink($scope.imageSelected.collection_id)
        var win = $window.open('', '_blank')
        promise.then(function (res) {
          win.location = res.link
        })
      }
      $scope.goToOwnerLink = function () {
        var promise = imageQueryService.getOwnerLink($scope.imageSelected.owner_id)
        var win = $window.open('', '_blank')
        promise.then(function (res) {
          win.location = res.link
        })
      }
      $scope.goToPhotographerLink = function () {
        var win = $window.open('', '_blank')
        var promise = imageQueryService.getPhotographerLink($scope.imageSelected.photographer_id)
        promise.then(function (res) {
          win.location = res.link
        })
      }

      // Get images without apriori propositions
      // ---------------------------------------
      $scope.getNoApriori = function () {
        imageQueryService.getNoApriori($scope.collectionId, $scope.ownerId).then(function (images) {
          $scope.images = images
          $scope.imageSelected = $scope.images[0]
          $scope.switchOsdPicture()
        })
      }
      // If the page is open in picture mode
      $scope.getNearestImages = function (){
        return $q(function (resolve, reject) {
          // if ($scope.searchMode === false) {
            // Update image list and slider
            if ($scope.searchMode){
              filter = $scope.search
            }else{
              filter = null
            }
            imageQueryService.getNearest($scope.imageSelected.id, $scope.imageSelected.georef_3d_bool, $scope.collectionId, $scope.ownerId, filter)
            .then(function (images) {
                $scope.images = images
                $scope.imagesInSlider = {}
                var listIds = []
                var listImages = []
                for (id in $scope.images) {
                  var image = $scope.images[id]
                  // var point = turf.point([image.longitude, image.latitude])
                  // var selectedPoint = turf.point([$scope.imageSelected.longitude, $scope.imageSelected.latitude])
                  // var dist = turf.distance(point, selectedPoint, 'kilometers')
                  // image.dist = dist
                  listIds.push(image.id)
                  listImages.push(image)
                }
              $scope.imagesInSlider.ids = listIds.slice(0, $scope.nImagesSlider)
              $scope.imagesInSlider.images = listImages.slice(0, $scope.nImagesSlider)
              resolve()
            })
          // } else {
          //   // empty slider
          //   $scope.imagesInSlider = {}
          //   var listIds = []
          //   var listImages = []
          //   for (id in $scope.images) {
          //     var image = $scope.images[id]
          //     var point = turf.point([image.longitude, image.latitude])
          //     var selectedPoint = turf.point([$scope.imageSelected.longitude, $scope.imageSelected.latitude])
          //     var dist = turf.distance(point, selectedPoint, 'kilometers')
          //     image.dist = dist
          //     listIds.push(image.id)
          //     listImages.push(image)
          //   }
          //   $scope.imagesInSlider.ids = listIds //.slice(0, $scope.nImagesSlider)
          //   $scope.imagesInSlider.images = listImages // .slice(0, $scope.nImagesSlider)
          //   resolve()
          // }
        })
      }
  // Get all images for leaflet
  // --------------------------
      $scope.getAllImages = function (deleteBool) {
    // var sendData = {
    //   collectionId: $scope.collectionId,
    //   ownerId: $scope.ownerId
    // }
        if (deleteBool) {
          leafletData.getMap().then(function (map) {
            map.eachLayer(function (layer) {
              if ((!layer._url)  && (!layer.dragging)) {
                map.removeLayer(layer)
              }
            })
            try {
              map.removeLayer($scope.markersRed)
            }catch (err){
              console.log('markers layers doesnt exists')
            }
            try {
              map.removeLayer($scope.markersGreen)
            }catch (err){
              console.log('markers layers doesnt exists')
            }
            if ($scope.searchMode){
              filter = $scope.search
            }else{
              filter = null
            }
            imageQueryService.getMapImages($scope.collectionId, $scope.ownerId, $scope.geolocalized, filter).then(function (data) {

              var geojson = data.geojson
              $scope.images = data.images

              if ($scope.geolocalized){
                $scope.markersGreen = leafService.drawImages(geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                map.addLayer($scope.markersGreen)
              } else {
                $scope.markersRed = leafService.drawImages(geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                map.addLayer($scope.markersRed)
              }
              // Select the last image
              $scope.imageSelected = $scope.images[0]
            })
          })
        }else{
        }
      }
  // Get images in leaflet bbox
      $scope.getImagesBbox = function (deleteBool) {
    // var sendData = {
    //   bbox: $scope.bbox,
    //   collectionId: $scope.collectionId
        if (deleteBool) {
          leafletData.getMap().then(function (map) {
            map.eachLayer(function (layer) {
              if ((!layer._url) && (!layer.dragging)) {
                map.removeLayer(layer)
              }
            })
            try {
              map.removeLayer($scope.markersRed)
            }catch (err){
              console.log('markers red layers doesnt exists')
            }
            try {
              map.removeLayer($scope.markersGreen)
            }catch (err){
              console.log('markers green layers doesnt exists')
            }
            if ($scope.searchMode){
              filter = $scope.search
            }else{
              filter = null
            }
            imageQueryService.getMapImagesInBbox($scope.collectionId, $scope.ownerId, $scope.geolocalized, $scope.bbox, filter).then(function (data) {
              var geojson = data.geojson
              if ($scope.geolocalized){
                $scope.markersGreen = leafService.drawImages(geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                map.addLayer($scope.markersGreen)
              } else {
                $scope.markersRed = leafService.drawImages(geojson, $scope.geolocalized, $scope.mouseOverSlider, $scope.clickSlider)
                map.addLayer($scope.markersRed)
              }

              $scope.images = data.images

              // Select the last image
              $scope.imageSelected = $scope.images[0]

            })
          })
        }else {

        }
    // }

      }

      // Map move event
      var timeoutHandler // For the lag after moveend
      $scope.map2 = leafletData.getMap().then(function (map) {
        function formatJSON (rawjson) {	// callback that remap fields name
          var json = {},
            key, loc, disp = []

          for (var i in rawjson) {
            disp = rawjson[i].display_name.split(',')

            key = disp[0] + ', ' + disp[1]

            loc = L.latLng(rawjson[i].lat, rawjson[i].lon)

            json[ key ] = loc	// key,value format
          }

          return json
        }

        var searchOpts = {
          url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
          jsonpParam: 'json_callback',
          formatData: formatJSON,
          zoom: 18,
          minLength: 2,
          autoType: false,
          marker: {
            icon: false,
            animate: false
          }
        }

        map.addControl( new L.Control.Search(searchOpts) );

        $scope.bounds = map.getBounds()
        $scope.bbox = {
          top: $scope.bounds._northEast.lat,
          bottom: $scope.bounds._southWest.lat,
          left: $scope.bounds._southWest.lng,
          right: $scope.bounds._northEast.lng
        }
        if ($scope.initMode === 'map'){
          // $scope.getAllImages(true)
          imageQueryService.getNumberNotGeoref($scope.ownerId, $scope.collectionId).then(function(res){
            $scope.nNotGeoref = res
          })
        }



        var moveEndEvent = 'leafletDirectiveMap.moveend'
        $scope.$on(moveEndEvent, function () {
          window.clearTimeout(timeoutHandler)
          // create new timeout to fire getImages event after 500ms
          timeoutHandler = $window.setTimeout(function() {
            leafletData.getMap().then(function (map) {
              $scope.bounds = map.getBounds()
              $scope.bbox = {
                top: $scope.bounds._northEast.lat,
                bottom: $scope.bounds._southWest.lat,
                left: $scope.bounds._southWest.lng,
                right: $scope.bounds._northEast.lng
              }
              if ($scope.searchMode === false) {
            // Update image list and slider
                $scope.getImagesBbox(true)
              } else {
            // empty slider
                $scope.imagesInSlider = {}
            // Clip images in the zoom area
                var polygon = turf.polygon([[
             [$scope.bounds._southWest.lng, $scope.bounds._southWest.lat],
             [$scope.bounds._southWest.lng, $scope.bounds._northEast.lat],
             [$scope.bounds._northEast.lng, $scope.bounds._northEast.lat],
             [$scope.bounds._northEast.lng, $scope.bounds._southWest.lat],
             [$scope.bounds._southWest.lng, $scope.bounds._southWest.lat]
                ]], {})
            // Check if point is imagesInSlider
                var listIds = []
                var listImages = []
                for (id in $scope.images) {
                  var image = $scope.images[id]
                  var point = turf.point([image.longitude, image.latitude])
                  var isInside = turf.inside(point, polygon)
                  if (isInside) {
                    if (listIds.indexOf(image.id) === -1){
                      listIds.push(image.id)
                      listImages.push(image)
                    }
                  }
                }
                $scope.imagesInSlider.ids = listIds.slice(0, $scope.nImagesSlider)
                $scope.imagesInSlider.images = listImages.slice(0, $scope.nImagesSlider)
              }
            })
          }, 800) // lag in ms
        })
      })
      // Parmameters of the search bar
      $scope.getGPSCoordinates = function(input) {
        return $q(function (resolve, reject) {
          var url = "https://nominatim.openstreetmap.org/search?format=json&countrycodes=ch&q=" + input
          $http.get(url).then(function(gpsJSON) {
            resolve(gpsJSON.data)
          })
        })
      }
      $scope.selectInputLocation = function ($item, $model, $label, $event) {
        // Zoom to selected item
        leafletData.getMap().then(function (map) {
          map.setView([$item.lat, $item.lng], 19)
        })

      }

      // Parameters of the collection filter
      $scope.selectAllCollection = function (id){
        $scope.selectedCollection.name = $scope.texts.everyCollection[$scope.lang]
        $scope.collectionId = null
        for (var i=0; i<$scope.collections.length; i++){
          $scope.collections[i].selected = false
        }
        $scope.getImagesBbox(true)
        $location.search('')
      }
      $scope.selectGetCollection = function (id){
        $scope.collectionId = id
        $scope.selectCollection()
        // Put the collection in the filter
        for (var i=0; i<$scope.collections.length; i++){
          if ($scope.collections[i].id == id){
            $scope.search.collections.selected = [$scope.collections[i]]
          }
        }
        //
        imageQueryService.getNumberNotGeoref($scope.ownerId, $scope.collectionId).then(function(res){
          $scope.nNotGeoref = res
          $scope.getImagesBbox(true)
        })
      }

      // Bad geoloc
      $scope.showBadGeolocButton = true
      $scope.submitBadGeoloc = function(){
        badGeolocService.submit($scope.imageSelected.id, $scope.volunteer.id)
        $scope.showBadGeolocButton = false
      }
    }])
