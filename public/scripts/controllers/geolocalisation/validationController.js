georefApp.controller('validationCtrl', ['$scope', '$window', 'errorsService',
'validationsService',
  function ($scope, $window, errorsService, validationsService) { // FileSaver' , 'Blob'

  console.log('scope validation', $scope.validation)
  $scope.getAllErrors = function () {
    errorsService.getAllErrors().then(function (errors) {
      $scope.allErrors = errors

      //Create error model
      var n = Object.keys(errors).length
      for (var i=0; i<n; i++) {
        var e = $scope.allErrors[i]
        e.is_checked = false
        var text = e.translations
        e.translations = JSON.parse(e.translations)
        if (e.title === 'Other') {
          $scope.otherErrorIndex = i
          e.validated = false
          e.text = ''
        }
      }
      $scope.errorsRemark = {
        text: '',
        validated: false,
        checked: false

      }
    })

  }
  $scope.getAllErrors()

  $scope.validateOtherReason = function () {
    $scope.allErrors[$scope.otherErrorIndex].validated = true
  }
  $scope.validateErrorsRemark = function () {
    $scope.errorsRemark.validated = true
  }

  $scope.submitValidation = function () {
    console.log('submited validation', $scope.validation)
    $scope.validation.errors = $scope.allErrors
    $scope.validation.remark = $scope.errorsRemark
    //validationsService.submitValidation($scope.imageId, $scope.volunteerId, $scope.validatorId, $scope.validation).then(function () {
    validationsService.submitValidation(parseInt($scope.imageId), $scope.validation.volunteerId, $scope.validatorId, $scope.validation).then(function () {
      // Close tab
      $window.close()
    })

  }
  // $scope.saveReasons = function () {
  //
  // }
}])
