helpApp.controller('helpCtrl', ['$scope', '$http', '$window', '$anchorScroll',
'$location', '$uibModal', 'AuthService', 'searchService', 'textService',
'langService',
function ($scope, $http, $window, $anchorScroll, $location, $uibModal, AuthService,
  searchService, textService, langService) {



  $scope.errors = {
    'error1': {
      isOpen: false
    },
    'error2': {
      isOpen: false
    },
    'error3': {
      isOpen: false
    },
    'error4': {
      isOpen: false
    },
    'error5': {
      isOpen: false
    }
  }

  $scope.problems = {
    // This view point doesnt exists
    'prob1': {
      isOpen: false
    },
    // Invalid position
    'prob2': {
      isOpen: false
    },
    // I cant validate the geolocalisation of a picture.
    'prob3': {
      isOpen: false
    },
    // I cant compute the location of a <b>vertical</b> aerial photo.
    'prob4': {
      isOpen: false
    }
  }

  $scope.tutorial = {
    // Chose an image
    'tuto1': {
      isOpen: false
    },
    // Provide the photographer's location
    'tuto2': {
      isOpen: false
    },
    // Provide the photo's direction
    'tuto3': {
      isOpen: false
    },
    // Align the photo with the 3D landscape
    'tuto4': {
      isOpen: false
    },
    // Share observations and improve known information
    'tuto5': {
      isOpen: false
    }
  }
  // Get lang
  // --------
  $scope.lang = langService.getBrowserLang()
  // Switch the lang
  $scope.switchLang = function (lang) {
    $scope.lang = lang
  }
  // Get languages model
  $scope.langs = langService.getLangs()

  // Get user
  // --------
  $scope.getMyName = function () {
    return $q(function (resolve, reject) {
      if (AuthService.isAuthenticated()) {
        $http.get('/volunteers/memberinfo').then(function (result) {
          $scope.volunteer = result.data.volunteer
          $scope.myName = $scope.volunteer.username
          $scope.isLogged = true
          if ($scope.volunteer.lang){
            $scope.lang = $scope.volunteer.lang
          }
          resolve()
        })
      } else {
        resolve()
      }
    })
  }
  // Get the texts
  // -------------
  $scope.loadTextPromise = textService.loadText('help').then(function (texts) {
    $scope.texts = texts
    console.log('$scope.texts', $scope.texts)
  })

  $scope.closeTuto = function () {
    //$scope.tutorial.isOpen = false
    var keys = Object.keys($scope.tutorial)
    var n = keys.length
    for (var i = 0; i < n; i++) {
      $scope.tutorial[keys[i]].isOpen = false
    }
  }

  $scope.closeProblems = function () {
    var keys = Object.keys($scope.problems)
    var n = keys.length
    for (var i = 0; i < n; i++) {
      $scope.problems[keys[i]].isOpen = false
    }
  }

  $scope.closeErrors = function () {
    var keys = Object.keys($scope.errors)
    var n = keys.length
    for (var i = 0; i < n; i++) {
      $scope.errors[keys[i]].isOpen = false
    }
  }

  // Scroll to error
  // ---------------
  $scope.goToAnchor = function (anchorId) {
    console.log('anchor id', anchorId, anchorId.slice(0,4))
    // Check the type of anchor
    if (anchorId.slice(0,5) === 'error'){
      // Open error
      try {
        $scope.errors[anchorId].isOpen = true
        // Scroll
        $location.hash(anchorId)
        $anchorScroll()
      } catch (err) {
        console.log('Error in initial scroll: invalid error', err)
      }
    }else if (anchorId.slice(0,4) === 'tuto'){
      // Open tuto
      try {
        $scope.tutorial[anchorId].isOpen = true
        // Scroll
        $location.hash(anchorId)
        $anchorScroll()
      } catch (err) {
        console.log('Error in initial scroll: invalid tuto', err)
      }

    }else if (anchorId.slice(0,4) === 'prob'){
      // Open tuto
      try {
        $scope.problems[anchorId].isOpen = true
        // Scroll
        $location.hash(anchorId)
        $anchorScroll()
      } catch (err) {
        console.log('Error in initial scroll: invalid prob', err)
      }
    }
  }
  // Test if the url contain an anchor
  $scope.anchorId = $location.hash()
  if (($scope.errorId != null) || ($scope.errorId !== '') || ($scope.errorId !== None)) {
    $scope.goToAnchor($scope.anchorId)
  }

  // if (($scope.errorId != null) || ($scope.errorId !== '') || ($scope.errorId !== None)) {
  // if ($scope.errorId) {
  //   if($scope.errorId != 'invalid'){ // Ugly trick to open invalid pose help
  //     // Jump to the error
  //     $scope.goToError($scope.errorId)
  //
  //     $scope.closeTuto()
  //   }else{
  //     $scope.problems['prob2'].isOpen = true
  //     $scope.closeTuto()
  //   }
  // }

  $scope.clickProblem  = function () {
    $scope.closeTuto()
    $scope.closeErrors()
  }

  $scope.clickError  = function () {
    $scope.closeTuto()
    $scope.closeProblems()
  }

  $scope.clickTitle  = function () {
    $scope.closeProblems()
    $scope.closeErrors()
  }

  // Scroll to titles
  $scope.goToTuto = function () {
    // Open error
    $scope.tutorial.isOpen = true
    // Close other
    $scope.closeErrors()
    $scope.closeProblems()
    // Scroll
    $location.hash('tutorial')
    $anchorScroll()
  }

  $scope.goToErrors = function () {
    // Open error
    $scope.errors['error1'].isOpen = true
    // Close other
    $scope.closeTuto()
    $scope.closeProblems()
    // Scroll
    $location.hash('errors')
    $anchorScroll()
  }

  $scope.goToProblems = function () {
    // Open error
    $scope.problems['prob1'].isOpen = true
    // Close other
    $scope.closeTuto()
    $scope.closeErrors()
    // Scroll
    $location.hash('problems')
    $anchorScroll()
  }

  // Links
  $scope.goToGeoref = function () {
    location.href = '/map/?geolocalized=false'
  }
  $scope.goToMap = function () {
    location.href = '/map/'// '?collectionId='+$scope.challengeCollection.id;//'; //faire passer l'id de la collection?
  }
}])
