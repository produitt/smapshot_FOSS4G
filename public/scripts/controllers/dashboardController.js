dashboardApp.controller('dashboardCtrl', ['$scope', '$http', '$window', '$uibModal',
'$q','AuthService',
'searchService', 'dbService', 'validationsService',
'volunteersService', 'correctionsService', 'remarksService', 'observationsService',
'imageQueryService', 'collectionsQueryService', 'ownerQueryService', 'problemsService',
'badGeolocService',
'$anchorScroll', '$location',
'$timeout', //  'errorsService',
function ($scope, $http, $window, $uibModal, $q, AuthService, searchService,
  dbService, validationsService, volunteersService, correctionsService,
  remarksService, observationsService, imageQueryService, collectionsQueryService,
  ownerQueryService, problemsService, badGeolocService,
  $anchorScroll, $location, $timeout) { // errorsService,



  // SWITCH MODES
  // ------------
  $scope.modes = {
    0: 'geolocalizations',
    1: 'observations',
    2: 'metadata',
    3: 'images',
    4: 'ranking',
    5: 'mailing',
    6: 'download',
    7: 'roles',
    8: 'notLogged',
    9: 'problems',
    10: 'badGeoloc'
  }
  $scope.currentMode = 8

  $scope.switchTo = function (val) {
    if (val === 'metadata') {
      $location.url('metadata')
      $scope.currentMode = 2
      $scope.switchToCorrections()
    } else if (val === 'observations') {
      $location.url('observations')
      $scope.currentMode = 1
      $scope.switchToObservations()
    } else if (val === 'geolocalizations') {
      $location.url('geolocalizations')
      $scope.currentMode = 0
      $scope.switchToGeolocalizations()
    } else if (val === 'images') {
      $location.url('images')
      $scope.currentMode = 3
      $scope.switchToImages()
    } else if (val === 'ranking') {
      $location.url('ranking')
      $scope.currentMode = 4
      $scope.switchToRanking()
    }else if (val === 'mailing') {
      $location.url('mailing')
      $scope.currentMode = 5
      $scope.switchToMailing()
    }else if (val === 'download') {
      $location.url('download')
      $scope.currentMode = 6
      $scope.switchToDownload()
    }else if (val === 'roles') {
      $location.url('roles')
      $scope.currentMode = 7
      $scope.switchToRoles()
    }else if (val === 'problems') {
      $location.url('problems')
      $scope.currentMode = 9
      $scope.switchToProblems()
    }else if (val === 'badGeoloc') {
      $location.url('badGeoloc')
      $scope.currentMode = 10
      $scope.switchToBadGeoloc()
    }
    else {
      $scope.currentMode = 0
    }
  }


  $scope.openDoc = function(type) {
    if (type === 'geolocalizations'){
      $window.open('./doc/geolocalizations.pdf')
    }
    else if (type === 'problems'){
      $window.open('./doc/problems.pdf')
    }
    else if (type === 'downloads'){
      $window.open('./doc/downloads.pdf')
    }
  }

  // $scope.getToValidateImages = function () {
  //   if (AuthService.isAuthenticated()) {
  //     $http.get('/volunteers/memberinfo').then(function (result) {
  //       $scope.validator = result.data.volunteer
  //       $scope.isValidatorOnly = false
  //       $scope.isSuperAdmin = false
  //       // Check validator status
  //       if ($scope.validator.super_admin ||
  //         $scope.validator.owner_admin != null ||
  //         $scope.validator.owner_validator != null) {
  //         // is super admin
  //         $scope.isSuperAdmin = true
  //
  //         if (!$scope.validator.super_admin){
  //           if ($scope.validator.owner_admin != null){
  //             // Is the owner admin
  //             $scope.ownerId = $scope.validator.owner_admin
  //             $scope.isSuperAdmin = false
  //           }
  //           else if ($scope.validator.owner_validator != null){
  //             // Is a owner validator
  //             $scope.ownerId = $scope.validator.owner_validator
  //             $scope.isValidatorOnly = true
  //             $scope.isSuperAdmin = false
  //           }
  //         }
  //       }
  //       $scope.switchToGeolocalizations()
  //     })
  //   } else {
  //     $scope.redirectLogin()
  //   }
  // }

  $scope.ownerId = null
  $scope.getMyName = function () {
    if (AuthService.isAuthenticated()) {
      $http.get('/volunteers/memberinfo').then(function (result) {
        $scope.validator = result.data.volunteer
        $scope.isSuperAdmin = false
        $scope.isValidatorOnly = false
        $scope.isOwnerAdmin = false
        // Check validator status
        if ($scope.validator.super_admin ||
          $scope.validator.owner_admin != null ||
          $scope.validator.owner_validator != null) {
          // is super admin
          $scope.isSuperAdmin = true
          $scope.isValidatorOnly = false
          $scope.isOwnerAdmin = false

          if (!$scope.validator.super_admin){
            if ($scope.validator.owner_admin != null){
              // Is the owner admin
              $scope.ownerId = $scope.validator.owner_admin
              $scope.isSuperAdmin = false
              $scope.isValidatorOnly = false
              $scope.isOwnerAdmin = true
            }
            else if ($scope.validator.owner_validator != null){
              // Is a owner validator
              $scope.ownerId = $scope.validator.owner_validator
              $scope.isSuperAdmin = false
              $scope.isValidatorOnly = true
              $scope.isOwnerAdmin = false
            }
          }
          //$scope.ownerId = 4
        }
        $scope.currentMode = 0
        // Get the list of the owners
        ownerQueryService.getList().then(function (response) {
          $scope.owners = response
          // Get the list of the collections
          collectionsQueryService.getList($scope.ownerId).then(function (response) {
            $scope.collections = response

            // Process the url
            // Recognize the mode
            // ------------------
            var urlParts = $window.location.href.split('#')
            // Detect mode
            if (urlParts[1]){
              var mode = urlParts[1].replace('/','')
              console.log('mode', mode)
              $scope.switchTo(mode)
            }else{
              $scope.switchTo('geolocalizations')
            }
            // Get the list of the validators
            volunteersService.getValidatorsListByOwner($scope.ownerId).then(function (response) {
              $scope.validators = response
              //$scope.getToValidateImages()
            })
          })
        })
      })
    }
  }
  $scope.getMyName()



  $scope.redirectLogin = function () {
    if (!AuthService.isAuthenticated()) {
      var win = $window.open('/login', '_blank')
      var timer = setInterval(checkChild, 500)
      function checkChild () {
        if (win.closed) {
          clearInterval(timer)
          AuthService.loadUserCredentials()
          $scope.getMyName()
          //$window.location.reload()
        }
      }
    }else{
      $scope.currentMode = 0
      $scope.getMyName()
      // $scope.getToValidateImages()
    }
  }



  // Functions for the geolocalization
  // ---------------------------------
  $scope.switchToGeolocalizations = function () {
    console.log('switch to geoloc')
    $scope.resetFilter()
    // Set filter for initial get
    var filter = $scope.imageFilter
    filter.owner.id = $scope.ownerId
    filter.processed = false
    // Mode is geolocalisation
    validationsService.getDashboard(filter).then(function (response) {
      $scope.toValidateImages = response
      $scope.nTask = response.length
    })
  }

  // Validate geolocation
  $scope.validateGeoloc = function (imageId, volunteerId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.validateGeoloc(imageId)
      promise.then(function () {

        $scope.validation = {}
        $scope.validation.imageId = imageId
        $scope.validation.volunteerId = volunteerId // id of the volunteer who geolocalize this image
        $scope.validation.validatorId = $scope.validator.id // id of the logged user
        $scope.validation.status = 'validated'

        validationsService.submitValidation(imageId, volunteerId, $scope.validator.id, $scope.validation).then(function() {
          // Set validated status
          for (var i in $scope.toValidateImages) {
            if ($scope.toValidateImages[i].image_id === imageId) {
              $scope.toValidateImages[i].processed = true
              //var index = i
            }
          }
          //$scope.toValidateImages.splice(index, 1)
        })

      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Reject geolocation
  $scope.rejectGeoloc = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.rejectGeoloc(imageId)
      promise.then(function () {
        // Supress from the list
        for (var i in $scope.toValidateImages) {
          if ($scope.toValidateImages[i].id == imageId) {
            var index = i
          }
        }
        $scope.toValidateImages.splice(index, 1)
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Add image in the game
  $scope.addToGame = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.addToGame(imageId)
      promise.then(function () {
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Remove image from the game
  $scope.removeFromGame = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.removeFromGame(imageId)
      promise.then(function () {
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Reload images
  // $scope.reloadValidations = function () {
  //   console.log('reload')
  //   $scope.getToValidateImagesPromise = imageQueryService.getToValidateImages($scope.ownerId)
  //   $scope.getToValidateImagesPromise.then(function (response) {
  //     $scope.toValidateImages = response
  //   })
  // }

  // Functions for the observations
  // ------------------------------
  $scope.switchToObservations = function () {
    $scope.resetFilter()
    var filter = JSON.parse(JSON.stringify($scope.imageFilterIni)) //Deepcopy trick
    filter.owner.id = $scope.ownerId
    observationsService.getDashboard(filter).then(function (response) {
      $scope.toValidateObservations = response
      $scope.nTask = response.length
    })
    // observationsService.getToValidateObservations($scope.ownerId).then(function (response) {
    //   $scope.toValidateObservations = response
    // })
  }
  // $scope.reloadObservations = function (){
  //   observationsService.getDashboard(filter).then(function (response) {
  //     $scope.toValidateObservations = response
  //   })
  //   // observationsService.getToValidateObservations($scope.ownerId).then(function (response) {
  //   //   $scope.toValidateObservations = response
  //   // })
  // }

  // Functions for the corrections
  // -----------------------------
  $scope.switchToCorrections = function () {
    // Reset the filter
    $scope.resetFilter()
    // Set the filter for the initial query
    var filter = JSON.parse(JSON.stringify($scope.imageFilterIni)) //Deepcopy trick
    filter.owner.id = $scope.ownerId
    correctionsService.getDashboard(filter).then(function (response) {
      $scope.toValidateCorrections = response
      $scope.nTask = response.length
    })
  }

  // Links
  // -----
  $scope.goHome = function () {
    $window.open('/map/')
  }
  $scope.goToMono = function (imageId) {
    $window.open('/map/?imageId=' + imageId)
  }
  $scope.goToHelp = function () {
    // location.href = '/georef/?imageId='+imageId;
    $window.open('/help/')
  }
  $scope.goToGeoref = function (imageId) {
    // Switch image to the status validated
    for (var i=0; i<$scope.toValidateImages.length; i++){
      var im = $scope.toValidateImages[i]
      if (im.id === imageId){
        im.processed = true
      }
    }
    // Open the image in the georeferencer
    $window.open('/georef/?imageId=' + imageId)
  }
  $scope.goToLogin = function (imageId) {
    // location.href = '/georef/?imageId='+imageId;
    $window.open('/georef/?imageId=' + imageId)
  }

  // $scope.getReward = function (imageId) {
  //   if (AuthService.isAuthenticated()) {
  //     var sendData = {
  //       imageId: imageId,
  //       volunteerId: $scope.validator.id
  //     }
  //     win = $window.open('', '_blank')
  //     var prom = $http.get('http://smapshot.heig-vd.ch/reward/' + imageId + '/' + $scope.validator.id)
  //     // var httpPromise =  $http.post('/reward', sendData)
  //     prom.then(function (res) {
  //       $scope.imageP = res.data
  //       $scope.showIm = true
  //       // $window.open("data:image/jpg;base64, " + res.data);
  //       win.location = 'data:image/jpg;base64, ' + res.data
  //     })
  //   }
  // }
  //
  // $scope.logout = function () {
  //   AuthService.logout()
  //   location.href = '/home/'
  //   // $state.go('outside.login');
  // }
  //
  // $scope.updateVolunteer = function () {
  //   AuthService.update($scope.validator)
  // }

// Ordering validation images
// --------------------------
  $scope.propertyName = 'id'
  $scope.reverse = true

  $scope.sortBy = function (propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false
    $scope.propertyName = propertyName
  }

  // Ranking
  // -------
  $scope.switchToRanking = function(){
    ownerQueryService.getList().then(function(response){
      $scope.owners = response
    })
  }
  $scope.generateStringDate = function (date) {
    // var split = date.split('/')
    // var month = split[1]
    // var year = split[2]
    // var date = split[0]
    // // Generate start and end dates string
    // if (month.toString().length < 2) {
    //   var stringMonth = '0' + month
    // } else {
    //   var stringMonth = month
    // }
    // if (date.toString().length < 2) {
    //   var stringDate = '0' + month
    // } else {
    //   var stringDate = month
    // }
    var stringDate = [year, stringMonth, stringDate].join('-')
    var month = date.getMonth()+1
    var year = date.getFullYear()
    var date = date.getDate()
    // Generate start and end dates string
    if (month.toString().length < 2) {
      var stringMonth = '0' + month
    } else {
      var stringMonth = month
    }
    var stringDate = [year, stringMonth, date].join('-')
    return stringDate
  }
  $scope.selectRankingType = function(type) {
    $scope.rankingFilter.type = type
  }
  $scope.getRanking = function(){
    // var startDate = $scope.generateStringDate($scope.rankingFilter.dates.min)//.replace(/-/g, '/') //
    // var stopDate = $scope.generateStringDate($scope.rankingFilter.dates.max)//.replace(/-/g, '/') //

    // All year is selected
    if (($scope.rankingFilter.selectedYear === 'All') || ($scope.rankingFilter.selectedMonth === 0)){
      if ($scope.rankingFilter.selectedYear === 'All'){
        var startDate = '2016-01-01'
        var stopDate = $scope.rankingFilter.currentYear+'-12-31'
      }
      // All month is selected
      else if ($scope.rankingFilter.selectedMonth === 0){
        var startDate = $scope.rankingFilter.selectedYear+'-01-01'
        var stopDate = $scope.rankingFilter.selectedYear+'-12-31'
      }
    }else {
      var startDate = $scope.generateStringDate(new Date($scope.rankingFilter.selectedYear, $scope.rankingFilter.selectedMonth-1, 1))
      var stopDate = $scope.generateStringDate(new Date($scope.rankingFilter.selectedYear, $scope.rankingFilter.selectedMonth, 0))
    }
    var searchData = {
      ownerId: $scope.rankingFilter.owner.id,
      collectionId: $scope.rankingFilter.collection.id,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: $scope.rankingFilter.type
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  // Date selection
  // Initialize start date
  $scope.rankingFilter = {
    owner: {
      name: 'All',
      id: null
    },
    collection: {
      name: 'All',
      id: null
    },
    type: 'geoloc',
    dates: {
      min: null,
      max: null
    }
  }
  $scope.rankingFilter.currentDate = new Date() // Today
  // Get previous month
  $scope.rankingFilter.currentYear = $scope.rankingFilter.currentDate.getFullYear()
  $scope.rankingFilter.currentMonth = $scope.rankingFilter.currentDate.getMonth() + 1

  $scope.rankingFilter.selectedYear = $scope.rankingFilter.currentYear
  $scope.rankingFilter.selectedMonth = $scope.rankingFilter.currentMonth

  // list of years
  $scope.listYears = []
  for (var i = 2017; i<=$scope.rankingFilter.currentYear; i++){
    $scope.listYears.push(i)
  }
  $scope.listYears.push('All')

  // list of months
  $scope.listMonths = {
    0: {fr: 'All', en: 'All', de: 'All', it: 'All', mid: 0, short: 'All'},
    1: {fr: 'Janvier', en: 'January', de: 'Januar', it: 'Gennaio', mid: 1, short: 'Jan'},
    2: {fr: 'Février', en: 'February', de: 'Februar', it: 'Febbraio', mid: 2, short: 'Feb'},
    3: {fr: 'Mars', en: 'March', de: 'März', it: 'Marzo', mid: 3, short: 'Mar'},
    4: {fr: 'Avril', en: 'April', de: 'April', it: 'Aprile', mid: 4, short: 'Apr'},
    5: {fr: 'Mai', en: 'May', de: 'Mai', it: 'Maggio', mid: 5, short: 'Mai'},
    6: {fr: 'Juin', en: 'June', de: 'Juni', it: 'Giugno', mid: 6, short: 'Jun'},
    7: {fr: 'Juillet', en: 'July', de: 'Juli', it: 'Luglio', mid: 7, short: 'Jul'},
    8: {fr: 'Août', en: 'August', de: 'August', it: 'Agosto', mid: 8, short: 'Aug'},
    9: {fr: 'Septembre', en: 'September', de: 'September', it: 'Settembre', mid: 9, short: 'Sep'},
    10: {fr: 'Octobre', en: 'October', de: 'Oktober', it: 'Ottobre', mid: 10, short: 'Oct'},
    11: {fr: 'Novembre', en: 'November', de: 'November', it: 'Novembre', mid: 11, short: 'Nov'},
    12: {fr: 'Décembre', en: 'December', de: 'Dezember', it: 'Dicembre', mid: 12, short: 'Dec'},

  }
  // Change start month for ranking
  $scope.selectMonth = function (month) {
    $scope.rankingFilter.selectedMonth = month.mid
  }
  // Change start year
  $scope.selectYear = function (year) {
    $scope.rankingFilter.selectedYear = year
    if (year === 'All'){
      $scope.rankingFilter.selectedMonth = 0
    }
  }

  $scope.selectRankingOwner = function (owner){
    if (owner){
      $scope.rankingFilter.owner = owner
    } else {
      $scope.rankingFilter.owner = {
        id: null,
        name: 'All'
      }
    }
    collectionsQueryService.getList($scope.rankingFilter.owner.id).then(function (response) {
      $scope.collections = response
    })
  }
  $scope.selectRankingCollection = function (collection){
    if (collection) {
      $scope.rankingFilter.collection = collection
    } else{
      $scope.rankingFilter.collection = {
        id: null,
        name: 'All'
      }
    }
  }

  // Create data for the ranking
  $scope.generateDataForRanking = function (response) {
    $scope.dataRanking = []
    // Fill the data with the users
    for (var i = 0; i < response.length; i++) {
      // Get current user in the ranking
      var curUser = response[i]
      var d = {
        username: curUser.username,
        nImages: parseInt(curUser.nImages),
        id: curUser.volunteer_id,
        rank: i+1
      }
      $scope.dataRanking.push(d)
    }
    // Generate fake data
    // var n = 50
    // for (var i = 0; i < n; i++) {
    //   // Get current user in the ranking
    //   var curUser = response[i]
    //   var d = {
    //     username: 'mr. ' + i,
    //     nImages: i * 2,
    //     id: i,
    //     rank: i+1
    //   }
    //   $scope.dataRanking.push(d)
    // }
    //$scope.validator.id = 25
    $scope.dataRankingEnd = $scope.dataRanking.splice(3, $scope.dataRanking.length - 3)

    // $timeout(function() {
    //   $location.hash('row-'+$scope.validator.id)
    //   // call $anchorScroll()
    //   $anchorScroll()
    // })
  }

  // Corrections validations
  // -----------------------
  $scope.startTitleUpdate = function (correction) {
    correction.titleIsUpdated = true
    correction.newTitle = correction.sub_title
  }

  $scope.cancelTitleUpdate = function (correction) {
    correction.titleIsUpdated = false
  }

  $scope.restartTitleValidation = function(correction){
    correction.title_processed = false
  }

  $scope.restartCaptionValidation = function(correction){
    correction.caption_processed = false
  }

  $scope.startCaptionUpdate = function (correction) {
    correction.captionIsUpdated = true
    correction.newCaption = correction.sub_caption
  }

  $scope.cancelCaptionUpdate = function (correction) {
    correction.captionIsUpdated = false
  }

  $scope.updateTitle = function (correction) {
    correctionsService.updateTitle(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          // $scope.toValidateCorrections[i].processed = true
          // $scope.toValidateCorrections[i].title_status = 'Updated'

          $scope.toValidateCorrections[i].title_result = 'updated'
          $scope.toValidateCorrections[i].title_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username
          $scope.toValidateCorrections[i].title_update = correction.newTitle
        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.updateCaption = function (correction) {
    correctionsService.updateCaption(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          // $scope.toValidateCorrections[i].caption_status = 'Updated'

          $scope.toValidateCorrections[i].caption_result = 'updated'
          $scope.toValidateCorrections[i].caption_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username
          $scope.toValidateCorrections[i].caption_update = correction.newCaption
        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.acceptTitle = function (correction) {
    correctionsService.acceptTitle(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          //$scope.toValidateCorrections[i].title_status = 'Accepted'
          $scope.toValidateCorrections[i].title_result = 'accepted'
          $scope.toValidateCorrections[i].title_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username
        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.rejectTitle = function (correction) {
    correctionsService.rejectTitle(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          // $scope.toValidateCorrections[i].title_status = 'Rejected'
          $scope.toValidateCorrections[i].title_result = 'rejected'
          $scope.toValidateCorrections[i].title_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username
        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.acceptCaption = function (correction) {
    correctionsService.acceptCaption(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          // $scope.toValidateCorrections[i].caption_status = 'Accepted'

          $scope.toValidateCorrections[i].caption_result = 'accepted'
          $scope.toValidateCorrections[i].caption_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username

        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.rejectCaption = function (correction) {
    correctionsService.rejectCaption(correction, $scope.validator.id).then(function () {
      for (var i=0; i<$scope.toValidateCorrections.length; i++){
        if ($scope.toValidateCorrections[i].corr_id === correction.corr_id){
          // $scope.toValidateCorrections[i].caption_status = 'Rejected'
          $scope.toValidateCorrections[i].caption_result = 'rejected'
          $scope.toValidateCorrections[i].caption_processed = true
          $scope.toValidateCorrections[i].validator_name = $scope.validator.username
        }
      }
      // correctionsService.getToValidateCorrections($scope.ownerId).then(function (response) {
      //   $scope.toValidateCorrections = response
      // })
    })
  }

  $scope.openSelectedCorrection = function (correction) {
    $scope.observationModal = $uibModal.open({
      animation: false,
      templateUrl: 'correction.html',
      controller: 'correctionCtrl',
      bindToController: true,
      size: 'lg',
      resolve: {
        texts: function () {
          return $scope.texts
        },
        lang: function () {
          return $scope.lang
        },
        correction: function () {
          return correction
        }
      }
    })
    // $scope.observationModal.result.then(function (param) {
    //
    // })
  }

  // Process remarks
  // ---------------
  $scope.startReply = function (remark) {
    remark.hasReply = true
    remark.reply = 'Reply'
  }

  $scope.checkedRemark = function (remark) {
    remarksService.checked(remark).then(function () {
      remarksService.getToValidateRemarks().then(function (response) {
        $scope.toValidateRemarks = response
      })
    })
  }

  // Process observations
  // ---------------
  $scope.watchObservation = function (observation) {
    // Open full resolution image
    $scope.openSelectedObservation(observation)
  }

  $scope.acceptObservation = function (observation) {
    observationsService.acceptObservation(observation, $scope.validator.id).then(function () {
      for (var i =0; i<$scope.toValidateObservations.length; i++){
        if ($scope.toValidateObservations[i].obs_id === observation.obs_id){
          console.log('update tovalidate')
          $scope.toValidateObservations[i].processed = true
          $scope.toValidateObservations[i].validated = true
          $scope.toValidateObservations[i].remark = observation.remark
        }
      }
      // observationsService.getToValidateObservations($scope.ownerId).then(function (response) {
      //   $scope.toValidateObservations = response
      // })
    })
  }

  $scope.rejectObservation = function (observation) {
    observationsService.rejectObservation(observation, $scope.validator.id).then(function () {
      for (var i =0; i<$scope.toValidateObservations.length; i++){
        if ($scope.toValidateObservations[i].obs_id === observation.obs_id){
          $scope.toValidateObservations[i].processed = true
          $scope.toValidateObservations[i].validated = false
          $scope.toValidateObservations[i].remark = observation.remark
        }
      }
      // observationsService.getToValidateObservations($scope.ownerId).then(function (response) {
      //   $scope.toValidateObservations = response
      // })
    })
  }

  $scope.openSelectedObservation = function (observation) {
    $scope.observationModal = $uibModal.open({
      animation: false,
      templateUrl: 'observation.html',
      controller: 'observationCtrl',
      bindToController: true,
      size: 'lg',
      resolve: {
        texts: function () {
          return $scope.texts
        },
        lang: function () {
          return $scope.lang
        },
        observation: function () {
          return observation
        }
      }
    })
    $scope.observationModal.result.then(function (param) {
      if (param.result === 'reject') {
        observationsService.rejectObservation(param.observation, $scope.validator.id).then(function () {
          for (var i =0; i<$scope.toValidateObservations.length; i++){
            if ($scope.toValidateObservations[i].obs_id === observation.obs_id){
              $scope.toValidateObservations[i].processed = true
              $scope.toValidateObservations[i].validated = false
            }
          }
        })
    } else if (param.result === 'accept') {
        observationsService.acceptObservation(param.observation, $scope.validator.id).then(function () {
          for (var i =0; i<$scope.toValidateObservations.length; i++){
            if ($scope.toValidateObservations[i].obs_id === param.observation.obs_id){
              $scope.toValidateObservations[i].processed = true
              $scope.toValidateObservations[i].validated = true
            }
          }
        })
      }
    })
  }

  // Functions for the list of images
  // --------------------------------
  $scope.switchToImages = function () {
    // Reset filter
    $scope.resetFilter(load=false)
    // Set filter for initial get
    var filter = $scope.imageFilter
    filter.owner.id = $scope.ownerId
    filter.processed = false
    // Mode is images
    // imageQueryService.getDashboard($scope.imageFilter).then(function (response) {
    //   $scope.images = response
    //   $scope.nTask = response.length
    // })
  }
  $scope.publishSelected = function() {
    var listIds = []
    for (var i=0; i<$scope.images.length; i++){
      if ($scope.images[i].selected === true){
        listIds.push($scope.images[i].id)
      }
    }
    imageQueryService.publishList(listIds).then(function(){
      imageQueryService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.images = response
        $scope.nTask = response.length
      })
    })
  }
  $scope.unpublishSelected = function() {
    var listIds = []
    for (var i=0; i<$scope.images.length; i++){
      if ($scope.images[i].selected === true){
        listIds.push($scope.images[i].id)
      }
    }
    imageQueryService.unpublishList(listIds).then(function() {
      imageQueryService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.images = response
        $scope.nTask = response.length
      })
    })
  }

  // Functions for the filter
  // ------------------------
  $scope.imageFilterIni = {
    originalId: null,
    Id: null,
    owner: {
      id: null,
      name: 'All'
    },
    collection: {
      id: null,
      name: 'All'
    },
    processed: false,
    dates:{
      min: null,
      max: null,
    },
    // geolocalized: 'All',
    // observed: false,
    // corrected: false,
    volunteer: {
      id: null,
      username: null,
      // type: 'All'
    },
    validator: {
      id: null,
      username: 'All',
      // type: 'All'
    },
    geolocalizations: false,
    metadata: false,
    observations: false
  }
  $scope.imageFilter =  JSON.parse(JSON.stringify($scope.imageFilterIni)) //Deepcopy trick
  // Reset the filter
  $scope.resetFilter = function (load) {
    $scope.imageFilter = JSON.parse(JSON.stringify($scope.imageFilterIni)) //Deepcopy trick
    $scope.imageFilter.owner.id = $scope.ownerId
    if (load !== false){
      $scope.getImages()
    }
  }
  $scope.selectOwner = function (owner){
    if (owner){
      $scope.imageFilter.owner = owner
    } else {
      $scope.imageFilter.owner = {
        id: null,
        name: 'All'
      }
    }
    // Update the collection list according to the owner
    collectionsQueryService.getList($scope.imageFilter.owner.id).then(function (response) {
      $scope.collections = response
    })
  }
  $scope.selectCollection = function (collection){
    if (collection) {
      $scope.imageFilter.collection = collection
    } else{
      $scope.imageFilter.collection = {
        id: null,
        name: 'All'
      }
    }
  }
  // $scope.selectGeoloc = function(val) {
  //   $scope.imageFilter.geolocalized = val
  // }
  // $scope.selectVolunteerType= function(val) {
  //   $scope.imageFilter.volunteer.type = val
  // }
  // $scope.selectValidatorType= function(val) {
  //   $scope.imageFilter.validator.type = val
  // }
  $scope.selectValidator= function(val) {
    if (val === 'all'){
      $scope.imageFilter.validator.username = null
      $scope.imageFilter.validator.id = null
    }else{
      $scope.imageFilter.validator.username = val.username
      $scope.imageFilter.validator.id = val.id
    }
  }
  $scope.getImages = function() {
    if ($scope.ownerId){
      $scope.imageFilter.owner.id = $scope.ownerId
    }
    if ($scope.currentMode == 0){
      // Mode is geolocalisation
      validationsService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.toValidateImages = response
        $scope.nTask = response.length
      })
    }
    else if ($scope.currentMode == 1){
      // Mode is observations
      observationsService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.toValidateObservations  = response
        $scope.nTask = response.length
      })
    }
    else if ($scope.currentMode == 2){
      // Mode is metadata
      correctionsService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.toValidateCorrections = response
        $scope.nTask = response.length
      })
    }
    else if ($scope.currentMode == 3){
      // Mode is images
      imageQueryService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.images = response
        $scope.nTask = response.length
      })
    }
    else if ($scope.currentMode == 5){
      // Mode is mailing
      volunteersService.getEmails($scope.imageFilter).then(function (response) {
        $scope.volunteers = response
        $scope.nVolunteers = response.length
      })
    }
    else if ($scope.currentMode == 9){
      // Mode is images
      problemsService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.toValidateProblems = response
        $scope.nTask = response.length
      })
    }
  }

  // Functions for the problems
  // ---------------------------------
  // Initialize problem model
  // $scope.newProblem = {
  //   inverted: false,
  //   photomontage: false,
  //   other: false,
  //   otherText: '',
  // }

  $scope.getProblems = function(filter){
    problemsService.getDashboard(filter).then(function (response) {
      $scope.toValidateProblems = response
      $scope.nTask = response.length
    })
  }
  $scope.deleteProblem = function(problemId){
    problemsService.deleteProblem(problemId)
    .then(function (response) {
      problemsService.getDashboard($scope.imageFilter).then(function (response) {
        $scope.toValidateProblems = response
        $scope.nTask = response.length
      })
    })
  }
  $scope.openNewProblem = function(imageId){
    $scope.problemModal = $uibModal.open({
      animation: false,
      templateUrl: 'createProb.html',
      controller: 'newProbCtrl',
      bindToController: true,
      size: 'lg',
      resolve: {
        texts: function () {
          return $scope.texts
        },
        lang: function () {
          return $scope.lang
        }
        ,
        problem: function () {
          var newProblem = {
            inverted: false,
            photomontage: false,
            other: false,
            otherText: '',
            imageId: imageId
          }
          return newProblem
        }
      }
    })
    $scope.problemModal.result.then(function (param) {
      var promises = []
      if (param.result === 'create') {
        if (param.problem.inverted) {
          var problem = {
            title: 'Image is inverted'
          }
          promises.push(problemsService.submitProblem(param.problem.smapshotId, $scope.validator.id, problem))
        }
        if (param.problem.photomontage) {
          var problem = {
            title: 'Image is a photomontage'
          }
          promises.push(problemsService.submitProblem(param.problem.smapshotId, $scope.validator.id, problem))
        }
        if (param.problem.other) {
          var problem = {
            title: 'Other problem',
            description: param.problem.otherText
          }
          promises.push(problemsService.submitProblem(param.problem.smapshotId, $scope.validator.id, problem))
        }
        $q.all(promises).then(function () {
          $scope.getProblems($scope.imageFilter)
        })

      }
    })
  }
  $scope.switchToProblems = function () {
    $scope.propertyName = 'id'
    $scope.resetFilter()
    // Set filter for initial get
    var filter = $scope.imageFilter
    filter.owner.id = $scope.ownerId
    filter.processed = false
    $scope.getProblems(filter)
  }
  // Publish the image
  $scope.publish = function(imageId){
    imageQueryService.publish(imageId)
    for (var i =0; i<$scope.toValidateProblems.length; i++){
      if ($scope.toValidateProblems[i].image_id === imageId){
        $scope.toValidateProblems[i].is_published = true
      }
    }
  }
  // Unpublish the image
  $scope.unpublish = function(imageId){
    imageQueryService.unpublish(imageId)
    for (var i =0; i<$scope.toValidateProblems.length; i++){
      if ($scope.toValidateProblems[i].image_id === imageId){
        $scope.toValidateProblems[i].is_published = false
      }
    }
  }

  $scope.ownerProcessed = function(problemId){
    problemsService.ownerProcessed(problemId).then(function(){
      var filter = $scope.imageFilter
      filter.owner.id = $scope.ownerId
      $scope.getProblems(filter)
    })
  }

  $scope.smapshotProcessed = function(problemId){
    problemsService.smapshotProcessed(problemId).then(function(){
      var filter = $scope.imageFilter
      filter.owner.id = $scope.ownerId
      $scope.getProblems(filter)
    })
  }

  // Functions for the mailing
  // -------------------------

  $scope.switchToMailing = function () {
    $scope.propertyName = 'username'
    $scope.resetFilter()
    $scope.imageFilter.owner.id = $scope.ownerId
    $scope.getVolunteers()
  }
  $scope.getVolunteers = function () {
    volunteersService.getEmails($scope.imageFilter).then(function (response) {
      $scope.volunteers = response.volunteers
      $scope.emailExport = response.emails
      $scope.nVolunteers = response.volunteers.length
    })
  }

  // Functions for the download
  // -------------------------
  var downloadFilterIni = {
    type: 'images',
    dates : {
      type: 'download',
      min: null,
      max: null
    },
    owner: {
      id: null,
      name: 'All'
    },
    format: 'wkt',
    collection: {
      id: null,
      name: 'All'
    },
    ids: null,
    download: null
  }
  $scope.selectDownloadCollection = function (collection){
    if (collection) {
      $scope.downloadFilter.collection = collection
    } else{
      $scope.downloadFilter.collection = {
        id: null,
        name: 'All'
      }
    }
  }
  $scope.switchToDownload = function () {
    $scope.nDownload = 0
    $scope.resetDownloadFilter()
  }
  $scope.resetDownloadFilter = function(){
    $scope.downloadFilter = JSON.parse(JSON.stringify(downloadFilterIni)) // Deepcopy trick
    if ($scope.ownerId){
      $scope.downloadFilter.owner.id = $scope.ownerId
    }
  }
  $scope.getDataForDownload = function (){
    // Process ids
    if ($scope.downloadFilter.ids){
      if (typeof $scope.downloadFilter.ids === 'string'){
        // $scope.downloadFilter.ids = $scope.downloadFilter.ids.replace(' ', '')
        $scope.downloadFilter.ids = $scope.downloadFilter.ids.split(',')
      }
    }

    if ($scope.downloadFilter.type === 'images'){
      imageQueryService.getDataForDownload($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })

    }else if ($scope.downloadFilter.type === 'notes'){
      observationsService.getDataForDownload($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })

    }else if ($scope.downloadFilter.type === 'titles'){
      correctionsService.getTitleDataForDownload($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })
    }else if ($scope.downloadFilter.type === 'captions'){
      correctionsService.getCaptionDataForDownload($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })
    }else if ($scope.downloadFilter.type === 'links'){
      imageQueryService.getSmapshotAddresses($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })
    }else if ($scope.downloadFilter.type === 'all'){
      imageQueryService.fullDownload($scope.downloadFilter)
      .then(function(dataForDownload){
        $scope.nDownload = dataForDownload.length
        if ($scope.nDownload !== 0){
          $scope.downloadHeaders = Object.keys(dataForDownload[0])
          $scope.dowloadExport = dataForDownload
        }
      })
    }
  }

  // Functions for the roles
  // -------------------------
  $scope.switchToRoles = function () {
    $scope.propertyName = 'username'
    $scope.roles = {
      search: null
    }
    // Get the list owners
    ownerQueryService.getList().then(function (response) {
      console.log('response owner', response)
      var owners = {}
      for (var i=0; i< response.length; i++){
        owners[response[i].id]= response[i]
      }
      $scope.owners = owners
    })
  }

  $scope.selectAdminOwner = function(volunteer, owner) {
    console.log('vol own', volunteer, owner)
    volunteer.owner_admin = owner.id
  }

  $scope.selectValidatorOwner = function(volunteer, owner) {
    console.log('vol own', volunteer, owner)
    volunteer.owner_validator = owner.id
  }

  $scope.giveValidatorRights = function (volunteer){
    if ($scope.isSuperAdmin){
      if (volunteer.owner_validator){
        volunteersService.giveValidatorRights(volunteer.id, volunteer.owner_validator)
      }else{
        alert('you have to provide the owner')
      }
    }else {
      volunteersService.giveValidatorRights(volunteer.id, $scope.ownerId)
    }
  }

  $scope.giveSuperAdminRights = function (volunteer){
    volunteersService.giveSuperAdminRights(volunteer.id)
  }

  $scope.giveOwnerAdminRights = function (volunteer){
    if (volunteer.owner_admin){
      volunteersService.giveOwnerAdminRights(volunteer.id, volunteer.owner_admin)
    }else{
      alert('you have to provide the owner')
    }
  }

  $scope.removeRights = function (volunteer){
    volunteersService.removeRights(volunteer.id)
  }

  $scope.getUsersByEmail = function (){
    console.log('$scope.searchEmail', $scope.roles.search)
    volunteersService.getUsersByEmail($scope.roles.search)
    .then(function(result){
      console.log('result user by email', result)
      $scope.users = result
    })
  }

  // Functions for the bad geolocalizations
  // --------------------------------------
  $scope.switchToBadGeoloc = function () {
    $scope.propertyName = 'id'
    $scope.resetFilter()
    // Set filter for initial get
    var filter = JSON.parse(JSON.stringify($scope.imageFilterIni)) //Deepcopy trick
    filter.owner.id = $scope.ownerId
    filter.processed = false
    $scope.getBadGeolocs(filter)
  }
  $scope.getBadGeolocs = function(){
    badGeolocService.getDashboard($scope.imageFilter).then(function (response){
      console.log('response', response)
      $scope.badGeolocs = response
      $scope.nBadGeolocs = response.length
    })
  }
  $scope.rejectBadGeoloc = function(imageId){
    badGeolocService.reject(imageId, $scope.validator.id).then(function(){

    })
  }
  $scope.validateBadGeoloc = function(imageId){
    badGeolocService.validate(imageId, $scope.validator.id).then(function(){

    })
  }



}])

dashboardApp.controller('observationCtrl', function ($scope, $uibModalInstance, texts, lang, observation) {
  $scope.texts = texts
  $scope.lang = lang
  console.log('observation', observation)
  $scope.observation = observation
  $scope.optionsViewer =
  {
    prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
    gestureSettingsMouse: {
      clickToZoom: false,
      dblClickToZoom: false
    },
    showNavigator: false,
    showHomeControl: false,
    showFullPageControl: true,
    showZoomControl: true,
    tileSources: ['../data/collections/' + observation.collection_id + '/images/tiles/'.concat(observation.image_id).concat('.dzi')],
    overlays: [{
      id: 'example-overlay',
      x: parseFloat(observation.coord_x),
      y: parseFloat(observation.coord_y),
      width: parseFloat(observation.width),
      height: parseFloat(observation.height),
      //placement: 'CENTER',
      className: 'highlight',
      //placement: 'RIGHT',
      checkResize: false
    }]
  }
  $scope.mysd = {}
  $scope.accept = function (observation) {
    var param = {
      result: 'accept',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
  $scope.reject = function (observation) {
    var param = {
      result: 'reject',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
  $scope.cancel = function (observation) {
    var param = {
      result: 'cancel',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
})

dashboardApp.controller('correctionCtrl', function ($scope, $uibModalInstance, texts, lang, correction) {
  $scope.texts = texts
  $scope.lang = lang
  $scope.correction = correction
  $scope.optionsViewer =
  {
    prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
    gestureSettingsMouse: {
      clickToZoom: false,
      dblClickToZoom: false
    },
    showNavigator: false,
    showHomeControl: false,
    showFullPageControl: true,
    showZoomControl: true,
    tileSources: ['../data/collections/' + correction.collection_id + '/images/tiles/'.concat(correction.image_id).concat('.dzi')],
  }
  $scope.mysd = {}
  $scope.cancel = function (observation) {
    var param = {
      result: 'cancel',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
})

dashboardApp.controller('newProbCtrl', function ($scope, $uibModalInstance, texts, lang, problem) {
  $scope.texts = texts
  $scope.lang = lang
  $scope.problem = problem
  $scope.cancel = function () {
    var param = {
      result: 'cancel',
    }
    $uibModalInstance.close(param)
  }
  $scope.create = function () {
    var param = {
      result: 'create',
      problem: $scope.problem
    }
    $uibModalInstance.close(param)
  }
})
