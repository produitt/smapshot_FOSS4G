georefApp.controller('georefCtrl', ['$scope', '$http', '$q', '$uibModal',
'$window', '$location', '$timeout', 'AuthService', 'topoService', 'cesiumService',
'osdService', 'dbService', 'searchService', 'leafletData', 'langService',
'textService', 'correctionsService', 'observationsService', 'remarksService',
'problemsService', 'validationsService', 'imageQueryService', 'deviceDetector',
  function ($scope, $http, $q, $uibModal, $window, $location, $timeout, AuthService,
    topoService, cesiumService, osdService, dbService, searchService,
    leafletData, langService, textService, correctionsService, observationsService,
    remarksService, problemsService, validationsService, imageQueryService, deviceDetector
    ) { // FileSaver' , 'Blob'

    // Avoid path error on staging server
    $window['CESIUM_BASE_URL'] = '/bower_components/cesium/Build/Cesium/'

    // Lock management
    // ---------------

    // Unlock the image when the image is close
    $window.addEventListener("beforeunload", function (e) {
      if ($scope.locked === false){
        imageQueryService.unlock($scope.imageId)
        .then(function(res){
          return
        })
      }else{
        return
      }
    })

    $scope.openLockInstruction = function (size) {
      var lockInstructionModal = $uibModal.open({
        animation: false,
        templateUrl: 'lockInstruction.html',
        bindToController: true,
        controller: 'lockCtrl',
        resolve: {
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
      lockInstructionModal.closed.then(function(){
        $window.close()
      })
    }
    // Check lock after loading
    angular.element(document).ready(function () {
      if ($scope.image.georef_3d_bool === false){
        checkLock()
      }
    })
    // Close the tab after 2h
    function loaded () {
      $window.setTimeout(CloseMe, 7200000) //
    }
    function CloseMe () {
      imageQueryService.unlock($scope.imageId).then(function(res){
        alert($scope.texts.inactive[$scope.lang])
        $window.close()
      })
    }
    loaded()

    // Get the url parameters
    // ----------------------
    $scope.imageId = $location.search().imageId
    $scope.lang = langService.getBrowserLang()
    $scope.menuMode = 'geoloc' // for the selection of the menu

    // Switch the lang
    $scope.switchLang = function(lang) {
      $scope.lang = lang
    }
    $scope.changeVolunteerLang = function (lang) {
      if (AuthService.isAuthenticated()){
        volunteersService.submitLang(lang)
      }
      $scope.lang = lang
    }
    $scope.langs = langService.getLangs()

    // Get the texts
    // -------------
    $scope.myName = 'login'

    // for the content
    $scope.loadTextPromise = textService.loadText('georef').then(function (texts) {
      $scope.texts = texts
      $scope.instruction = $scope.texts.instrTuto[$scope.lang]
      $scope.longInstruction = $scope.texts.instrTutoLong[$scope.lang]

      // Detect browser
      // // Open modal to give browser instructions
      // $scope.openBrowserInstruction = function () {
      //   var browserInstructionModal = $uibModal.open({
      //     animation: false,
      //     templateUrl: 'browserInstruction.html',
      //     bindToController: true,
      //     controller: 'browserCtrl',
      //     resolve: {
      //       texts: function () {
      //         return $scope.texts
      //       },
      //       lang: function () {
      //         return $scope.lang
      //       }
      //     }
      //   })
      // }
      // if (deviceDetector.browser === 'ie'){
      //   $scope.openBrowserInstruction()
      // }
    })


    // for the header
    $scope.loadHeaderTextPromise = textService.loadText('header').then(function (texts) {
      $scope.hTexts = texts
      $scope.myName = $scope.hTexts.login[$scope.lang]
    })

    // Show or hide buttons
    // --------------------
    $scope.showValidate = true
    $scope.show = {
      validateOrientation: false,
      validateLocation: false,
      validateDirection: false,
      validate: true,
      back: false,
      editTitle: false,
      editCaption: false,
      editDate: false,
      observationInput: false,
      observationClickText: false,
      observationValidationText: false,
      observationValidation: false,
      remarkInput: false,
      otherProblem: false,
      osd: true
    }

    // Initialise step model
    // ---------------------
    // set initial step
    $scope.currentStep = 0
    $scope.progression = [false, false, false, false, false]

    // steps of georeferencing, used to switch between the views
    $scope.steps = {
      0: 'training',
      1: 'location',
      2: 'direction',
      3: 'correspondences',
      4: 'modifications',
      5: 'problem',
      6: 'validation_viz', // for validation
      7: 'validation_com' // for validation
    }

    // Initialise problem model
    // ------------------------
    $scope.problems = {
      inverted: false,
      photomontage: false,
      georefImpossible: false,
      notRecognizable: false,
      other: false,
      otherText: '',
      otherValidated: false
    }

    // Initialise gltf model model
    // ----------------------
    $scope.transparency = 1
    $scope.model = null

    // Query the image
    // ---------------
    $scope.getImage = function () {
      return $q(function (resolve, reject) {
        $scope.httpPromise = imageQueryService.getImage($scope.imageId)
        $scope.httpPromise.then(function (image) {
          // Record image infos
          $scope.image = image
          $scope.ownerId = image.owner_id
          // Check if the owners has an owner page
          if (image.url_name){
            $scope.has_url = true
          }else{
            $scope.has_url = false
          }
          $scope.image.hasApriori = true
          $scope.imageTitle = image.title
          if ($scope.image.georef_3d_bool) {
            $scope.providedCamera.location.latitude = image.latitude
            $scope.providedCamera.location.longitude = image.longitude
            $scope.providedCamera.location.height = image.height
          }else {
            if ($location.search().lat) {
              var lat = parseFloat($location.search().lat)
              var lng = parseFloat($location.search().long)
              if (!isNaN(lat)){
                $scope.providedCamera.location.latitude = lat
                $scope.image.latitude = lat
              }else{
                $scope.image.hasApriori = false
                $scope.providedCamera.location.latitude = 46.84081
                $scope.image.latitude = 46.84081
              }
              if (!isNaN(lng)){
                $scope.providedCamera.location.longitude = lng
                $scope.image.longitude= lng
              }else{
                $scope.image.hasApriori = false
                $scope.providedCamera.location.longitude = 8.16948
                $scope.image.longitude= 8.16948
              }
            }else{
              $scope.image.hasApriori = false
              $scope.providedCamera.location.latitude = 46.84081
              $scope.providedCamera.location.longitude = 8.16948
              $scope.image.latitude = 46.84081
              $scope.image.longitude= 8.16948
            }
            $scope.providedCamera.location.height = image.apriori_height
          }
          resolve()
        })
      })
    }
    $scope.locked = false
    var checkLock = function () {
      console.log('$scope.image.delta_last_start', $scope.image.delta_last_start)
      if ($scope.image.delta_last_start !== null){
        if ($scope.image.delta_last_start < 120){
          $scope.locked = true
          $scope.loadTextPromise.then(function(texts){
            $scope.openLockInstruction()
          })
        }else {
          // lock the image
          imageQueryService.lock($scope.imageId)
        }
      }else {
        // lock the image
        imageQueryService.lock($scope.imageId)
      }
      // return
    }
    // Initialize update mode to false
    //$scope.updateMode = false

    // Check if the user is logged
    $scope.isLogged = false
    AuthService.getMyName()
    .then(function (volunteer) {
      $scope.isLogged = true
      // success
      $scope.volunteer = volunteer
      $scope.myName = $scope.volunteer.username
      if ($scope.volunteer.lang){
        $scope.lang = $scope.volunteer.lang
      }
      // Get image attributes
      $scope.getImagePromise = $scope.getImage().then(function () {
        // Check if image is already georeference
        $scope.checkValidationUpdate().then(function () {
          if ($scope.updateMode === false) {

            // if ($scope.validationMode === false) {
            //   // Check the lock
            //   checkLock()
            // }
            // Check if the user already see the tutorial
            if ($scope.volunteer.has_one_validated) {
              $scope.currentStep = 1
              $scope.switchToLocationStep()
              // For debug
              // $scope.currentStep = 4
              // $scope.switchToCorrectionsStep()
            }
          }
        })
      })
    }, function () {
      // user is not logged
      $scope.myName = 'Login'
      $scope.getImage().then(function () {
        // Check if image is already georeferenced
        $scope.checkValidationUpdate()
        //.then(function () {
          // Check the lock
          // checkLock()
        //})
      })
    })


    // Check for update mode
    $scope.rightBox = null // Value to keep the globe open (avoid crash and reintialisation)
    $scope.checkValidationUpdate = function(){
      return $q(function (resolve, reject) {
        if ($scope.image.georef_3d_bool) {
            // Check if logged
            if (AuthService.isAuthenticated()) {
              AuthService.getUserInfo().then(function (res) {
                // if user is validator: validation mode
                if ($scope.volunteer.is_validator === true) {
                  $scope.validationMode = true
                  $scope.updateMode = false
                  $scope.validatorId = $scope.volunteer.id
                  $scope.volunteerId = $scope.image.volunteer_id
                  $scope.parseGCP()
                  $scope.modelIsDrawn = true
                  $scope.rightBox = 'globe' // Initialize globe
                  $scope.switchToVisualizationStep()
                }
                // The logged user is the same as the volunteer: update mode
                else if ($scope.image.volunteer_id === $scope.volunteer.id){
                  $scope.switchToCorrespondancesStep()
                  // Image geolocation is modified
                  $scope.updateMode = true
                  $scope.validationMode = false
                  // Switch to georef mode
                  // $scope.switchToGlobe()
                  $scope.parseGCP()

                }
                // if user is not validator nor the user who perform the geoloc
                else {
                  $scope.updateMode = false
                  $scope.validationMode = false
                  alert($scope.texts.notYou[$scope.lang])
                }
              })
            } else {
              // if not logged
              $scope.updateMode = false
              $scope.validationMode = false
              alert($scope.texts.mustBeLogged[$scope.lang])
              location.href = '/login/'
            }
            resolve()
        // The picture is not 3d georeferenced
        } else {
          // Check if the image is locked by another user
          // checkLock()
          // the visitor is logged
          if (AuthService.isAuthenticated()) {
            AuthService.getUserInfo().then(function (res) {
              $scope.volunteerId = $scope.volunteer.id
              validationsService.createValidation($scope.imageId, $scope.volunteerId)
              $scope.updateMode = false
              $scope.validationMode = false
              resolve()
            })
          // the visitor is anonymous
          } else {
            $scope.volunteerId = 14
            validationsService.createValidation($scope.imageId, $scope.volunteerId)
            $scope.updateMode = false
            $scope.validationMode = false
            resolve()
          }
        }
      })
    }

  // Goes to next step
  $scope.backFromModif = false
  $scope.changeStep = function (direction) {
    if (direction === 'forward') {
      if ($scope.currentStep === 0) {
        $scope.currentStep = 1
        $scope.switchToLocationStep($scope.imageId)
      } else if ($scope.currentStep === 1) {
        $scope.currentStep = 2
        $scope.switchToDirectionStep($scope.imageId)
      } else if ($scope.currentStep === 2) {
        $scope.currentStep = 3
        $scope.switchToCorrespondancesStep()
      } else if ($scope.currentStep === 3) {
        // Save camera before changing the view
        $scope.saveCamera().then(function(){
          if ($scope.validationMode) {
            // This is a improvement in validation models
            $scope.switchToReason()
          } else {
            // Goes to correction
            $scope.currentStep === 4
            $scope.switchToCorrectionsStep()
          }
        })
      } else if ($scope.currentStep === 4) {
        $scope.saveCorrections().then(function () {
          try {
            $window.close()
          }catch(err){
            $scope.goToMono()
          }
        })
      } else if ($scope.currentStep === 5) {
        $scope.submitProblem()
      } else if ($scope.currentStep === 7) {
        $scope.submitErrors()
      }
    } else {
      console.log('backward', $scope.currentStep)
      if ($scope.currentStep === 1) {
        // step is location: back to tuto
        $scope.currentStep =  0
        $scope.show.back = false
      } else if ($scope.currentStep === 2) {
        // step is direction: back to location
        $scope.currentStep =  1
        $scope.switchToLocationStep($scope.imageId)
      } else if ($scope.currentStep === 3) {
        if (!$scope.validationMode){
          $scope.myGCPs = {
            curGCPid: 0,
            selectedGCPid: null,
            dictGCPs: {},
            file: null
          }
          $scope.currentStep =  1
          $scope.updateMode = false
          $scope.switchToLocationStep($scope.imageId)
        }
        else {
          $scope.hideGCP()
          $scope.switchToVisualizationStep()
        }
      } else if ($scope.currentStep === 4) {
        $scope.currentStep =  3
        // step is modification: back to correspondences
        $scope.backFromModif = true
        $scope.switchToCorrespondancesStep()

      } else if ($scope.currentStep === 5) {
        $scope.currentStep =  $scope.stepBeforeProblem-1
        $scope.changeStep('forward')
        // step is modification: back to correspondences
        //$scope.switchToCorrespondancesStep()
      }
    }

  }

  $scope.initializeGlobe = function (){
    $scope.hideGCP()
    $scope.resetGCP()
    $scope.reinitializeCamera()
  }
  $scope.eraseDirectionMarker = function(){
    leafletData.getMap().then(function (map) {
      if ($scope.marker){
        map.removeLayer($scope.marker)
      }
      if ($scope.clickArrow){
        map.removeLayer($scope.clickArrow)
      }
      if ($scope.clickArrowHead){
        map.removeLayer($scope.clickArrowHead)
      }
      if ($scope.moveArrow){
        map.removeLayer($scope.moveArrow)
      }
      if ($scope.moveArrowHead){
        map.removeLayer($scope.moveArrowHead)
      }
    })
  }
  $scope.showDirectionMarker = function () { // Called with data-ng-init in the main html file
    leafletData.getMap().then(function (map) {
      if ($scope.marker){
        map.removeLayer($scope.marker)
      }
      $scope.marker = L.marker([$scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude],
        {
          icon: L.icon(localIcons.yellowIcon),
          draggable: false
        }).addTo(map).bindPopup($scope.texts.popupDir[$scope.lang], {'maxWidth': ($scope.leafWidth / 5) + 'px', 'className': 'popupLeaflet'}).openPopup()

        //Create initial direction
        $scope.computeDirectionCoordinates($scope.marker.getLatLng().lat-0.005, $scope.marker.getLatLng().lng).then(function(direction){

            // Draw direction
            if ($scope.clickArrow) {
              //Delete row if exists
              map.removeLayer($scope.clickArrow)
              map.removeLayer($scope.clickArrowHead)
            }
            //Draw arrow
            $scope.clickArrow = L.polyline(
              [[$scope.marker.getLatLng().lat, $scope.marker.getLatLng().lng], [direction.lat, direction.lng]],
              {
                color: '#FFB028',
                weight: 6,
                opacity: 1
              }
            ).addTo(map)

            $scope.clickArrowHead = L.polylineDecorator($scope.clickArrow, {
              patterns: [
                {
                  offset: '100%',
                  repeat: 0,
                  symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true, color: '#FFB028', opacity: 1}})
                }
              ]
            }).addTo(map)
          })
        })
  }
  $scope.showLocationMarker = function () { // Called with data-ng-init in the main html file
    leafletData.getMap().then(function (map) {

      // Set map center
      $scope.center = {
        lat: $scope.image.latitude, // 46.47532,
        lng: $scope.image.longitude, // 7.13708,
      }
      // If the image has no apriori location there is no zoom on the point
      if ($scope.image.hasApriori){
        $scope.center.zoom = 19
      }else{
        $scope.center.zoom = 15
      }
      if ($scope.marker){
        map.removeLayer($scope.marker)
      }

      $scope.marker = L.marker([$scope.image.latitude, $scope.image.longitude],
        {
          icon: L.icon(localIcons.yellowIcon),
          draggable: true,
      }).addTo(map)
      .bindPopup($scope.texts.popupLoc[$scope.lang], {'maxWidth': ($scope.leafWidth / 5) + 'px', 'className': 'popupLeaflet'}).openPopup()

      // Stores marker coordinates
      $scope.marker.on('dragend', function (e, args) {
        $scope.providedCamera.location.latitude = e.target.getLatLng().lat
        $scope.providedCamera.location.longitude = e.target.getLatLng().lng
        $scope.image.latitude = e.target.getLatLng().lat
        $scope.image.longitude = e.target.getLatLng().lng
      })
    })
  }

    // Initialise variables
    // --------------------
    // Initialise camera provided
    $scope.providedCamera = {
      location: {
        latitude: '',
        longitude: '',
        X: '',
        Y: '',
        Z: ''
      },
      fixed: false,
      orientation: {
        azimuth: '',
        tilt: '',
        roll: ''
      }
    }
    $scope.poseInstructionShown = false

    // Initialaise the camera orientation model
    $scope.cameraParameters = 'undefined'

    // Initialise the GCP model
    // ------------------------
    $scope.myGCPs = {
      curGCPid: 0,
      selectedGCPid: null,
      dictGCPs: {},
      file: null
    }
    // Array of gcp is sync with the dictonary of gcp
    $scope.gcpArray = []
      // $scope.orientationArray = []
      // camera parameters computed with LM

    // if the dictonary is modified, the gcp array is updated
    $scope.$watch('myGCPs.dictGCPs', function () {
      $scope.dictToArray()
    }, true)

    // watch if a gcp is selected
    $scope.showDelete = false
    $scope.$watch('myGCPs.selectedGCPid', function () {
      if ($scope.myGCPs.selectedGCPid != null) {
        $scope.showDelete = true
      } else {
        $scope.showDelete = false
      }
    }, true)

    // Reshape the dictionary in array (for csv export and filter)
      $scope.dictToArray = function () {
        var gcpArray = []

      // for (var id in $scope.myGCPs.dictGCPs){
        var arr = Object.keys($scope.myGCPs.dictGCPs)
        var nP = arr.length
        for (id = 0; id < nP; id++) {
          var curGCP = $scope.myGCPs.dictGCPs[arr[id]]
        // Avoid that null values perturb pose estimation
          if ((curGCP.x != null) && (curGCP.X != null)) {
            gcpArray.push(curGCP)
          }
        }
        $scope.gcpArray = gcpArray
        var nGCP = $scope.gcpArray.length
      // If there are four GCP: open modal instruction
        if (nGCP == 4) {
          if(($scope.gcpArray[3].x != null) && ($scope.gcpArray[3].X != null)) {
          //if (($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid - 1].x != null) && ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid - 1].X != null)) {
            $scope.fourGCP = true
            $scope.longInstruction = $scope.texts.instrFour[$scope.lang]
            if ($scope.poseInstructionShown === false) {
              //$scope.openPoseInstruction()
            }
          }
        }
        if (nGCP >= 4) {
          $scope.enoughGCP = true

        }else {
          $scope.enoughGCP = false
        }
        // if (nGCP >= 1) {
        //   $scope.showTable = true
        // }
      }

    $scope.parseGCP = function() {
      // Create gcp
      if ($scope.image.gcp_json) {
        var gcpJson = JSON.parse($scope.image.gcp_json)
        if (gcpJson.length >= 4) {
          $scope.enoughGCP = true
        }
        // Fill the table of GCP
        var maxId = 0
        var nGCP = Object.keys(gcpJson).length
        for (var i=0; i<nGCP; i++){
          var gcp = gcpJson[i]
          // Create a GCP
          var GCP = {
            id: parseInt(i),
            x: parseFloat(gcp.x),
            y: parseFloat(gcp.y),
            X: parseFloat(gcp.X),
            Y: parseFloat(gcp.Y),
            Z: parseFloat(gcp.Z),
            eM: null,
            ePix: null,
            enabled: true
          }
          if (GCP.id > maxId) {
            maxId = GCP.id
          }
          // Push it in the dictionary
          $scope.myGCPs.dictGCPs[GCP.id] = GCP
        }
        if (nGCP >= 4) {
          $scope.enoughGCP = true
        }
        $scope.myGCPs.curGCPid = maxId + 1
      }// if gcp
      // The function which draws the GCP ($scope.drawGCP) is called from angular-osd-georef!!!
      // Create empty GCP
      var GCP = {
        id: $scope.myGCPs.curGCPid,
        x: null,
        y: null,
        X: null,
        Y: null,
        Z: null,
        eM: null,
        ePix: null
      }
      // Push it in the dictionary
      $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      // Select the current GCP
      $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
      console.log('loadgcp', $scope.myGCPs.dictGCP)
    }

    // Initialise Leaflet
    // ------------------
    $scope.map = {
      defaults: {
        crs: new L.Proj.CRS('EPSG:21781', '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 ' + '+k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs', {
          resolutions: [4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 250, 100, 50, 20, 10, 5, 2.5, 2, 1.5, 1, 0.5],
          origin: [420000, 350000]
        }),
        continuousWorld: true,
        zoomControl: false
      },
      layers: {
        baselayers: {
          swisstopo: {
            name: 'Swisstopo',
            type: 'xyz',
            url: 'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/21781/{z}/{y}/{x}.jpeg',
            layerOptions: {
              attribution: 'Map data &copy; 2015 swisstopo',
              maxZoom: 26,
              minZoom: 0,
              showOnSelector: false
            }
          }
        }
      },
      controls: {
        // zoom: {
        //   position: 'topright'
        // }
      }
    }
    // L.control.zoom({
    //      position:'topright'
    // }).addTo(map);

  // Get map generated by leaflet and update some parameters
    $scope.map2 = leafletData.getMap().then(function (map) {
      function formatJSON (rawjson) {	// callback that remap fields name
        var json = {},
          key, loc, disp = []

        for (var i in rawjson) {
          disp = rawjson[i].display_name.split(',')

          key = disp[0] + ', ' + disp[1]

          loc = L.latLng(rawjson[i].lat, rawjson[i].lon)

          json[ key ] = loc	// key,value format
        }

        return json
      }
    // For the place name search
      var searchOpts = {
        url: 'http://nominatim.openstreetmap.org/search?format=json&q={s}',
        jsonpParam: 'json_callback',
        formatData: formatJSON,
        zoom: 18,
        minLength: 2,
        autoType: false,
        marker: {
          icon: false,
          animate: false
        },
        position: 'topright'
      }
      map.addControl(new L.Control.Search(searchOpts))
      var zoomOptions = {
        position: 'topright'
      }
      map.addControl(new L.Control.Zoom(zoomOptions))


    })

    // Set geojson layer
    $scope.markers = new Array()

    // Set path for direction
    $scope.paths = {}

    // Create markers icons
    var localIcons = {
      defaultIcon: {},
      redIcon: {
        iconUrl: '../icons/camera_red.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -48]
      },
      greenIcon: {
        iconUrl: '../icons/camera_green.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -48]
      },
      yellowIcon: {
        iconUrl: '../icons/camera_yellow.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -48]
      }
    }
    // Set map center
    $scope.center = {
      lat: 46.78099, // 46.47532,
      lng: 8.01153, // 7.13708,
      zoom: 15
    }

    // Map events
    // ----------
    $scope.computeDirectionCoordinates = function (lat, lng) {

      return $q(function (resolve, reject) {
        leafletData.getMap().then(function (map) {
          var bbox = map.getBounds()
          var fromPt = turf.point([bbox.getWest(), bbox.getNorth()])
          var toPt = turf.point([bbox.getWest(), bbox.getSouth()])
          var distEW = turf.distance(fromPt, toPt, 'degrees')

          var fromPt = turf.point([bbox.getWest(), bbox.getSouth()])
          var toPt = turf.point([bbox.getEast(), bbox.getSouth()])
          var distNS = turf.distance(fromPt, toPt, 'degrees')

          var minExt = Math.min(distEW, distNS) / 4

          // Create line
          var line = turf.lineString([
         [$scope.marker.getLatLng().lng, $scope.marker.getLatLng().lat],
         [lng, lat]
          ], {})
          var length = turf.lineDistance(line, 'degrees') // Km

          // unitary vector
          var ux = (lng - $scope.marker.getLatLng().lng) / length
          var uy = (lat - $scope.marker.getLatLng().lat) / length

          var direction = {
            lat: $scope.marker.getLatLng().lat + uy * minExt,
            lng: $scope.marker.getLatLng().lng + ux * minExt
          }
          resolve(direction)
        })
      })
    }
    // Draw direction

    var mouseMoveEvent = 'leafletDirectiveMap.mousemove'
    $scope.$on(mouseMoveEvent, function (event, args) {
      if ((($scope.currentStep === 2) || ($scope.currentStep === 3)) && (!$scope.directionSetOnce)){

          $scope.computeDirectionCoordinates(args.leafletEvent.latlng.lat, args.leafletEvent.latlng.lng).then(function(direction){
            leafletData.getMap().then(function (map) {
              // Draw direction
              if ($scope.moveArrow) {
                //Delete row if exists
                map.removeLayer($scope.moveArrow)
                map.removeLayer($scope.moveArrowHead)
              }
              //Draw arrow
              $scope.moveArrow = L.polyline([[$scope.marker.getLatLng().lat, $scope.marker.getLatLng().lng], [direction.lat, direction.lng]],
                {
                  color: '#FFB028',
                  weight: 6,
                  opacity: 0.5
                }
              ).addTo(map)
              $scope.moveArrow.on('click', function(event) {
                $scope.directionSetOnce = true // the moving arrow is shown only if the direction is not set
                $scope.drawArrow(event.latlng)
              })
              $scope.moveArrowHead = L.polylineDecorator($scope.moveArrow, {
                patterns: [
                  {
                    offset: '100%',
                    repeat: 0,
                    symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true, color: '#FFB028', opacity: 0.5}})
                  }
                ]
              }).addTo(map)
              $scope.moveArrowHead.on('click', function(event) {
                $scope.directionSetOnce = true // the moving arrow is shown only if the direction is not set
                $scope.drawArrow(event.latlng)
              })
            })
          })
      }
    })
    // Function to compute azimuth
    XYtoAzimuth = function (XY1, XY2) {
      var dx = (XY2[0] - XY1[0])
      var dy = (XY2[1] - XY1[1])
      var alpha = 180.0 / Math.PI * Math.atan2(dx, dy)
      if (alpha < 0) {
        var alpha = alpha + 360
      }
      return alpha
    }
    // Draw provided direction
    $scope.directionSetOnce = false // The on move arrow is displayed only once
    $scope.$on('leafletDirectiveMap.click', function (event, args) {
    // if the client is mobile (phone or tablet)
      if ($scope.currentStep === 2){
        $scope.directionSetOnce = true
        $scope.drawArrow(args.leafletEvent.latlng)
      }else if ($scope.currentStep === 3){
        $scope.directionSetOnce = true
        $scope.drawArrow(args.leafletEvent.latlng)

        // Change the direction in cesium
        cesiumService.changeAzimuth($scope.providedCamera.orientation.azimuth, $scope.cesiumObject.cesium)
      }
    })

    $scope.drawArrow = function(latlng){
      $scope.computeDirectionCoordinates(latlng.lat, latlng.lng).then(function(direction){

        leafletData.getMap().then(function (map) {
          // Draw direction
          if ($scope.clickArrow) {
            //Delete row if exists
            try {
              map.removeLayer($scope.clickArrow)
            }catch(err){

            }
            try {
              map.removeLayer($scope.clickArrowHead)
            }catch(err){

            }
            try {
              map.removeLayer($scope.moveArrow)
            }catch(err){

            }
            try {
              map.removeLayer($scope.moveArrowHead)
            }catch(err){

            }

          }
          //Draw arrow
          $scope.clickArrow = L.polyline(
            [[$scope.marker.getLatLng().lat, $scope.marker.getLatLng().lng], [direction.lat, direction.lng]],
            {
              color: '#FFB028',
              weight: 6,
              opacity: 1
            }
          ).addTo(map);
          $scope.clickArrowHead = L.polylineDecorator($scope.clickArrow, {
            patterns: [
              {
                offset: '100%',
                repeat: 0,
                symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true, color: '#FFB028', opacity: 1}})
              }
            ]
          }).addTo(map)
        })
      })

    // Transform coordinates
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var XYmarker = proj4(WGS84proj, ch1903plusproj, [$scope.marker.getLatLng().lng, $scope.marker.getLatLng().lat])
      var XYclic = proj4(WGS84proj, ch1903plusproj, [latlng.lng, latlng.lat])
      var provAz = XYtoAzimuth(XYmarker, XYclic)

      $scope.providedCamera.location.X = XYmarker[0]
      $scope.providedCamera.location.Y = XYmarker[1]
      $scope.providedCamera.orientation.azimuth = provAz
    }

    // Openseadragon parameters
    // ------------------------
    $scope.options = {
      gcpIcon: '../icons/georef/target_green.png',
      gcpIconSelected: '../icons/georef/target_yellow.png',
    }

    // Initialise the image viewer
    $scope.optionsViewer =
    {
      prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
      gestureSettingsMouse: {
        clickToZoom: false,
        dblClickToZoom: false
      },
      showNavigator: false,
      showHomeControl: false,
      showFullPageControl: false,
      showZoomControl: true,
      gcpIconSelected: '../icons/georef/target_yellow.png',
      gcpIcon: '../icons/georef/target_green.png',
    }

    // Draw cursor in the image (world2image projection)
    $scope.drawCursorImage = function (projCoord) {
      var p = $scope.cameraParameters
      if (p != 'undefined') {
        var XYZ = math.matrix([[projCoord[0], projCoord[1], projCoord[2]], [projCoord[0], projCoord[1], projCoord[2]]])
      // Generate matrix of the parameters
        pMat = math.matrix([p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]])
      // Perspective
        var xy_cursor = project3Dto2D(XYZ, pMat)
        osdService.drawCursor($scope.osdObject.mysd, xy_cursor)
      }
    }
    $scope.osdObject = {
      mysd: null
    } // replace $scope.mysd, is used to avoid a stange behavior of ng-switch with the scope inheritance

  // Cesium parameters
  // -----------------
  // $scope.cesiumObject.cesium = null
  $scope.cesiumObject = {
    cesium: null,
    buildings: {
      tileset1: null,
      tileset2: null
    }
  } // replace $scope.cesium, is used to avoid a stange behavior of ng-switch with the scope inheritance

  // Reset camera location
    $scope.reinitializeCamera = function () {
    // Reset camera location
      var camera = $scope.cesiumObject.cesium.camera
      camera.frustum.fov = Cesium.Math.PI_OVER_THREE
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees($scope.cameraOrientationIni.longitude, $scope.cameraOrientationIni.latitude, $scope.cameraOrientationIni.height),
        orientation: {
          heading: $scope.cameraOrientationIni.heading,
          pitch: $scope.cameraOrientationIni.pitch,
          roll: $scope.cameraOrientationIni.roll
        }
      })
    }

  // Fix or relax camera location
    $scope.fixCamera = function () {
      if ($scope.providedCamera.fixed == true) {
        $scope.providedCamera.fixed = false
        var camEntity = $scope.cesiumObject.cesium.entities.getById('cameraPosition')
        camEntity.point.color.setValue(Cesium.Color.GREEN)
      } else {
        $scope.providedCamera.fixed = true
        var camEntity = $scope.cesiumObject.cesium.entities.getById('cameraPosition')
        camEntity.point.color.setValue(Cesium.Color.RED)
      }
    }

  // Compute footprint
    $scope.computeFootprint = function () {
    // Use the matrix of locations to generate the footprint
      var matLoc = $scope.locationMatrix.cartMatrix
      var matSize = math.size(matLoc)
      var height = math.subset(matSize, math.index(1))
      var width = math.subset(matSize, math.index(0))
    // Compute left border
      var left = []
      for (var i = 0; i < height; i++) {
      // Get value
        var val = math.subset(matLoc, math.index(0, i, 0))
        if (val != 0) {
          var cartesian = new Cesium.Cartesian3(math.subset(matLoc, math.index(0, i, 0)), math.subset(matLoc, math.index(0, i, 1)), math.subset(matLoc, math.index(0, i, 2)))
          var carto = Cesium.Cartographic.fromCartesian(cartesian)
          left.push([(carto.longitude * 180 / Cesium.Math.PI).toFixed(5), (carto.latitude * 180 / Cesium.Math.PI).toFixed(5), (carto.height + 10).toFixed(0)])
        }
      }
      left.reverse()
    // Compute right border
      var right = []
      for (var i = 0; i < height; i++) {
      // Get value
        var val = math.subset(matLoc, math.index(width - 1, i, 0))
        if (val != 0) {
          var cartesian = new Cesium.Cartesian3(math.subset(matLoc, math.index(width - 1, i, 0)), math.subset(matLoc, math.index(width - 1, i, 1)), math.subset(matLoc, math.index(width - 1, i, 2)))
          var carto = Cesium.Cartographic.fromCartesian(cartesian)
          right.push([(carto.longitude * 180 / Cesium.Math.PI).toFixed(5), (carto.latitude * 180 / Cesium.Math.PI).toFixed(5), (carto.height + 10).toFixed(0)])
        }
      }
    // Compute bottom
      var bottom = []
      for (var i = 0; i < width; i++) {
      // Get value
        var val = math.subset(matLoc, math.index(i, height - 1, 0))
        if (val != 0) {
          var cartesian = new Cesium.Cartesian3(math.subset(matLoc, math.index(i, height - 1, 0)), math.subset(matLoc, math.index(i, height - 1, 1)), math.subset(matLoc, math.index(i, height - 1, 2)))
          var carto = Cesium.Cartographic.fromCartesian(cartesian)
          bottom.push([(carto.longitude * 180 / Cesium.Math.PI).toFixed(5), (carto.latitude * 180 / Cesium.Math.PI).toFixed(5), (carto.height + 10).toFixed(0)])
        }
      }
      bottom.reverse()
    // Compute top
      var top = []
      for (var i = 0; i < width; i++) {
        var findBool = false
        for (var j = 0; j < height; j++) {
          if (findBool == false) {
            var val = math.subset(matLoc, math.index(i, j, 0))
            if (val != 0) {
              var cartesian = new Cesium.Cartesian3(math.subset(matLoc, math.index(i, j, 0)), math.subset(matLoc, math.index(i, j, 1)), math.subset(matLoc, math.index(i, j, 2)))
              var carto = Cesium.Cartographic.fromCartesian(cartesian)
              top.push([(carto.longitude * 180 / Cesium.Math.PI).toFixed(5), (carto.latitude * 180 / Cesium.Math.PI).toFixed(5), (carto.height + 10).toFixed(0)])
              findBool = true
            }
          }
        }
      }
    // Merge sides
      var poly = left.concat(top).concat(right).concat(bottom)
    // Close Polygon
      poly.push(left[0])
      var data = {
        line: poly
      }
      var footprintGeojson = {
        type: 'LineString',
        coordinates: poly,
        crs: {
          type: 'name',
          properties: {
            name: 'EPSG:4326'
          }
        }
      }
      $scope.footprintGeojson = JSON.stringify(footprintGeojson)
      var postFootprintPromise = dbService.postFootprint($scope.imageId, $scope.footprintGeojson, $scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude)
    // Send geojson to the database
      postFootprintPromise.then(function successCallback (response) {
      }, function errorCallback (response) {
        console.log('save footprint fails', response)
      })
    }

    $scope.getWorldCoordinates = function (maxMatSize) {

      return $q(function (resolve, reject) {
        // $scope.setCollisionDetection();
          cesiumService.setCollisionDetection($scope.cesiumObject.cesium)
        // Save matrix of coordinates (to link image mouse with globe)
        // -----------------------------------------------------------
        // Problem 1: the canvas size dont have the same ratio as the image
        // either the width match (portrait) or the height (landscape)
        // Problem 2: We can not store every location (to much memory and time)
        // Set max size of the matrix.
        // var maxMatSize = 10;
        // Get canvas size
          var canvasWidth = $scope.cesiumObject.cesium.canvas.width
          var canvasHeight = $scope.cesiumObject.cesium.canvas.height
        // Get image size
          var imageWidth = $scope.osdObject.mysd.viewer.source.Image.Size.Width
          var imageHeight = $scope.osdObject.mysd.viewer.source.Image.Size.Height
        // Compute image ratio
          var ratioIm = imageHeight / imageWidth
        // Compute canvas ratio
          var ratioCanvas = canvasHeight / canvasWidth
        // The comparison of the ratio indicates if the height or the width is limiting
          if (ratioCanvas < ratioIm) {
          // The height is fitted, there is a horizontal offset
          // Compute the width of the image in the canvas
            var imWidthInCanvas = canvasHeight / ratioIm
            var imHeightInCanvas = canvasHeight
          // Compute offset
            var widthOffset = (canvasWidth - imWidthInCanvas) / 2
            var heightOffset = 0
          // Compute scale (image to canvas)
            var scale = canvasHeight / imageHeight
          // Compute scale (canvas to matrix)
            var scaleMat = maxMatSize / canvasHeight
          } else {
          // The width is fitted, there is a vertical offset
          // Compute the height of the image in the canvas
            var imHeightInCanvas = canvasWidth * ratioIm
            var imWidthInCanvas = canvasWidth
          // Compute offset
            var heightOffset = (canvasHeight - imHeightInCanvas) / 2
            var widthOffset = 0
          // Compute scale (image to canvas)
            var scale = canvasWidth / imageWidth
          // Compute scale (canvas to matrix)
            var scaleMat = maxMatSize / canvasWidth
          };
        // Compute size of the matrix
          var matWidth = Math.round(imWidthInCanvas * scaleMat)
          var matHeight = Math.round(imHeightInCanvas * scaleMat)
          $scope.cartMatrixPromise = cesiumService.cartMatrixPromiseFunction($scope.cesiumObject.cesium, matWidth, matHeight, widthOffset, heightOffset, scaleMat)
          $scope.cartMatrixPromise.then(function (res) {
            $scope.loading = false
            $scope.locationMatrix = res
            $scope.computeFootprint()
            resolve()
            // var topoPromise = topoService.getToponyms($scope.footprintGeojson, $scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude)
            // topoPromise.then(function (response) {
            //   //$scope.toponyms.show = true
            //   $scope.geojsonToponyms = response
            //
            //   $scope.geojsonToponyms = topoService.filterToponyms($scope.geojsonToponyms, $scope.orientationArray[0].lat, $scope.orientationArray[0].long)// $scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude)
            // // Filter toponyms by the distance
            //   var topo3DPromise = topoService.geojsonTo3D($scope.geojsonToponyms, $scope.cesiumObject.cesium, $scope.cesiumObject.cesium.terrainProvider)
            //   topo3DPromise.then(function (response) {
            //     // topoService.drawToponyms(response, $scope.cesiumObject.cesium, $scope.tilesLoadedPromise)
            //   .then(function (response) {
            //     $scope.toponyms.geotags = response.geotags
            //     $scope.toponyms.icons = response.icons
            //     topoService.postTags($scope.toponyms.geotags, $scope.imageId).then(function (response) {
            //       resolve()
            //     })
            //   })
            //   })
            // })
          }, function (error) {
            console.log('promise error', error)
          })
        // Set parameter to false to avoid the record in the next camera movement
          $scope.computePoseBool = false

      })
    }

  $scope.$watch('cesiumObject.cesium', function(){
    if ($scope.mode == 'validation_viz'){
      //$scope.drawGCP()
    } else if ($scope.updateMode && $scope.mode == 'correspondences'){
      $scope.hideGCP()
      $scope.drawGCP()
    }
  })


  // Function to switch steps
  // ------------------------
  // $scope.switchStep = function () {
  //   if ($scope.mode == 'location'){
  //     $scope.switchToDirectionStep()
  //   } else if ($scope.mode == 'direction') {
  //     $scope.switchToCorrespondancesStep()
  //   }
  // }
  $scope.switchToCorrectionsStep = function () {

    // if the corrections or the observations are enabled
    if ($scope.image.observation_enabled || $scope.image.correction_enabled){
      // show
      $scope.show.back = true
      $scope.showValidate = true
      //progression
      $scope.progression = [true, true, true, true, true]
      $scope.instruction = $scope.texts.instrMeta[$scope.lang]
      $scope.longInstruction = $scope.texts.instrMetaLong[$scope.lang]

      // change step if necessary
      $scope.currentStep = 4
      $scope.mode = 'correction'

      // initialise correction model
      if (!$scope.correction) {
        $scope.correction = {
          dateShot: $scope.image.date_shot,
          title: $scope.image.title,
          caption: $scope.image.caption,
          dateShotCorr: false,
          titleCorr: false,
          captionCorr: false
        }
      }

      // initialise remark model
      if (!$scope.remark) {
        $scope.remark = {
          text: '',
          provided: false
        }
      }

      // initialise observation model
      if (!$scope.observations) {
        $scope.observations = {
          provided: false,
          list: [],
          currentId: 1,
          currentObservation: {
            observation: null,
            coord_x: null,
            coord_y: null,
            id: 1
          }
        }
      }
    }else{
      // The corrections and observation are not enabled. Go to mono
      try {
        $window.close()
      }catch(err){
        $scope.goToMono()
      }


    }



  }

    // Switch to location mode
    $scope.switchToLocationStep = function (imageId) {

      $scope.rightBox = 'map'
      $scope.eraseDirectionMarker()
      $scope.showLocationMarker()

      // show
      $scope.show.back = true
      $scope.showValidate = true
      // progression
      $scope.progression = [true, false, false, false, false]
      $scope.instruction = $scope.texts.instructionChooseLocationShort[$scope.lang]
      $scope.longInstruction = $scope.texts.instructionChooseLocation[$scope.lang]

      // change step if necessary
      $scope.mode = 'location'

      // Initialize provide camera
      $scope.providedCamera.location.latitude = $scope.image.latitude
      $scope.providedCamera.location.longitude = $scope.image.longitude

      // Initalise camera orientation
      if (!$scope.cameraOrientationIni) {
        $scope.cameraOrientationIni = {}
        $scope.cameraOrientationIni.longitude = null
        $scope.cameraOrientationIni.latitude = null
        $scope.cameraOrientationIni.height = null
        $scope.cameraOrientationIni.heading = null
        $scope.cameraOrientationIni.pitch = null
        $scope.cameraOrientationIni.roll = null
      }
    }

    $scope.switchToTutoStep = function () {
      // show
      $scope.show.back = false
      $scope.showValidate = true
      // Change step
      $scope.currentStep = 0
      $scope.mode = 'tutorial'
    }

    $scope.switchToDirectionStep = function () {

      $scope.rightBox = 'map'

      $scope.showDirectionMarker()

      // show
      $scope.show.back = true
      $scope.showValidate = true
      //progression
      $scope.progression = [true, true, false, false, false]
      $scope.instruction = $scope.texts.instructionChooseDirectionShort[$scope.lang]
      $scope.longInstruction = $scope.texts.instructionChooseDirection[$scope.lang]

      // Change step
      $scope.currentStep = 2
      $scope.mode = 'direction'

      //AuthService.getMyName().then(function () {

        // Get coordinates of the marker
        var WGS84proj = 'EPSG:4326'
        var ch1903proj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs '
        // var XY = proj4(WGS84proj, ch1903proj, [$scope.marker.getLatLng().lng, $scope.marker.getLatLng().lat])
        var XY = proj4(WGS84proj, ch1903proj, [$scope.providedCamera.location.longitude, $scope.providedCamera.location.latitude])

        $scope.providedCamera.location.X = XY[0]
        $scope.providedCamera.location.Y = XY[1]
        $scope.providedCamera.location.Z = 0

        $scope.providedCamera.orientation.azimuth = 0
      //})
    }

    // Function to change the transparency
    $scope.transparency = {
      value: 1
    }
    $scope.setTransparency = function (){
      cesiumService.changeModelTransparency($scope.model, $scope.transparency.value)
    }

    // Function to change the scale
    $scope.scales = {
      linear: 1,
      log: 0
    }
    try {
      $scope.scales.log = Math.log10($scope.scales.linear)
    } catch (exception) {
      $scope.scales.log = Math.log($scope.scales.linear)/Math.log(10)
    }
    $scope.changeScaleLog = function () {
      $scope.scales.linear = Math.pow(10, $scope.scales.log)
      $scope.model.scale = $scope.scales.linear
    }

    $scope.switchToVisualizationStep = function () {
      // show
      $scope.show.back = false
      $scope.showValidate = false
      //progression
      $scope.progression = [true, true, true, true, true]
      $scope.instruction = $scope.texts.checkGeoloc[$scope.lang]
      $scope.longInstruction = ''

      // Open virtual globe
      $scope.showGlobe = true

      // Change step if necessary
      $scope.currentStep = 6
      $scope.mode = 'validation_viz'

      // Initialize globe view parameters
      // DrawGCP() is called in angular-osd-georef.js
      $scope.initializeGlobeView()
    }

    $scope.switchToReason = function () {
      // show
      $scope.show.back = false
      $scope.showValidate = true
      //progression
      $scope.progression = [true, true, true, true, true]
      $scope.instruction = $scope.texts.instrReject[$scope.lang]
      $scope.longInstruction = ''
      // Change step if necessary
      $scope.currentStep = 7
    }

    $scope.switchToCorrespondancesStep = function () {
      console.log('switcht zo correspondencse')

      //$scope.drawGCP() is called from angular-osd-georef.js

      // show
      $scope.rightBox = 'globe' //keep the right part as a globe
      $scope.show.back = true
      $scope.show.validateOrientation = false
      $scope.showValidate = false
      //progression
      $scope.progression = [true, true, true, false, false]
      $scope.instruction = $scope.texts.instrMatchShort[$scope.lang]
      $scope.longInstruction = $scope.texts.instrMatch[$scope.lang]
      // Open virtual globe
      $scope.showGlobe = true // useless?

      // Change step if necessary
      $scope.currentStep = 3
      $scope.mode = 'correspondences'

      // Draw the mini map
      $scope.directionSetOnce = false
      $scope.markers = {
          main: {
              lat: $scope.providedCamera.location.latitude,
              lng: $scope.providedCamera.location.longitude,
              focus: true,
              icon: localIcons.yellowIcon,
              // message: "This place is in London",
              draggable: true,
              enable: ['dragend']
          }
      }
      // $scope.dirArrowHead = {
      //     markers: {
      //         coordinates: [
      //           [$scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude],
      //           [$scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude + 0.1]
      //         ],
      //         patterns: [
      //             {
      //               offset: '100%',
      //               repeat: 0,
      //               symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true, color: '#FFB028', opacity: 1}})
      //             }
      //         ]
      //     }
      // }
      // $scope.dirArrow = {
      //   polyline: {
      //      type: "polyline",
      //      color: '#FFB028',
      //      weight: 8,
      //      latlngs: [
      //        {lat: $scope.providedCamera.location.latitude, lng: $scope.providedCamera.location.longitude},
      //        {lat: $scope.providedCamera.location.latitude, lng: $scope.providedCamera.location.longitude+0.1}
      //      ]
      //   }
      // }
      $scope.center = {
        lat: $scope.providedCamera.location.latitude,
        lng: $scope.providedCamera.location.longitude,
        zoom: 19
      }
      $scope.$on('leafletDirectiveMap.load', function (event, args) {
        console.log('LOAD1', $scope.clickArrowHead)
        console.log('LOAD2', $scope.clickArrowHead._paths[0][1])
        // Show the camera position
        leafletData.getMap().then(function (map) {

          $scope.drawArrow($scope.clickArrowHead._paths[0][1])

            // if ($scope.marker){
            //   map.removeLayer($scope.marker)
            // }
            // $scope.marker = L.marker([$scope.providedCamera.location.latitude, $scope.providedCamera.location.longitude],
            //   {
            //     icon: L.icon(localIcons.yellowIcon),
            //     draggable: true
            //   }).addTo(map)//.bindPopup($scope.texts.popupDir[$scope.lang], {'maxWidth': ($scope.leafWidth / 5) + 'px', 'className': 'popupLeaflet'}).openPopup()
              // $scope.clickArrow = L.polyline(
              //   [[$scope.marker.getLatLng().lat, $scope.marker.getLatLng().lng], [direction.lat, direction.lng]],
              //   {
              //     color: '#FFB028',
              //     weight: 6,
              //     opacity: 1
              //   }
              // ).addTo(map)
              //
              // $scope.clickArrowHead = L.polylineDecorator($scope.clickArrow, {
              //   patterns: [
              //     {
              //       offset: '100%',
              //       repeat: 0,
              //       symbol: L.Symbol.arrowHead({pixelSize: 15, polygon: false, pathOptions: {stroke: true, color: '#FFB028', opacity: 1}})
              //     }
              //   ]
              // }).addTo(map)
          })

          // Stores marker coordinates
          // $scope.marker.on('dragend', function (e, args) {
          //   $scope.providedCamera.location.latitude = e.target.getLatLng().lat
          //   $scope.providedCamera.location.longitude = e.target.getLatLng().lng
          //   $scope.image.latitude = e.target.getLatLng().lat
          //   $scope.image.longitude = e.target.getLatLng().lng
          // })
          //.bindPopup($scope.texts.popupDir[$scope.lang], {'maxWidth': ($scope.leafWidth / 5) + 'px', 'className': 'popupLeaflet'}).openPopup()
      // if the client is mobile (phone or tablet)
        // if ($scope.currentStep === 2) {
        //   $scope.directionSetOnce = true
        //
        // }
      })
      $scope.markerMoved = false
      var dragEndEvent = 'leafletDirectiveMarker.dragend'
      $scope.$on(dragEndEvent, function (event, args) {
        $scope.markerMoved = true

        leafletData.getMap().then(function (map) {
          // Trick for the arrow move with the marker
          $scope.marker = L.marker([args.leafletEvent.target._latlng.lat, args.leafletEvent.target._latlng.lng])
          // Move the arrow
          try {
            map.removeLayer($scope.clickArrow)
          }catch(err){

          }
          try {
            map.removeLayer($scope.clickArrowHead)
          }catch(err){

          }
          $scope.clickArrow._latlngs[0] = args.leafletEvent.target._latlng
          $scope.clickArrowHead._paths[0][0] = args.leafletEvent.target._latlng
          $scope.clickArrow.addTo(map)
          $scope.clickArrowHead.addTo(map)

          var lat = args.leafletEvent.target._latlng.lat
          var lng = args.leafletEvent.target._latlng.lng
          map.panTo(args.leafletEvent.target._latlng)
          cesiumService.changeLatLng(lat, lng, $scope.cesiumObject.cesium, $scope.cesiumObject.cesium.terrainProvider)
          // var XYmarker = proj4(WGS84proj, ch1903plusproj, [lng, lat])
          //$scope.providedCamera.location.X = XYmarker[0]
          //$scope.providedCamera.location.Y = XYmarker[1]
          $scope.providedCamera.location.lat = lat
          $scope.providedCamera.location.lng = lng

        })
      })
      // var moveEvent = 'leafletDirectiveMarker.move'
      // $scope.$on(moveEvent, function (event, args) {
      //   // $scope.center = {
      //   //   lat: args.leafletEvent.latlng.lat,
      //   //   lng: args.leafletEvent.latlng.lng,
      //   //   zoom: 19}
      //   // leafletData.getMap().then(function (map) {
      //   //   map.panTo(args.leafletEvent.latlng)
      //   // })
      //   // Center the map
      // })



      // if update mode or back from modifications the gcp are already initialized
      if (($scope.updateMode === false) && ($scope.backFromModif === false)) {
        // Create empty GCP
        var GCP = {
          id: 0,
          x: null,
          y: null,
          X: null,
          Y: null,
          Z: null,
          eM: null,
          ePix: null
        }
        $scope.myGCPs.curGCPid = 0
        // Push it in the dictionary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
        // Select the current GCP
        $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
        console.log('update gcps')
      } else {
        // Draw gcp
        // The drawing is perfomed by osd

        // if ($scope.stepBeforeProblem !== 3 ){ // to avoid a conflict after report a problem in correspondeces mode
        //   console.log('in')
        //   $scope.hideGCP()
        //   $scope.drawGCP()
        // }

      }

      $scope.initializeGlobeView()

      // Camera location is set in angular-cesium-georef.js

    // Set camera location
      // var camera = $scope.cesiumObject.cesium.camera
      // if ($scope.cameraPosition.height != null) {
      //   camera.setView({
      //     destination: Cesium.Cartesian3.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude, $scope.cameraPosition.height + 1000),
      //     orientation: {
      //       heading: $scope.cameraOrientation.heading,
      //       pitch: -Cesium.Math.PI_OVER_FOUR,
      //       roll: $scope.cameraOrientation.roll
      //     }
      //   })
      //   camera.flyTo({
      //     destination: Cesium.Cartesian3.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude, $scope.cameraPosition.height),
      //     orientation: {
      //       heading: $scope.cameraOrientation.heading,
      //       pitch: $scope.cameraOrientation.pitch,
      //       roll: $scope.cameraOrientation.roll
      //     },
      //     duration: 10
      //   })
      // } else {
      //   var position = [Cesium.Cartographic.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude)]
      //   var promise = Cesium.sampleTerrain($scope.cesiumObject.cesium.terrainProvider, 17, position)
      //   Cesium.when(promise, function (updatedPosition) {
      //     $scope.cameraPosition.height = updatedPosition[0].height + parseFloat($scope.image.apriori_height)
      //     $scope.cameraOrientationIni.height = updatedPosition[0].height + parseFloat($scope.image.apriori_height)
      //     camera.setView({
      //       destination: Cesium.Cartesian3.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude, $scope.cameraPosition.height + 1000),
      //       orientation: {
      //         heading: $scope.cameraOrientation.heading,
      //         pitch: -Cesium.Math.PI_OVER_FOUR,
      //         roll: $scope.cameraOrientation.roll
      //       }
      //     })
      //     camera.flyTo({
      //       destination: Cesium.Cartesian3.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude, $scope.cameraPosition.height),
      //       orientation: {
      //         heading: $scope.cameraOrientation.heading,
      //         pitch: $scope.cameraOrientation.pitch,
      //         roll: $scope.cameraOrientation.roll
      //       },
      //       duration: 10
      //     })
      //   })
      // }
    }

    $scope.initializeGlobeView = function () {

      // Initialise globe view
      if ($scope.image.georef_3d_bool === true) {
        $scope.cameraPosition = {
          longitude: parseFloat($scope.image.longitude),
          latitude: parseFloat($scope.image.latitude),
          height: parseFloat($scope.image.height)
        }
        $scope.cameraOrientation = {
          heading: Cesium.Math.toRadians($scope.image.azimuth),
          pitch: Cesium.Math.toRadians($scope.image.tilt),
          roll: Cesium.Math.toRadians($scope.image.roll)
        }

    // Comming from leaflet
      } else {
        $scope.cameraPosition = {
          longitude: parseFloat($scope.providedCamera.location.longitude),
          latitude: parseFloat($scope.providedCamera.location.latitude)
          // if the height is not specified the height of the DEM + the apriori height is used as apriori
        }
        $scope.cameraOrientation = {
          heading: Cesium.Math.toRadians($scope.providedCamera.orientation.azimuth),
          roll: 0.0
        }
        //Set pitch according to the type of photography
        if ($scope.image.view_type === 'terrestrial') {
          $scope.cameraOrientation.pitch = Cesium.Math.PI / 8
        } else if ($scope.image.view_type === 'lowOblique') {
          $scope.cameraOrientation.pitch = 0
        } else if ($scope.image.view_type === 'highOblique') {
          $scope.cameraOrientation.pitch = -Cesium.Math.PI_OVER_FOUR
        } else if ($scope.image.view_type === 'nadir') {
          $scope.cameraPosition.pitch = -Cesium.Math.PI_OVER_FOUR
        } else {
          $scope.cameraOrientation.pitch = -Cesium.Math.PI / 8
        }
      }

      // To reinitialise cesium view
      $scope.cameraOrientationIni = {
        longitude: $scope.cameraPosition.longitude,
        latitude: $scope.cameraPosition.latitude,
        height: $scope.cameraPosition.height,
        heading: $scope.cameraOrientation.heading, // -Cesium.Math.PI_OVER_FOUR,
        pitch: $scope.cameraOrientation.pitch,
        roll: $scope.cameraOrientation.roll
      }
    }


      // Functions for the interaction between the modules
      // -------------------------------------------------
      // Create new GCP
        $scope.CreateNewGCP = function () {
        // increment GCP identifier
          $scope.myGCPs.curGCPid = $scope.myGCPs.curGCPid + 1
        // Create empty GCP
          var GCP = {
            id: $scope.myGCPs.curGCPid,
            x: null,
            y: null,
            X: null,
            Y: null,
            Z: null,
            eM: null,
            ePix: null
          }
        // Push it in the dictionary
          $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
        // Select the current GCP
        // $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid;
        }
      // Get world coordinates from cesium
        $scope.cesiumGCP = function (XYZ) {
        // If current GCP is empty
          if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X == null) {
          // Push correspondance in the dictionnary
            $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X = XYZ[0]
            $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Y = XYZ[1]
            $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Z = XYZ[2]

            $scope.unselectAllGCP()
            $scope.selectGCP($scope.myGCPs.curGCPid)
          // Check if GCP is finished
            if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x != null) {
              $scope.CreateNewGCP()
            }
          } else {
            alert($scope.texts.alertOSD[$scope.lang])
          };
        }

      // Get image coordinate from OSD
        $scope.osdGCP = function (xy, viewportPoint) {
          console.log('$scope.myGCPs osdGCP', $scope.myGCPs)
          if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x == null) {
          // Push correspondance in the dictionnary
            $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x = xy[0]
            $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].y = xy[1]

            osdService.drawGCPLabel($scope.osdObject.mysd, $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid], $scope.optionsViewer)
            $scope.unselectAllGCP()
            $scope.selectGCP($scope.myGCPs.curGCPid)

          // Check if GCP is finished
            if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X != null) {
            // $scope.unselectExceptCurGCP();
              $scope.CreateNewGCP()
            }
          } else {
            alert($scope.texts.alertGlobe[$scope.lang])
          };
        }
        $scope.alertClickGlobe = function () {
          alert($scope.texts.alertGlobe[$scope.lang])
        }
        $scope.alertClickImage = function () {
          alert($scope.texts.alertOSD[$scope.lang])
        }
        $scope.alertClickBuilding = function () {
          alert($scope.texts.alertBuilding[$scope.lang])
        }
      // Draw a GCP in OSD and cesium
        $scope.drawOneGCP = function (gcp) {
        // Draw openseadragon
          osdService.drawGCPLabel($scope.osdObject.mysd, gcp, $scope.options)
        // Draw cesium
          var WGS84proj = 'EPSG:4326'
          var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
          var longLat = proj4(ch1903plusproj, WGS84proj, [gcp.X, gcp.Y])
          gcp.longitude = longLat[0]
          gcp.latitude = longLat[1]
          cesiumService.drawGCPLabel($scope.cesiumObject.cesium, gcp)
        }

      // Draw all GCP
        $scope.drawGCP = function () {
          // Called from angular-osd-georef when initializing osd
          // $scope.hideGCP()
          for (var i in $scope.myGCPs.dictGCPs) {
            var gcp = $scope.myGCPs.dictGCPs[i]
            if ((gcp.x != null) && (gcp.X != null)) {
              $scope.drawOneGCP(gcp)
            }
          }
        }

      // Function select GCP
        $scope.selectGCP = function (idClicked) {
        // if is reprojection
          if (idClicked >= 200) {
            idClicked = idClicked - 200
          }
        // Unselection
        // -----------
        // unselect osd
          osdService.unselectAll($scope.osdObject.mysd, $scope.optionsViewer)
        // Unselect cesium
          cesiumService.unselectAll($scope.cesiumObject.cesium, $scope.myGCPs)
        // Unselect GCPs
          if ($scope.myGCPs.selectedGCPid == idClicked) {
            $scope.myGCPs.selectedGCPid = null
          } else {
          // Selection
          // ---------
          // Select Viewer
            if ($scope.myGCPs.dictGCPs[idClicked].x != null) {
              osdService.selectGCP($scope.osdObject.mysd, $scope.optionsViewer, idClicked)
            }
          // Select Cesium
            if ($scope.myGCPs.dictGCPs[idClicked].X != null) {
              cesiumService.selectGCP($scope.cesiumObject.cesium, idClicked)
            }
          // Select Table
            $scope.myGCPs.selectedGCPid = idClicked
          }
        }
      // Function unselect All GCP
        $scope.unselectAllGCP = function () {
        // Unselection
        // -----------
        // unselect viewer
          osdService.unselectAll($scope.osdObject.mysd, $scope.optionsViewer)
        // Unselect cesium
          cesiumService.unselectAll($scope.cesiumObject.cesium, $scope.myGCPs)
        // Unselect GCPs
          $scope.myGCPs.selectedGCPid = null
        }

      // Function unselect All GCP
        $scope.unselectExceptCurGCP = function () {
          $scope.selectGCP($scope.myGCPs.curGCPid)
        }
      // Function delete GCP
        $scope.deleteSelectedGCP = function () {
          // If a point is  selected
          if ($scope.myGCPs.selectedGCPid != null) {
            // erase all the GCPs
            $scope.hideGCP()

            idSelected = $scope.myGCPs.selectedGCPid

            // Delete element in the dictionary
            delete $scope.myGCPs.dictGCPs[idSelected]
            // Reset the labels
            // Get the keys
            var keys = Object.keys($scope.myGCPs.dictGCPs)
            var newDict = {}
            for (var i=0; i<keys.length; i++){
              $scope.myGCPs.dictGCPs[keys[i]].id = i
              var gcp = $scope.myGCPs.dictGCPs[keys[i]]
              gcp.id = i
              newDict[i] = gcp
            }
            // Create empty gcp if doesnt exists (if the last gcp is deleted)
            if ((newDict[i-1].x != null) && (newDict[i-1].X != null)){
              newDict[i] = {
                x: null,
                y: null,
                X: null,
                Y: null,
                Z: null,
                id: i
              }
              i+=1
            }
            $scope.myGCPs.dictGCPs = newDict
            $scope.myGCPs.curGCPid = i - 1
            $scope.myGCPs.selectedGCPid = null
            // draw gcps
            $scope.drawGCP()
          }
        }

    $scope.resetGCP = function () {
      // Reinitialize GCP model
      $scope.myGCPs = {
        curGCPid: 0,
        selectedGCPid: null,
        dictGCPs: {},
        file: null
      }

      // Create empty GCP
      var GCP = {
        id: $scope.myGCPs.curGCPid,
        x: null,
        y: null,
        X: null,
        Y: null,
        Z: null,
        eM: null,
        ePix: null
      }
      // Push it in the dictionary
      $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      //$scope.CreateNewGCP()
    }
    $scope.hideGCP = function () {
      // Delete GCP in OSD and cesium
      var keys = Object.keys($scope.myGCPs.dictGCPs)
      for (var i = 0; i < keys.length; i++) {
        // var id = $scope.gcpArray[i].id
        var id = keys[i]
        osdService.deleteGCP($scope.osdObject.mysd, id)
        cesiumService.deleteGCP($scope.cesiumObject.cesium, $scope.myGCPs, id)
      }
    }
    $scope.deleteAllGCP = function(){
      $scope.hideGCP()
      $scope.resetGCP()
    }


    // Move camera within cesium
    $scope.moveUp = function () {
      cesiumService.moveUp($scope.cesiumObject.cesium, $scope.cesiumObject.cesium.terrainProvider)
    }
    $scope.moveLeft = function () {
      cesiumService.moveLeft($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.moveRight = function () {
      cesiumService.moveRight($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.moveDown = function () {
      cesiumService.moveDown($scope.cesiumObject.cesium)
    }
    $scope.moveFront = function () {
      cesiumService.moveFront($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.moveBack = function () {
      cesiumService.moveBack($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.moveIn = function () {
      cesiumService.moveFront($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.moveOut = function () {
      cesiumService.moveBack($scope.cesiumObject.cesium)
      var camera = $scope.cesiumObject.cesium.camera
      // Get camera projected coordinates
      var camCarto = Cesium.Cartographic.fromCartesian(camera.position)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)
      $scope.moveMap(camLong,camLat)
    }
    $scope.turnUp = function () {
      cesiumService.turnUp($scope.cesiumObject.cesium)
    }
    $scope.turnLeft = function () {
      cesiumService.turnLeft($scope.cesiumObject.cesium)
    }
    $scope.turnRight = function () {
      cesiumService.turnRight($scope.cesiumObject.cesium)
    }
    $scope.turnDown = function () {
      cesiumService.turnDown($scope.cesiumObject.cesium)
    }
    $scope.zoomIn = function () {
      cesiumService.zoomIn($scope.cesiumObject.cesium)
    }
    $scope.zoomOut = function () {
      cesiumService.zoomOut($scope.cesiumObject.cesium)
    }

    // Functions of the commands
    // -------------------------
    // Save camera orientation
    $scope.saveCamera = function () {
      return $q(function (resolve, reject) {
        if ($scope.showValidate){
          // if ($scope.showValidateOrientation){
          //   AuthService.loadUserCredentials();
          if (AuthService.isAuthenticated()) {
            // $scope.showLogin = false
            $scope.volunteerId = $scope.volunteer.id
          } else {
            // popup login
            $scope.openLoginInfo()
            // $scope.showLogin = true
            $scope.volunteerId = 14 // Default volunteer
          }
          //     $scope.getMyName()
          $scope.orientationIsStored = true

          // Go to location
          // --------------
          $scope.hideGCP()
          $scope.transparency.value = 1
          cesiumService.changeModelTransparency($scope.model, $scope.transparency.value)
          cesiumService.lockCamera($scope.cesiumObject.cesium)
          $scope.showGlobeButton = false// hide navigation buittons
          $scope.cesiumObject.cesium.camera.frustum.fov = $scope.cesiumObject.cesiumFov // reset fov
          $scope.model.show = true // show Model
          cesiumService.goToModelPromise($scope.orientationArray, $scope.cesiumObject.cesium.camera).then(function () {
              // Create footprint
              // ----------------
            var maxMatSize = 20
            $scope.getWorldCoordinates(maxMatSize).then(function(){
              // Save camera orientation in the database
              // ---------------------------------------
              // Create GCP json
              var gcpJson = new Object()
              var nGCP = $scope.gcpArray.length
              for (var i = 0; i<nGCP; i++){//} in $scope.gcpArray) {
                var id = i.toString()
                gcpJson[id] = $scope.gcpArray[i]
                if (typeof $scope.gcpArray[i].x !== 'string'){
                  gcpJson[id].x = $scope.gcpArray[i].x.toFixed(1)
                  gcpJson[id].y = $scope.gcpArray[i].y.toFixed(1)
                  gcpJson[id].X = $scope.gcpArray[i].X.toFixed(1)
                  gcpJson[id].Y = $scope.gcpArray[i].Y.toFixed(1)
                  gcpJson[id].Z = $scope.gcpArray[i].Z.toFixed(1)
                }
              }
              if ($scope.updateMode === true) {
                if (!$scope.validatorId) {
                  $scope.validatorId = null
                }
                dbService.updateOrientation($scope.imageId, $scope.orientationArray, $scope.volunteerId, gcpJson, $scope.scoreGCPs, $scope.surfaceGCPs, $scope.validationMode, $scope.validatorId).then(function () {
                  dbService.copyGltf($scope.imageId, $scope.image.collection_id).then(function () {
                    resolve()
                  })
                })
              } else {
                dbService.saveOrientation($scope.imageId, $scope.orientationArray, $scope.volunteerId, gcpJson, $scope.scoreGCPs, $scope.surfaceGCPs).then(function () {
                  dbService.copyGltf($scope.imageId, $scope.image.collection_id).then(function () {
                    resolve()
                  })
                })
              }
            })
          })

        }else{
          alert($scope.texts.alertValidationPose[$scope.lang])
          reject()
        }
      })
    }
    $scope.imagery = {
      id: 'swisstopo',
      name: 'Switch to Bing Map'
    }


    $scope.switchImagery = function () {

      var layers = $scope.cesiumObject.cesium.imageryLayers
      var baseLayer = layers.get(0);
      layers.remove(baseLayer);

      if ($scope.imagery.id === 'bing'){
        //$scope.imagery.id = 'bing'
        $scope.imagery.name = 'Switch to swisstopo'

        layers.addImageryProvider(new Cesium.BingMapsImageryProvider({
          url : 'https://dev.virtualearth.net',
          key : 'Ai1ve4rnjQSwI9BpNNF0cv23lthgOcFuL6EJiwHQu1xf7qxHcRu82xjNJnWDpa5F', //'get-yours-at-https://www.bingmapsportal.com/',
          mapStyle : Cesium.BingMapsStyle.AERIAL
        }))

      }else if ($scope.imagery.id === 'swisstopo'){
        //$scope.imagery.id = 'swisstopo'
        $scope.imagery.name = 'Switch to Bing Map'
        var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741);
        layers.addImageryProvider(new Cesium.UrlTemplateImageryProvider({
          url: "//wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/{z}/{x}/{y}.jpeg",
          subdomains: '56789',
          availableLevels: [8, 10, 12, 14, 15, 16, 17, 18],
          minimumRetrievingLevel: 8,
          maximumLevel: 17,
          tilingScheme: new Cesium.GeographicTilingScheme({
            numberOfLevelZeroTilesX: 2,
            numberOfLevelZeroTilesY: 1
          }),
          rectangle: rectangle
          })
        )
      }

    }

    $scope.computeGCPSurface = function () {
      // Compute the surface covered by the bounding box of the GCP
      var listPoints = []
      var nGCP = $scope.gcpArray.length
      for (var i = 0; i < nGCP; i++) {
        var gcp = $scope.gcpArray[i]
        var point = turf.point([gcp.x, gcp.y])
        listPoints.push(point)
      }
      var bbox = turf.bbox(turf.featureCollection(listPoints))
      var areaBbox = (bbox[2] - bbox[0]) * (bbox[3] - bbox[1])
      // Compute the image surface
      var imageWidth = $scope.osdObject.mysd.viewer.source.Image.Size.Width
      var imageHeight = $scope.osdObject.mysd.viewer.source.Image.Size.Height
      var areaImage = imageWidth * imageHeight
      // Compute the ratio of the surface
      var surfaceRatio = Math.round(areaBbox / areaImage * 100)
      return surfaceRatio
    }

      // Compute the camera orientation and position
      $scope.computePoseBool = false
      $scope.computePose = function (cameraFixed) {

        $scope.providedCamera.fixed = cameraFixed
        if ($scope.enoughGCP) {
          // Is called from the button
          // Load the modal

          $scope.computePoseBool = true // to show the legend of the reprojections
          $scope.loading = true
          $scope.showShowModel = true
          $scope.showHideShowButton = true
          $scope.show.osd = false
          // $scope.openLoading()
          // If the camera location is fixed
          if ($scope.providedCamera.fixed == true) {
            if ($scope.markerMoved){
              // Get swiss coordinates of the marker
              var WGS84proj = 'EPSG:4326'
              var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
              var projCoord = proj4(WGS84proj, ch1903plusproj, [$scope.providedCamera.location.lng, $scope.providedCamera.location.lat])

              var position = [Cesium.Cartographic.fromDegrees($scope.providedCamera.location.lng, $scope.providedCamera.location.lat)]

            }else{

              // Get camera location and orientation from the apriori position
              var WGS84proj = 'EPSG:4326'
              var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
              var projCoord = proj4(WGS84proj, ch1903plusproj, [$scope.cameraPosition.longitude, $scope.cameraPosition.latitude])
              var position = [Cesium.Cartographic.fromDegrees($scope.cameraPosition.longitude, $scope.cameraPosition.latitude)]
            }

            // Get height of the marker
            var promise = Cesium.sampleTerrain($scope.cesiumObject.cesium.terrainProvider, 17, position)
            Cesium.when(promise, function (updatedPosition) {

              var Z = updatedPosition[0].height + 3
              var camXYZ = [projCoord[0], projCoord[1], Z]

              // Camera angles
              var camPitch = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.pitch)
              var camHeading = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.heading)
              var camRoll = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.roll)

              var imageWidth = $scope.osdObject.mysd.viewer.source.Image.Size.Width
              var imageHeight = $scope.osdObject.mysd.viewer.source.Image.Size.Height

              $scope.launchLM(camXYZ, camHeading, camPitch, camRoll, imageWidth, imageHeight).then(function(){
                console.log('fin lm')
                $scope.checkValidity(camXYZ, imageWidth, imageHeight).then(function(isValid){
                  console.log('fin is  valid', isValid)

                  if (isValid){

                    $scope.checkAltitude().then(function(){
                      console.log('check altitude fin')
                      var WGS84proj = 'EPSG:4326'
                      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                      var longLat = proj4(ch1903plusproj, WGS84proj, [$scope.p[0], $scope.p[1]])

                      $scope.moveMap(longLat[0], longLat[1])

                      $scope.moveGlobe(imageWidth, imageHeight)

                      $scope.computeErrors(imageWidth, imageHeight)
                    })
                  }
                })
              })
            })
          } else {
            // Get camera location and orientation from the virtual camera
            var camCartesian = $scope.cesiumObject.cesium.scene.camera.position
            // Get camera projected coordinates
            var camCarto = Cesium.Cartographic.fromCartesian(camCartesian)
            var camLong = Cesium.Math.toDegrees(camCarto.longitude)
            var camLat = Cesium.Math.toDegrees(camCarto.latitude)
            var WGS84proj = 'EPSG:4326'
            var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
            var projCoord = proj4(WGS84proj, ch1903plusproj, [camLong, camLat])
            var camXYZ = [projCoord[0], projCoord[1], camCarto.height]

            // Camera angles
            var camPitch = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.pitch)
            var camHeading = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.heading)
            var camRoll = Cesium.Math.toDegrees($scope.cesiumObject.cesium.scene.camera.roll)

            var imageWidth = $scope.osdObject.mysd.viewer.source.Image.Size.Width
            var imageHeight = $scope.osdObject.mysd.viewer.source.Image.Size.Height

            $scope.launchLM(camXYZ, camHeading, camPitch, camRoll, imageWidth, imageHeight).then(function(){
              console.log('fin lm')
              $scope.checkValidity(camXYZ, imageWidth, imageHeight).then(function(isValid){
                console.log('fin validity', isValid)
                if (isValid){
                  $scope.checkAltitude().then(function(){
                    console.log('fin altitude')
                    var WGS84proj = 'EPSG:4326'
                    var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                    var longLat = proj4(ch1903plusproj, WGS84proj, [$scope.p[0], $scope.p[1]])

                    $scope.moveMap(longLat[0], longLat[1])
                    $scope.moveGlobe(imageWidth, imageHeight)
                    $scope.computeErrors(imageWidth, imageHeight)
                  })
                }
              })
            })
          }
       }// If enough gcp
      } // Compute pose

      $scope.launchLM = function(camXYZ, camHeading, camPitch, camRoll, imageWidth, imageHeight){
        return $q(function (resolve, reject) {
          // Set apriori parameters
          // ----------------------
          // focal in pixel, set to a normal focal: focal = diagonal
          var f = Math.sqrt(Math.pow(imageWidth, 2) + Math.pow(imageHeight, 2))
          // Principal point in the middle of the image
          var cx = imageWidth / 2.0 // half column size
          var cy = imageHeight / 2.0 // half height size
          // location of the camera
          var X0 = camXYZ[0]
          var Y0 = camXYZ[1]
          var Z0 = camXYZ[2]
          // Convert cesium angles for the pose estimation algorithm
          angles = cesium_to_LM_angles(camHeading, camPitch, camRoll)
          var az = angles[0]
          var tilt = angles[1]
          var roll = angles[2]
          // Generate input of the LM algorithm
          var pApriori = [X0, Y0, Z0, az, tilt, roll, f, cx, cy] // Vector of the camera orientation
          // Set parameters to be optimised to true
          if ($scope.providedCamera.fixed == true) {
            var boolUnknowns = [false, false, false, true, true, true, true, false, false]
          } else {
            var boolUnknowns = [true, true, true, true, true, true, true, false, false]
          }
          // Get GCP
          var GCPs = $scope.gcpArray
          // Compute camera orientation
          // --------------------------
          var p = estimatePoseLM(pApriori, boolUnknowns, GCPs)
          $scope.p = p
          resolve()
        })
      }
      $scope.checkValidity = function(camXYZ, imageWidth, imageHeight){
        return $q(function (resolve, reject) {
          // Check pose validity
          // -------------------
          $scope.show.validateOrientation = false
          // $scope.show.validate = false
          // Check fov
          $scope.fovValid = false
          var imSide = Math.max(imageWidth, imageHeight)
          var fov = 2 * Math.atan(imSide / 2 / $scope.p[6]) * 180 / Math.PI
          if ((fov > 10) && (fov < 100)) {
            $scope.fovValid = true
          }
          // Check distance
          $scope.distValid = false
          var dX = $scope.p[0] - camXYZ[0]
          var dY = $scope.p[1] - camXYZ[1]
          var dist = Math.sqrt(dX * dX + dY * dY)
          if (dist < 5000) {
            $scope.distValid = true
          }
          if (($scope.distValid) && ($scope.fovValid)) {
            resolve(true)
            // return true
          }
          else {
            $scope.openInvalidPose()
            resolve(false)
            // return false
          }// if pose is valid
        })
      }
      $scope.checkAltitude = function(){
        console.log('in check altitude')
        return $q(function (resolve, reject) {
          // Check altitude
          // --------------
          // Coordinate transformation
          var WGS84proj = 'EPSG:4326'
          var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
          var longLat = proj4(ch1903plusproj, WGS84proj, [$scope.p[0], $scope.p[1]])
          var position = [Cesium.Cartographic.fromDegrees(longLat[0], longLat[1])]

          var promise = Cesium.sampleTerrain($scope.cesiumObject.cesium.terrainProvider, 17, position)
          Cesium.when(promise, function (updatedPosition) {

            //if ($scope.image.view_type == 'terrestrial') {
              // console.log('image is terrestrial')
              var demZ = updatedPosition[0].height + 3 // height of the ground + height of the photographer
              var compZ = $scope.p[2] // Computed Z
              if (compZ < demZ) { // Computed position is within the ground
                // Generate input of the LM algorithm
                console.log('p before', $scope.p)
                $scope.p[2] = demZ
                console.log('p after', $scope.p)
                //if ($scope.providedCamera.fixed == true) {
                // if the image is within the ground, it is pushed at the surface and the position is fixed in all case
                var boolUnknowns = [false, false, false, true, true, true, true, false, false] // fix the position
                //} else {
                //  var boolUnknowns = [true, true, true, true, true, true, true, false, false] // fix the position
                //}
                // Compute camera orientation force height
                // --------------------------
                console.log('avant estimate pose terrestrial', boolUnknowns)
                // Get GCP
                var GCPs = $scope.gcpArray
                $scope.p = estimatePoseLM($scope.p, boolUnknowns, GCPs)
                console.log('fin estimate pose terrestrial')
                resolve()
                  //$scope.p = new Promise(function(resolve, reject) {

                  //});
              //}else{
              //  resolve()
              //}
            }else{
              resolve()
            }
          })
        })
      }
      $scope.moveMap = function(lng, lat){
        $scope.markers = {
            main: {
                lat: lat,
                lng: lng,
                focus: true,
                icon: localIcons.yellowIcon,
                // message: "This place is in London",
                draggable: true,
                enable: ['dragend']
            }
        }
        leafletData.getMap().then(function (map) {
          map.panTo({lat:lat, lng: lng})
        })
      }
      $scope.moveGlobe = function(imageWidth, imageHeight){
        // Set camera location
        // -------------------
        var p = $scope.p
        // Meridian convergence (Difference between geographic north (cesium) and cartographic north (Ch1903))
        $scope.muDegre = convergenceMeridien(p[0] - 2600000, p[1] - 1200000) // In civil coordinates (centered in bern)
        muRad = $scope.muDegre * Math.PI / 180
        azCorrected = p[3] + muRad
        // Convert optimisation angles in cesium angles
        var angles = LM_to_cesium_angles(azCorrected, p[4], p[5])
        $scope.heading = angles[0]// +$scope.muDegre;//angles[0];
        $scope.tilt = angles[1]
        $scope.roll = angles[2]
        // Coordinate transformation
        var WGS84proj = 'EPSG:4326'
        var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
        var longLat = proj4(ch1903plusproj, WGS84proj, [p[0], p[1]])
        var cartesian = Cesium.Cartesian3.fromDegrees(longLat[0], longLat[1], p[2])
        // save result
        // -----------
        $scope.cameraParameters = p
        $scope.orientationArray = [{
          X: p[0],
          Y: p[1],
          Z: p[2],
          azimuth: $scope.heading,
          tilt: $scope.tilt,
          roll: $scope.roll,
          f: p[6],
          cx: p[7],
          cy: p[8],
          long: longLat[0],
          lat: longLat[1]
        }]
        // Generate gltf
        // -------------
        var image = {
          id: $scope.imageId,
          longitude: longLat[0],
          latitude: longLat[1],
          height: p[2],
          collection_id: $scope.image.collection_id
        }
        var gltfPromise = dbService.createGltf($scope.cameraParameters, $scope.imageId, $scope.image.collection_id)
        .then(function (response) {
          var tempBool = true
          $scope.model = cesiumService.drawModel(image, $scope.cesiumObject.cesium, $scope.transparency, $scope.model, tempBool)
          $scope.modelIsDrawn = true
          $scope.modelIsLoaded = true
        })

        // Draw camera location
        // -------------------
        cesiumService.drawCameraPosition($scope.cesiumObject.cesium, $scope.orientationArray)

        // Update camera
        // -------------
        var camera = $scope.cesiumObject.cesium.camera
        // Mimic image geometry
        var ratioImage = imageHeight / imageWidth
        var canvasHeight = $scope.cesiumObject.cesium.canvas.height
        var canvasWidth = $scope.cesiumObject.cesium.canvas.width
        var ratioCanvas = canvasHeight / canvasWidth
        // Get image focal
        var fIm = p[6]
        // The comparison of the ratio indicates if the canvas height or the width must be fitted
        if (ratioCanvas < ratioImage) {
          // The height must be fitted
          // -------------------------
          // Compute the canvas focal
          var scale = canvasHeight / imageHeight
          var fCanvas = fIm * scale
          if (ratioCanvas < 1) {
            // The canvas height is smaller than the widht the horizontal fov must be provided
            var fov = 2 * Math.atan(canvasWidth / 2 / fCanvas)
          } else {
            // The canvas height is bigger than the widht the vertical fov must be provided
            var fov = 2 * Math.atan(canvasHeight / 2 / fCanvas)
          }
          var yCanvasOffset = 0
          var xCanvasOffset = (canvasWidth - imageWidth * scale) / 2
        } else {
          // The width must be fitted
          // -------------------------
          // Compute the canvas focal
          var scale = canvasWidth / imageWidth
          var fCanvas = fIm * scale
          if (ratioCanvas < 1) {
            // The canvas height is smaller than the widht the horizontal fov must be provided
            var fov = 2 * Math.atan(canvasWidth / 2 / fCanvas)
          } else {
            // The canvas height is bigger than the widht the vertical fov must be provided
            var fov = 2 * Math.atan(canvasHeight / 2 / fCanvas)
          }
          var xCanvasOffset = 0
          var yCanvasOffset = (canvasHeight - imageHeight * scale) / 2
        }
        // Set canvas fov
        // --------------
        $scope.cesiumObject.cesiumFov = fov // + 8*Math.PI/180
        camera.frustum.fov = fov + 8*Math.PI/180
        // Set camera location
        // -------------------
        cesiumService.relaxCollisionDetection($scope.cesiumObject.cesium)
        $scope.transparency.value = 0.8
        cesiumService.flyToModel($scope.orientationArray, $scope.cesiumObject.cesium.camera, $scope.model)
      }
      $scope.computeErrors = function (imageWidth, imageHeight){
        var f = Math.sqrt(Math.pow(imageWidth, 2) + Math.pow(imageHeight, 2))
        var GCPs = $scope.gcpArray
        // Compute error in image coordinate
        // ---------------------------------
        var res = computeError($scope.p, GCPs)
        var dxy = res[1]
        var xy_reproj = res[0]
        var x_reproj = math.squeeze(xy_reproj[0])
        var y_reproj = math.squeeze(xy_reproj[1])
        // Fill the table
        var nGCP = GCPs.length
        var maxErr = 0

        for (var i = 0; i < nGCP; i++) {
          var id = $scope.gcpArray[i].id
          var err = (dxy[i] / f * 100).toFixed(2)
          $scope.myGCPs.dictGCPs[id].ePix = err
          if (err > maxErr) {
            var maxErr = err
          }
          if (err < 1) {
            $scope.myGCPs.dictGCPs[id].eClass = 'error-low'
          } else if (err < 2.5) {
            $scope.myGCPs.dictGCPs[id].eClass = 'error-medium'
          } else {
            $scope.myGCPs.dictGCPs[id].eClass = 'error-high'
          }
        }
        $scope.scoreGCPs = maxErr
        $scope.surfaceGCPs = $scope.computeGCPSurface()
        // Draw reprojection
        osdService.drawReprojections($scope.osdObject.mysd, xy_reproj, $scope.gcpArray, $scope.myGCPs)
        // Check gcp validity
        // ------------------
        if ((nGCP > 5) && (maxErr < 2.5))  {
          //if (maxErr < 2.5) {
            // $scope.show.validateOrientation = true
            $scope.showValidate = true
            $scope.longInstruction = $scope.texts.instrSix[$scope.lang]
            //$scope.show.validate = true
          //}
        } else if ((nGCP > 5) && (maxErr < 2.5)){
          $scope.show.validateOrientation = false
          $scope.showValidate = false
          $scope.longInstruction = $scope.texts.instrSixWrong[$scope.lang]
        } else {
          $scope.showValidate = false
        }
      }

      $scope.modelIsDrawn = false
      $scope.modelIsLoaded = false
      $scope.checkModel = function () {
        if (!$scope.modelIsLoaded){
          var tempBool = false
          $scope.model = cesiumService.drawModel($scope.image, $scope.cesiumObject.cesium, $scope.transparency.value, $scope.model, tempBool)
          $scope.modelIsDrawn = true
          $scope.modelIsLoaded = true
        }
      }
      $scope.buildings = {
        show: true
      }
      $scope.hideShowBuildings = function () {
        $scope.cesiumObject.buildings = cesiumService.hideShowBuildings($scope.cesiumObject.buildings, $scope.buildings.show)
      }

      $scope.changeTransparency = function () {
        cesiumService.changeModelTransparency($scope.model, $scope.transparency.value)
      }

      $scope.goToMono = function () {
        location.href = '/map/?imageId=' + $scope.imageId
      }

      $scope.goToInvalidHelp = function () {
        location.href = '/help#prob2'
      }
      $scope.goToTips = function () {
        //location.href = '/help#tuto4'
        $window.open('/help#tuto4', '_blank')
        // location.href = '/help#tuto4'
      }
      $scope.goToTuto = function () {
        //location.href = '/help#tuto4'
        $window.open('/help', '_blank')
      }

    // Corrections Functions
    // ---------------------
    $scope.editTitle = function () {
      $scope.show.editTitle = true
      // $scope.correction.titleCorr = true
    }

    $scope.cancelTitle = function () {
      $scope.show.editTitle = false
      $scope.correction.title = $scope.image.title
      $scope.correction.titleCorr = false
    }

    $scope.validateTitle = function () {
      $scope.correction.titleCorr = true
      $scope.show.editTitle = false
    }

    $scope.editCaption = function () {
      $scope.show.editCaption = true
      // $scope.correction.captionCorr = true
    }

    $scope.cancelCaption = function () {
      $scope.show.editCaption = false
      $scope.correction.caption = $scope.image.caption
      $scope.correction.captionCorr = false
    }

    $scope.validateCaption = function () {
      $scope.correction.captionCorr = true
      $scope.show.editCaption = false
    }

    // $scope.editDate = function () {
    //   $scope.show.editDate = true
    //   $scope.correction.dateCorr = true
    // }

    // $scope.cancelDate = function () {
    //   $scope.show.editDate = false
    //   $scope.correction.date = $scope.image.date_shot
    //   $scope.correction.dateShotCorr = false
    // }



    $scope.saveCorrections = function () {
      return $q(function (resolve, reject) {
        // var defer = $q.defer()
        var promises = []
        // submit correction
        if ($scope.correction.titleCorr === false) {
          $scope.correction.title = null
        }
        if ($scope.correction.captionCorr === false) {
          $scope.correction.caption = null
        }
        if ($scope.correction.dateShotCorr === false) {
          $scope.correction.dateShot = null
        }
        if ($scope.correction.titleCorr || $scope.correction.captionCorr || $scope.correction.dateShotCorr) {
          promises.push(correctionsService.submitCorrection($scope.imageId, $scope.volunteerId, $scope.correction))
        }

        // // submit remark
        // if ($scope.remark.provided) {
        //   promises.push(remarksService.submitRemark($scope.imageId, $scope.volunteerId, $scope.remark.text))
        // }

        // submit observation
        console.log('save obs')
        var n = $scope.observations.list.length
        for (var i = 0; i < n; i++) {
          promises.push(observationsService.submitObservation($scope.imageId, $scope.volunteerId, $scope.observations.list[i]))
        }
        $q.all(promises).then(function () {
          resolve()
        })
      })
    }

    // $scope.createRemark = function () {
    //   $scope.show.remarkInput = true
    // }
    //
    // $scope.cancelRemark = function () {
    //   $scope.remark.text = $scope.texts.defaultRemark[$scope.lang]
    //   $scope.remark.provided = false
    //   $scope.show.remarkInput = false
    // }
    //
    // $scope.validateRemark = function () {
    //   $scope.remark.provided = true
    //   $scope.show.remarkInput = false
    // }

    // $scope.show.observationCreateBtn = true
    $scope.obsProvided = false
    $scope.createObservation = function () {
      // $scope.show.observationCreateBtn = false
      // $scope.show.observationValidationText = false
      // $scope.show.observationInput = false //true
      // $scope.show.observationSkip = true
      //$scope.show.observationClickText = true
      $scope.mode = 'correction'
      // Enable selection in the image
      // osdService.startRectangle($scope.osdObject.mysd)
      console.log('enable selecetion')
      if ($scope.obsProvided){
        osdService.enableSelection($scope.osdObject.mysd, $scope.selectionValidation)
      }else{
        osdService.initializeSelection($scope.osdObject.mysd, $scope.selectionValidation)
        $scope.obsProvided = true
      }

    }
    $scope.selectionValidation = function (rect){
      // Stores rectangle
      $scope.observations.currentObservation.coord_x = rect.x
      $scope.observations.currentObservation.coord_y = rect.y
      $scope.observations.currentObservation.height = rect.height
      $scope.observations.currentObservation.width = rect.width
      // Draw rectangle
      osdService.disableSelection($scope.osdObject.mysd)
      osdService.drawRectangleInput($scope.osdObject.mysd, $scope.observations.currentObservation, $scope.validateObservation, $scope.cancelObservation)
      osdService.disableMouseNav($scope.osdObject.mysd)
    }
    $scope.cancelObservation = function () {
      osdService.enableMouseNav($scope.osdObject.mysd)
    }
    $scope.validateObservation = function (observation) {
      $scope.observations.currentObservation = observation
      osdService.enableMouseNav($scope.osdObject.mysd)
      osdService.drawRectangleObservation($scope.osdObject.mysd, $scope.observations.currentObservation, true)
      $scope.mode = 'correction'

      $scope.observations.list.push($scope.observations.currentObservation)
      $scope.observations.currentId += 1
      $scope.observations.currentObservation = {
        observation: null,
        coord_x: null,
        coord_y: null,
        id: $scope.observations.currentId
      }
    }

    $scope.stepBeforeProblem = null
    $scope.reportProblem = function () {
      $scope.stepBeforeProblem = $scope.currentStep
      if ($scope.stepBeforeProblem === 3){
        $scope.updateMode = true
        $scope.hideGCP()
      }
      $scope.currentStep = 5
      $scope.mode = 'problem'

      $scope.instruction = $scope.texts.instrProblemShort[$scope.lang]
      $scope.longInstruction = $scope.texts.instrProblemLong[$scope.lang]

      // Initialize problem model
      $scope.problems = {
        inverted: false,
        photomontage: false,
        georefImpossible: false,
        notRecognizable: false,
        other: false,
        otherText: '',
        otherValidated: false
      }
    }

    $scope.validateOtherProblem = function () {
      $scope.problems.otherValidated = true
      $scope.show.otherProblem = false
    }

    $scope.$watch('problems.other', function () {
      if ($scope.problems.other === true) {
        $scope.show.otherProblem = true
      }else{
        $scope.show.otherProblem = false
      }
    }, true)

    $scope.submitProblem = function () {
      console.log('submit problem')
      if (AuthService.isAuthenticated()) {
        // $scope.showLogin = false
        $scope.volunteerId = $scope.volunteer.id
      } else {
        $scope.volunteerId = 14 // Default volunteer
      }
      var promises = []
      if ($scope.problems.inverted) {
        var problem = {
          title: 'Image is inverted'
        }
        console.log('1')
        promises.push(problemsService.submitProblem($scope.imageId, $scope.volunteerId, problem))

      }
      if ($scope.problems.photomontage) {
        var problem = {
          title: 'Image is a photomontage'
        }
        console.log('2')
        promises.push(problemsService.submitProblem($scope.imageId, $scope.volunteerId, problem))
      }
      if ($scope.problems.georefImpossible) {
        var problem = {
          title: 'The georeferencing is impossible'
        }
        promises.push(problemsService.submitProblem($scope.imageId, $scope.volunteerId, problem))
      }
      if ($scope.problems.notRecognizable) {
        var problem = {
          title: 'The location cant be recognized'
        }
        promises.push(problemsService.submitProblem($scope.imageId, $scope.volunteerId, problem))
      }
      if ($scope.problems.otherValidated) {
        var problem = {
          title: 'Other problem',
          description: $scope.problems.otherText
        }
        console.log('3')
        promises.push(problemsService.submitProblem($scope.imageId, $scope.volunteerId, problem))
      }

      $q.all(promises).then(function () {
        $window.close()
      })
    }

    // Validation Functions
    // --------------------
    $scope.validateGeoloc = function (imageId) {

      if (AuthService.isAuthenticated()) {

        $scope.validation.imageId = $scope.imageId
        $scope.validation.volunteerId = $scope.volunteerId
        $scope.validation.validatorId = $scope.validatorId
        $scope.validation.status = 'validated'

        var promise = dbService.validateGeoloc($scope.imageId)
        promise.then(function () {
          validationsService.submitValidation($scope.imageId, $scope.volunteerId, $scope.validatorId, $scope.validation).then(function(){
            $window.close()
          })
        })
      } else {
        // Go to login
        location.href = '/login/'
      }
    }

    // Reject geolocation
    $scope.rejectGeoloc = function (imageId) {
      if (AuthService.isAuthenticated()) {
        //var promise = dbService.rejectGeoloc(imageId)
        //promise.then(function () {
        $scope.hideGCP()
        $scope.resetGCP()
        //Update validation model
        $scope.validation.imageId = $scope.imageId
        $scope.validation.volunteerId = $scope.volunteerId
        $scope.validation.validatorId = $scope.validatorId
        $scope.validation.status = 'rejected'
        $scope.switchToReason()
        //})
      } else {
        // Go to login
        location.href = '/login/'
      }
    }


    $scope.improveGeoloc = function () {
      $scope.validation.imageId = $scope.imageId
      $scope.validation.volunteerId = $scope.volunteerId
      $scope.validation.validatorId = $scope.validatorId
      $scope.validation.status = 'improved'
      $scope.updateMode = true
      $scope.modelIsDrawn = false
      $scope.hideGCP()
      $scope.imageModel = {
        show: false
      }
      $scope.switchToCorrespondancesStep()

    }

    // Validation model and Functions
    // ---------------------------
    $scope.validation = {
      imageId: null,
      validatorId: null,
      volunteerId: null,
      status: null,
      errors: null
    }

    $scope.drawModel = function () {
      console.log('in draw model')
      var tempBool = true
      $scope.model = cesiumService.drawModel($scope.image, $scope.cesiumObject.cesium, $scope.transparency.value, $scope.model, tempBool)
    }
    // $scope.show.otherReason = false
    // $scope.show.reasonRemark = false
    // $scope.reasons = {
    //   geolocNotCorrect: false,
    //   correspondencesInaccurate: false,
    //   hidenRelief: false,
    //   gcpDistribution: false,
    //   other: false,
    //   otherValidated: false,
    //   otherText: '',
    //   remark: false,
    //   remarkValidated: false,
    //   remarkText: ''
    // }
    //
    // $scope.validateOtherReason = function () {
    //   $scope.reasons.otherValidated = true
    // }
    // $scope.validateReasonRemark = function () {
    //   $scope.reasons.remarkValidated = true
    // }
    // $scope.saveReasons = function () {
    //
    // }

    // Modals
    // ------

    // Login
    $scope.openLoginInfo = function (size) {
      $scope.loginInfoModal = $uibModal.open({
        animation: false,
        templateUrl: 'loginInfo.html',
        controller: 'loginCtrl',
        bindToController: true,
        resolve: {
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
      $scope.loginInfoModal.closed.then(function () {
        console.log('close modal')
        AuthService.loadUserCredentials()
        $scope.getMyName()
        console.log('volunteer', $scope.volunteer)
      })
    }

    $scope.goToLogin = function () {
      $scope.winLogin = $window.open('/login', '_blank')
    }

    // Computed pose is invalid
      $scope.openInvalidPose = function (size) {
        $scope.invalidModal = $uibModal.open({
          animation: false,
          templateUrl: 'invalid.html',
          controller: 'invalidPoseCtrl',
          bindToController: true,
          resolve: {
            texts: function () {
              return $scope.texts
            },
            lang: function () {
              return $scope.lang
            },
            // goToInvalidHelp: function(){
            //   //$scope.goToInvalidHelp()
            // }
          }
        })

      }

    // Open modal to give GCP instructions
      $scope.openPoseInstruction = function (size) {
        $scope.poseInstructionShown = true
        var poseInstructionModal = $uibModal.open({
          animation: false,
          templateUrl: 'computePoseInstruction.html',
          bindToController: true,
          controller: 'poseInstructionCtrl',
          resolve: {
            texts: function () {
              return $scope.texts
            },
            lang: function () {
              return $scope.lang
            }
          }
        })
      }

    $scope.goToHelp = function () {
      url = '/help/'
      $window.open(url, '_blank')
    }

    // Links buttons
    $scope.goHome = function () {
      url = '/'
      $window.open(url, '_blank')
    }

    // Functions for the parameter box
    // ------------------------------
    $scope.getMyName = function () {
      return $q(function (resolve, reject) {
        if (AuthService.isAuthenticated()) {
          $http.get('/volunteers/memberinfo').then(function (result) {
            $scope.volunteer = result.data.volunteer
            $scope.myName = $scope.volunteer.username
            $scope.isLogged = true
            if ($scope.volunteer.lang){
              $scope.lang = $scope.volunteer.lang
            }
            resolve()
          })
        } else {
          $scope.myName = "M'identifier"
          resolve()
        }
      })
    }
    $scope.showParameters = false
    $scope.openParameters = function () {
      $scope.showParameters ^= true
    }
    $scope.closeParameters = function () {
      $scope.showParameters = false
    }
    $scope.logout = function () {
      AuthService.logout()
      $scope.isLogged = false
    }
    $scope.login = function () {
      var win = $window.open('/login', '_blank')
      var timer = setInterval(checkChild, 500)
      function checkChild () {
        if (win.closed) {
          clearInterval(timer)
          AuthService.loadUserCredentials()
          $scope.getMyName()
          //$window.location.reload()
        }
      }
    }
    $scope.goToMy = function () {
      url = '/mysmapshot/'
      $window.open(url, '_blank')
    }
  }])

  georefApp.controller('loginCtrl', function ($scope, $uibModalInstance, $window, texts, lang) {
    $scope.texts = texts
    $scope.lang = lang
    $scope.goToLogin = function() {
      //$window.open('/login', '_blank')
      var win = $window.open('/login', '_blank')
      var timer = setInterval(checkChild, 500)
      function checkChild () {
        if (win.closed) {
          clearInterval(timer)
          //AuthService.loadUserCredentials()
          $uibModalInstance.close()
          //$window.location.reload()
        }
      }
    }
    $scope.closeLoginInfo = function () {
      $uibModalInstance.close()
    }
  })

  georefApp.controller('invalidPoseCtrl', function ($scope, $uibModalInstance, $window, texts, lang) {
    $scope.texts = texts
    $scope.lang = lang
    $scope.goToInvalidHelp = function () {
      //location.href = '/help#invalid'
      $window.open('/help#prob2', '_blank')
    }
    $scope.closeInvalidPose = function () {
      $uibModalInstance.close()
    }
  })

  georefApp.controller('lockCtrl', function ($scope, $uibModalInstance, texts, lang) {
    $scope.texts = texts
    $scope.lang = lang
    $scope.closeLockInstruction = function () {
      $uibModalInstance.close()
    }
  })

  georefApp.controller('poseInstructionCtrl', function ($scope, $uibModalInstance, texts, lang) {
    $scope.texts = texts
    $scope.lang = lang
    $scope.closePoseInstruction = function () {
      $uibModalInstance.close()
    }
  })

  georefApp.controller('browserCtrl', function ($scope, $uibModalInstance, $window, texts, lang) {
    $scope.texts = texts
    $scope.lang = lang
    $scope.closeBrowserInfo = function () {
      $uibModalInstance.close()
    }
  })
