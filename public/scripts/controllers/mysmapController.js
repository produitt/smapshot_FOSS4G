mysmapApp.controller('mysmapCtrl', ['$scope', '$http', '$window', '$uibModal', 'AuthService',
'searchService', 'dbService', 'validationsService', 'errorsService',
'volunteersService', 'correctionsService', 'remarksService', 'observationsService',
'imageQueryService', 'collectionsQueryService', '$anchorScroll', '$location',
'$timeout',
function ($scope, $http, $window, $uibModal, AuthService, searchService,
  dbService, validationsService, errorsService, volunteersService, correctionsService,
  remarksService, observationsService, imageQueryService, collectionsQueryService,
  $anchorScroll, $location, $timeout) {


  $scope.volunteer = {
    username: '',
    first_name: '',
    last_name: '',
    email: '',
    letter: '',
    nImages: 0,
    rank: 0
  }

  $scope.user = {
    name: '',
    rank: '',
    nImages: 0,
    diffRank: 0,
    diffImages: 0,
    sign: ''
  }
  $scope.data = []

  $scope.myImages = {
    minDate: new Date(1800, 1, 1),
    maxDate: new Date()
  }
  $scope.myCorrections = {
    minDate: new Date(1800, 1, 1),
    maxDate: new Date()
  }
  $scope.myObservations = {
    minDate: new Date(1800, 1, 1),
    maxDate: new Date()
  }
  $scope.showLogin = false
  $scope.getRanking = false
  $scope.getVvolunteer = false
  $scope.rankingReady = false
  $scope.showImages = true

  $scope.$watch('getRanking', function () {
    if (($scope.getRanking) && ($scope.getVolunteer)) {
      $scope.rankingReady = true
    }
  })

  $scope.$watch('getVolunteer', function () {
    if (($scope.getRanking) && ($scope.getVolunteer)) {
      $scope.rankingReady = true
    }
  })

  // SWITCH MODES
  // ------------
  $scope.modes = {
    0: 'parameters',
    1: 'myImages',
    2: 'ranking',
    3: 'metadata',
    4: 'observations',
    5: 'metadataVal',
    6: 'observationsVal',
    7: 'geolocalisationVal'
  }
  $scope.currentMode = 0
  $scope.switchTo = function (val) {
    if (val === 'parameters') {
      $scope.currentMode = 0
    } else if (val === 'myImages') {
      $scope.currentMode = 1
    } else if (val === 'ranking') {
      $scope.currentMode = 2
      collectionsQueryService.getMy($scope.volunteer.id).then(function (myCollections) {
        $scope.myCollections = myCollections
        // Generate dates
        $scope.endDate = new Date()
        //$scope.endDate = new Date($scope.endYear, $scope.endMonth, 0)
        $scope.endDate = new Date($scope.endDate.getFullYear(), $scope.endDate.getMonth()+1, 0)
        var limitDates = $scope.generateStringDates($scope.endDate)
        var startDate = limitDates[0]
        var stopDate = limitDates[1]
        if ($scope.myCollections.list.length === 0){
          var searchData = {
            collectionId: null,
            type: 'geoloc',
            startDate: startDate,
            stopDate: stopDate,
          }
        }else{
          console.log('mycollections', $scope.myCollections)
          $scope.collectionId = $scope.myCollections.selected.collection_id
          var searchData = {
            collectionId: $scope.myCollections.selected.collection_id,
            type: 'geoloc',
            startDate: startDate,
            stopDate: stopDate,
          }
        }

          volunteersService.getRanking(searchData).then(function(response) {
            $scope.generateDataForRanking(response)
        })
      })
      //volunteersService.getRanking()
    } else if (val === 'metadata') {
      $scope.currentMode = 3

      // Reset filters
      $scope.selectedStates = [] //'accepted', 'improve', 'rejected', 'waiting']
      $scope.selectedOwners = []
      $scope.selectedCollections = []

      correctionsService.getMy($scope.volunteer.id).then(function (res) {
        $scope.myCorrections = {
          list: res.corrections,
          selectedList: res.corrections,
          minDate: res.minDate,
          maxDate: res.maxDate,
          listOwners: res.listOwners,
          listCollections: res.listCollections
        }
      })
    } else if (val === 'observations') {
      $scope.currentMode = 4

      // Reset filters
      $scope.selectedStates = [] //'accepted', 'improve', 'rejected', 'waiting']
      $scope.selectedOwners = []
      $scope.selectedCollections = []

      observationsService.getMy($scope.volunteer.id).then(function (res) {
        $scope.myObservations = {
          list: res.listObservations,
          selectedList: res.listObservations,
          minDate: res.minDate,
          maxDate: res.maxDate,
          listOwners: res.listOwners,
          listCollections: res.listCollections
        }
      })

    } else if (val === 'metadataVal') {
      $scope.currentMode = 5
    } else if (val === 'observationsVal') {
      $scope.currentMode = 6
    } else if (val === 'geolocalisationVal') {
      $scope.currentMode = 7
    } else {
      $scope.currentMode = 0
    }
  }

  $scope.redirectLogin = function () {
    if (!AuthService.isAuthenticated()) {
      $scope.showLogin = true
      var win = $window.open('/login', '_blank')
      var timer = setInterval(checkChild, 500)
      function checkChild () {
        if (win.closed) {
          clearInterval(timer)
          AuthService.loadUserCredentials()
          $scope.getMyName()
        }
      }
    }
  }
  $scope.redirectLogin()

  $scope.goToLogin = function () {
    $window.open('/login', '_blank', 'menubar=no, status=no, scrollbars=no, menubar=no, width=500, height=500')
  }

  // texts
  // -----
  // Set initial language
  var lang = $window.navigator.language || $window.navigator.userLanguage
  lang = lang.slice(0, 2)
  if (lang == 'fr') {
    $scope.lang = 'fr'
  } else if (lang == 'it') {
    $scope.lang = 'it'
  } else if (lang == 'de') {
    $scope.lang = 'de'
  } else {
    $scope.lang = 'en'
  }
  // $scope.lang = 'fr'
  // Language switchers
  $scope.switchToFrench = function () {
    $scope.lang = 'fr'
  }
  $scope.switchToGerman = function () {
    $scope.lang = 'de'
  }
  $scope.switchToEnglish = function () {
    $scope.lang = 'en'
  }
  $scope.switchToItalian = function () {
    $scope.lang = 'it'
  }
  $http.get('../texts/mysmapshot.json', {
    headers: {
      'Accept': 'application/json;charset=utf-8'
      // "Accept-Charset":"charset=utf-8"
    }
  })
  .success(function (data, headers) {
    $scope.texts = data.texts

    // Generate status
    $scope.status[0]['en'] = $scope.texts.validated['en']
    $scope.status[0]['fr'] = $scope.texts.validated['fr']
    $scope.status[0]['de'] = $scope.texts.validated['de']
    $scope.status[0]['it'] = $scope.texts.validated['it']

    $scope.status[1]['en'] = $scope.texts.improved['en']
    $scope.status[1]['fr'] = $scope.texts.improved['fr']
    $scope.status[1]['de'] = $scope.texts.improved['de']
    $scope.status[1]['it'] = $scope.texts.improved['it']

    $scope.status[2]['en'] = $scope.texts.rejected['en']
    $scope.status[2]['fr'] = $scope.texts.rejected['fr']
    $scope.status[2]['de'] = $scope.texts.rejected['de']
    $scope.status[2]['it'] = $scope.texts.rejected['it']

    $scope.status[3]['en'] = $scope.texts.waiting['en']
    $scope.status[3]['fr'] = $scope.texts.waiting['fr']
    $scope.status[3]['de'] = $scope.texts.waiting['de']
    $scope.status[3]['it'] = $scope.texts.waiting['it']

  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })
  // Load text for the header
  $http.get('../texts/header.json', {
    headers: {
      'Accept': 'application/json;charset=utf-8'
    }
  })
  .success(function (data, headers) {
    $scope.hTexts = data.texts
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })

  // Functions for the navbar (require AuthService)
  // ------------------------
  $scope.getMyName = function () {
    if (AuthService.isAuthenticated()) {
      $http.get('/volunteers/memberinfo').then(function (result) {
        $scope.volunteer = result.data.volunteer
        $scope.myName = $scope.volunteer.username
        $scope.getVolunteer = true
        $scope.myParameters.newsletters = $scope.volunteer.letter
        $scope.myParameters.notifications = $scope.volunteer.notifications
        $scope.localLogin = $scope.volunteer.local_login
        if ($scope.volunteer.lang){
          $scope.lang = $scope.volunteer.lang
        }


        //var httpPromise = searchService.getMyImages($scope.volunteer.id)
        var httpPromise = imageQueryService.getMyImages($scope.volunteer.id)
        httpPromise.then(function (result) {
          $scope.myImages.listCollections = result.listCollections
          $scope.myImages.listOwners = result.listOwners
          $scope.myImages.minDate = result.minDate
          $scope.myImages.maxDate = result.maxDate
          $scope.myImages.list = result.listImages
          $scope.myImages.selectedList = result.listImages

          if (result.length === 0) {
            $scope.showImages = false
          }

          $scope.getVolunteersImagesPromise = imageQueryService.getVolunteersImages()
          $scope.getVolunteersImagesPromise.then(function (response) {

            // Fill data
            $scope.data = []

            // Check if the user has already geolocated an image
            var hasGeolocated = false
            for (id in response) {
              var curUser = response[id]
              if (curUser.volunteer_id == String($scope.volunteer.id)) {
                hasGeolocated = true
              }
            }
            if (hasGeolocated == false) {
              var d = {
                rank: response.length + 1,
                // nImages: parseInt(curUser.nImages),
                username: $scope.volunteer.username,
                value: 0,
                color: 'red'
              }
              // Fill data with the logged volunteer
              $scope.data.push(d)

              // Add attributes to volunteer
              $scope.volunteer.nImages = 0
              $scope.volunteer.rank = response.length + 1
              // intialise the values of the compared user
              $scope.user.diffImages = 0
              $scope.user.diffRank = 0
            }

            // Fill the data with the users
            for (id in response) {
              var curUser = response[id]

              if (hasGeolocated == false) {
                var rank = parseInt(id) + 1
              } else {
                var rank = parseInt(id) + 1
              }

              var d = {
                rank: rank,
                // nImages: parseInt(curUser.nImages),
                username: curUser.username,
                value: parseInt(curUser.nImages),
                color: ''
              }
              // Fill data with the logged volunteer
              if (curUser.volunteer_id == String($scope.volunteer.id)) {
                hasGeolocated = true
                // Change color of the bar
                d.color = 'red'
                // Add attributes to volunteer
                $scope.volunteer.nImages = parseInt(curUser.nImages)
                $scope.volunteer.rank = rank
                // intialise the values of the compared user
                $scope.user.diffImages = $scope.user.nImages - $scope.volunteer.nImages
                $scope.user.diffRank = $scope.user.rank - $scope.volunteer.rank
              } else {
                d.color = 'green'
              }
              $scope.data.push(d)
            }

            $scope.getRanking = true

          })
        })
      })
    } else {
      $scope.myName = "M'identifier"
    }
  }
  $scope.getMyName()

  // $scope.getMyName();

  $scope.clickMy = function () {
    if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
      location.href = '/mysmapshot/'
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Validate geolocation
  $scope.validateGeoloc = function (imageId, volunteerId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.validateGeoloc(imageId)
      promise.then(function () {

        $scope.validation = {}
        $scope.validation.imageId = imageId
        $scope.validation.volunteerId = volunteerId // id of the volunteer who geolocalize this image
        $scope.validation.validatorId = $scope.volunteer.id // id of the logged user
        $scope.validation.status = 'validated'

        validationsService.submitValidation(imageId, volunteerId, $scope.volunteer.id, $scope.validation).then(function() {
          // Supress from the list
          for (var i in $scope.toValidateImages) {
            if ($scope.toValidateImages[i].id == imageId) {
              var index = i
            }
          }
          $scope.toValidateImages.splice(index, 1)
        })

      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Reject geolocation
  $scope.rejectGeoloc = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.rejectGeoloc(imageId)
      promise.then(function () {
        // Supress from the list
        for (var i in $scope.toValidateImages) {
          if ($scope.toValidateImages[i].id == imageId) {
            var index = i
          }
        }
        $scope.toValidateImages.splice(index, 1)
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Add image in the game
  $scope.addToGame = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.addToGame(imageId)
      promise.then(function () {
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }

  // Remove image from the game
  $scope.removeFromGame = function (imageId) {
    if (AuthService.isAuthenticated()) {
      var promise = dbService.removeFromGame(imageId)
      promise.then(function () {
      })
    } else {
      // Go to login
      location.href = '/login/'
    }
  }
  $scope.goHome = function () {
    $window.open('/map/')
  }
  $scope.goToMono = function (imageId) {
    $window.open('/map/?imageId=' + imageId)
  }
  $scope.goToHelp = function () {
    // location.href = '/georef/?imageId='+imageId;
    $window.open('/help/')
  }
  $scope.goToGeoref = function (imageId) {
    // location.href = '/georef/?imageId='+imageId;
    $window.open('/georef/?imageId=' + imageId)
  }
  $scope.goToDashboard = function (imageId) {
    // location.href = '/georef/?imageId='+imageId;
    $window.open('/dashboard')
  }

  $scope.getReward = function (image) {
    if (AuthService.isAuthenticated()) {
      var sendData = {
        imageId: image.id,
        volunteerId: $scope.volunteer.id
      }
      if (image.download_link){
        imageQueryService.incrementDownloads(image.id).then(function () {
          // $window.open(image.download_link, '_blank')
          var win = $window.open('', '_blank')
          win.location = image.download_link
        })
      }else{
        win = $window.open('', '_blank')
        var prom = $http.get('/reward/' + image.id + '/' + $scope.volunteer.id)
        // var httpPromise =  $http.post('/reward', sendData)
        prom.then(function (res) {
          $scope.imageP = res.data
          $scope.showIm = true
          // $window.open("data:image/jpg;base64, " + res.data);
          win.location = 'data:image/jpg;base64, ' + res.data
        })
      }
    }
  }

  $scope.logout = function () {
    AuthService.logout()
    location.href = '/home/'
    // $state.go('outside.login');
  }

  $scope.updateVolunteer = function () {
    AuthService.update($scope.volunteer)
  }

// Ordering validation images
// --------------------------
  $scope.propertyName = 'age'
  $scope.reverse = true

  $scope.sortBy = function (propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false
    $scope.propertyName = propertyName
  }

  // filters
  // -------
  $scope.status = [
    {
      status: 'validated'
    },
    {
      status: 'improved'
    },
    {
      status: 'rejected'
    },
    {
      status: 'waiting'
    }
  ]//['validated', 'improved', 'rejected', 'waiting']
  $scope.selectedStates = [] //'accepted', 'improve', 'rejected', 'waiting']
  $scope.selectedOwners = []
  $scope.selectedCollections = []
  $scope.selectOwner = function (owner, source) {
    if (!$scope.selectedOwners.includes(owner)) {
      $scope.selectedOwners.push(owner)
    }
    $scope.filter(source)
  }
  $scope.selectCollection = function (collection, source) {
    if (!$scope.selectedCollections.includes(collection)) {
      $scope.selectedCollections.push(collection)
    }
    $scope.filter(source)
  }

  $scope.selectState = function (state, source) {
    if (!$scope.selectedStates.includes(state)) {
      $scope.selectedStates.push(state)
    }
    $scope.filter(source)
  }

  $scope.filter = function (source) {
    if (source === 'images') {
      var list = $scope.myImages.list
    }else if (source === 'corrections') {
      var list = $scope.myCorrections.list
    }else if (source === 'observations') {
      var list = $scope.myObservations.list
    }
    if (list) {
      if ($scope.selectedStates.length !== 0) {
        list = $scope.filterState(list)
      }
      if ($scope.selectedOwners.length !== 0) {
        list = $scope.filterOwner(list)
      }
      if ($scope.selectedCollections.length !== 0) {
        list = $scope.filterCollection(list)
      }
      if (source === 'images') {
        list = $scope.filterDate(list, $scope.myImages.minDate, $scope.myImages.maxDate)
      }else if (source === 'corrections') {
        list = $scope.filterDate(list, $scope.myCorrections.minDate, $scope.myCorrections.maxDate)
      }else if (source === 'observations') {
        list = $scope.filterDate(list, $scope.myObservations.minDate, $scope.myObservations.maxDate)
      }

      if (source === 'images') {
        $scope.myImages.selectedList = list
      }else if (source === 'corrections') {
        $scope.myCorrections.selectedList = list
      }else if (source === 'observations') {
        $scope.myObservations.selectedList = list
      }
    }
  }

  $scope.filterState = function (listImages) {
    var nImages = listImages.length // $scope.myImages.list.length
    var listSelected = []
    var listStates = []
    for (var i=0; i<$scope.selectedStates.length; i++){
      listStates.push($scope.selectedStates[i]['status'])
    }
    for (var i = 0; i < nImages; i++) {
      var image = listImages[i]
      if (listStates.length !== 0) {
        if (listStates.includes(image.status)) {
          listSelected.push(image)
        }
      }
    }
    return listSelected
    //$scope.myImages.selectedList = listSelected
  }

  $scope.filterCollection = function (listImages) {
    var nImages = listImages.length // $scope.myImages.list.length
    var listSelected = []
    for (var i = 0; i < nImages; i++) {
      var image = listImages[i]
      if ($scope.selectedCollections.length !== 0) {
        if ($scope.selectedCollections.includes(image.collectionname)) {
          listSelected.push(image)
        }
      }
    }
    //$scope.myImages.selectedList = listSelected
    return listSelected
  }

  $scope.filterOwner = function (listImages) {
    var nImages = listImages.length // $scope.myImages.list.length
    var listSelected = []
    for (var i = 0; i < nImages; i++) {
      var image = listImages[i]
      if ($scope.selectedOwners.length !== 0) {
        if ($scope.selectedOwners.includes(image.ownername)) {
          listSelected.push(image)
        }
      }
    }
    return listSelected
  }

  $scope.filterDate = function (listImages, minDate, maxDate) {
    var nImages = listImages.length // $scope.myImages.list.length
    var listSelected = []
    for (var i = 0; i < nImages; i++) {
      var image = listImages[i]
      if (image.date >= minDate) {
        if (image.date <= maxDate) {
          listSelected.push(image)
        }
      }
    }
    return listSelected
  }

  $scope.unselectState = function (state, source) {
    $scope.selectedStates.splice($scope.selectedStates.indexOf(state), 1)
    $scope.filter(source)
  }
  $scope.unselectCollection = function (collection, source) {
    $scope.selectedCollections.splice($scope.selectedCollections.indexOf(collection), 1)
    $scope.filter(source)
  }
  $scope.unselectOwner = function (owner, source) {
    $scope.selectedOwners.splice($scope.selectedOwners.indexOf(owner), 1)
    $scope.filter(source)
  }
  $scope.$watch('myImages.minDate', function (newV, oldV) {
    if ($scope.myImages.minDate){
      $scope.filter('images')
    }
  })
  $scope.$watch('myImages.maxDate', function () {
    if ($scope.myImages.maxDate){
      $scope.filter('images')
    }
  })
  $scope.$watch('myCorrections.minDate', function (newV, oldV) {
    if ($scope.myCorrections.minDate){
      $scope.filter('corrections')
    }
  })
  $scope.$watch('myCorrections.maxDate', function () {
    if ($scope.myCorrections.maxDate){
      $scope.filter('corrections')
    }
  })
  $scope.$watch('myObservations.minDate', function (newV, oldV) {
    if ($scope.myCorrections.minDate){
      $scope.filter('observations')
    }
  })
  $scope.$watch('myObservations.maxDate', function () {
    if ($scope.myCorrections.maxDate){
      $scope.filter('observations')
    }
  })

  $scope.popupMinDate = {
    opened: false
  }
  $scope.popupMaxDate = {
    opened: false
  }
  $scope.openMaxDate = function () {
    $scope.popupMaxDate.opened = true
  }
  $scope.openMinDate = function () {
    $scope.popupMinDate.opened = true
  }

  // My Parameters
  // -------------
  $scope.myParameters = {}

  $scope.submitUsername = function () {
    // AuthService.submitUsername($scope.volunteer.username)
    volunteersService.submitUsername($scope.volunteer.username)
  }
  $scope.changeLang = function (lang) {
    $scope.lang = lang
    volunteersService.submitLang(lang)
  }
  $scope.updateNewsletter = function () {
    volunteersService.submitNewsletters($scope.myParameters.newsletters)
  }
  $scope.updateNotifications = function () {
    volunteersService.submitNotifications($scope.myParameters.notifications)
  }
  // Local registration
  $scope.pwds = {
    oldPwd: null,
    newPwd1: null,
    newPwd2: null,
    same: false
  }
  $scope.$watch('pwds.newPwd2', function () {
    if ($scope.pwds.newPwd1 !== null){
      if ($scope.pwds.newPwd1 === $scope.pwds.newPwd2) {
        $scope.pwds.same = true
      }else{
        $scope.pwds.same = false
      }
    }else{
      $scope.pwds.same = false
    }
  })
  $scope.updatePwd = function () {
    // Check if the new pwd are the same

      AuthService.updatePwd($scope.pwds).then(function (response) {
        AuthService.storeUserCredentials(response.token)
        $scope.pwds = {
          oldPwd: null,
          newPwd1: null,
          newPwd2: null,
          same: true
        }
        alert($scope.texts.pwdUpdated[$scope.lang])
      }, function (errMsg) {
        alert(errMsg)
        // if (errMsg === 'Email already exists.') {
        //   alert($scope.texts.emailExists[$scope.lang])
        //   $window.location.reload()
        // } else if (errMsg === 'Email is not valid') {
        //   alert($scope.texts.emailNotValid[$scope.lang])
        //   $window.location.reload()
        // }
      })
  }

  $scope.openErrors = function (image) {
    errorsService.getErrors(image.errors_list).then(function (errors) { // validationResult.
      $scope.errorsModal = $uibModal.open({
        animation: false,
        templateUrl: 'errors.html',
        bindToController: true,
        controller: 'errorsCtrl',
        resolve: {
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          },
          image: function () {
            return image
          },
          errors: function () {
              return errors
          }
        }
      })
    })
  }

  // my Ranking
  // ----------
  // Date selection
  // Initialize start date
  // $scope.startDate = new Date() // Today
  // // Get previous month
  // $scope.startDate.setMonth($scope.startDate.getMonth() - 1)
  // $scope.startYear = $scope.startDate.getFullYear()
  // $scope.startMonth = $scope.startDate.getMonth() + 1

  // Initialize end date
  $scope.endDate = new Date()
  $scope.endYear = $scope.endDate.getFullYear()
  $scope.endMonth = $scope.endDate.getMonth() + 1

  // list of years
  $scope.listYears = [$scope.endYear - 1, $scope.endYear]

  // list of months
  $scope.listMonths = {
    0: {fr: 'Tous', en: 'All', de: 'Alle', it: 'Tutti', mid: 0, short: 'All'},
    1: {fr: 'Janvier', en: 'January', de: 'Januar', it: 'Gennaio', mid: 1, short: 'Jan'},
    2: {fr: 'Février', en: 'February', de: 'Februar', it: 'Febbraio', mid: 2, short: 'Feb'},
    3: {fr: 'Mars', en: 'March', de: 'März', it: 'Marzo', mid: 3, short: 'Mar'},
    4: {fr: 'Avril', en: 'April', de: 'April', it: 'Aprile', mid: 4, short: 'Apr'},
    5: {fr: 'Mai', en: 'May', de: 'Mai', it: 'Maggio', mid: 5, short: 'Mai'},
    6: {fr: 'Juin', en: 'June', de: 'Juni', it: 'Giugno', mid: 6, short: 'Jun'},
    7: {fr: 'Juillet', en: 'July', de: 'Juli', it: 'Luglio', mid: 7, short: 'Jul'},
    8: {fr: 'Août', en: 'August', de: 'August', it: 'Agosto', mid: 8, short: 'Aug'},
    9: {fr: 'Septembre', en: 'September', de: 'September', it: 'Settembre', mid: 9, short: 'Sep'},
    10: {fr: 'Octobre', en: 'October', de: 'Oktober', it: 'Ottobre', mid: 10, short: 'Oct'},
    11: {fr: 'Novembre', en: 'November', de: 'November', it: 'Novembre', mid: 11, short: 'Nov'},
    12: {fr: 'Décembre', en: 'December', de: 'Dezember', it: 'Dicembre', mid: 12, short: 'Dec'}
  }

  // Change end month
  $scope.selectEndMonth = function (month) {
    $scope.endMonth = month.mid
    if (month.mid !== 0){
      $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0) // Get last day of current month
      var limitDates = $scope.generateStringDates($scope.endDate)
      var startDate = limitDates[0]
      var stopDate = limitDates[1]
    }else{
      $scope.endDate = null
      $scope.endYear = $scope.texts.all[$scope.lang]
      startDate = null
      stopDate = null
    }
    // $scope.endMonth = month.mid
    // $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0) // Get last day of current month
    // var limitDates = $scope.generateStringDates($scope.endDate)
    // var startDate = limitDates[0]
    // var stopDate = limitDates[1]
    // console.log('start', startDate)
    // console.log('stop', stopDate)
    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  $scope.selectEndYear = function (year) {
    $scope.endYear = year
    if (year === $scope.texts.all[$scope.lang]){
      startDate = null
      stopDate = null
    }else{

      if ($scope.endMonth === 0){
        var startDate = year+'-01-01'
        var stopDate = year+'-12-31'
      }else{
        $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0)
        var limitDates = $scope.generateStringDates($scope.endDate)
        var startDate = limitDates[0]
        var stopDate = limitDates[1]
      }
    }


    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }

  $scope.generateStringDates = function (endDate) {
    // Generate start and end dates string
    // Get month
    var month = String(endDate.getMonth()+1)
    if (month.length < 2) {
      month = '0' + month
    }
    // Get year
    var year = endDate.getFullYear()
    // Generate strings
    var stopDate = [year, month, String(endDate.getDate())].join('-')
    var startDate = [year, month, '01'].join('-')

    return [startDate, stopDate]
  }
  // $scope.generateLimitDates = function () {
  //   // Generate start and end dates string
  //   if ($scope.endMonth.toString().length < 2) {
  //     var endMonth = '0' + $scope.endMonth
  //   } else {
  //     var endMonth = $scope.endMonth
  //   }
  //   var stopDateDay = new Date($scope.endYear, $scope.endMonth, 0).getDate() // Get last date of current month
  //   var stopDate = [$scope.endYear, endMonth, stopDateDay].join('-')
  //   var startDate = [$scope.endYear, endMonth, '01'].join('-')
  //   return [startDate, stopDate]
  // }
  // Click to change the collection
  $scope.selectCollectionRanking = function (collection) {
    $scope.myCollections.selected = collection
    $scope.collectionId = collection.collection_id
    if ($scope.endYear === $scope.texts.all[$scope.lang]) {
      var startDate = null
      var stopDate = null
    } else if ($scope.endMonth === 0) {
      var startDate = $scope.endYear + '-01-01'
      var stopDate = $scope.endYear + '-12-31'
    }
    else{
      var limitDates = $scope.generateStringDates($scope.endDate)//generateLimitDates()
      var startDate = limitDates[0]
      var stopDate = limitDates[1]
    }


    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  // Create data for the ranking
  $scope.generateDataForRanking = function (response) {
    $scope.dataRanking = []
    // Fill the data with the users
    for (var i = 0; i < response.length; i++) {
      // Get current user in the ranking
      var curUser = response[i]
      var d = {
        username: curUser.username,
        nImages: parseInt(curUser.nImages),
        id: curUser.volunteer_id,
        rank: i+1
      }
      $scope.dataRanking.push(d)
    }
    // Generate fake data
    // var n = 50
    // for (var i = 0; i < n; i++) {
    //   // Get current user in the ranking
    //   var curUser = response[i]
    //   var d = {
    //     username: 'mr. ' + i,
    //     nImages: i * 2,
    //     id: i,
    //     rank: i+1
    //   }
    //   $scope.dataRanking.push(d)
    // }
    //$scope.volunteer.id = 25
    $scope.dataRankingEnd = $scope.dataRanking.splice(3, $scope.dataRanking.length - 3)
    $timeout(function() {
      $location.hash('row-'+$scope.volunteer.id)
      // call $anchorScroll()
      $anchorScroll()
    })
  }

  // Corrections validations
  // -----------------------
  $scope.startTitleUpdate = function (correction) {
    correction.titleIsUpdated = true
    correction.newTitle = correction.sub_title
  }

  $scope.startCaptionUpdate = function (correction) {
    correction.captionIsUpdated = true
    correction.newCaption = correction.sub_caption
  }

  $scope.updateTitle = function (correction) {
    correctionsService.updateTitle(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  $scope.updateCaption = function (correction) {
    correctionsService.updateCaption(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  $scope.acceptTitle = function (correction) {
    correctionsService.acceptTitle(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  $scope.rejectTitle = function (correction) {
    correctionsService.rejectTitle(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  $scope.acceptCaption = function (correction) {
    correctionsService.acceptCaption(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  $scope.rejectCaption = function (correction) {
    correctionsService.rejectCaption(correction).then(function () {
      correctionsService.getToValidateCorrections().then(function (response) {
        $scope.toValidateCorrections = response
      })
    })
  }

  // Process remarks
  // ---------------
  $scope.startReply = function (remark) {
    remark.hasReply = true
    remark.reply = 'Reply'
  }

  $scope.checkedRemark = function (remark) {
    remarksService.checked(remark).then(function () {
      remarksService.getToValidateRemarks().then(function (response) {
        $scope.toValidateRemarks = response
      })
    })
  }

  // Process observations
  // ---------------
  $scope.watchObservation = function (observation) {
    // Open full resolution image
    $scope.openSelectedObservation(observation)
  }

  $scope.acceptObservation = function (observation) {
    observationsService.acceptObservation(observation).then(function () {
      observationsService.getToValidateObservations().then(function (response) {
        $scope.toValidateObservations = response
      })
    })
  }

  $scope.rejectObservation = function (observation) {
    observationsService.rejectObservation(observation).then(function () {
      observationsService.getToValidateObservations().then(function (response) {
        $scope.toValidateObservations = response
      })
    })
  }

  $scope.openSelectedObservation = function (observation) {
    $scope.observationModal = $uibModal.open({
      animation: false,
      templateUrl: 'observation.html',
      controller: 'observationCtrl',
      bindToController: true,
      size: 'lg',
      resolve: {
        texts: function () {
          return $scope.texts
        },
        lang: function () {
          return $scope.lang
        },
        observation: function () {
          return observation
        }
      }
    })
    $scope.observationModal.result.then(function (param) {
      if (param.result === 'reject') {
        observationsService.rejectObservation(param.observation).then(function () {
          observationsService.getToValidateObservations().then(function (response) {
            $scope.toValidateObservations = response
          })
        })
      // Go to image mode
    } else if (param.result === 'accept') {
        observationsService.acceptObservation(param.observation).then(function () {
          observationsService.getToValidateObservations().then(function (response) {
            $scope.toValidateObservations = response
          })
        })
      }
    })
  }

}])

mysmapApp.controller('errorsCtrl', function ($scope, $uibModalInstance, $window, texts, lang, image, errors) {
  $scope.texts = texts
  $scope.lang = lang
  $scope.image = image
  $scope.errors = errors
  // Opens a new tab with the info about the error
  $scope.errorInfo = function (e) {
    // More info about the error
    var erStr = '/help#error' + e.id
    $window.open(erStr, '_blank')
  }
  $scope.closeErrors = function () {
    $uibModalInstance.close()
  }
})

mysmapApp.controller('observationCtrl', function ($scope, $uibModalInstance, texts, lang, observation) {
  $scope.texts = texts
  $scope.lang = lang
  $scope.observation = observation
  $scope.optionsViewer =
  {
    prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
    gestureSettingsMouse: {
      clickToZoom: false,
      dblClickToZoom: false
    },
    showNavigator: false,
    showHomeControl: false,
    showFullPageControl: true,
    showZoomControl: true,
    tileSources: ['../data/collections/' + observation.collection_id + '/images/tiles/'.concat(observation.image_id).concat('.dzi')],
    overlays: [{
      id: 'example-overlay',
      x: parseFloat(observation.coord_x),
      y: parseFloat(observation.coord_y),
      width: 0.1,
      height: 0.1,
      placement: 'CENTER',
      className: 'highlight'
    }]
  }
  $scope.mysd = {}
  $scope.accept = function (observation) {
    var param = {
      result: 'accept',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
  $scope.reject = function (observation) {
    var param = {
      result: 'reject',
      observation: observation
    }
    $uibModalInstance.close(param)
  }
})
