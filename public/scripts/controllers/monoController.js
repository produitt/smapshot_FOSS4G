monoApp.controller('monoCtrl', ['$scope', '$http', '$q', '$window', '$location', '$sce', 'AuthService',
  'topoService', 'cesiumService', 'imageQueryService', 'storyService', 'forumService',
  'markerService',
  function ($scope, $http, $q, $window, $location, $sce, AuthService, topoService, cesiumService,
  imageQueryService, storyService, forumService, markerService) {
  // View parameters
  // ---------------
    $scope.showValidateToponym = false
    $scope.showValidateStory = false
    $scope.showValidateComment = false
    $scope.showValidateMarker = false
    $scope.imageMode = true
    $scope.isCollapsed = false
    $scope.showGlobe = false
    $scope.showPrevNext = false


  angular.element($window).bind('resize', function () {
    $scope.$apply(function(){
      $scope.cesiumHeight = angular.element(document.getElementById('adaptBox'))[0].clientHeight;
      $scope.cesiumWidth = angular.element(document.getElementById('globeBox'))[0].clientWidth;
    });
  });

  //texts
  //-----
  //Set initial language
  var lang = $window.navigator.language || $window.navigator.userLanguage;
  lang = lang.slice(0,2)
  if (lang == 'fr') {
    $scope.lang = 'fr'
  }else if (lang == 'it') {
    $scope.lang = 'it'
  }else if (lang == 'de') {
    $scope.lang = 'de'
  }else {
    $scope.lang = 'en'
  }
  //$scope.lang = 'fr'
  $scope.hTexts = {
    "login":{
      "fr": "M'identifier"
    }
  }
  $scope.myName = $scope.hTexts.login[$scope.lang]

  //Language switchers
  $scope.switchToFrench = function(){
    $scope.lang = 'fr'
    if (AuthService.isAuthenticated() == false){
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToGerman = function(){
    $scope.lang = 'de'
    if (AuthService.isAuthenticated() == false){
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToEnglish = function(){
    $scope.lang = 'en'
    if (AuthService.isAuthenticated() == false){
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToItalian = function(){
    $scope.lang = 'it'
    if (AuthService.isAuthenticated() == false){
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }

  $http.get("../texts/mono.json",{
    headers : {
      "Accept":"application/json;charset=utf-8",
      //"Accept-Charset":"charset=utf-8"
    }})  .success(function (data, headers) {
        $scope.texts = data.texts
        $scope.cesiumHeight = angular.element(document.getElementById('adaptBox'))[0].clientHeight
        $scope.cesiumWidth = angular.element(document.getElementById('globeBox'))[0].clientWidth
      })
      .error(function (data, status, headers, config) {
        console.log('error loading json')
      })
  // $scope.lang = 'fr'
    $scope.hTexts = {
      'login': {
        'fr': "M'identifier"
      }
    }
    $scope.myName = $scope.hTexts.login[$scope.lang]


  //Load text for the header
  $http.get("../texts/header.json",{
    headers : {
      "Accept":"application/json;charset=utf-8",
    }
  })

  // Functions for the navbar (require AuthService)
  // ------------------------
    $scope.getMyName = function () {
      return $q(function (resolve, reject) {
        if (AuthService.isAuthenticated()) {
          $http.get('/volunteers/memberinfo')
        .then(function (result) {
          $scope.volunteer = result.data.volunteer
          $scope.myName = $scope.volunteer.username
          resolve()
        })
        } else {
          $scope.myName = "M'identifier"
          resolve()
        }
      })
    }
    $scope.getMyName()

    $scope.clickMy = function () {
      if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
        location.href = '/mysmapshot/'
      } else {
      // Go to login
        var win = $window.open('/login', '_blank', 'menubar=no, status=no, scrollbars=no, menubar=no, width=500, height=500')
        var timer = setInterval(checkChild, 500)

        function checkChild () {
          if (win.closed) {
              // alert("Child window closed");
            clearInterval(timer)
            location.href = '/home/'
              // $scope.apply
              // $scope.$apply(function(){
              // $scope.getMyName();
              // })
          }
        }
      }
    }

    $scope.fillSlider = function () {
      if ($scope.visitId != null) {
        $scope.images.imagesInSlider = {}
        for (var i in $scope.images.dicImages) {
          var img = {
            id: $scope.images.dicImages[i].id,
            thumbSrc: '../images/thumbnails/'.concat($scope.images.dicImages[i].id).concat('.jpg')
          }
          $scope.images.imagesInSlider[img.id] = img
        }
      } else {
        var promi = imageQueryService.getImagesForSlider($scope.listImageId, $scope.collectionId, $scope.imageId, $scope.images.selectedImage.latitude, $scope.images.selectedImage.longitude)
        promi.then(function (res) {
          var images = res.data
          $scope.images.imagesInSlider = {}
          for (var i in images) {
            var img = {
              id: images[i].id,
              thumbSrc: '../images/thumbnails/'.concat(images[i].id).concat('.jpg')
            }
            $scope.images.imagesInSlider[img.id] = img
          }
        })
      }
    }

    $scope.getVisits = function () {
      return $q(function (resolve, reject) {
        promiseVisits = imageQueryService.getVisits()
        promiseVisits.then(function (response) {
          $scope.visits = []
          var nVisit = response.length
          for (id = 0; id < nVisit; id++) {
            var visit = {}
            visit.id = response[id].id
            visit.title = {}
            visit.text = {}
            visit.title.fr = response[id].title_fr
            visit.title.de = response[id].title_de
            visit.title.en = response[id].title_en
            visit.title.it = response[id].title_it
            visit.text.fr = response[id].text_fr
            visit.text.de = response[id].text_de
            visit.text.en = response[id].text_en
            visit.text.it = response[id].text_it
            $scope.visits[id] = visit
          }
        })
      })
    }
    $scope.getVisits()
    $scope.selectVisit = function (index) {
      if (index != 'free') {
        $scope.visitId = $scope.visits[index].id
      } else {
        $scope.visitId = null
      }

      $scope.imageId = null
      $scope.collectionId = null
      $scope.listImageId = null

    // Initialise images
      $scope.images = {
        show: false,
        geojson: null,
        dicImages: {},
        selectedImage: null,
        dicKeys: [],
        curKeyIndex: 0
      }
    // Visit the collection
      $scope.readImagesInList().then(function () {
        $scope.selectImage($scope.images.dicImages[$scope.images.dicKeys[0]].id)
      // Fill the image slider
        $scope.fillSlider()
      })
    }

    $scope.readImagesInList = function () {
      return $q(function (resolve, reject) {
        $scope.httpPromiseReadImages = imageQueryService.getImagesForMono($scope.listImageId, $scope.collectionId, $scope.imageId, $scope.visitId)
        $scope.httpPromiseReadImages.then(function (res) {
          var nFeat = res.data.features.length
          var features = res.data.features
          for (idFeat = 0; idFeat < nFeat; idFeat++) { // (var idFeat in features){
            var feature = features[idFeat]
            try {
              feature.geometry.coordinates.push(feature.properties.height)
            } catch (exception) {
              console.log('feature exception', feature)
            }

            var image = {
              id: feature.properties.id,
              longitude: feature.geometry.coordinates[0],
              latitude: feature.geometry.coordinates[1],
              height: feature.geometry.coordinates[2],
              azimuth: feature.properties.azimuth,
              tilt: feature.properties.tilt,
              roll: feature.properties.roll,
              focal: feature.properties.focal,
              cx: feature.properties.px,
              cy: feature.properties.py,
              name: feature.properties.title,
              date_shot: feature.properties.date_shot,
              gltfModel: null,
              gltfLoaded: false,
              georef_3d_bool: feature.properties.georef_3d_bool,
              pointModel: null,
              footprint: feature.properties.footprint,
            // General information
              title: feature.properties.title,
              photographer_id: feature.properties.photographer_id,
              license: feature.properties.license,
              owner_id: feature.properties.owner_id,
              collection_id: feature.properties.collection_id,
              link: feature.properties.link,
              volunteer_id: feature.properties.volunteer_id,
              photographer: feature.properties.photographer,
              username: feature.properties.username,
              collection: feature.properties.collection,
              owner: feature.properties.owner,
              geolocRate: feature.properties.geoloc_rate,
              imageRate: feature.properties.image_rate
            }
            $scope.images.dicKeys.push(feature.properties.id)
            if ($scope.images.selectedImage) {

            } else {
            // if there is no image selected (collection mode), select the first image in the list
              $scope.imageId = image.id
              $scope.images.selectedImage = image

            // Get not georeferenced images
              $scope.bbox = {
                left: $scope.images.selectedImage.longitude - 0.1,
                right: $scope.images.selectedImage.longitude + 0.1,
                bottom: $scope.images.selectedImage.latitude - 0.1,
                top: $scope.images.selectedImage.latitude + 0.1
              }
              var promise = imageQueryService.getNotGeolocatedBbox($scope.bbox)
              promise.then(function (res) {
                $scope.notGeoref = res.nImages
              })

              $scope.showGlobe = true
              $scope.images.dicImages[feature.properties.id] = image
            }
            if (image.id != $scope.images.selectedImage.id) {
              $scope.images.dicImages[feature.properties.id] = image
            // $scope.drawImage(image)
            }
          }
        // Get keys
        // $scope.images.dicKeys= Object.keys($scope.images.dicImages)
          resolve()
        })
      })
    }

  // Initialise images
    $scope.images = {
      show: false,
      geojson: null,
      dicImages: {},
      selectedImage: null,
      dicKeys: [],
      curKeyIndex: 0
    }

  // Initialise rate array (store the id of rated image per session)
    $scope.rateArray = {
      image: [],
      geoloc: []
    }

  // Get the url parameters
  // ----------------------
    $scope.visitId = $location.search().visitId
    $scope.collectionId = $location.search().collectionId
    if ($location.search().listImageId) {
      $scope.listImageId = JSON.parse($location.search().listImageId)
      $scope.showPrevNext = true
    } else {
      $scope.listImageId = $location.search().listImageId
    }
    $scope.imageId = $location.search().imageId

  // Initialise the case (imageId, collectionId, listImageId, visitId)
    if ($scope.imageId) {
      $scope.showPrevNext = true
    // Get the image of the same collection for the navigation (or all images?)
      $scope.httpPromise = imageQueryService.getImage($scope.imageId)
      $scope.httpPromise.then(function (image) {
        image.date_shot = image.date_shot
        image.footprintGeojson = JSON.parse(image.footprint)

        image.geolocRate = parseInt(image.geoloc_rate)
        image.imageRate = parseInt(image.image_rate)

        $scope.images.selectedImage = image
        $scope.images.dicImages[$scope.imageId] = image
        $scope.showGlobe = true

      // Get not georeferenced images
        $scope.bbox = {
          left: $scope.images.selectedImage.longitude - 0.1,
          right: $scope.images.selectedImage.longitude + 0.1,
          bottom: $scope.images.selectedImage.latitude - 0.1,
          top: $scope.images.selectedImage.latitude + 0.1
        }
        var promise = imageQueryService.getNotGeolocatedBbox($scope.bbox)
        promise.then(function (res) {
          $scope.notGeoref = res.nImages
        })
      // Visit the collection
        $scope.readImagesInList().then(function () {
        // Fill the image slider
          $scope.fillSlider()
        })
      })
    } else if ($scope.listImageId) {
      $scope.imageId = $scope.listImageId[0]
      $scope.httpPromise = imageQueryService.getImage($scope.imageId)
      $scope.httpPromise.then(function (image) {
        image.date_shot = image.date_shot
        image.footprintGeojson = JSON.parse(image.footprint)
        $scope.images.selectedImage = image
        $scope.images.dicImages[$scope.imageId] = image
        $scope.showGlobe = true

      // Get not georeferenced images
        $scope.bbox = {
          left: $scope.images.selectedImage.longitude - 0.1,
          right: $scope.images.selectedImage.longitude + 0.1,
          bottom: $scope.images.selectedImage.latitude - 0.1,
          top: $scope.images.selectedImage.latitude + 0.1
        }
        var promise = imageQueryService.getNotGeolocatedBbox($scope.bbox)
        promise.then(function (res) {
          $scope.notGeoref = res.nImages
        })

      // Visit the collection
        $scope.readImagesInList().then(function () {
        // Fill the image slider
          $scope.fillSlider()
        })
      })
    } else if ($scope.collectionId) {
      $scope.showPrevNext = true

    // Visit the collection
      $scope.readImagesInList().then(function () {
      // Fill the image slider
        $scope.fillSlider()
      })
    } else if ($scope.visitId) {
      $scope.showPrevNext = true

    // Visit the collection
      $scope.readImagesInList().then(function () {
      // Fill the image slider
        $scope.fillSlider()
      })
    } else {
      $scope.showPrevNext = true
    // Visit the collection
      $scope.readImagesInList().then(function () {
      // Fill the image slider
        $scope.fillSlider()
      })
    }

  // Read the image id from the url
  // var url = location.pathname;
  // var urlSplit = url.split('/');
  // var n = urlSplit.length;

  // Initialise the image
  // $scope.imageId = urlSplit[n-1];

  // Initialise the globe
    $scope.cesium = null
  // imageryProvider
    var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741)
  // terrainProvider
    var terrainProvider = new Cesium.CesiumTerrainProvider({
      url: '//3d.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/default/20160115/4326/',
    // url : '//terrain1.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/default/20160115/4326/',
      availableLevels: [8, 9, 10, 12, 14, 16, 17],
    // availableLevels:[8,9,10,12,14],
    // minimumRetrievingLevel: 8,
    // maximumLevel: 10,
    // minimumTerrainLevel:8,
    // maximumTerrainLevel:14,
      rectangle: rectangle // Doesn't work without
    })
    $scope.terrainProvider = terrainProvider

    $scope.transparency = 100
    $scope.scale = 1.0
    try {
      $scope.scaleLog = Math.log10($scope.scale)
    } catch (exception) {
      $scope.scaleLog = Math.log($scope.scale)
    }
    $scope.$watch('scaleLog', function () {
      $scope.scale = Math.pow(10, $scope.scaleLog)
      if ($scope.images.selectedImage) {
        $scope.images.selectedImage.scale = $scope.scale
      }
    }, true)

  // Select an image from the slider
    $scope.selectImage = function (id) {
    // Get current index
      $scope.images.curKeyIndex = $scope.images.dicKeys.indexOf(id)

    // Select image
      $scope.images.selectedImage = $scope.images.dicImages[id]

      $scope.flyToSelected()
    }

  $scope.drawModel = function(){
    var tempBool = false
    var model = cesiumService.drawModel($scope.images.selectedImage, $scope.cesium, 1, null, tempBool)
    $scope.images.selectedImage.gltfModel = model;
    $scope.images.selectedImage.gltfLoaded = true;

      $scope.images.dicImages[$scope.images.selectedImage.id].gltfModel = model
      $scope.images.dicImages[$scope.images.selectedImage.id].gltfLoaded = true
    }

    $scope.collections = {
      all: [],
      dict: {}
    }
    imageQueryService.getCollections()
  .then(function (response) {
    for (id in response) {
      $scope.collections.all.push(response[id].name)
    }
    $scope.collections.dict = response

    var imTot = 0
    var imGeorefTot = 0
    var nColl = $scope.collections.dict.length
    for (index = 0; index < nColl; index++) {
      var collection = $scope.collections.dict[index]
      var ratio = (parseInt(collection.nGeoref) / parseInt(collection.nImages) * 100).toFixed(1)
      $scope.collections.dict[index].ratio = ratio
      imTot = imTot + parseInt(collection.nImages)
      imGeorefTot = imGeorefTot + parseInt(collection.nGeoref)
    }
    $scope.ratioGeoloc = (imGeorefTot / imTot * 100).toFixed(1)
    $scope.ratioNotGeoloc = (100 - $scope.ratioGeoloc).toFixed(1)

    // $scope.search.toponyms.propositions = response
    // $scope.search.toponyms.propositions = $scope.search.toponyms.propositions.concat(response)
  })

  // model
  // -----
  // Tool mode for interaction
    $scope.toolMode = {
      addTopo: false,
      addMarker: false,
      addStory: false,
      addComment: false
    }

  // //Initialise osm layers
  // $scope.OSMlayers = {
  //   hydrology: {
  //     show: false,
  //     data: null
  //   },
  //   forest: {
  //     show: false,
  //     data: null
  //   },
  //   infrastructure: {
  //     show: false,
  //     data: null
  //   }
  // };

  // Initialise forum
    $scope.markers = {
      loaded: false,
      addMarkerMode: false,
      show: true,
      curMarkerId: 0,
      dicMarker: {},
      dicMarkerReady: {},
      curMarker: {
        title: null,
        text: null,
        commentId: 0,
        date: null
      }
    }
    $scope.$watch('markers.curMarker', function () {
      if (($scope.markers.curMarker.title != null) && ($scope.markers.curMarker.height != null) && ($scope.markers.curMarker.date != null)) {
        $scope.showValidateMarker = true
      };
    }, true)

  // Initialise forum
    $scope.forum = {
      loaded: false,
      addCommentMode: false,
      show: true,
      curCommentId: 0,
      dicComment: {},
      curComment: {
        title: null,
        text: null,
        username: null,
        commentId: 0
      }
    }
    $scope.$watch('forum.curComment', function () {
      if ($scope.forum.curComment.text != null) {
        $scope.showValidateComment = true
      };
    }, true)

  // Initialise toponyms
    $scope.toponyms = {
      loaded: false,
      addTopoMode: false,
      show: false,
      data: null,
      curTopoId: 0,
      newTopoList: [],
      list: [],
      curTopo: {
        name: null,
        latitude: null,
        longitude: null,
        height: null,
        lang: null,
        elementId: 'new'
      }
    }
    $scope.$watch('toponyms.curTopo', function () {
      if (($scope.toponyms.curTopo.name != null) && ($scope.toponyms.curTopo.height != null)) {
        $scope.showValidateToponym = true
      // topoService.eraseCurTopo($scope.toponyms.curTopo, $scope.cesium)
      // topoService.drawCurTopo($scope.toponyms.curTopo, $scope.cesium)
      };
    }, true)

  // stories
    $scope.stories = {
      loaded: false,
      show: false,
      dicStory: {},
      curStoryId: 0
    }
    var curStory = {
      date: null,
      volunteerId: null,
      title: null,
      text: null,
      latitude: null,
      longitude: null,
      height: null,
      storyId: $scope.stories.curStoryId,
      style: null
    }
    $scope.stories.curStory = curStory
    $scope.$watch('stories.curStory', function () {
      if (($scope.stories.curStory.title != null) && ($scope.stories.curStory.height != null) &&
    ($scope.stories.curStory.date != null)) {
        $scope.showValidateStory = true
      };
    }, true)

    $scope.goToImageLink = function () {
    // location.href = $scope.images.selectedImage.link
      $window.open($scope.images.selectedImage.link, '_blank')
    }
    $scope.goToCollectionLink = function () {
      var win = $window.open('', '_blank')
      var promise = imageQueryService.getCollectionLink($scope.images.selectedImage.collection_id)
      promise.then(function (res) {
        win.location = res.link
      // $window.open(res.link, '_blank');
      })
    }
    $scope.goToOwnerLink = function () {
      var win = $window.open('', '_blank')
      var promise = imageQueryService.getOwnerLink($scope.images.selectedImage.owner_id)
      promise.then(function (res) {
        win.location = res.link
      // $window.open(res.link, '_blank');
      })
    }
    $scope.goToPhotographerLink = function () {
      var win = $window.open('', '_blank')
      var promise = imageQueryService.getPhotographerLink($scope.images.selectedImage.photographer_id)
      promise.then(function (res) {
        win.location = res.link
      // $window.open(res.link, '_blank');
      })
    }
    $scope.goToGeorefBbox = function () {
      location.href = '/georef/?left=' + $scope.bbox.left + '&right=' + $scope.bbox.right + '&bottom=' + $scope.bbox.bottom + '&top=' + $scope.bbox.top
    }
    $scope.goToMap = function () {
      location.href = '/map/?left=' + $scope.bbox.left + '&right=' + $scope.bbox.right + '&bottom=' + $scope.bbox.bottom + '&top=' + $scope.bbox.top
    }
  // Click on a create button (story, topo, marker)
  // ----------------------------------------------
    $scope.changeTopoToolMode = function () {
      if ($scope.toolMode.addTopo == false) {
        $scope.toolMode.addTopo = true
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      } else {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      };
    }

    $scope.changeStoryToolMode = function () {
      if ($scope.toolMode.addStory == false) {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = true
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      } else {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      };
    }
    $scope.changeCommentToolMode = function () {
      if ($scope.toolMode.addComment == false) {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = true
        $scope.toolMode.addMarker = false
      } else {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      };
    }
    $scope.changeMarkerToolMode = function () {
      if ($scope.toolMode.addMarker == false) {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = true
      } else {
        $scope.toolMode.addTopo = false
        $scope.toolMode.addStory = false
        $scope.toolMode.addComment = false
        $scope.toolMode.addMarker = false
      };
    }

    $scope.cancelMarker = function () {
      $scope.markers.curMarker.title = null
      $scope.markers.curMarker.latitude = null
      $scope.markers.curMarker.longitude = null
      $scope.markers.curMarker.height = null
      $scope.showValidateMarker = false

      $scope.markers.curMarkerId += 1
      $scope.markers.curMarker.markerId = $scope.markers.curMarkerId
    }

    $scope.submitMarker = function () {
      AuthService.loadUserCredentials()
      if (AuthService.isAuthenticated()) {
        var promise = $scope.getMyName()
        promise.then(function () {
          if ($scope.markers.curMarker.title != null) {
            if ($scope.markers.curMarker.height != null) {
            // Send new toponym in the database
              var postMarkerPromise = markerService.postMarker($scope.markers.curMarker, $scope.volunteer.id, $scope.imageId)
            .then(function () {
              $scope.markers.curMarker.markerId = $scope.markers.curMarkerId
              $scope.markers.dicMarker[$scope.markers.curMarker.markerId] = $scope.markers.curMarker
              $scope.cancelMarker()
              var location = markerService.drawCurMarkers($scope.curMarker, $scope.cesium)
              $scope.markers.curMarker.x = location.x
              $scope.markers.curMarker.y = location.y
              $scope.markers.curMarker.style = {
                'position': 'fixed',
                'top': location.y,
                'left': location.x,
                'z-index': 10
              }
              $scope.markers.dicMarker[curMarkerId] = $scope.markers.curMarker
              $scope.markers.dicMarkerReady = $scope.markers.dicMarker

              // Change toolMode
              $scope.toolMode.addMarker = false
            })
            } else {
              alert($scope.texts.markersAlertLoc[$scope.lang])
            }
          } else {
            alert($scope.texts.markersAlertName[$scope.lang])
          }
        })
      } else {
        alert($scope.texts.alertLogin[$scope.lang])
        $window.open('/login', '_blank')
      }
    }

    $scope.cancelToponym = function () {
      $scope.toponyms.curTopo.name = null
      $scope.toponyms.curTopo.latitude = null
      $scope.toponyms.curTopo.longitude = null
      $scope.toponyms.curTopo.height = null
      $scope.toponyms.curTopo.elementId = 'new'

    // $scope.showInputTopo = false;
      $scope.showValidateToponym = false
    }
    $scope.submitTopo = function () {
      AuthService.loadUserCredentials()
      if (AuthService.isAuthenticated()) {
        var promise = $scope.getMyName()
        promise.then(function () {
          if ($scope.toponyms.curTopo.name != null) {
            if ($scope.toponyms.curTopo.height != null) {
            // Send new toponym in the database
              var postTopoPromise = topoService.postTopo($scope.toponyms.curTopo, $scope.volunteer.id, $scope.imageId)
            .then(function () {
              $scope.toponyms.curTopo.elementId = $scope.toponyms.curTopo.name
              $scope.toponyms.list.push($scope.toponyms.curTopo)
              $scope.cancelToponym()

              // Change toolMode
              $scope.toolMode.addTopo = false
            })
            } else {
              alert($scope.texts.topoAlertLoc[$scope.lang])
            }
          } else {
            alert($scope.texts.topoAlertName[$scope.lang])
          }
        })
      } else {
        alert($scope.texts.alertLogin[$scope.lang])
        $window.open('/login', '_blank')
      }
    }

    $scope.cancelStory = function () {
      $scope.stories.curStory.title = null
      $scope.stories.curStory.latitude = null
      $scope.stories.curStory.longitude = null
      $scope.stories.curStory.height = null
      $scope.stories.curStory.elementId = 'new'
      $scope.stories.curStory.text = null
    // Increment story id
      $scope.stories.curStoryId += 1
      $scope.stories.curStory.storyId = $scope.stories.curStoryId
      $scope.showValidateStory = false
    }
    $scope.submitStory = function () {
      AuthService.loadUserCredentials()
      if (AuthService.isAuthenticated()) {
        var promise = $scope.getMyName()
        promise.then(function () {
          if ($scope.stories.curStory.title != null) {
            if ($scope.stories.curStory.height != null) {
              if ($scope.stories.curStory.text != null) {
              // Delete stories icon
                storyService.deleteStories($scope.stories.dicStory, $scope.cesium)

              // Add story to the dictionary
                $scope.stories.curStory.elementId = $scope.stories.curStory.name
                $scope.stories.dicStory[$scope.stories.curStory.storyId] = $scope.stories.curStory

              // Draw icon stories
                storyService.drawStories($scope.stories.dicStory, $scope.cesium, $scope.tilesLoadedPromise)
              .then(function (response) {
                $scope.stories.dicStoryReady = response
              })

                var o = $scope.stories.curStory

              // Send new toponym in the database
                var postStoryPromise = storyService.postStory($scope.stories.curStory, $scope.volunteer.id, $scope.imageId)
              .then(function () {
                $scope.cancelStory()

                // Change toolMode
                $scope.toolMode.addStory = false
              })
              }
            } else {
              alert($scope.texts.topoAlertLoc[$scope.lang])
            }
          } else {
            alert($scope.texts.topoAlertName[$scope.lang])
          }
        })
      } else {
        alert($scope.texts.alertLogin[$scope.lang])
        $window.open('/login', '_blank')
      }
    }
    $scope.cancelComment = function () {
      $scope.forum.curComment.title = null
      $scope.forum.curComment.text = null
    // Increment comment id
      $scope.forum.curCommentId += 1
      $scope.forum.curComment.commentId = $scope.forum.curCommentId
      $scope.showValidateComment = false
    }
    $scope.submitComment = function () {
      AuthService.loadUserCredentials()
      if (AuthService.isAuthenticated()) {
        var promise = $scope.getMyName()
        promise.then(function () {
          if ($scope.forum.curComment.text != null) {
          // Send new toponym in the database
            var postCommentPromise = forumService.postComment($scope.forum.curComment, $scope.volunteer.id, $scope.imageId)
          .then(function () {
            var comment = {
              username: $scope.volunteer.username,
              commentId: $scope.forum.curCommentId,
              text: $scope.forum.curComment.text,
              title: $scope.forum.curComment.title,
              date: new Date()
            }
            $scope.forum.dicComment[$scope.forum.curCommentId] = comment

            $scope.cancelComment()
            // Change toolMode
            $scope.toolMode.addComment = false
          })
          } else {
            alert($scope.texts.forumAlertText[$scope.lang])
          }
        })
      } else {
        alert($scope.texts.alertLogin[$scope.lang])
        $window.open('/login', '_blank')
      }
    }

  // Change from image to navigation mode
  // ------------------------------------
    $scope.lockUnlock = function () {
      if ($scope.imageMode == true) {
      // Image mode
      // ---------
      // cesiumService.lock

      } else {
      // Navigation mode
      // Reset scale and transparency
      // ---------------------------
        try {
          $scope.images.selectedImage.gltfModel.scale = 2
          var model = $scope.images.selectedImage.gltfModel
        // var mat = model.getMaterial("material_0");
        // mat.setValue('transparency', 0.95);
        // $scope.transparency=0.95;
          $scope.scale = 2
        } finally {
        // Navigation mode
        // ---------------
        // Enable navigation
          cesiumService.goToNavigationMode($scope.cesium)
        // var scene =$scope.cesium.scene
        // scene.screenSpaceCameraController.enableRotate = true;
        // scene.screenSpaceCameraController.enableTranslate = true;
        // scene.screenSpaceCameraController.enableTilt = true;
        // scene.screenSpaceCameraController.enableZoom = true;
        // //Enable collision detection
        // scene.screenSpaceCameraController.enableCollisionDetection = true;
        // scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000;
        // Suppress overlays
          topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)
          $scope.stories.show = false
        }
      }
    }

  // Show or hide toponyms
  // ---------------------
    $scope.showHideTopo = function () {
    // Toponyms should have been loaded before
      if ($scope.toponyms.show == true) {
        $scope.toponyms.show = false
      } else {
      // Delete toponyms
        $scope.toponyms.show = true
      }
    }
  // Load hydrology osm data
  // -----------------------
  // $scope.getHydroPromiseFunction = function(){
  //   return $q(function(resolve, reject) {
  //
  //     var long = parseFloat($scope.images.selectedImage.longitude);
  //     var lat = parseFloat($scope.images.selectedImage.latitude);
  //     //Get OSM data: test with http://overpass-turbo.eu/
  //     var bboxStr = geojson2Turbo($scope.images.selectedImage.footprint);
  //     //Build a query for overpass (see overpass-turbo)
  //     var query = '[out:json];(way[waterway]({{bbox}});way[natural = "water"]({{bbox}});way[natural = "glacier"]({{bbox}}););out body;>;out;';
  //     var query = query.replace(/{{bbox}}/g,bboxStr);
  //     var data_url = "http://overpass-api.de/api/interpreter?data=".concat(query);
  //     // Fais le boulot XHR habituel
  //     var req = new XMLHttpRequest();
  //     req.open('GET', data_url);
  //     req.onload = function() {
  //       // Ceci est appelé même pour une 404 etc.
  //       // aussi vérifie le statut
  //       if (req.readyState == 4 && req.status == 200) {
  //         // Accomplit la promesse avec le texte de la réponse
  //         var jsonData = JSON.parse(req.responseText);
  //         var geojsonData = osmtogeojson(jsonData);
  //         $scope.OSMlayers.hydrology.data = geojsonData;
  //         resolve(geojsonData);
  //       }
  //       else {
  //         // Sinon rejette avec le texte du statut
  //         // qui on l’éspère sera une erreur ayant du sens
  //         reject(Error(req.statusText));
  //       }
  //     };
  //     // Gère les erreurs réseau
  //     req.onerror = function() {
  //       reject(Error("Erreur réseau"));
  //     };
  //     // Lance la requête
  //     req.send();
  //   })
  // };

  // Initial click on show topo: load and show
  // ----------------------------------------
    $scope.loadShowTopo = function () {
    // $scope.toponyms.show = true
      if ($scope.toponyms.loaded == false) {
        $scope.toponyms.show = true
        $scope.toponyms.loaded = true
        var topoPromise = topoService.getToponyms($scope.images.selectedImage.footprint, $scope.images.selectedImage.latitude, $scope.images.selectedImage.longitude)
        topoPromise.then(function (response) {
          $scope.geojsonToponyms = response
          $scope.geojsonToponyms = topoService.filterToponyms($scope.geojsonToponyms, $scope.images.selectedImage.latitude, $scope.images.selectedImage.longitude)
          var topo3DPromise = topoService.geojsonTo3D($scope.geojsonToponyms, $scope.cesium, $scope.cesium.terrainProvider)
          topo3DPromise.then(function (response) {
          // $scope.toponyms.list = response;
            topoService.drawToponyms(response, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
              $scope.toponyms.list = response.icons
              $scope.toponyms.geoJsonData = response.geoJsonData
            })
          })
        })
      };
    }

    $scope.loadShowComments = function () {
      if ($scope.forum.loaded == false) {
        $scope.forum.loaded = true
        $scope.getComments()
      };
    }
    $scope.getComments = function () {
      $scope.forum.dicComment = {}
      $scope.forum.curCommentId = 0
      forumService.getComments($scope.images.selectedImage.id)
    .then(function (response) {
      var commentArray = response.data
      var nComm = commentArray.length
      for (id = 0; id < nComm; id++) {
        var comment = {
          date: new Date(commentArray[id].timestamp).toDateString(),
          volunteerId: null,
          title: commentArray[id].title,
          text: commentArray[id].text,
          username: commentArray[id].username
        }
        $scope.forum.dicComment[$scope.forum.curCommentId] = comment
        $scope.forum.curCommentId += 1
      }
      $scope.forum.curComment.commentId = $scope.forum.curCommentId
    })
    }

    $scope.loadShowStories = function () {
      if ($scope.stories.loaded == false) {
        $scope.stories.show = true
        $scope.stories.loaded = true
      // Get stories
        storyService.getStories($scope.imageId)
      .then(function (response) {
        var storiesArray = response.data
        for (var id in storiesArray) {
          var story = {
            date: storiesArray[id].date_story,
            volunteerId: null,
            title: storiesArray[id].title,
            text: storiesArray[id].text,
            latitude: storiesArray[id].latitude,
            longitude: storiesArray[id].longitude,
            height: storiesArray[id].height,
            username: storiesArray[id].username,
            storyId: id,
            html: $sce.trustAsHtml('<i class="fa fa-comment" aria-hidden="true"></i> "' + storiesArray[id].text + '"</br><span class="glyphicon glyphicon-user"></span> ' + storiesArray[id].username + '</br><span class="glyphicon glyphicon-calendar"></span> ' + new Date(storiesArray[id].date_story).toDateString())
          }
          $scope.stories.dicStory[id] = story
          $scope.stories.curStoryId = id + 1
        }

        $scope.stories.curStory.storyId = $scope.stories.curStoryId

        // Draw story
        storyService.drawStories($scope.stories.dicStory, $scope.cesium, $scope.tilesLoadedPromise)
        .then(function (response) {
          $scope.stories.dicStoryReady = response
        })
        //
      })
      };
    }

    $scope.loadShowMarkers = function () {
      if ($scope.markers.loaded == false) {
        $scope.markers.loaded = true
      // Get stories
        markerService.getMarkersImage($scope.imageId)
      .then(function (markersArray) {
        if (markersArray) {
          // var markersArray = response;
          for (var id in markersArray) {
            var marker = {
              date: markersArray[id].date_marker,
              volunteerId: null,
              title: markersArray[id].title,
              text: markersArray[id].text,
              latitude: markersArray[id].latitude,
              longitude: markersArray[id].longitude,
              height: markersArray[id].height,
              username: markersArray[id].username,
              markerId: id
            }
            $scope.markers.dicMarker[id] = marker
          }
          $scope.markers.curMarkerId = id + 1
          $scope.markers.curMarker.markerId = $scope.markers.curMarkerId

          // Draw story
          markerService.drawMarkers($scope.markers.dicMarker, $scope.cesium, $scope.tilesLoadedPromise)
          .then(function (response) {
            $scope.markers.dicMarkerReady = response
          })
        }

        //
      })
      };
    }

    $scope.readImages = function () {
    // Generate url
      var urlRead = '/dbViz/readDB/'

    // Get image info from the database
      $scope.httpPromiseReadImages = $http.get(urlRead)
    // Wait the answer of the database
      $scope.httpPromiseReadImages.then(function (res) {
        var geoJson = res.data
        $scope.images.geojson = geoJson

      // Create dictionary
      // $scope.images.dicImages = {}

      // Add third dimension to point
        for (var idFeat in geoJson.features) {
          var feature = geoJson.features[idFeat]
          feature.geometry.coordinates.push(feature.properties.height)
        // Process footprint to make array
          var image = {
            id: feature.properties.id,
            longitude: feature.geometry.coordinates[0],
            latitude: feature.geometry.coordinates[1],
            height: feature.geometry.coordinates[2],
            azimuth: feature.properties.azimuth,
            tilt: feature.properties.tilt,
            roll: feature.properties.roll,
            focal: feature.properties.focal,
            cx: feature.properties.px,
            cy: feature.properties.py,
            name: feature.properties.title,
            date: feature.properties.date_shot,
            gltfModel: null,
            georef_3d_bool: feature.properties.georef_3d_bool,
            footprint: feature.properties.footprint,

          // General information
            title: feature.properties.title,
            photographer_id: feature.properties.photographer_id,
            license: feature.properties.license,
            owner_id: feature.properties.owner_id,
            collection_id: feature.properties.collection_id,
            link: feature.properties.link,
            volunteer_id: feature.properties.volunteer_id
          }
          if (image.id != $scope.images.selectedImage.id) {
            $scope.images.dicImages[feature.properties.id] = image
          }
        }
      })
    }
  // $scope.readImages();
  // $scope.drawImages = function(){
  //
  //   //Draw Images
  //   for (var id in $scope.images.dicImages){
  //     var image = $scope.images.dicImages[id];
  //     if ((image.georef_3d_bool == true) && (image.id != $scope.images.selectedImage.id)){
  //
  //       var model = cesiumService.drawModel(image, $scope.cesium, 0.95, null)
  //       $scope.images.dicImages[id].gltfModel = model
  //       //$scope.drawCylinder(feature);
  //
  //     }
  //     //$scope.drawMarker(feature);
  //   }
  //   for (var id in $scope.images.dicImages){
  //     var image = $scope.images.dicImages[id];
  //     if (image.georef_3d_bool == true){
  //       var prim = cesiumService.drawCylinder(image, $scope.cesium)
  //       $scope.images.dicImages[id].cylinderModel = prim
  //     }
  //     var prim = cesiumService.drawMarker(image, $scope.cesium, $scope.terrainProvider)
  //     $scope.images.dicImages[id].pointModel = prim
  //   }
  // };
    $scope.drawMarkers = function () {
      for (var id in $scope.images.dicImages) {
        var image = $scope.images.dicImages[id]
      // if (image.georef_3d_bool == true){
        var prim = cesiumService.drawCylinder(image, $scope.cesium)
        $scope.images.dicImages[id].cylinderModel = prim
      // }
        var prim = cesiumService.drawMarker(image, $scope.cesium, $scope.terrainProvider)
        $scope.images.dicImages[id].pointModel = prim
      }
    }


  $scope.drawImage = function(image){

      if (image.id != $scope.images.selectedImage.id){
        var tempBool = false
        var model = cesiumService.drawModel(image, $scope.cesium, 0.95, null, tempBool)
        $scope.images.dicImages[image.id].gltfModel = model
        $scope.images.dicImages[image.id].gltfLoaded = true

        // var prim = cesiumService.drawMarker(image, $scope.cesium, $scope.terrainProvider)
        // $scope.images.dicImages[image.id].pointModel = prim
      }
    }

    $scope.hideShow = function () {
      cesiumService.hideShowModel($scope.images.selectedImage.gltfModel)
    }

    $scope.changeTransparency = function () {
      cesiumService.changeModelTransparency($scope.images.selectedImage.gltfModel, $scope.transparency)
    }

    $scope.changeScaleLog = function () {
      var model = $scope.images.selectedImage.gltfModel
    // var scale = model.scale;
      $scope.scale = Math.pow(10, $scope.scaleLog)
      model.scale = $scope.scale
      var scale = model.scale
    }
    $scope.flyToSelected = function () {
    // Hide toponyms and stories
      storyService.deleteStories($scope.stories.dicStory, $scope.cesium)
      topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)
      $scope.stories.show = false
      $scope.toponyms.loaded = false
      $scope.forum.loaded = false
      $scope.getComments()

    // Select image
    // $scope.images.selectedImage = $scope.images.dicImages[$scope.images.dicKeys[$scope.images.curKeyIndex]]
    // var selectedImage = $scope.images.selectedImage;

    // if necessary load model
      if ($scope.images.selectedImage.gltfLoaded == false) {
        $scope.drawModel()
      }
      $scope.images.selectedImage.gltfModel.show = true

      $scope.imageMode = true

      cesiumService.flyToImage($scope.images.selectedImage, $scope.cesium)

    // Get not georeferenced images
      $scope.bbox = {
        left: $scope.images.selectedImage.longitude - 0.1,
        right: $scope.images.selectedImage.longitude + 0.1,
        bottom: $scope.images.selectedImage.latitude - 0.1,
        top: $scope.images.selectedImage.latitude + 0.1
      }
      var promise = imageQueryService.getNotGeolocatedBbox($scope.bbox)
      promise.then(function (res) {
        $scope.notGeoref = res.nImages
      })

      $scope.fillSlider()
    }

    $scope.nextImage = function () {
    // Increment index
      $scope.images.curKeyIndex += 1
      if ($scope.images.curKeyIndex >= $scope.images.dicKeys.length) {
        $scope.images.curKeyIndex = 0
      }
    // Select image
      $scope.images.selectedImage = $scope.images.dicImages[$scope.images.dicKeys[$scope.images.curKeyIndex]]

      $scope.flyToSelected()
    }

    $scope.prevImage = function () {
    // Increment index
      $scope.images.curKeyIndex -= 1
      if ($scope.images.curKeyIndex <= -1) {
        $scope.images.curKeyIndex = $scope.images.dicKeys.length - 1
      }
    // Select image
      $scope.images.selectedImage = $scope.images.dicImages[$scope.images.dicKeys[$scope.images.curKeyIndex]]

      $scope.flyToSelected()
    }

    $scope.moveUp = function () {
      cesiumService.moveUp($scope.cesium, $scope.cesium.terrainProvider)
    }
    $scope.moveLeft = function () {
      cesiumService.moveLeft($scope.cesium)
    }
    $scope.moveRight = function () {
      cesiumService.moveRight($scope.cesium)
    }
    $scope.moveDown = function () {
      cesiumService.moveDown($scope.cesium)
    }
    $scope.moveFront = function () {
      cesiumService.moveFront($scope.cesium)
    }
    $scope.moveBack = function () {
      cesiumService.moveBack($scope.cesium)
    }
    $scope.turn = function (direction) {
    // Hide toponyms and stories
      var topoWasTrue = false
      if ($scope.toponyms.show) {
        topoWasTrue = true
        $scope.toponyms.show = false
        topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)
      }
      var storyWasTrue = false
      if ($scope.stories.show) {
        storyWasTrue = true
        $scope.stories.show = false
        storyService.deleteStories($scope.stories.dicStory, $scope.cesium)
      }
    // Perform rotation
      cesiumService.turn($scope.cesium, direction).then(function () {
      // Redraw
        if (topoWasTrue) {
          topoService.drawToponyms($scope.toponyms.geoJsonData, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
            $scope.toponyms.list = response.icons
            $scope.toponyms.geoJsonData = response.geoJsonData
            $scope.toponyms.show = true
          })
        }
      // Redraw
        if (storyWasTrue) {
          storyService.drawStories($scope.stories.dicStory, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
            $scope.stories.dicStoryReady = response
            $scope.stories.show = true
          })
        }
      })
    }

    $scope.zoom = function (direction) {
    // Hide toponyms and stories
      var topoWasTrue = false
      if ($scope.toponyms.show) {
        topoWasTrue = true
        $scope.toponyms.show = false
        topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)
      }
      var storyWasTrue = false
      if ($scope.stories.show) {
        storyWasTrue = true
        $scope.stories.show = false
        storyService.deleteStories($scope.stories.dicStory, $scope.cesium)
      }
      cesiumService.zoom($scope.cesium, direction)
    // Redraw
      if (topoWasTrue) {
        topoService.drawToponyms($scope.toponyms.geoJsonData, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
          $scope.toponyms.list = response.icons
          $scope.toponyms.geoJsonData = response.geoJsonData
          $scope.toponyms.show = true
        })
      }
    // Redraw
      if (storyWasTrue) {
        storyService.drawStories($scope.stories.dicStory, $scope.cesium, $scope.tilesLoadedPromise).then(function (response) {
          $scope.stories.dicStoryReady = response
          $scope.stories.show = true
        })
      }
    }
    $scope.zoomOut = function () {
    // Hide toponyms and stories
      $scope.toponyms.show = false
      $scope.stories.show = false
      storyService.deleteStories($scope.stories.dicStory, $scope.cesium)
      topoService.deleteToponyms($scope.toponyms.list, $scope.cesium)

      cesiumService.zoomOut($scope.cesium)
    }

    $scope.shareFb = function () {
      FB.ui(
        {
          method: 'share',
          href: 'http://smapshot.heig-vd.ch/mono/?imageId=' + $scope.images.selectedImage.id
        }, function (response) {
        console.log('response fb', response)
      })
    }

    $scope.rateGeoloc = function () {
      var index = $scope.rateArray.geoloc.indexOf($scope.images.selectedImage.id)
      if (index == -1) {
      // User didnt rate this image
      // --------------------------
      // rate
        var url = '/rate/geoloc/'
        var sendData = {
          imageId: $scope.images.selectedImage.id,
          rate: $scope.images.selectedImage.geolocRate
        }
        var postRatePromise = $http.post(url, sendData)
      .then(function () {
        // Update array
        $scope.rateArray.geoloc.push($scope.images.selectedImage.id)
      })
      }
    }

    $scope.rateImage = function () {
      var index = $scope.rateArray.image.indexOf($scope.images.selectedImage.id)
      if (index == -1) {
      // User didnt rate this image
      // --------------------------
      // rate
        var url = '/rate/image/'
        var sendData = {
          imageId: $scope.images.selectedImage.id,
          rate: $scope.images.selectedImage.imageRate
        }
        var postRatePromise = $http.post(url, sendData)
      .then(function () {
        // Update array
        $scope.rateArray.image.push($scope.images.selectedImage.id)
      })
      }
    }
  }])
