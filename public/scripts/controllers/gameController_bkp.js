gameApp.controller('gameCtrl', [ '$scope', '$http', '$uibModal', '$window', '$q',
  '$location', 'leafletData', 'leafletBoundsHelpers', 'leafletMarkerEvents', 'leafletMapEvents',
  function ($scope, $http, $uibModal, $window, $q, $location, leafletData, leafletBoundsHelpers,
  leafletMarkerEvents, leafletMapEvents) {
    angular.element($window).bind('resize', function () {
      $scope.$apply(function () {
        if ($scope.poseComputed == false) {
        // Leaflet sizes
          $scope.leafWidth = parseInt($window.innerWidth / 2.1)
          $scope.leafHeight = parseInt($window.innerHeight * 3 / 5)
          if ($scope.leafHeight > $scope.leafWidth) {
            $scope.leafHeight = $scope.leafWidth
          }
        // Openseadragon size
          $scope.osdWidth = $scope.leafWidth
          $scope.osdHeight = $scope.leafHeight// parseInt($scope.leafHeight*2/3);
          $scope.osdStyle = {
            'height': String($scope.osdHeight).concat('px'),
            'width': String($scope.osdWidth).concat('px')
          }
        } else {
        // Leaflet sizes
          $scope.leafWidth = parseInt($window.innerWidth / 3.1)
          if ($scope.leafHeight > $scope.leafWidth) {
            $scope.leafHeight = $scope.leafWidth
          }
        // Openseadragon size
          $scope.osdWidth = $scope.leafWidth
          $scope.osdStyle = {
            'height': String($scope.osdHeight).concat('px'),
            'width': String($scope.osdWidth).concat('px')
          }
        };
      })
    })

    $scope.openInitial = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'initial.html',
        bindToController: true,
        controller: 'initialCtrl'
      })
    // Promise
    // modalInstance.close.then(function () {
    //   //Reinitialize find
    //   $scope.gameSteps.find = false
    //   //Update step
    //   $scope.gameSteps.curStep = $scope.gameSteps.list[3];
    //   //Update score
    //   //$scope.score.final = $scope.score.final + $scope.score.partial;
    //   $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
    //   //Show only selected cell
    //   $scope.geojson.grid.data = $scope.selectedCell;
    // }, function () {
    //   $log.info('Modal dismissed at: ' + new Date());
    // });
    }
    $scope.openInitial()

  // Get a random georeferenced image from database
    $scope.getRandomImage = function () {
      $http.get('/game/randomImage/')
      .success(function (data) {
        $scope.image = {
          lat: data.latitude,
          lng: data.longitude,
          height: data.height,
          tilt: data.tilt,
          heading: data.azimuth,
          pitch: data.roll,
          title: data.title,
          id: data.id
        }

        $scope.mysd.viewer.open('../images/tiles/'.concat($scope.image.id).concat('.dzi'))

        // //updateOverlay
        // var ov = $scope.mysd.viewer.getOverlayById('overlay');
        // console.log('ov', ov)
        //
        // var ration = $scope.mysd.Viewport.getAspectRatio();
        // console.log('ratio', ratio)

        // var overlay = new Overlay({
        //   id: 'rectangle-overlay',
        //   x: 0.,
        //   y: 0.9,
        //   width: 1,
        //   height: 0.1,
        // })
        // var img = document.createElement("img");
        // var rectangle = new Rect(0, 0.9, 1, 0.1)
        // var ov = $scope.mysd.viewer.addOverlay(overlay);

        // $scope.mysd.viewer.addOverlay();

        // Create ground truth marker
        // var marker = {
        //  lat: $scope.image.lat,
        //  lng: $scope.image.lng,
        //  draggable: false
        // };
        // $scope.markers = new Array();
        // $scope.markers.push(marker);
      })
      .error(function (error) {
        console.log('Error in database loading: ' + error)
      })
    }
    $scope.getRandomImage()

  // Get the url parameters
  // ----------------------
    $scope.collectionId = $location.search().collectionId
    $scope.imageId = $location.search().imageId
    console.log('collectionId', $scope.collectionId)
    console.log('imageId', $scope.imageId)

  // Customization
  // -------------
  // Leaflet sizes
    $scope.leafWidth = parseInt(window.innerWidth / 2.1)
    $scope.leafHeight = parseInt(window.innerHeight * 3 / 5)
  // Openseadragon size
    $scope.osdWidth = $scope.leafWidth
    $scope.osdHeight = $scope.leafHeight// parseInt($scope.leafHeight*2/3);
    $scope.osdStyle = {
      'height': String($scope.osdHeight).concat('px'),
      'width': String($scope.osdWidth).concat('px')
    }
    $scope.widgetSize = 'col-md-6 col-lg-6'
  // Message Customization
    $scope.cssMessage = {
      neutral: 'alert-info', // 'message-neutral';
      positive: 'alert-success', // 'message-neutral';
      negative: 'alert-danger', // 'message-neutral';
      current: 'alert-info'
    }
    $scope.isCollapsed = true

  // Initialize OpenSeadragon
    $scope.optionsViewer = {
    // tileSources: ["../images/tiles/1.dzi"], //Fake image
      prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
      gestureSettingsMouse: {
        clickToZoom: false,
        dblClickToZoom: false
      },
      showNavigator: false,
      showHomeControl: false,
      showFullPageControl: true,
      showZoomControl: false,
      gcpIconSelected: '../icons/arrow_red.png',
      gcpIcon: '../icons/arrow_red.png'
    }
  // Cesium show or hide
    $scope.cesiumBool = false
    $scope.osdBool = true
    $scope.leafletBool = true
    $scope.clueBool = true
    $scope.poseComputed = false

  // Cesium initial position
    $scope.providedCamera = {
      latitude: 46.22015, // null,
      longitude: 7.48232, // null
      height: null,
      heading: 30, // null
      tilt: null,
      roll: null,
      fov: null
    }
    $scope.computedCamera = {
      latitude: null,
      longitude: null,
      height: null,
      heading: null,
      tilt: null,
      roll: null,
      fov: null
    }

  // Initialise the GCP model
    $scope.myGCPs = {
      curGCPid: 0,
      selectedGCPid: null,
      dictGCPs: {},
      file: null,
      fullBool: false
    }
  // Array of gcp is sync with the dictonary of gcp
    $scope.gcpArray = []
  // if the dictonary is modified, the gcp array is updated
    $scope.$watch('myGCPs.dictGCPs', function () {
      $scope.dictToArray()
    }, true)
  // Reshape the dictionary in array
    $scope.dictToArray = function () {
      var gcpArray = []
      var nGCP = 0
      for (var id in $scope.myGCPs.dictGCPs) {
        var curGCP = $scope.myGCPs.dictGCPs[id]
        if ((curGCP.x != null) && (curGCP.X != null)) {
          gcpArray.push(curGCP)
          nGCP += 1
        }
      }
      $scope.gcpArray = gcpArray
      if (nGCP == 5) {
        $scope.myGCPs.fullBool = true
        $scope.computePose()
      }
    }

  // Set leaflet
  // -----------
  // Set leaflet baselayer
    $scope.map = {
      defaults: {
        crs: new L.Proj.CRS('EPSG:21781', '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 ' + '+k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs', {
          resolutions: [4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 250, 100, 50, 20, 10, 5, 2.5, 2, 1.5, 1, 0.5],
          origin: [420000, 350000]
        }),
        continuousWorld: true
        // zoomControl: false,
        // scrollWheelZoom: false
      },
      layers: {
        baselayers: {
          swisstopo: {
            name: 'Swisstopo',
            type: 'xyz',
            url: 'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/21781/{z}/{y}/{x}.jpeg',
                // visible: true,
            layerOptions: {
              attribution: 'Map data &copy; 2015 swisstopo',
              maxZoom: 26,
              minZoom: 0,
              showOnSelector: false
            }
          }
        }
      }
    // overlays:{}
    }
  // $scope.layers = {
  //   baselayers: {
  //     swisstopo: {
  //       name: 'Swisstopo',
  //       //url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  //       url: 'https://wmts20.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/3857/{z}/{x}/{y}.jpeg',
  //       type: 'xyz',
  //       layerOptions: {
  //         attribution: 'Map data &copy; 2015 swisstopo'
  //       }
  //     },
  //   }
  // };

  // Set geojson layer
    $scope.markers = new Array()
    $scope.geojson = {}
    $scope.buffer = {
      name: 'Buffer',
      type: 'geoJSONShape',
      data: {},
      visible: false,
      style: {
        color: 'black',
        fillColor: 'green',
        weight: 4.0,
        opacity: 1,
        fillOpacity: 0.5
      }
    }
  // Set path for direction
    $scope.paths = {}
  // Set selected canton
    $scope.selectedCanton = {}

  // Create icons
    var local_icons = {
      default_icon: {},
      red_icon: {
        iconUrl: '../icons/camera_red.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
      },
      green_icon: {
        iconUrl: '../icons/camera_green.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
      }
    }
  // Get geojsons
    $.getJSON('../geodata/grid.geojson', function (json) {
    // $scope.grid = json;
      $scope.grid = {
        name: 'Grid',
        type: 'geoJSONShape',
        data: json,
      // visible: false,
        style: {
          color: 'black',
          fillColor: 'yellow',
          weight: 4.0,
          opacity: 1,
          fillOpacity: 0.1
        }
      }
    // console.log('$scope.geojson.grid', $scope.geojson.grid)
    })
    $.getJSON('../geodata/cantons.geojson', function (json) {
      $scope.geojson.cantons = {
        name: 'Cantons',
        type: 'geoJSONShape',
        data: json,
        visible: true,
        style: {
          color: 'black',
          fillColor: 'yellow',
          weight: 4.0,
          opacity: 1,
          fillOpacity: 0.1
        }
      }
    })

  // Game
  // ----
    $scope.score = {
      final: 60,
      partial: 10,
      partialType: 'progress-green-light', // success',
      finalType: 'success'
    }
    $scope.$watch('score.partial', function () {
      if ($scope.score.partial >= 6) {
      // $scope.score.partialType = 'success'
        $scope.score.partialType = 'greenLight'
      } else if ($scope.score.partial >= 4) {
      // $scope.score.partialType = 'warning';
        $scope.score.partialType = 'orangeLight'
      } else {
      // $scope.score.partialType = 'danger'
        $scope.score.partialType = 'redLight'
      }
      $scope.score.remainingPoints = $scope.score.final - $scope.score.partial
    }, true)
    $scope.$watch('score.final', function () {
      if ($scope.score.final > 45) {
        $scope.score.finalType = 'success'
      } else if ($scope.score.final > 30) {
        $scope.score.finalType = 'warning'
      } else {
        $scope.score.finalType = 'danger'
      }
      $scope.score.remainingPoints = $scope.score.final - $scope.score.partial

      if ($scope.score.final > 50) {
        $scope.congratulation = 'Excellent!'
      } else if ($scope.score.final > 40) {
        $scope.congratulation = 'Bravo!'
      } else if ($scope.score.final > 30) {
        $scope.congratulation = 'Bien!'
      } else if ($scope.score.final > 20) {
        $scope.congratulation = 'Oulàlà.'
      } else {
        $scope.congratulation = ''
      }
    }, true)
  // $scope.$watch('score', function() {
  //   $scope.stacked = [];
  //   //Create bar for the final
  //   bar = {
  //     value: ($scope.score.final-$scope.score.partial)/60*100,
  //     type: 'info',
  //     point: $scope.score.final
  //   };
  //   //Create bar for the partial
  //   // if (bar.value>45){
  //   //   bar.type = 'success'
  //   // }else if (bar.value>30) {
  //   //   bar.type = 'warning'
  //   // }
  //   // else {
  //   //   bar.type = 'danger'
  //   // };
  //   $scope.stacked.push(bar);
  //   //Create bar for the partial
  //   bar = {
  //     value: $scope.score.partial/60*100,
  //     type: '',
  //     point: $scope.score.partial
  //   };
  //   if ($scope.score.partial>=6){
  //     bar.type = 'success'
  //   }else if ($scope.score.partial>=5) {
  //     bar.type = 'warning'
  //   }
  //   else {
  //     bar.type = 'danger'
  //   };
  //   $scope.stacked.push(bar);
  // }, true);
  // Create game steps
    $scope.gameSteps = {}
  // Is current task solved
    $scope.gameSteps.find = false
    $scope.gameSteps.list = {
      1: {
        id: 1,
        task: 'Trouve le canton dans le quel la photographie a été prise.',
        messageSuccess: 'Bravo tu as trouvé le bon canton',
        messageFail: "Ce n'est pas le bon canton",
        message: '',
        scoreMax: 10,
        zoom: 17
      },
      2: {
        id: 2,
        task: 'Trouve la cellule dans la quelle la photographie a été prise.',
        messageSuccess: 'Bravo tu as trouvé la bonne cellule.',
        messageFail: "Ce n'est pas la bonne cellule.",
        message: '',
        scoreMax: 10,
        zoom: 19
      },
      3: {
        id: 3,
        task: 'Clique sur le lieu où la photographie a été prise.',
        messageSuccess: 'Bravo tu as trouvé un point qui est à moins de 1km de la vraie position.',
        messageFail: 'Trop loin.',
        message: '',
        scoreMax: 10,
        zoom: 19
      },
      4: {
        id: 4,
        task: "Trouve la direction de l'image",
        messageSuccess: 'Bravo tu as trouvé la bonne direction à 20 degrés près',
        messageFail: "Ce n'est pas la bonne direction.",
        message: '',
        scoreMax: 10,
        zoom: 19
      },
      5: {
        id: 5,
      // task: "Clique sur le lieu de la camera. Tu peux te déplacer dans le globe virtuel en maintenant les boutons de la souris appuyés).",
      // messageSuccess: "Bravo tu as trouvé un lieu qui est dans un rayon de 200m",
      // messageFail: "Trop loin"
        task: "Clique sur 5 points de correspondances (Sommets de montagne, croisement de routes, etc) pour calculer le lieu et la direction de l'appareil photo",
        messageSuccess: 'Bravo tu as trouvé la bonne direction à 20 degrés près',
        messageFail: "Ce n'est pas la bonne direction",
        message: '',
        scoreMax: 10
      }
    // 6: {
    //   id: 6,
    //   task: "Clique sur 3 points de correspondances (Sommets de montagne, croisement de routes, etc) pour trouver la direction de la caméra ",
    //   messageSuccess: "Bravo tu as trouvé la bonne direction à 20 degrés près",
    //   messageFail: "Ce n'est pas la bonne direction"
    // },
    }
    $scope.gameSteps.curStep = $scope.gameSteps.list[1]

  // Set map center
    $scope.center = {
      lat: 46.78099, // 46.47532,
      lng: 8.01153, // 7.13708,
      zoom: 15
    }

  // Function to compute azimuth
    XYtoAzimuth = function (XY1, XY2) {
      var dx = (XY2[0] - XY1[0])
      var dy = (XY2[1] - XY1[1])
      var alpha = 180.0 / Math.PI * Math.atan2(dx, dy)
      if (alpha < 0) {
        var alpha = alpha + 360
      }
      return alpha
    }

  // Map events
  // ----------
    var mouseMoveEvent = 'leafletDirectiveMap.mousemove'
    $scope.$on(mouseMoveEvent, function (event, args) {
      if ($scope.gameSteps.curStep.id == 4) {
      // Draw direction
        if ($scope.gameSteps.find == false) {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
        } else {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        }
      }
    })
    $scope.$on('leafletDirectiveMap.click', function (event, args) {
    // if the client is mobile (phone or tablet)
      if ($scope.gameSteps.curStep.id == 4) {
      // Check direction on click
        if ($scope.gameSteps.find == false) {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
        } else {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        }
      // Compare provided direction with real direction
        var GtAz = $scope.image.heading
        if (GtAz > 360) {
          var GtAz = GtAz - 360
        };
      // Transform coordinates
        var WGS84proj = 'EPSG:4326'
        var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
        var XYmarker = proj4(WGS84proj, ch1903plusproj, [$scope.lastMarker.lng, $scope.lastMarker.lat])
        var XYclic = proj4(WGS84proj, ch1903plusproj, [args.leafletEvent.latlng.lng, args.leafletEvent.latlng.lat])
        var provAz = XYtoAzimuth(XYmarker, XYclic)
      // Compute difference
        var deltaAz = Math.abs(GtAz - provAz)
        if (deltaAz > 180) {
          var deltaAz = 360 - deltaAz
        };
        $scope.distance = deltaAz.toFixed(1)
        if (deltaAz < 20) {
        // solution is found
          $scope.gameSteps.find = true
          $scope.sendMessage($scope.gameSteps.list[4].messageSuccess, $scope.cssMessage.positive)
        // $scope.message= $scope.gameSteps.list[4].messageSuccess;
        // $scope.cssMessage.current=$scope.cssMessage.positive;
        // Draw accepted direction
          var acceptedDirection = {
            color: 'green',
            weight: 8,
            latlngs: [
              { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
              { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
            ]
          }
          $scope.paths.push(acceptedDirection)
        // Record provided azimuth
          $scope.providedCamera.heading = provAz
        // Open modal
          $scope.openDirectionFound()
        } else {
          if ($scope.gameSteps.find == false) {
            $scope.sendMessage($scope.gameSteps.list[4].messageFail, $scope.cssMessage.negative)
          // $scope.message= $scope.gameSteps.list[4].messageFail;
          // $scope.cssMessage.current=$scope.cssMessage.negative;
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Draw wrong direction
            var falseDirection = {
              color: 'red',
              weight: 4,
              latlngs: [
                { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
                { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
              ]
            }
            $scope.paths.push(falseDirection)
          }
        }
      };
    })
    $scope.sendMessage = function (message, css) {
      $scope.isCollapsed = true
      $scope.messageStdBy = message
      $scope.cssMessageStdBy = css
    // $scope.isCollapsed = false;
    }
    $scope.openNextMessage = function () {
      $scope.isCollapsed = false
      $scope.message = $scope.messageStdBy
      $scope.cssMessage.current = $scope.cssMessageStdBy
    }
  // Click on a path layer
  // ---------------------
    $scope.$on('leafletDirectivePath.click', function (event, args) {
    // Check direction on click
      if ($scope.gameSteps.find == false) {
        $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
      } else {
        $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
      }
    // Compare provided direction with real direction
      var GtAz = $scope.image.heading
      if (GtAz > 360) {
        var GtAz = GtAz - 360
      };
    // Transform coordinates
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var XYmarker = proj4(WGS84proj, ch1903plusproj, [$scope.lastMarker.lng, $scope.lastMarker.lat])
      var XYclic = proj4(WGS84proj, ch1903plusproj, [args.leafletEvent.latlng.lng, args.leafletEvent.latlng.lat])
      var provAz = XYtoAzimuth(XYmarker, XYclic)
    // Compute difference
      var deltaAz = Math.abs(GtAz - provAz)
      if (deltaAz > 180) {
        var deltaAz = 360 - deltaAz
      }
      $scope.distance = deltaAz.toFixed(1)
      if (deltaAz < 20) {
      // solution is found
        $scope.gameSteps.find = true
        $scope.sendMessage($scope.gameSteps.list[4].messageSuccess, $scope.cssMessage.positive)
      // $scope.message= $scope.gameSteps.list[4].messageSuccess;
      // $scope.cssMessage.current=$scope.cssMessage.positive;
      // Draw accepted direction
        var acceptedDirection = {
          color: 'green',
          weight: 8,
          latlngs: [
            { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
            { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
          ]
        }
        $scope.paths.push(acceptedDirection)
      // Record provided azimuth
        $scope.providedCamera.heading = provAz
      // Open modal
        $scope.openDirectionFound()
      } else {
        if ($scope.gameSteps.find == false) {
          $scope.sendMessage($scope.gameSteps.list[4].messageFail, $scope.cssMessage.negative)
        // $scope.message= $scope.gameSteps.list[4].messageFail;
        // $scope.cssMessage.current=$scope.cssMessage.negative;
        // Update score
          if ($scope.score.partial - 1 >= 0) {
            $scope.score.partial = $scope.score.partial - 1
            $scope.score.final = $scope.score.final - 1
          };
        // Draw wrong direction
          var falseDirection = {
            color: 'red',
            weight: 4,
            latlngs: [
              { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
              { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
            ]
          }
          $scope.paths.push(falseDirection)
        }
      }
    })
  // Click on a geojson layer
  // ------------------------
    $scope.clipGridPromiseFunction = function (canton) {
      return $q(function (resolve, reject) {
      // Remove canton from leaflet
        $scope.geojson.cantons = {}
      // Use turf to clip the cells intersecting the canton
      // var canton = $scope.selectedCanton;
        var features = []
        $scope.grid.data.features.forEach(function (feature) {
          for (var id in canton.geometry.coordinates) {
            var cPoly = turf.polygon(canton.geometry.coordinates[id])
            var gPoly = turf.polygon(feature.geometry.coordinates)
            var intersection = turf.intersect(gPoly, cPoly)
            if (intersection) {
              if (intersection.geometry.type == 'Polygon') {
                var intersection = turf.multiPolygon([intersection.geometry.coordinates])
              }
              features.push(intersection)
            }
          }
        })
        var fc = turf.featureCollection(features)
      // Add to leaflet
      // $scope.grid.data.features = fc.features;
      // $scope.geojson.grid = $scope.grid;
        resolve(fc)
      })
    }
    $scope.$on('leafletDirectiveGeoJson.click', function (ev, leafletPayload) {
    // Click on a geojson object
      $scope.shapeClick(leafletPayload.leafletObject, leafletPayload.leafletEvent)
    })
  // Click on a shape file
    $scope.shapeClick = function (shape, event) {
    // If the solution is not solved
      if ($scope.gameSteps.find == false) {
      // Get clicked feature
        shape = shape.feature
      // Check the current game step
        if ($scope.gameSteps.curStep.id == 1) {
        // Select canton
        // -------------
        // Check if inside with turf
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var multiPoly = turf.multiPolygon(shape.geometry.coordinates)
          var isInside = turf.inside(pt, multiPoly)
        // if is inside
          if (isInside == true) {
          // solution is found
            $scope.gameSteps.find = true
          // $scope.isCollapsed=true;
          // $scope.isCollapse=false;
            $scope.sendMessage($scope.gameSteps.list[1].messageSuccess, $scope.cssMessage.positive)
          // $scope.message= $scope.gameSteps.list[1].messageSuccess;
          // $scope.cssMessage.current=$scope.cssMessage.positive;
          // Record the canton for the clip
            $scope.selectedCanton = shape
          // Change style of clicked objects
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: 'green', // '',
              fillOpacity: 0.5
            })
          // Zoom on the canton
            leafletData.getMap().then(function (map) {
              var centroidPt = turf.centroid($scope.selectedCanton.geometry)
              var latlng = L.latLng(centroidPt.geometry.coordinates[1], centroidPt.geometry.coordinates[0])
              map.setView(latlng, $scope.gameSteps.curStep.zoom)
            })

          // $scope.clipGridPromise = $scope.clipGridPromiseFunction($scope.selectedCanton);
          // $scope.clipGridPromise.then(function(res){
          //   $scope.geojson.cantons = {};
          //   $scope.grid.data.features = res.features;
          //   $scope.geojson.grid = $scope.grid;
          //   console.log('clip finished')
          //   //Zoom on the canton
          //   leafletData.getMap().then(function(map) {
          //     var centroidPt = turf.centroid(shape.geometry);
          //     var latlng = L.latLng(centroidPt.geometry.coordinates[1], centroidPt.geometry.coordinates[0]);
          //     map.setView(latlng, $scope.gameSteps.curStep.zoom)
          //   });
          // }, function(error){
          //   console.log('promise error', error)
          // });

          // //Remove canton from leaflet
          // $scope.geojson.cantons = {};
          // //Use turf to clip the cells intersecting the canton
          // var canton = $scope.selectedCanton;
          // var features = [];
          // $scope.grid.data.features.forEach(function(feature) {
          //   for (var id in canton.geometry.coordinates){
          //     var cPoly = turf.polygon(canton.geometry.coordinates[id])
          //     var gPoly = turf.polygon(feature.geometry.coordinates)
          //     var intersection = turf.intersect( gPoly, cPoly);
          //     if (intersection){
          //       if (intersection.geometry.type == 'Polygon'){
          //         var intersection = turf.multiPolygon([intersection.geometry.coordinates]);
          //       }
          //       features.push(intersection)
          //     }
          //   }
          // });
          // var fc = turf.featureCollection(features);
          // //Add to leaflet
          // $scope.grid.data.features = fc.features;
          // $scope.geojson.grid = $scope.grid;

          // Open modal
            $scope.openCantonFound()
          } else {
            $scope.sendMessage($scope.gameSteps.list[1].messageFail, $scope.cssMessage.negative)
          // $scope.message = $scope.gameSteps.list[1].messageFail;
          // $scope.cssMessage.current=$scope.cssMessage.negative;
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Change style of clicked objects
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: 'red',
              fillOpacity: 0.5
            })
          }
      // step 2 check if inside cell
        } else if ($scope.gameSteps.curStep.id == 2) {
        // Select cell
        // -----------
        // Check if inside with turf
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var poly = turf.multiPolygon(shape.geometry.coordinates)
          var isInside = turf.inside(pt, poly)
        // if is inside
          if (isInside == true) {
          // solution is found
            $scope.gameSteps.find = true
            $scope.sendMessage($scope.gameSteps.list[2].messageSuccess, $scope.cssMessage.positive)
          // $scope.message = $scope.gameSteps.list[2].messageSuccess;
          // $scope.cssMessage.current=$scope.cssMessage.positive;
          // Record cell
            $scope.selectedCell = shape
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: 'green',
              fillOpacity: 0.5
            })
          // Zoom on the cell
            leafletData.getMap().then(function (map) {
              var centroidPt = turf.centroid(shape.geometry)
              var latlng = L.latLng(centroidPt.geometry.coordinates[1], centroidPt.geometry.coordinates[0])
              map.setView(latlng, $scope.gameSteps.curStep.zoom)
            })
          // Open modal
            $scope.openCellFound()
          // Change leaflet style
          } else {
            $scope.sendMessage($scope.gameSteps.list[2].messageFail, $scope.cssMessage.negative)
          // $scope.message= $scope.gameSteps.list[2].messageFail;
          // $scope.cssMessage.current=$scope.cssMessage.negative;
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Change leaflet style
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: 'red',
              fillOpacity: 0.5
            })
          }
      // step 3: test locations
        } else if ($scope.gameSteps.curStep.id == 3) {
        // Get click location
          loc = event.latlng
        // Check distance
          var fromPt = turf.point([ $scope.image.lng, $scope.image.lat])
          var toPt = turf.point([ loc.lng, loc.lat])
          var dist = turf.distance(fromPt, toPt) // KM
          var limitDist = 1
          if (dist < limitDist) {
          // solution is found
            $scope.gameSteps.find = true
            $scope.message = $scope.gameSteps.list[3].messageSuccess
            $scope.cssMessage.current = $scope.cssMessage.positive
          // Stores location
            $scope.providedCamera.longitude = loc.lng
            $scope.providedCamera.latitude = loc.lat
            $scope.distance = dist.toFixed(1)
          // Draw leaflet marker
            marker = {
              lat: loc.lat,
              lng: loc.lng,
              icon: local_icons.green_icon
            }
            $scope.lastMarker = marker
            leafletData.getMap().then(function (map) {
              var latlng = L.latLng(loc.lat, loc.lng)
              map.setView(latlng, $scope.gameSteps.curStep.zoom)
            })
          // Open modal
            $scope.openLocationFound()
          } else {
            $scope.sendMessage($scope.gameSteps.list[3].messageFail, $scope.cssMessage.negative)
          // $scope.message = $scope.gameSteps.list[3].messageFail;
          // $scope.cssMessage.current=$scope.cssMessage.negative;
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // console.log('$scope.score.partial',$scope.score.partial)
          // if ($scope.score.partial === 9){
          //   //Draw leaflet marker
          //   marker = {
          //     lat: loc.lat,
          //     lng: loc.lng,
          //     icon: local_icons.red_icon,
          //     message: "Ce n'est pas le bon endroit, essaie encore",
          //     focus: true
          //   }
          //
          // }else{
          // Draw leaflet marker
            marker = {
              lat: loc.lat,
              lng: loc.lng,
              icon: local_icons.red_icon
            }
          // }
          }
          $scope.markers.push(marker)
        }
      }
    }

  // Click on nextStep Button
  // $scope.nextStep = function(){
  //   console.log('next')
  //   //Reinitialize find
  //   $scope.gameSteps.find = false
  //   $scope.message = "";
  //   $scope.cssMessage.current = $scope.cssMessage.neutral;
  //   //Goes to cell step
  //   if ($scope.gameSteps.curStep.id == 1){
  //     //Update step
  //     $scope.gameSteps.curStep = $scope.gameSteps.list[2];
  //     //Update score
  //     //$scope.score.final = $scope.score.final + $scope.score.partial;
  //     $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
  //     //Remove canton from leaflet
  //     $scope.geojson.cantons = {};
  //     //Use turf to clip the cells intersecting the canton
  //     var canton = $scope.selectedCanton;
  //     var features = [];
  //     $scope.grid.data.features.forEach(function(feature) {
  //       for (var id in canton.geometry.coordinates){
  //         var cPoly = turf.polygon(canton.geometry.coordinates[id])
  //         var gPoly = turf.polygon(feature.geometry.coordinates)
  //         var intersection = turf.intersect( gPoly, cPoly);
  //         if (intersection){
  //           if (intersection.geometry.type == 'Polygon'){
  //             var intersection = turf.multiPolygon([intersection.geometry.coordinates]);
  //           }
  //           features.push(intersection)
  //         }
  //       }
  //     });
  //     var fc = turf.featureCollection(features);
  //     //Add to leaflet
  //     $scope.grid.data.features = fc.features;
  //     $scope.geojson.grid = $scope.grid;
  //   //Goes to location step
  //   }else if ($scope.gameSteps.curStep.id == 2){
  //     //Update step
  //     $scope.gameSteps.curStep = $scope.gameSteps.list[3];
  //     //Update score
  //     //$scope.score.final = $scope.score.final + $scope.score.partial;
  //     $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
  //     //Show only selected cell
  //     $scope.geojson.grid.data = $scope.selectedCell;
  //   //Goes to direction step
  //   }else if ($scope.gameSteps.curStep.id == 3){
  //     $scope.gameSteps.curStep = $scope.gameSteps.list[4];
  //     //Update score
  //     //$scope.score.final = $scope.score.final + $scope.score.partial;
  //     $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
  //     $scope.markers = new Array();
  //     //Draw marker
  //     markerGT = {
  //       lat: $scope.image.lat,
  //       lng: $scope.image.lng,
  //       draggable: false
  //     }
  //     $scope.markers.push(markerGT);
  //     $scope.markers.push($scope.lastMarker);
  //     //Create path for direction
  //     $scope.paths = new Array();
  //     var direction = {
  //       color: 'red',
  //       weight: 8,
  //       latlngs: [
  //           { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
  //           { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng-1},
  //       ],
  //     };
  //     $scope.paths.push(direction);
  //   //Goes to cesium
  //   }else if($scope.gameSteps.curStep.id == 4){
  //     $scope.gameSteps.curStep = $scope.gameSteps.list[5];
  //     //Hide leaflet, show cesium
  //     $scope.cesiumBool = true;
  //     $scope.leafletBool = false;
  //     //Update score
  //     //$scope.score.final = $scope.score.final + $scope.score.partial;
  //     $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
  //     //Set camera
  //     var position = [Cesium.Cartographic.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude)];
  //     //Get camera altitude
  //     var promise = Cesium.sampleTerrain($scope.cesium.terrainProvider, 11, position);
  //     Cesium.when(promise, function(updatedPosition) {
  //       $scope.providedCamera.altitude=updatedPosition[0].height+100;
  //       $scope.relaxCollisionDetection();
  //       //Set camera location
  //       var camera = $scope.cesium.camera;
  //       camera.setView({
  //           destination : Cesium.Cartesian3.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude, $scope.providedCamera.altitude),
  //           orientation: {
  //               heading : Cesium.Math.toRadians($scope.providedCamera.heading),
  //               pitch : 0,
  //               roll : 0
  //           },
  //       });
  //     })
  //     //Create empty GCP
  //     var GCP = {
  //       id: $scope.myGCPs.curGCPid,
  //       x: null,
  //       y: null,
  //       X: null,
  //       Y: null,
  //       Z: null,
  //       eM:null,
  //       ePix:null,
  //     };
  //     //Push it in the dictionary
  //     $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP;
  //     //Select the current GCP
  //     $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid;
  //     //increment GCP identifier
  //     //$scope.myGCPs.curGCPid = $scope.myGCPs.curGCPid+1;
  //     //Fix camera location
  //     var scene =$scope.cesium.scene;
  //     scene.screenSpaceCameraController.enableRotate = false;
  //     scene.screenSpaceCameraController.enableTranslate = false;
  //     scene.screenSpaceCameraController.enableTilt = false;
  //     scene.screenSpaceCameraController.enableZoom = false;
  //   }//if step
  // };//end function
  // Reinitialize camera
    $scope.reinitializeCamera = function () {
    // Set camera location
      var camera = $scope.cesium.camera
      camera.frustum.fov = Cesium.Math.PI_OVER_THREE
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude, $scope.providedCamera.altitude),
        orientation: {
          heading: Cesium.Math.toRadians($scope.providedCamera.heading),
          pitch: 0,
          roll: 0
        }
      })
    }
    $scope.changeCameraPosition = function () {
    // Set camera location
      var camera = $scope.cesium.camera
      camera.frustum.fov = Cesium.Math.PI_OVER_THREE
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees($scope.image.lng, $scope.image.lat, $scope.image.height + 20),
        orientation: {
          heading: Cesium.Math.toRadians($scope.providedCamera.heading),
          pitch: 0,
          roll: 0
        }
      })
    // $scope.score.partial = $scope.score.partial-5;
      $scope.score.final = $scope.score.final - 5
    }
  // Draw all GCP
    $scope.drawGCP = function () {
      for (var i in $scope.myGCPs.dictGCPs) {
        var gcp = $scope.myGCPs.dictGCPs[i]
        $scope.drawOneGCP(gcp)
      }
    }
  // Draw One GCP
    $scope.drawOneGCP = function (gcp) {
    // Draw openseadragon
      var img = document.createElement('img')
      img.src = $scope.options.gcpIcon
      img.id = gcp.id
      img.isReproj = false
      var viewportPoint = $scope.mysd.viewer.viewport.imageToViewportCoordinates(gcp.x, gcp.y)
      var ov = $scope.mysd.viewer.addOverlay(img, viewportPoint, 'CENTER')
    // $scope.mysd.viewer.forceRedraw();
    // $scope.mysd.viewer.viewport.update();
      var bounds = $scope.mysd.viewer.viewport.getBounds()
    // $scope.mysd.viewer.viewport.fitBounds(bounds);

    // Draw cesium
      var WGS84proj = 'EPSG:4326'
    // var ch1903proj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs ';
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var longLat = proj4(ch1903plusproj, WGS84proj, [gcp.X, gcp.Y])
      var cartesian = Cesium.Cartesian3.fromDegrees(longLat[0], longLat[1], gcp.Z + 1)
      var curEntity = $scope.cesium.entities.add({
        id: (gcp.id).toString(),
        position: cartesian,
        point: {
          pixelSize: 5,
          outlineColor: Cesium.Color.WHITE,
          outlineWidth: 2
        },
        label: {
          show: true,
          text: (gcp.id).toString(), // ($scope.myGCPs.curGCPid).toString(),
          font: '14pt monospace',
          style: Cesium.LabelStyle.FILL_AND_OUTLINE,
          outlineWidth: 2,
          verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
          pixelOffset: new Cesium.Cartesian2(0, -9)
        }
      })
      curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.BLACK)
    }
  // Create new GCP
    $scope.CreateNewGCP = function () {
    // increment GCP identifier
      $scope.myGCPs.curGCPid = $scope.myGCPs.curGCPid + 1
    // Create empty GCP
      var GCP = {
        id: $scope.myGCPs.curGCPid,
        x: null,
        y: null,
        X: null,
        Y: null,
        Z: null,
        eM: null,
        ePix: null
      }
    // Push it in the dictionary
      $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
    // Select the current GCP
      $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
    }
    $scope.cesiumGCP = function (XYZ, cartesian) {
      if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X == null) {
      // Push correspondance in the dictionnary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X = XYZ[0]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Y = XYZ[1]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Z = XYZ[2]

      // Draw cesium point
        var curEntity = $scope.cesium.entities.add({
          id: ($scope.myGCPs.curGCPid).toString(),
          position: cartesian,
          point: {
            pixelSize: 5,
            outlineColor: Cesium.Color.WHITE,
            outlineWidth: 2
          },
          label: {
            show: true,
            text: ($scope.myGCPs.curGCPid + 1).toString(),
            font: '14pt monospace',
            style: Cesium.LabelStyle.FILL_AND_OUTLINE,
            outlineWidth: 2,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            pixelOffset: new Cesium.Cartesian2(0, -9)
          }
        })
        curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.RED)

      // Check if GCP is finished
        if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x != null) {
          $scope.CreateNewGCP()
        }
      } else {
        alert("Tu dois cliquer dans le point correspondant de l'image.")
      };
    }
    $scope.getClue = function () {
    // Hide clue button
      $scope.clueBool = false
    // Remove overlay
      $scope.mysd.viewer.removeOverlay('overlay')
    // Decrease score
      $scope.score.final = $scope.score.final - 10
    // show message
      $scope.sendMessage($scope.image.title, $scope.cssMessage.positive)
    // $scope.message = $scope.image.title;
    // $scope.cssMessage.current=$scope.cssMessage.positive;
    }
    $scope.osdGCP = function (xy, viewportPoint) {
      if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x == null) {
      // Push correspondance in the dictionnary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x = xy[0]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].y = xy[1]
      // Draw osd point
        var img = document.createElement('img')
        img.src = $scope.optionsViewer.gcpIcon
        img.id = $scope.myGCPs.curGCPid.toString()
        img.isReproj = false
        img.className = 'gcp'
      // var viewportPoint = $scope.mysd.viewer.TiledImage.imageToViewportCoordinates(xy[0], xy[1]);
        $scope.mysd.viewer.forceRedraw()
        var ov = $scope.mysd.viewer.addOverlay(img, viewportPoint, 'CENTER')// CENTER, var ov =
      // var zoom = $scope.mysd.viewer.viewport.getZoom();
      // console.log('zoom', zoom)
      // $scope.mysd.viewer.viewport.goHome();
      // var ov = $scope.mysd.viewer.getOverlayById(img.id);
      // $scope.mysd.viewer.updateOverlay(img.id, viewportPoint,'CENTER')
      // console.log('ov', ov, 'vp',viewportPoint, 'img',img)
      // ov.adjust(viewportPoint);
      // ov.update(viewportPoint, 'CENTER');
      // $scope.mysd.viewer.forceRedraw();
      // $scope.mysd.viewer.viewport.update();
      // console.log('pia')
      // var bounds = $scope.mysd.viewer.viewport.getBounds();
      // $scope.mysd.viewer.viewport.fitBounds(bounds);

        var label = document.createElement('p')
        label.id = $scope.myGCPs.curGCPid + '_label'
        label.innerHTML = ($scope.myGCPs.curGCPid + 1).toString()
        label.className = 'label'
      // var viewportPoint = $scope.mysd.viewer.TiledImage.imageToViewportCoordinates(xy[0], xy[1]);
        var ov = $scope.mysd.viewer.addOverlay(label, viewportPoint, 'CENTER')

      // Check if GCP is finished
        if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X != null) {
          console.log('create new from osd')
          $scope.CreateNewGCP()
        }
      } else {
        alert('Tu dois cliquer dans le point correspondant du globe virtuel.')
      };
    }
    $scope.setCollisionDetection = function () {
      var scene = $scope.cesium.scene
      scene.screenSpaceCameraController.enableCollisionDetection = true
      scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000
    }
    $scope.relaxCollisionDetection = function () {
      var scene = $scope.cesium.scene
      scene.screenSpaceCameraController.enableCollisionDetection = false
    }
    $scope.moveLeafLeft = function () {
      leafletData.getMap().then(function (map) {
        var vector = L.point(-$scope.leafWidth / 10, 0)
        map.panBy(vector)
      })
    }
    $scope.moveLeafRight = function () {
      leafletData.getMap().then(function (map) {
        var vector = L.point($scope.leafWidth / 10, 0)
        map.panBy(vector)
      })
    }
    $scope.moveLeafUp = function () {
      leafletData.getMap().then(function (map) {
        var vector = L.point(0, -$scope.leafHeight / 10)
        map.panBy(vector)
      })
    }
    $scope.moveLeafDown = function () {
      leafletData.getMap().then(function (map) {
        var vector = L.point(0, $scope.leafHeight / 10)
        map.panBy(vector)
      })
    }
    $scope.moveLeft = function () {
      var camera = $scope.cesium.camera
      camera.flyTo({
        destination: camera.position,
        orientation: {
          heading: camera.heading - Cesium.Math.PI / 20,
          pitch: camera.pitch,
          roll: camera.roll
        },
        duration: 0.25
      })
    }
    $scope.moveRight = function () {
      var camera = $scope.cesium.camera
      camera.flyTo({
        destination: camera.position,
        orientation: {
          heading: camera.heading + Cesium.Math.PI / 20,
          pitch: camera.pitch,
          roll: camera.roll
        },
        duration: 0.25
      })
    }
    $scope.moveUp = function () {
      var camera = $scope.cesium.camera
      if (camera.pitch < Cesium.Math.PI / 2 - 2 * Cesium.Math.PI / 20) {
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch + Cesium.Math.PI / 20,
            roll: camera.roll
          },
          duration: 0.25
        })
      }
    }
    $scope.moveDown = function () {
      var camera = $scope.cesium.camera
      if (camera.pitch > -Cesium.Math.PI / 2 + 2 * Cesium.Math.PI / 20) {
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch - Cesium.Math.PI / 20,
            roll: camera.roll
          },
          duration: 0.25
        })
      }
    }
    $scope.zoomIn = function () {
      var camera = $scope.cesium.camera
      var fov = camera.frustum.fov
      if (fov > Cesium.Math.PI / 20) {
        camera.frustum.fov = fov - 0.05
      };
    }
    $scope.zoomOut = function () {
      var camera = $scope.cesium.camera
      var fov = camera.frustum.fov
      if (fov < Cesium.Math.PI / 3) {
        camera.frustum.fov = fov + 0.05
      };
    }
    $scope.computePose = function () {
    // Get virtual camera parameters
    // -----------------------------
    // Get camera location and orientation
      var camCartesian = $scope.cesium.scene.camera.position
      var camCarto = Cesium.Cartographic.fromCartesian(camCartesian)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)

    // Get camera projected coordinates
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var projCoord = proj4(WGS84proj, ch1903plusproj, [camLong, camLat])
      var trueXY = proj4(WGS84proj, ch1903plusproj, [$scope.image.lng, $scope.image.lat])

      var camXYZ = [projCoord[0], projCoord[1], camCarto.height]
    // var camXYZ = [parseFloat($scope.providedCamera.X), parseFloat($scope.providedCamera.Y), parseFloat($scope.providedCamera.Z)];
      var camPitch = 0// Cesium.Math.toDegrees($scope.cesium.scene.camera.pitch);
      var camHeading = $scope.providedCamera.heading// Cesium.Math.toDegrees($scope.cesium.scene.camera.heading);
      var camRoll = 0// Cesium.Math.toDegrees($scope.cesium.scene.camera.roll);

    // Set apriori parameters
    // ----------------------
      var imageWidth = $scope.mysd.viewer.source.Image.Size.Width
      var imageHeight = $scope.mysd.viewer.source.Image.Size.Height
    // focal in pixel, set to a normal focal: focal = diagonal
      var f = Math.sqrt(Math.pow(imageWidth, 2) + Math.pow(imageHeight, 2))
    // Principal point
      var cx = imageWidth / 2.0 // half column size
      var cy = imageHeight / 2.0 // half height size
    // location of the camera
      var X0 = camXYZ[0]
      var Y0 = camXYZ[1]
      var Z0 = camXYZ[2]
    // Camera angles
      angles = cesium_to_LM_angles(camHeading, camPitch, camRoll)
      var az = angles[0]// 203;//angles[0];
      var tilt = angles[1]
      var roll = angles[2]

    // Generate input of the LM algorithm
      var pApriori = [X0, Y0, Z0, az, tilt, roll, f, cx, cy] // Vector of the camera orientation
    // The orientation and focal are optimized
      var boolUnknowns = [true, true, true, true, true, true, true, false, false]
    // Get GCPs
      var GCPs = $scope.gcpArray
    // Compute camera orientation
      var p = estimatePoseLM(pApriori, boolUnknowns, GCPs)
      $scope.p = p

    // Check altitude
    // --------------
    // Coordinate transformation
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var longLat = proj4(ch1903plusproj, WGS84proj, [p[0], p[1]])

      var position = [Cesium.Cartographic.fromDegrees(longLat[0], longLat[1])]
      var promise = Cesium.sampleTerrain($scope.cesium.terrainProvider, 11, position)
      Cesium.when(promise, function (updatedPosition) {
        var demZ = updatedPosition[0].height + 5
        var compZ = $scope.p[2]
        if (compZ < demZ) {
        // Generate input of the LM algorithm
          $scope.p[2] = demZ
          var boolUnknowns = [false, false, false, true, true, true, true, false, false]
        // Compute camera orientation force heiht
        // --------------------------
          $scope.p = estimatePoseLM($scope.p, boolUnknowns, GCPs)
        }

      // Compute error for the game
      // --------------------------
        Xcomputed = $scope.p[0]
        Ycomputed = $scope.p[1]
        Xtrue = trueXY[0]
        Ytrue = trueXY[1]
        dist = Math.sqrt((Xcomputed - Xtrue) * (Xcomputed - Xtrue) + (Ycomputed - Ytrue) * (Ycomputed - Ytrue))
        $scope.distance = dist.toFixed(0)
      // Compute p
        maxDist = 500
        maxScore = $scope.gameSteps.curStep.scoreMax
        score = Math.round(-maxScore / maxDist * dist + maxScore)
        if (score < 0) {
          score = 0
        }
      // Update score
        $scope.score.partial = score
        $scope.score.distancePenalty = $scope.gameSteps.curStep.scoreMax - score
        $scope.score.final = $scope.score.final + (-maxScore + score)
        if ($scope.score.final < 0) {
          $scope.score.final = 0
        }

      // Set camera location
      // -------------------
        var p = $scope.p
      // Coordinate transformation
        var WGS84proj = 'EPSG:4326'
        var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
        var longLat = proj4(ch1903plusproj, WGS84proj, [p[0], p[1]])
        $scope.computedCamera.longitude = longLat[0]
        $scope.computedCamera.latitude = longLat[1]
        $scope.computedCamera.height = p[2]
        var cartesian = Cesium.Cartesian3.fromDegrees(longLat[0], longLat[1], p[2])

        var angles = LM_to_cesium_angles(p[3], p[4], p[5])
        var newHeading = angles[0]
        var newTilt = angles[1]
        var newRoll = angles[2]
        $scope.computedCamera.heading = angles[0]
        $scope.computedCamera.tilt = angles[1]
        $scope.computedCamera.roll = angles[2]

      // Update camera
      // -------------
        var camera = $scope.cesium.camera
        camera.frustum.fov = Cesium.Math.PI / 3
      // $scope.relaxCollisionDetection();
      // var cartesian = Cesium.Cartesian3.fromDegrees($scope.computedCamera.longitude, $scope.computedCamera.latitude, $scope.computedCamera.height)
        camera.flyTo({
          destination: cartesian,
          orientation: {
            heading: Cesium.Math.toRadians($scope.computedCamera.heading),
            pitch: Cesium.Math.toRadians($scope.computedCamera.tilt),
            roll: Cesium.Math.toRadians($scope.computedCamera.roll)
          },
          complete: $scope.postFly
        })

      // Compute error in image coordinate
      // ---------------------------------
        var res = computeError(p, GCPs)
        var dxy = res[1]
        var xy_reproj = res[0]
        var x_reproj = math.squeeze(xy_reproj[0])
        var y_reproj = math.squeeze(xy_reproj[1])

      // Draw reprojection
        var nGCP = $scope.gcpArray.length
        for (var i = 0; i < nGCP; i++) {
          var id = $scope.gcpArray[i].id + 200

        // Get image coordinates
          var xCur = math.subset(x_reproj, math.index(i))
          var yCur = math.subset(y_reproj, math.index(i))
          var viewportPoint = $scope.mysd.viewer.viewport.imageToViewportCoordinates(xCur, yCur)

        // Remove existing reproj
          $scope.mysd.viewer.removeOverlay(id.toString())

        // Draw reprojected points
          var img = document.createElement('img')
          img.src = '../icons/reproj_yellow.png'
          img.id = id.toString()
          img.isReproj = true
          var ov = $scope.mysd.viewer.addOverlay(img, viewportPoint, 'CENTER')//, 'CENTER'
          $scope.mysd.viewer.forceRedraw()
          $scope.mysd.viewer.viewport.update()
        };

      // Show three modules
      // ------------------
        $scope.widgetSize = 'col-md-4 col-lg-4'
        $scope.cesiumBool = true
        $scope.leafletBool = true
        $scope.osdBool = true
      // resize width
        $scope.$apply(function () {
        // Leaflet sizes
          $scope.leafWidth = parseInt($window.innerWidth / 3.2)
        // Openseadragon size
          $scope.osdWidth = $scope.leafWidth
          $scope.osdStyle = {
            'height': String($scope.osdHeight).concat('px'),
            'width': String($scope.osdWidth).concat('px')
          }
        })
        leafletData.getMap().then(function (map) {
          map.invalidateSize()
        })
      // Draw leaflet markers
      // --------------------
        $scope.paths = new Array()
        $scope.geojson = {}

        $scope.markers = new Array()
        markerTrue = {
          lat: $scope.image.lat,
          lng: $scope.image.lng,
          icon: local_icons.green_icon
        }
        $scope.markers.push(markerTrue)
        markerComp = {
          lat: $scope.computedCamera.latitude,
          lng: $scope.computedCamera.longitude,
          icon: local_icons.red_icon
        }
        $scope.markers.push(markerComp)
      // Draw distance
        var distLine = {
          color: 'red',
          dashArray: '20,15',
          lineJoin: 'round',
          weight: 4,
          latlngs: [
            { lat: markerTrue.lat, lng: markerTrue.lng },
            { lat: markerComp.lat, lng: markerComp.lng}
          ]
        }
        $scope.paths.push(distLine)
      // Zoom on markers
        leafletData.getMap().then(function (map) {
          var m1 = L.latLng(markerTrue.lat, markerTrue.lng)
          var m2 = L.latLng(markerComp.lat, markerComp.lng)
          var bounds = L.latLngBounds(m1, m2)
          map.fitBounds(bounds)
        })
        $scope.poseComputed = true
        $scope.openPoseComputed()
      })// Promise
    } // Compute pose
    $scope.postFly = function () {
    // $scope.$apply(function(){
      // $scope.osdBool = false;
      // $scope.leafletBool = true;
    // });

    // Draw leaflet markers
    // --------------------
    // $scope.paths = new Array();
    // $scope.geojson = {};
    //
    // $scope.markers = new Array();
    // markerTrue = {
    //   lat: $scope.image.lat,
    //   lng: $scope.image.lng,
    //   icon: local_icons.green_icon
    // };
    // $scope.markers.push(markerTrue);
    // markerComp = {
    //   lat: $scope.computedCamera.latitude,
    //   lng: $scope.computedCamera.longitude,
    //   icon: local_icons.red_icon
    // };
    // $scope.markers.push(markerComp);
    // //Draw distance
    // var distLine = {
    //   color: 'red',
    //   dashArray: '20,15',
    //   lineJoin: 'round',
    //   weight: 4,
    //   latlngs: [
    //       { lat: markerTrue.lat, lng: markerTrue.lng },
    //       { lat: markerComp.lat, lng: markerComp.lng},
    //   ],
    // }
    // $scope.paths.push(distLine);
    // //Zoom on markers
    // leafletData.getMap().then(function(map) {
    //    var m1 = L.latLng(markerTrue.lat, markerTrue.lng);
    //    var m2 = L.latLng(markerComp.lat, markerComp.lng);
    //    var bounds = L.latLngBounds(m1, m2);
    //    map.fitBounds(bounds);
    //    console.log(map,m1,m2,bounds)
    // });

    // Open modal
    // $scope.openPoseComputed()
    }

    $scope.openCantonFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'cantonFound.html',
        bindToController: true,
        controller: 'cantonFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          }
        }
      })

    // Promise is open
    // modalInstance.opened.then(function(){

    // })

    // modalInstance.closed.then(function () {
    //   console.log('closed')
    // })
    // modalInstance.dismiss.then(function () {
    //   console.log('dismiss')
    // })
    // modalInstance.close.then(function () {
    //   console.log('close')
    // })

    // Promise is closed
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[2]
      // Update score
      // $scope.score.final = $scope.score.final + $scope.score.partial;
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax

        $scope.clipGridPromise = $scope.clipGridPromiseFunction($scope.selectedCanton)
        $scope.clipGridPromise.then(function (res) {
          $scope.geojson.cantons = {}
          $scope.grid.data.features = res.features
          $scope.geojson.grid = $scope.grid
          console.log('clip finished')
        }, function (error) {
          console.log('promise error', error)
        })
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    $scope.openCellFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'cellFound.html',
        bindToController: true,
        controller: 'cellFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          }
        }
      })
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[3]
      // Update score
      // $scope.score.final = $scope.score.final + $scope.score.partial;
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
      // Show only selected cell
        $scope.geojson.grid.data = $scope.selectedCell
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    $scope.openLocationFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'locationFound.html',
        bindToController: true,
        controller: 'locationFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          }
        }
      })
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[4]
      // Update score
      // $scope.score.final = $scope.score.final + $scope.score.partial;
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
        $scope.geojson = {}
        $scope.markers = new Array()
      // Draw marker
      // markerGT = {
      //  lat: $scope.image.lat,
      //  lng: $scope.image.lng,
      //  draggable: false
      // }
      // $scope.markers.push(markerGT);
        $scope.markers.push($scope.lastMarker)
      // Create path for direction
        $scope.paths = new Array()
        var direction = {
          color: 'red',
          weight: 8,
          latlngs: [
            { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
            { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng - 1}
          ]
        }
        $scope.paths.push(direction)
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    $scope.openDirectionFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'directionFound.html',
        bindToController: true,
        controller: 'directionFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          }
        }
      })
    // Hide leaflet, show cesium
      $scope.cesiumBool = true
      $scope.leafletBool = false
    // Set camera
      var position = [Cesium.Cartographic.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude)]
    // Get camera altitude
      var promise = Cesium.sampleTerrain($scope.cesium.terrainProvider, 11, position)
      Cesium.when(promise, function (updatedPosition) {
        $scope.providedCamera.altitude = updatedPosition[0].height + 100
        $scope.relaxCollisionDetection()
      // Set camera location
        var camera = $scope.cesium.camera
        camera.setView({
          destination: Cesium.Cartesian3.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude, $scope.providedCamera.altitude),
          orientation: {
            heading: Cesium.Math.toRadians($scope.providedCamera.heading),
            pitch: 0,
            roll: 0
          }
        })
      })
    // Fix camera location
      var scene = $scope.cesium.scene
      scene.screenSpaceCameraController.enableRotate = false
      scene.screenSpaceCameraController.enableTranslate = false
      scene.screenSpaceCameraController.enableTilt = false
      scene.screenSpaceCameraController.enableZoom = false
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
        $scope.gameSteps.curStep = $scope.gameSteps.list[5]
      // Update score
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
      // Create empty GCP
        var GCP = {
          id: $scope.myGCPs.curGCPid,
          x: null,
          y: null,
          X: null,
          Y: null,
          Z: null,
          eM: null,
          ePix: null
        }
      // Push it in the dictionary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      // Select the current GCP
        $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    $scope.openPoseComputed = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'poseComputed.html',
        bindToController: true,
        controller: 'poseComputedCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          distancePenalty: function () {
            return $scope.score.distancePenalty
          },
          congratulation: function () {
            return $scope.congratulation
          }
        }
      })

    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
        $scope.gameSteps.curStep = $scope.gameSteps.list[5]
      // Update score
      // $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
        $scope.poseComputed = true
      // Create empty GCP
        var GCP = {
          id: $scope.myGCPs.curGCPid,
          x: null,
          y: null,
          X: null,
          Y: null,
          Z: null,
          eM: null,
          ePix: null
        }
      // Push it in the dictionary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      // Select the current GCP
        $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }
    $scope.goToMono = function () {
    // location.href = '/mono/'+$scope.image.id;
      window.open('/mono/' + $scope.image.id, '_blank')
    }
    $scope.goToGame = function () {
      location.href = '/game/'
    // window.open('/game/','_blank');
    }
  }])

gameApp.controller('initialCtrl', function ($scope, $uibModalInstance) {
  $scope.closeInitial = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('cantonFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.closeCantonFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('cellFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.closeCellFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('locationFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.closeLocationFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('directionFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.closeDirectionFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('poseComputedCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, distancePenalty, congratulation) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.distancePenalty = distancePenalty
  $scope.congratulation = congratulation
  $scope.closePoseComputed = function () {
    $uibModalInstance.close()
  }
})
