newsApp.controller('newsCtrl', ['$scope', '$http', '$window', '$sce', 'AuthService', 'newsService', function ($scope, $http, $window, $sce, AuthService, newsService) {
  // texts
  // -----
  // Set initial language
  var lang = $window.navigator.language || $window.navigator.userLanguage
  lang = lang.slice(0, 2)
  if (lang == 'fr') {
    $scope.lang = 'fr'
  } else if (lang == 'it') {
    $scope.lang = 'it'
  } else if (lang == 'de') {
    $scope.lang = 'de'
  } else {
    $scope.lang = 'en'
  }

  $scope.hTexts = {
    'login': {
      'fr': "M'identifier"
    }
  }
  $scope.myName = $scope.hTexts.login[$scope.lang]

  // Language switchers
  $scope.switchToFrench = function () {
    $scope.lang = 'fr'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToGerman = function () {
    $scope.lang = 'de'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToEnglish = function () {
    $scope.lang = 'en'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToItalian = function () {
    $scope.lang = 'it'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }

  // Get news
  $scope.getNews = function () {
    newsService.getNews().then(
      function (response) {
        $scope.news = []
        var nNews = response.data.length
        for (i = 0; i < nNews; i++) {
          var curNew = {}
          curNew.title = {}
          curNew.text = {}
          curNew.title.fr = response.data[i].title_fr
          curNew.title.de = response.data[i].title_de
          curNew.title.en = response.data[i].title_en
          curNew.title.it = response.data[i].title_it
          curNew.text.fr = $sce.trustAsHtml(response.data[i].text_fr)
          curNew.text.de = $sce.trustAsHtml(response.data[i].text_de)
          curNew.text.en = $sce.trustAsHtml(response.data[i].text_en)
          curNew.text.it = $sce.trustAsHtml(response.data[i].text_it)
          curNew.author = response.data[i].author
          curNew.imageShow = true
          if (response.data[i].image_path === '') {
            curNew.imageShow = false
          } else {
            curNew.image_path = '../news_images/' + response.data[i].image_path
          }
          curNew.timestamp = response.data[i].timestamp
          curNew.category = response.data[i].category
          var d = new Date(curNew.timestamp)
          curNew.date = d.getDate()
          curNew.month = d.getMonth()
          curNew.year = d.getFullYear()
          $scope.news.push(curNew)
        }
      }
    )
  }
  $scope.getNews()

  // Load the text for the app
  $http.get('../texts/about/news.json', {
    headers: {
      'Accept': 'application/json;charset=utf-8'
    }
  })
  .success(function (data, headers) {
    $scope.texts = data.texts
    $scope.categories = [$scope.texts.media[$scope.lang], $scope.texts.volunteers[$scope.lang], $scope.texts.pictures[$scope.lang], $scope.texts.events[$scope.lang]]
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })

  // Load text for the header
  $http.get('../texts/about/header.json', {
    headers: {
      'Accept': 'application/json;charset=utf-8'
    }
  })
  .success(function (data, headers) {
    $scope.hTexts = data.texts
    $scope.myName = $scope.hTexts.login[$scope.lang]
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })

  // Functions for the navbar (require AuthService)
  // ------------------------
  $scope.getMyName = function () {
    if (AuthService.isAuthenticated()) {
      $http.get('/memberinfo').then(function (result) {
        $scope.volunteer = result.data.volunteer
        $scope.myName = $scope.volunteer.username
      })
    } else {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.getMyName()

  $scope.clickMy = function () {
    if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
      location.href = '/mysmapshot/'
    } else {
      // Go to login
      var win = $window.open('/login', '_blank')
      var timer = setInterval(checkChild, 500)
      function checkChild () {
        if (win.closed) {
              // alert("Child window closed");
          clearInterval(timer)
              // $window.open('/mysmapshot', '_blank')
          $window.location.reload()
              // $window.open('/mysmapshot', '_blank')
              // location.href = '/mysmapshot/';
              // $scope.apply
              // $scope.$apply(function(){
              // $scope.getMyName();
              // })
        }
      }
    }
      // $window.location.reload();
  }

  $scope.filters = { }
  $scope.changeCategory = function (category) {
    console.log('cat', category)
    $scope.currentCategory = category
    if (category == 'all') {
      $scope.filters = {}
    } else {
      $scope.filters.category = category
    }
  }

  // Links
  $scope.goToContribute = function () {
    location.href = '/contact/'
  }

  $scope.goToVisit = function () {
    location.href = '/mono/?collectionId=' + $scope.challengeCollection.id
  }

  $scope.goToMap = function () {
    location.href = '/map/' // faire passer l'id de la collection?
  }

  $scope.goToMono = function () {
    location.href = '/mono/?collectionId=' + $scope.challengeCollection.id
  }

  $scope.goToGame = function () {
    location.href = '/game/'
  }

  $scope.goToGeoref = function () {
    location.href = '/help/'
  }
}])
