contactApp.controller('contactCtrl', ['$scope', '$http', '$window', '$anchorScroll',
'$location', '$uibModal', 'AuthService', 'textService',
'langService',
function ($scope, $http, $window, $anchorScroll, $location, $uibModal, AuthService,
  textService, langService) {

  // Get lang
  // --------
  $scope.lang = langService.getBrowserLang()
  // Switch the lang
  $scope.switchLang = function (lang) {
    $scope.lang = lang
  }
  // Get languages model
  $scope.langs = langService.getLangs()

  // Get user
  // --------
  $scope.getMyName = function () {
    return $q(function (resolve, reject) {
      if (AuthService.isAuthenticated()) {
        $http.get('/volunteers/memberinfo').then(function (result) {
          $scope.volunteer = result.data.volunteer
          $scope.myName = $scope.volunteer.username
          $scope.isLogged = true
          if ($scope.volunteer.lang){
            $scope.lang = $scope.volunteer.lang
          }
          resolve()
        })
      } else {
        resolve()
      }
    })
  }
  // Get the texts
  // -------------
  $scope.loadTextPromise = textService.loadText('contact').then(function (texts) {
    $scope.texts = texts
    console.log('$scope.texts', $scope.texts)
  })

  // Links
  $scope.goToGeoref = function () {
    location.href = '/map/?geolocalized=false'
  }
  $scope.goToMap = function () {
    location.href = '/map/'// '?collectionId='+$scope.challengeCollection.id;//'; //faire passer l'id de la collection?
  }

}])
