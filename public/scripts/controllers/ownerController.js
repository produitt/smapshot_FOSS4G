ownerApp.controller('ownerCtrl',
  ['$scope', '$http', '$window', '$sce', '$q', 'ownerQueryService',
  'imageQueryService', 'volunteersService', 'AuthService', 'langService',
  'collectionsQueryService',
    function ($scope, $http, $window, $sce, $q, ownerQueryService,
      imageQueryService, volunteersService, AuthService, langService,
      collectionsQueryService
    ) {

      // Get the url name of the owner
      $scope.ownerUrlName = $window.location.href.split('/')[3].toLowerCase()
      // Initialize the language
      // lang
      // ----
      // Set initial language
      $scope.lang = langService.getBrowserLang()
      // Switch the lang
      $scope.switchLang = function (lang) {
        $scope.lang = lang
        $scope.listMonths[0].short = $scope.texts.all[$scope.lang]
      }
      // Get languages model
      $scope.langs = langService.getLangs()

      // $scope.hTexts = {
      //   'login': {
      //     'fr': "M'identifier"
      //   }
      // }
      // Load the text for the app
      $http.get('../texts/home.json', {
        headers: {
          'Accept': 'application/json;charset=utf-8'
        }
      })
      .success(function (data, headers) {
        $scope.texts = data.texts
        $scope.selectedCollection = $scope.texts.every_coll[$scope.lang]
        $scope.getMyName()
      })
      .error(function (data, status, headers, config) {
        console.log('error loading json')
      })
      // Initialize home page info
      $scope.ownerId = 4
      $scope.collectionId = 4
      $scope.selectedCollection = "Alle Bildersammlungen"
      $scope.showMoreVolunteers = false

      // Check if logged in
      // ------------------
      $scope.getMyName = function () {
        if (AuthService.isAuthenticated()) {
          $http.get('/volunteers/memberinfo').then(function (result) {
            $scope.volunteer = result.data.volunteer
            $scope.myName = $scope.volunteer.username
            if ($scope.volunteer.lang){
              $scope.lang = $scope.volunteer.lang
            }
          })
        } else {
          $scope.myName = $scope.texts.login[$scope.lang]
        }
      }
      //$scope.getMyName()

      // Ranking
      // -------
      // Date selection


      // Initialize end date
      $scope.endDate = new Date()
      $scope.endYear = $scope.endDate.getFullYear()
      $scope.endMonth = $scope.endDate.getMonth() + 1

      // list of years
      $scope.listYears = [$scope.endYear - 1, $scope.endYear]

      // list of months

      $scope.listMonths = {
        0: {fr: 'Tous', en: 'All', de: 'Alle', it: 'Tutti', mid: 0, short: 'All'},
        1: {fr: 'Janvier', en: 'January', de: 'Januar', it: 'Gennaio', mid: 1, short: 'Jan'},
        2: {fr: 'Février', en: 'February', de: 'Februar', it: 'Febbraio', mid: 2, short: 'Feb'},
        3: {fr: 'Mars', en: 'March', de: 'März', it: 'Marzo', mid: 3, short: 'Mar'},
        4: {fr: 'Avril', en: 'April', de: 'April', it: 'Aprile', mid: 4, short: 'Apr'},
        5: {fr: 'Mai', en: 'May', de: 'Mai', it: 'Maggio', mid: 5, short: 'Mai'},
        6: {fr: 'Juin', en: 'June', de: 'Juni', it: 'Giugno', mid: 6, short: 'Jun'},
        7: {fr: 'Juillet', en: 'July', de: 'Juli', it: 'Luglio', mid: 7, short: 'Jul'},
        8: {fr: 'Août', en: 'August', de: 'August', it: 'Agosto', mid: 8, short: 'Aug'},
        9: {fr: 'Septembre', en: 'September', de: 'September', it: 'Settembre', mid: 9, short: 'Sep'},
        10: {fr: 'Octobre', en: 'October', de: 'Oktober', it: 'Ottobre', mid: 10, short: 'Oct'},
        11: {fr: 'Novembre', en: 'November', de: 'November', it: 'Novembre', mid: 11, short: 'Nov'},
        12: {fr: 'Décembre', en: 'December', de: 'Dezember', it: 'Dicembre', mid: 12, short: 'Dec'}
      }
      // Change end month
      $scope.selectEndMonth = function (month) {
        $scope.endMonth = month.mid
        if (month.mid !== 0){
          $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0) // Get last day of current month
          var limitDates = $scope.generateStringDates($scope.endDate)
          var startDate = limitDates[0]
          var stopDate = limitDates[1]
        }else{
          $scope.endYear = $scope.texts.all[$scope.lang]
          startDate = null
          stopDate = null
        }

        var searchData = {
          ownerId: $scope.ownerId,
          collectionId: $scope.collectionId,
          startDate: startDate,
          stopDate: stopDate,
          photographerId: null,
          type: 'geoloc'
        }
        // Get the ranking of the volunteers
        volunteersService.getRanking(searchData)
        .then(function (response) {
          $scope.generateDataForRanking(response)
        })
      }
      $scope.selectEndYear = function (year) {
        $scope.endYear = year
        if ($scope.endMonth === 0){
          var startDate = year+'-01-01'
          var stopDate = year+'-12-31'
        }else{
          $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0)
          var limitDates = $scope.generateStringDates($scope.endDate)
          var startDate = limitDates[0]
          var stopDate = limitDates[1]
        }


        var searchData = {
          ownerId: $scope.ownerId,
          collectionId: $scope.collectionId,
          startDate: startDate,
          stopDate: stopDate,
          photographerId: null,
          collectionId: null,
          type: 'geoloc'
        }
        // Get the ranking of the volunteers
        volunteersService.getRanking(searchData)
        .then(function (response) {
          $scope.generateDataForRanking(response)
        })
      }
      $scope.generateStringDates = function (endDate) {
        // Generate start and end dates string
        // Get month
        var month = String(endDate.getMonth()+1)
        if (month.length < 2) {
          month = '0' + month
        }
        // Get year
        var year = endDate.getFullYear()
        // Generate strings
        var stopDate = [year, month, String(endDate.getDate())].join('-')
        var startDate = [year, month, '01'].join('-')

        return [startDate, stopDate]
      }
      // Click to change the collection

      $scope.selectCollection = function (collection) {
        $scope.collectionId = collection.id
        $scope.selectedCollection = collection.name

        var limitDates = $scope.generateStringDates($scope.endDate)
        var startDate = limitDates[0]
        var stopDate = limitDates[1]

        var searchData = {
          ownerId: $scope.ownerId,
          collectionId: $scope.collectionId,
          startDate: startDate,
          stopDate: stopDate,
          photographerId: null,
          type: 'geoloc'
        }
        // Get the ranking of the volunteers
        volunteersService.getRanking(searchData)
        .then(function (response) {
          $scope.generateDataForRanking(response)
        })
      }
      // Create data for the ranking
      $scope.generateDataForRanking = function (response) {
        $scope.dataRanking = []
        // Fill the data with the users
        for (var i = 0; i < response.length; i++) {
          // Get current user in the ranking
          var curUser = response[i]
          var d = {
            rank: i+1,
            username: curUser.username,
            nImages: parseInt(curUser.nImages)
          }
          $scope.dataRanking.push(d)
        }
        $scope.dataRankingEnd = $scope.dataRanking.splice(3, $scope.dataRanking.length - 3)
        // Check if the ranking is empty
        if ($scope.dataRanking.length === 0){
          $scope.selectEndMonth($scope.listMonths[0])
        }
      }
      // Click to show the entire ranking
      $scope.moreVolunteers = function () {
        $scope.showMoreVolunteers ^= true
      }
      // Queries send at initialization
      $scope.startQueries = function () {
        // Get custom parameters for the rendering
        ownerQueryService.getCustomParameters($scope.ownerUrlName)
        .then(function (owner) {
          $scope.owner = owner
          $scope.owner.descriptions = JSON.parse($scope.owner.description)
          $scope.ownerId = owner.id
          $scope.collectionId = owner.challenge_collection

          collectionsQueryService.getDescription($scope.collectionId).then(function (res) {
            $scope.collection = res
            var descriptions = JSON.parse(res.long_descr)
            $scope.collection.descriptions = descriptions
            $scope.collection.ratioGeoref = Math.round(parseFloat(res.nGeoref)/parseFloat(res.nImages)*1000)/10 //Math.round(parseFloat(res.nGeoref) / parseFloat(res.nImages))

            // search data for ratio
            var searchData = {
              ownerId: $scope.ownerId,
              startDate: null,
              stopDate: null,
              photographerId: null,
              collectionId: $scope.collectionId
            }
            // Get number and ratio of georeferenced photographies
            imageQueryService.getRatio(searchData)
            .then(function (result) {
              $scope.nGeoref = result.nGeoref
              $scope.nNotGeoref = result.nNotGeoref
              $scope.ratioGeoref = Math.round(result.ratioGeoref*1000)/10
              $scope.ratioNotGeoref = Math.round(result.ratioNotGeoref)
              // Display ratio infos
              var bar = document.getElementById('challenge-bar')
              if ($scope.ratioGeoref < 5) {
                bar.style.width = '5%'
              } else {
                bar.style.width = $scope.ratioGeoref + '%'
              }
              // Get statistics
              ownerQueryService.getStats($scope.ownerId) // $scope.ownerId)
              .then(function (res) {
                // Process and display stats
                $scope.ratioGeorefOwner = Math.round(res.ngeoref / res.nimages * 100)
                $scope.nImagesOwner = res.nimages
                $scope.nVolunteers = res.nvolunteers
                // search data for the ranking
                $scope.endDate = new Date()
                var limitDates = $scope.generateStringDates($scope.endDate)
                var startDate = limitDates[0]
                var stopDate = limitDates[1]
                var searchData = {
                  ownerId: $scope.ownerId, // $scope.ownerId,
                  startDate: startDate,
                  stopDate: stopDate,
                  photographerId: null,
                  collectionId: null,
                  type: 'geoloc'
                }
                // Get the ranking of the volunteers
                volunteersService.getRanking(searchData)
                .then(function (response) {
                  $scope.generateDataForRanking(response)
                  // Get list of collections
                  ownerQueryService.getCollections($scope.ownerId)
                  .then(function (response) {
                    $scope.collections = [
                      {
                        name: 'Alle Bildersammlungen',
                        id: null
                      }
                    ]
                    for (var i = 0; i < response.length; i++) {
                      $scope.collections.push(response[i])
                    }
                    collectionsQueryService.getCollections($scope.ownerId).then(function (res){
                      console.log('res collections', res)
                      $scope.collectionsTab = res
                      console.log('$scope.collectionsTab', $scope.collectionsTab)
                    })
                  })
                })
              })
            })
          })
        })
      }
      $scope.startQueries()

      $scope.openCloseCollection = function (c) {
        c.showMore ^= true
      }
      // Go to map
      $scope.goToMapCollection = function (c) {
        location.href = '/map/'+$scope.ownerUrlName+'/?collectionId='+c.id
      }
      // Go to georeferencing
      $scope.goToGeorefCollection = function (c) {
        location.href = '/map/'+$scope.ownerUrlName + '/?geolocalized=false&collectionId='+c.id // /georef/'// +$scope.ownerUrlName
      }
      // Go to map
      $scope.goToMap = function () {
        location.href = '/map/'+$scope.ownerUrlName
      }
      // Go to georeferencing
      $scope.goToGeoref = function () {
        location.href = '/map/'+$scope.ownerUrlName + '/?geolocalized=false' // /georef/'// +$scope.ownerUrlName
      }
      // Click on login
      $scope.clickMy = function () {
        if (AuthService.isAuthenticated()) {
        // Go to mysMapShot
          location.href = '/mysmapshot/'
        } else {
        // Go to login
          var win = $window.open('/login', '_blank')
          var timer = setInterval(checkChild, 500)
          function checkChild () {
            if (win.closed) {
              clearInterval(timer)
              AuthService.loadUserCredentials()
              $scope.getMyName()
              //$window.location.reload()
            }
          }
        }
      }
    }
  ]
)
