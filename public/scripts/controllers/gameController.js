gameApp.controller('gameCtrl', [
  '$scope', '$http', '$uibModal', '$window', '$q', '$location', '$timeout', 'leafletData',
  'leafletBoundsHelpers', 'leafletMarkerEvents', 'leafletMapEvents', 'cesiumService', 'osdService', 'dbService', 'imageQueryService',
  function ($scope, $http, $uibModal, $window, $q, $location, $timeout, leafletData,
    leafletBoundsHelpers, leafletMarkerEvents, leafletMapEvents, cesiumService, osdService, dbService, imageQueryService) {
    angular.element($window).bind('resize', function () {
      $scope.$apply(function () {
        console.log('RESIZE')
        if ($scope.poseComputed == false) {
        // Leaflet sizes
          $scope.leafWidth = parseInt($window.innerWidth * 0.45)
          $scope.leafHeight = angular.element(document.getElementById('adaptBox'))[0].clientHeight * 0.7// parseInt($window.innerHeight*0.5);
          if ($scope.leafHeight > $scope.leafWidth) {
            $scope.leafHeight = $scope.leafWidth
          }
        // Openseadragon size
          $scope.osdWidth = $scope.leafWidth
          $scope.osdHeight = $scope.leafHeight// parseInt($scope.leafHeight*2/3);
          $scope.osdStyle = {
            'height': String($scope.osdHeight).concat('px'),
            'width': String($scope.osdWidth).concat('px'),
            'background-color': 'grey'
          }
        } else {
        // Leaflet sizes
          $scope.leafWidth = parseInt($window.innerWidth / 3.1)
          if ($scope.leafHeight > $scope.leafWidth) {
            $scope.leafHeight = $scope.leafWidth
          }
        // Openseadragon size
          $scope.osdWidth = $scope.leafWidth
          $scope.osdStyle = {
            'height': String($scope.osdHeight).concat('px'),
            'width': String($scope.osdWidth).concat('px'),
            'background-color': 'grey'
          }
        };
      })
    })

  // texts
  // -----
    $scope.texts = {
      task1: {
        fr: ''
      }
    }

  // Set initial language
    var lang = $window.navigator.language || $window.navigator.userLanguage
    lang = lang.slice(0, 2)
    if (lang == 'fr') {
      $scope.lang = 'fr'
    } else if (lang == 'it') {
      $scope.lang = 'it'
    } else if (lang == 'de') {
      $scope.lang = 'de'
    } else {
      $scope.lang = 'en'
    }

  // Language switchers
    $scope.switchLang = function (lang) {
      $scope.lang = lang
      $scope.gameSteps.curStep.task = $scope.texts.task1[$scope.lang]
      $scope.leafLegend.labels = [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
      $scope.updateTaskList()
    }
    // $scope.switchToFrench = function () {
    //   $scope.lang = 'fr'
    //   $scope.gameSteps.curStep.task = $scope.texts.task1[$scope.lang]
    //   $scope.leafLegend.labels = [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
    //   $scope.updateTaskList()
    // }
    // $scope.switchToGerman = function () {
    //   $scope.lang = 'de'
    //   $scope.gameSteps.curStep.task = $scope.texts.task1[$scope.lang]
    //   $scope.leafLegend.labels = [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
    //   $scope.updateTaskList()
    // }
    // $scope.switchToEnglish = function () {
    //   $scope.lang = 'en'
    //   $scope.gameSteps.curStep.task = $scope.texts.task1[$scope.lang]
    //   $scope.leafLegend.labels = [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
    //   $scope.updateTaskList()
    // }
    // $scope.switchToItalian = function () {
    //   $scope.lang = 'it'
    //   $scope.gameSteps.curStep.task = $scope.texts.task1[$scope.lang]
    //   $scope.leafLegend.labels = [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
    //   $scope.updateTaskList()
    // }
    $scope.updateTaskList = function () {
      $scope.gameSteps.list = {
        1: {
          id: 1,
          task: $scope.texts.task1[$scope.lang],
          messageSuccess: $scope.texts.task1Success[$scope.lang],
          messageFail: $scope.texts.task1Fail[$scope.lang],
          message: '',
          scoreMax: 10,
          zoom: 17
        },
        2: {
          id: 2,
          task: $scope.texts.task2[$scope.lang],
          messageSuccess: $scope.texts.task2Success[$scope.lang],
          messageFail: $scope.texts.task2Fail[$scope.lang],
          message: '',
          scoreMax: 10,
          zoom: 19
        },
        3: {
          id: 3,
          task: $scope.texts.task3[$scope.lang],
          messageSuccess: $scope.texts.task3Success[$scope.lang],
          messageFail: $scope.texts.task3Fail[$scope.lang],
          message: '',
          scoreMax: 10,
          zoom: 19
        },
        4: {
          id: 4,
          task: $scope.texts.task4[$scope.lang],
          messageSuccess: $scope.texts.task4Success[$scope.lang],
          messageFail: $scope.texts.task4Fail[$scope.lang],
          message: '',
          scoreMax: 10,
          zoom: 19
        },
        5: {
          id: 5,
          task: $scope.texts.task5[$scope.lang],
          messageSuccess: '',
          messageFail: '',
          message: '',
          scoreMax: 10
        }
      }
      $scope.gameSteps.curStep = $scope.gameSteps.list[$scope.gameSteps.curStep.id]
    }
  // Open initial instruction modal
    $scope.openInitial = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'initial.html',
        bindToController: true,
        controller: 'initialCtrl',
        resolve: {
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
    }

  // Initialize game steps
    $scope.gameSteps = {
      curStep: {
        id: 1,
        task: '',
        messageSuccess: '',
        messageFail: '',
        message: '',
        scoreMax: 10,
        zoom: 17
      }
    }

  // Load the text for the app
    $http.get('../texts/game.json', {
      headers: {
        'Accept': 'application/json;charset=utf-8'
      }
    })
  .success(function (data, headers) {
    $scope.texts = data.texts
    $scope.openInitial()
    // Create game steps
    $scope.gameSteps = {
      curStep: {
        id: 1
      }
    }
    // Is current task solved
    $scope.gameSteps.find = false
    $scope.updateTaskList()

    $scope.gameSteps.curStep = $scope.gameSteps.list[1]
    $scope.leafLegend = {
      position: 'bottomleft',
      colors: [ '#FF0000', '#FFC000', '#40FF00' ],
      labels: [$scope.texts.labelFar[$scope.lang], $scope.texts.labelClose[$scope.lang], $scope.texts.labelHere[$scope.lang]]
    }
    // Leaflet sizes
    $scope.leafWidth = parseInt(window.innerWidth * 0.45)
    $scope.leafHeight = angular.element(document.getElementById('adaptBox'))[0].clientHeight * 0.7// parseInt(window.innerHeight*0.4);//angular.element(document.getElementById('adaptBox'))[0].clientHeight//parseInt(window.innerHeight*3/5);
    if ($scope.leafHeight > $scope.leafWidth) {
      $scope.leafHeight = $scope.leafWidth
    }

    // Openseadragon size
    $scope.osdWidth = $scope.leafWidth
    $scope.osdHeight = $scope.leafHeight// parseInt($scope.leafHeight*2/3);
    $scope.osdStyle = {
      'height': String($scope.osdHeight).concat('px'),
      'width': String($scope.osdWidth).concat('px'),
      'background-color': 'grey'
    }
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })

  // Get the url parameters
  // ----------------------
    $scope.collectionId = $location.search().collectionId
    $scope.imageId = $location.search().imageId
    if ($scope.imageId == 'null') {
      $scope.imageId = null
    }
    if ($scope.collectionId == 'null') {
      $scope.collectionId = null
    }

  // Color management (canton and cell fill, icon color, direction line)
  // ----------------
    var redToGreen = {
      0: '#FF0000', // '(255, 64, 0)',
      1: '#FF4000', // '(255, 128, 0)',
      2: '#FF8000', // '(255, 192, 0)',
      3: '#FFC000', // '(255, 255, 0)',
      4: '#FFFF00', // '(191, 255, 0)',
      5: '#C0FF00', // '(127, 255, 0)',
      6: '#80FF00', // '(63, 255, 0)',
      7: '#40FF00'// '(0, 255, 0)',
    }

  // Canton color function of the distance
    $scope.cantonColor = function (dist) {
      var maxDist = 200 // km
      var maxColorId = 7
      var index = Math.round(dist * maxColorId / maxDist)
      index = -(index - 7)
      if (index < 0) {
        index = 0
      }
      if (index > 7) {
        index = 7
      }
      return redToGreen[index]
    }

  // Cell color function of the distance
    $scope.gridColor = function (dist) {
      var maxDist = 100 // km
      var maxColorId = 7
      var index = Math.round(dist * maxColorId / maxDist)
      index = -(index - 7)
      if (index < 0) {
        index = 0
      }
      if (index > 7) {
        index = 7
      }
      return redToGreen[index]
    }

  // Icon color function of the distance
    $scope.iconColor = function (dist) {
      var maxDist = 10 // km
      var maxColorId = 7
      var index = Math.round(dist * maxColorId / maxDist)
      index = -(index - 7)
      if (index < 0) {
        index = 0
      }
      if (index > 7) {
        index = 7
      }
      return index
    }

  // Direction color function of the distance
    $scope.directionColor = function (dist) {
      var maxDist = 180 // km
      var maxColorId = 7
      var index = Math.round(dist * maxColorId / maxDist)
      index = -(index - 7)
      if (index < 0) {
        index = 0
      }
      if (index > 7) {
        index = 7
      }
      return redToGreen[index]
    }

  // Get a random image for the game
    imageQueryService.getRandomImage($scope.collectionId, $scope.imageId)
    .then(function (image) {
      $scope.image = image
      $scope.mysd.viewer.open('../data/collections/'+ image.collectionId + '/images/tiles/'.concat($scope.image.id).concat('.dzi'))
    })

  // Customization
  // -------------
  // Shape of the cesium navigation buttons
    $scope.navMode = 'game'

    $scope.widgetSize = 'col-md-6 col-lg-6'
  // Message Customization
    $scope.cssMessage = {
      neutral: 'alert-info', // 'message-neutral';
      positive: 'alert-success', // 'message-neutral';
      negative: 'alert-danger', // 'message-neutral';
      current: 'alert-info'
    }
    $scope.isCollapsed = true

  // Initialize OpenSeadragon
    $scope.optionsViewer = {
      prefixUrl: '../bower_components/openseadragon/built-openseadragon/openseadragon/images/',
      gestureSettingsMouse: {
        clickToZoom: false,
        dblClickToZoom: false
      },
      showNavigator: false,
      showHomeControl: false,
      showFullPageControl: true,
      showZoomControl: false,
      gcpIcon: '../icons/georef/target_green.png',
      gcpIconSelected: '../icons/georef/target_yellow.png',
    }

  // Show or hide modules
    $scope.cesiumBool = false
    $scope.osdBool = true
    $scope.leafletBool = true
    $scope.clueBool = true
    $scope.poseComputed = false
    $scope.showClue = false
    $scope.showNextButtons = true
    $scope.showStuck = false

  // Cesium initial position
    $scope.providedCamera = {
      latitude: 46.22015, // null,
      longitude: 7.48232, // null
      height: null,
      heading: 30, // null
      tilt: null,
      roll: null,
      fov: null
    }
    $scope.cesiumObject = {
      cesium: null
    }

  // Initialise computeCamera model
    $scope.computedCamera = {
      latitude: null,
      longitude: null,
      height: null,
      heading: null,
      tilt: null,
      roll: null,
      fov: null
    }

  // Initialise the GCP model
    $scope.myGCPs = {
      curGCPid: 0,
      selectedGCPid: null,
      dictGCPs: {},
      file: null,
      fullBool: false
    }
  // Array of gcp is sync with the dictonary of gcp
    $scope.gcpArray = []
  // if the dictonary is modified, the gcp array is updated
    $scope.$watch('myGCPs.dictGCPs', function () {
      $scope.dictToArray()
    }, true)
  // Reshape the dictionary in array
    $scope.dictToArray = function () {
      var gcpArray = []
      var nGCP = 0
      for (var id in $scope.myGCPs.dictGCPs) {
        var curGCP = $scope.myGCPs.dictGCPs[id]
        if ((curGCP.x != null) && (curGCP.X != null)) {
          gcpArray.push(curGCP)
          nGCP += 1
        }
      }
      $scope.gcpArray = gcpArray
      if (nGCP == 5) {
        $scope.myGCPs.fullBool = true
        $scope.computePose()
      }
    }

  // Set leaflet
  // -----------
  // Set leaflet baselayer
    $scope.map = {
      defaults: {
        crs: new L.Proj.CRS('EPSG:21781', '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 ' + '+k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs', {
          resolutions: [4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 250, 100, 50, 20, 10, 5, 2.5, 2, 1.5, 1, 0.5],
          origin: [420000, 350000]
        }),
        continuousWorld: true
      },
      layers: {
        baselayers: {
          swisstopo: {
            name: 'Swisstopo',
            type: 'xyz',
            url: 'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/21781/{z}/{y}/{x}.jpeg',
                // visible: true,
            layerOptions: {
              attribution: 'Map data &copy; 2015 swisstopo',
              maxZoom: 26,
              minZoom: 0,
              showOnSelector: false
            }
          }
        }
      }
    }

  // Set geojson layer
    $scope.markers = new Array()
    $scope.geojson = {}
    $scope.buffer = {
      name: 'Buffer',
      type: 'geoJSONShape',
      data: {},
      visible: false,
      style: {
        color: 'black',
        fillColor: 'green',
        weight: 4.0,
        opacity: 1,
        fillOpacity: 0.5
      }
    }
  // Set path for direction
    $scope.paths = {}
  // Set selected canton
    $scope.selectedCanton = {}

  // Create icons
    var local_icons = {
      default_icon: {},
      red_icon: {
        iconUrl: '../icons/camera_red.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
      },
      green_icon: {
        iconUrl: '../icons/camera_green.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
      }
    }
  // Get geojsons cells
    $.getJSON('../geodata/grid.geojson', function (json) {
    // $scope.grid = json;
      $scope.grid = {
        name: 'Grid',
        type: 'geoJSONShape',
        data: json,
      // visible: false,
        style: {
          color: 'black',
          fillColor: 'yellow',
          weight: 4.0,
          opacity: 1,
          fillOpacity: 0.1
        }
      }
    })
  // Get geojsons cantons
    $.getJSON('../geodata/cantons.geojson', function (json) {
      $scope.geojson.cantons = {
        name: 'Cantons',
        type: 'geoJSONShape',
        data: json,
        visible: true,
        style: {
          color: 'black',
          fillColor: 'yellow',
          weight: 4.0,
          opacity: 1,
          fillOpacity: 0.1
        }
      }
    })

  // Game
  // ----
    $scope.score = {
      final: 60,
      partial: 10,
      partialType: 'progress-green-light', // success',
      finalType: 'success'
    }
  // If score change update bar color
    $scope.$watch('score.partial', function () {
      if ($scope.score.partial >= 6) {
        $scope.score.partialType = 'greenLight'
      } else if ($scope.score.partial >= 4) {
        $scope.score.partialType = 'orangeLight'
      } else {
        $scope.score.partialType = 'redLight'
      }
      $scope.score.remainingPoints = $scope.score.final - $scope.score.partial
    }, true)
  // If score change update bar color
    $scope.$watch('score.final', function () {
      if ($scope.score.final > 45) {
        $scope.score.finalType = 'success'
      } else if ($scope.score.final > 30) {
        $scope.score.finalType = 'warning'
      } else {
        $scope.score.finalType = 'danger'
      }
      $scope.score.remainingPoints = $scope.score.final - $scope.score.partial
      if ($scope.score.final > 50) {
        try {
          $scope.congratulation = $scope.texts.congrat1[$scope.lang]
        } catch (err) {

        } finally {
          console.log('')
        }
      } else if ($scope.score.final > 40) {
        $scope.congratulation = $scope.texts.congrat2[$scope.lang]
      } else if ($scope.score.final > 30) {
        $scope.congratulation = $scope.texts.congrat3[$scope.lang]
      } else if ($scope.score.final > 20) {
        $scope.congratulation = $scope.texts.congrat4[$scope.lang]
      } else {
        $scope.congratulation = ''
      }
    }, true)

  // Set map center
    $scope.center = {
      lat: 46.78099, // 46.47532,
      lng: 8.01153, // 7.13708,
      zoom: 15
    }

  // Function to compute azimuth
    XYtoAzimuth = function (XY1, XY2) {
      var dx = (XY2[0] - XY1[0])
      var dy = (XY2[1] - XY1[1])
      var alpha = 180.0 / Math.PI * Math.atan2(dx, dy)
      if (alpha < 0) {
        var alpha = alpha + 360
      }
      return alpha
    }

  // Switch to GCP mode
    $scope.switchToGCP = function (provAz, args) {
    // solution is found
      $scope.gameSteps.find = true
      $scope.sendMessage($scope.gameSteps.list[4].messageSuccess, $scope.cssMessage.positive)
    // Draw accepted direction
      if (args) {
        var acceptedDirection = {
          color: 'green',
          weight: 8,
          latlngs: [
            { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
            { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
          ]
        }
        $scope.paths.push(acceptedDirection)
      }
    // Record provided azimuth
      $scope.providedCamera.heading = provAz
    // Open modal
      $scope.openDirectionFound()
    }

  // Map events
  // ----------
    var mouseMoveEvent = 'leafletDirectiveMap.mousemove'
    $scope.$on(mouseMoveEvent, function (event, args) {
      if ($scope.gameSteps.curStep.id == 4) {
      // If in direction mode, the direction line is drawn
        if ($scope.gameSteps.find == false) {
          leafletData.getMap().then(function (map) {
            var bbox = map.getBounds()
            var fromPt = turf.point([bbox.getWest(), bbox.getNorth()])
            var toPt = turf.point([bbox.getWest(), bbox.getSouth()])
            var distEW = turf.distance(fromPt, toPt)

            var fromPt = turf.point([bbox.getWest(), bbox.getSouth()])
            var toPt = turf.point([bbox.getEast(), bbox.getSouth()])
            var distNS = turf.distance(fromPt, toPt)

            var minExt = Math.min(distEW, distNS) / 4

          // Create line
            var line = turf.lineString([
           [$scope.lastMarker.lng, $scope.lastMarker.lat],
           [args.leafletEvent.latlng.lng, args.leafletEvent.latlng.lat]
            ], {})
            var length = turf.lineDistance(line) // Km
            if (length > minExt) {
              var along = turf.along(line, minExt)
            // Draw direction
              $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
              $scope.paths[0].latlngs[1] = { lat: along.geometry.coordinates[1], lng: along.geometry.coordinates[0]}
            } else {
            // Draw direction
              $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
              $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
            }
          })
        // $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng };
        // $scope.paths[0].latlngs[1] = args.leafletEvent.latlng;
        } else {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        }
      }
    })
    $scope.$on('leafletDirectiveMap.click', function (event, args) {
    // if the client is mobile (phone or tablet)
      if ($scope.gameSteps.curStep.id == 4) {
      // Check direction on click
        if ($scope.gameSteps.find == false) {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
        } else {
          $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
          $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        }
      // Compare provided direction with real direction
        var GtAz = $scope.image.heading
        if (GtAz > 360) {
          var GtAz = GtAz - 360
        };
      // Transform coordinates
        var WGS84proj = 'EPSG:4326'
        var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
        var XYmarker = proj4(WGS84proj, ch1903plusproj, [$scope.lastMarker.lng, $scope.lastMarker.lat])
        var XYclic = proj4(WGS84proj, ch1903plusproj, [args.leafletEvent.latlng.lng, args.leafletEvent.latlng.lat])
        var provAz = XYtoAzimuth(XYmarker, XYclic)
      // Compute difference
        var deltaAz = Math.abs(GtAz - provAz)
        if (deltaAz > 180) {
          var deltaAz = 360 - deltaAz
        };
        $scope.distance = deltaAz.toFixed(1)
      // If distance is small, go to next step
        if (deltaAz < 20) {
          $scope.switchToGCP(provAz, args)
        } else {
        // Distance is too big
          if ($scope.gameSteps.find == false) {
          // Send error message
            $scope.sendMessage($scope.gameSteps.list[4].messageFail, $scope.cssMessage.negative)
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Update color
            var color = $scope.directionColor(deltaAz)
            $scope.paths[0].color = color
          // Draw wrong direction
            var falseDirection = {
              color: color,
              weight: 4,
              latlngs: [
                { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
                { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
              ]
            }
            $scope.paths.push(falseDirection)
          }
        }
      };
    })

  // Send message function
    $scope.sendMessage = function (message, css) {
      $scope.isCollapsed = true
      $scope.messageStdBy = message
      $scope.cssMessageStdBy = css
    // $scope.message = message;
    // $scope.cssMessage.current = css;
    }
  // Open next message function
    $scope.openNextMessage = function () {
      $scope.isCollapsed = false
      $scope.message = $scope.messageStdBy
      $scope.cssMessage.current = $scope.cssMessageStdBy
    }
  // Click on a path layer
  // ---------------------
  // Click with the mouse in direction mode
    $scope.$on('leafletDirectivePath.click', function (event, args) {
    // Check direction on click
      if ($scope.gameSteps.find == false) {
        $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        $scope.paths[0].latlngs[1] = args.leafletEvent.latlng
      } else {
        $scope.paths[0].latlngs[0] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
        $scope.paths[0].latlngs[1] = { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng }
      }
    // Compare provided direction with real direction
      var GtAz = $scope.image.heading
      if (GtAz > 360) {
        var GtAz = GtAz - 360
      };
    // Transform coordinates
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var XYmarker = proj4(WGS84proj, ch1903plusproj, [$scope.lastMarker.lng, $scope.lastMarker.lat])
      var XYclic = proj4(WGS84proj, ch1903plusproj, [args.leafletEvent.latlng.lng, args.leafletEvent.latlng.lat])
      var provAz = XYtoAzimuth(XYmarker, XYclic)
    // Compute difference
      var deltaAz = Math.abs(GtAz - provAz)
      if (deltaAz > 180) {
        var deltaAz = 360 - deltaAz
      }
      $scope.distance = deltaAz.toFixed(1)
    // If distance is small
      if (deltaAz < 20) {
        $scope.switchToGCP(provAz, args)
      } else {
        if ($scope.gameSteps.find == false) {
          $scope.sendMessage($scope.gameSteps.list[4].messageFail, $scope.cssMessage.negative)
        // Update score
          if ($scope.score.partial - 1 >= 0) {
            $scope.score.partial = $scope.score.partial - 1
            $scope.score.final = $scope.score.final - 1
          };
        // Update color
          var color = $scope.directionColor(deltaAz)
          $scope.paths[0].color = color
        // Draw wrong direction
          var falseDirection = {
            color: color,
            weight: 4,
            latlngs: [
              { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
              { lat: args.leafletEvent.latlng.lat, lng: args.leafletEvent.latlng.lng}
            ]
          }
          $scope.paths.push(falseDirection)
        }
      }
    })
  // Click on a geojson layer
  // ------------------------
    $scope.$on('leafletDirectiveGeoJson.click', function (ev, leafletPayload) {
    // Click on a geojson object
      $scope.shapeClick(leafletPayload.leafletObject, leafletPayload.leafletEvent)
    })

  // Switch to cell mode
    $scope.switchToGrid = function (shape, event) {
    // solution is found
      $scope.gameSteps.find = true
      $scope.sendMessage($scope.gameSteps.list[1].messageSuccess, $scope.cssMessage.positive)
    // Record the canton for the clip
      $scope.selectedCanton = shape
    // Change style of clicked objects
      if (event) {
        var layer = event.target
        layer.setStyle({
          weight: 4,
          color: 'black',
          fillColor: 'green',
          fillOpacity: 0.5
        })
      }
    // Zoom on the canton
      leafletData.getMap().then(function (map) {
        var bbox = turf.bbox($scope.selectedCanton.geometry)
        console.log('bbox', bbox)
      // map.fitBounds([
      //   [bbox[0], bbox[1]]
      //   [bbox[2], bbox[3]]
      // ]);
        var m1 = L.latLng(bbox[1], bbox[0])
        var m2 = L.latLng(bbox[3], bbox[2])
        var bounds = L.latLngBounds(m1, m2)
        map.fitBounds(bounds)
      // var centroidPt = turf.centroid($scope.selectedCanton.geometry);
      // var latlng = L.latLng(centroidPt.geometry.coordinates[1], centroidPt.geometry.coordinates[0]);
      // map.setView(latlng, $scope.gameSteps.curStep.zoom)
      })
    // Open modal
      $scope.openCantonFound()
    }
  // Switch to location mode
    $scope.switchToLocation = function (shape, event) {
    // solution is found
      $scope.gameSteps.find = true
      $scope.sendMessage($scope.gameSteps.list[2].messageSuccess, $scope.cssMessage.positive)
    // Record cell
      $scope.selectedCell = shape
      if (event) {
        var layer = event.target
        layer.setStyle({
          weight: 4,
          color: 'black',
          fillColor: 'green',
          fillOpacity: 0.5
        })
      }
    // Zoom on the cell
      leafletData.getMap().then(function (map) {
        var bbox = turf.bbox(shape.geometry)
        console.log('bbox', bbox)
      // map.fitBounds([
      //   [bbox[0], bbox[1]]
      //   [bbox[2], bbox[3]]
      // ]);
        var m1 = L.latLng(bbox[1], bbox[0])
        var m2 = L.latLng(bbox[3], bbox[2])
        var bounds = L.latLngBounds(m1, m2)
        map.fitBounds(bounds)
      // var centroidPt = turf.centroid(shape.geometry);
      // var latlng = L.latLng(centroidPt.geometry.coordinates[1], centroidPt.geometry.coordinates[0]);
      // map.setView(latlng, $scope.gameSteps.curStep.zoom)
      })
    // Open modal
      $scope.openCellFound()
    }

  // Switch to direction mode
    $scope.switchToDirection = function (loc) {
    // solution is found
      $scope.gameSteps.find = true
      $scope.message = $scope.gameSteps.list[3].messageSuccess
      $scope.cssMessage.current = $scope.cssMessage.positive
    // Stores location
      $scope.providedCamera.longitude = loc.lng
      $scope.providedCamera.latitude = loc.lat
    // Draw leaflet marker
      marker = {
        lat: loc.lat,
        lng: loc.lng,
        icon: local_icons.green_icon
      }
      $scope.markers.push(marker)
      $scope.lastMarker = marker
      leafletData.getMap().then(function (map) {
        var latlng = L.latLng(loc.lat, loc.lng)
        map.setView(latlng, $scope.gameSteps.curStep.zoom)
      })
    // Open modal
      $scope.openLocationFound()
    }

  // Click on a geojson object
    $scope.shapeClick = function (shape, event) {
    // If the solution is not solved
      if ($scope.gameSteps.find == false) {
      // Get clicked feature
        var shape = shape.feature
      // Check the current game step
        if ($scope.gameSteps.curStep.id == 1) {
        // Select canton
        // -------------
        // Check if inside with turf
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var multiPoly = turf.multiPolygon(shape.geometry.coordinates)
          var isInside = turf.inside(pt, multiPoly)
        // if is inside
          if (isInside == true) {
          // Go to next step
            $scope.switchToGrid(shape, event)
          } else {
            $scope.sendMessage($scope.gameSteps.list[1].messageFail, $scope.cssMessage.negative)
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Compute distance
            var fromPt = turf.point([ $scope.image.lng, $scope.image.lat])
            var toPt = turf.point([ shape.properties.longitude, shape.properties.latitude])
            var dist = turf.distance(fromPt, toPt) // KM
          // Get corresponding color
            var color = $scope.cantonColor(dist)
          // Change style of clicked objects
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: color,
              fillOpacity: 0.5
            })
          }
      // step 2 check if inside cell
        } else if ($scope.gameSteps.curStep.id == 2) {
        // Select cell
        // -----------
        // Check if inside with turf
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var poly = turf.multiPolygon(shape.geometry.coordinates)
          var isInside = turf.inside(pt, poly)
        // if is inside
          if (isInside == true) {
          // Go to next step
            $scope.switchToLocation(shape, event)
          } else {
            $scope.sendMessage($scope.gameSteps.list[2].messageFail, $scope.cssMessage.negative)
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Compute distance
            var fromPt = turf.point([ $scope.image.lng, $scope.image.lat])
            var toPt = turf.point([ shape.properties.longitude, shape.properties.latitude])
            var dist = turf.distance(fromPt, toPt) // KM
          // Get corresponding color
            var color = $scope.gridColor(dist)
          // Change style of clicked objects
            var layer = event.target
            layer.setStyle({
              weight: 4,
              color: 'black',
              fillColor: color,
              fillOpacity: 0.5
            })
          }
      // step 3: test locations
        } else if ($scope.gameSteps.curStep.id == 3) {
        // Get click location
          loc = event.latlng
        // Check distance
          var fromPt = turf.point([ $scope.image.lng, $scope.image.lat])
          var toPt = turf.point([ loc.lng, loc.lat])
          var dist = turf.distance(fromPt, toPt) // KM
          var limitDist = 1
          if (dist < limitDist) {
            $scope.distance = dist.toFixed(1)
          // Go to next step
            $scope.switchToDirection(loc)
          } else {
            $scope.sendMessage($scope.gameSteps.list[3].messageFail, $scope.cssMessage.negative)
          // Update score
            if ($scope.score.partial - 1 >= 0) {
              $scope.score.partial = $scope.score.partial - 1
              $scope.score.final = $scope.score.final - 1
            };
          // Get icon color
            var color = $scope.iconColor(dist)
            var icon = {
              iconUrl: '../icons/camera_' + color + '.png',
              iconSize: [48, 48], // size of the icon
              iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
            }
          // Draw leaflet marker
            marker = {
              lat: loc.lat,
              lng: loc.lng,
              icon: icon
            }
            $scope.markers.push(marker)
          }
        }
      }
    }

  // Reinitialize camera
    $scope.reinitializeCamera = function () {
    // Set camera location
      var camera = $scope.cesiumObject.cesium.camera
      camera.frustum.fov = Cesium.Math.PI_OVER_THREE
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude, $scope.providedCamera.altitude),
        orientation: {
          heading: Cesium.Math.toRadians($scope.providedCamera.heading),
          pitch: 0,
          roll: 0
        }
      })
    }
  // Reset camera location if the initial location is bad
    $scope.changeCameraPosition = function () {
    // Set camera location
      var camera = $scope.cesiumObject.cesium.camera
      camera.frustum.fov = Cesium.Math.PI_OVER_THREE
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees($scope.image.lng, $scope.image.lat, $scope.image.height + 20),
        orientation: {
          heading: Cesium.Math.toRadians($scope.providedCamera.heading),
          pitch: 0,
          roll: 0
        }
      })
    // Decrease score
      $scope.score.final = $scope.score.final - 5
    }

  // Draw all GCP
    $scope.drawGCP = function () {
      for (var i in $scope.myGCPs.dictGCPs) {
        var gcp = $scope.myGCPs.dictGCPs[i]
        $scope.drawOneGCP(gcp)
      }
    }

  // Draw One GCP
    $scope.drawOneGCP = function (gcp) {
      osdService.drawGCP($scope.mysd, gcp, $scope.options)
      cesiumService.drawGCP($scope.cesiumObject.cesium, gcp)
    }

  // Create new GCP
    $scope.CreateNewGCP = function () {
    // increment GCP identifier
      $scope.myGCPs.curGCPid = $scope.myGCPs.curGCPid + 1
    // Create empty GCP
      var GCP = {
        id: $scope.myGCPs.curGCPid,
        x: null,
        y: null,
        X: null,
        Y: null,
        Z: null,
        eM: null,
        ePix: null
      }
    // Push it in the dictionary
      $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
    // Select the current GCP
      $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
    }

  // Create GCP from cesium
    $scope.cesiumGCP = function (XYZ) {
    // If current GCP is empty, fill it with the values
      if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X == null) {
      // Push correspondance in the dictionnary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X = XYZ[0]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Y = XYZ[1]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].Z = XYZ[2]

      // Check if GCP is finished
        if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x != null) {
          $scope.CreateNewGCP()
        }
      }
    // World coordinates are already provided, user must click in image
      else {
        alert($scope.texts.alertGCP[$scope.lang])
      };
    }

  // Free the clue (image title)
    $scope.getClue = function () {
    // Hide clue button
      $scope.clueBool = false
      $scope.showClue = true
    // Decrease score
      $scope.score.final = $scope.score.final - 10
      $scope.clue = $scope.image.title
    }

  // GCP clicked in image
    $scope.osdGCP = function (xy, viewportPoint) {
    // If image coordinates are empty
      if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x == null) {
      // Push correspondance in the dictionnary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].x = xy[0]
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].y = xy[1]
        osdService.drawGCPLabel($scope.mysd, $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid], $scope.optionsViewer) //, viewportPoint
      // Check if GCP is finished
        if ($scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid].X != null) {
          $scope.CreateNewGCP()
        }
      }
    // image coordinates are already provided, user must click in the globe
      else {
        alert($scope.texts.alertGlobe[$scope.lang])
      };
    }

  // Cesium moves
    $scope.turnLeft = function () {
      cesiumService.turnLeft($scope.cesiumObject.cesium)
    }
    $scope.turnRight = function () {
      cesiumService.turnRight($scope.cesiumObject.cesium)
    }
    $scope.turnUp = function () {
      cesiumService.turnUp($scope.cesiumObject.cesium)
    }
    $scope.turnDown = function () {
      cesiumService.turnDown($scope.cesiumObject.cesium)
    }
    $scope.zoomIn = function () {
      var camera = $scope.cesiumObject.cesium.camera
      var fov = camera.frustum.fov
      if (fov > Cesium.Math.PI / 20) {
        camera.frustum.fov = fov - 0.05
      };
    }
    $scope.zoomOut = function () {
      var camera = $scope.cesiumObject.cesium.camera
      var fov = camera.frustum.fov
      if (fov < Cesium.Math.PI / 3) {
        camera.frustum.fov = fov + 0.05
      };
    }

  // Switch to solution
    $scope.switchToSolution = function () {
    // Show three modules
    // ------------------
      $scope.cesiumBool = true
      $scope.showStuck = false
      $scope.leafletBool = true
      $scope.osdBool = true
      $scope.showNextButtons = false

    // resize width
      $scope.$apply(function () {
      // Leaflet sizes
        $scope.leafWidth = parseInt($window.innerWidth / 3.2)
      // Openseadragon size
        $scope.osdWidth = $scope.leafWidth
        $scope.osdStyle = {
          'height': String($scope.osdHeight).concat('px'),
          'width': String($scope.osdWidth).concat('px'),
          'background-color': 'grey'
        }
      })
      leafletData.getMap().then(function (map) {
        map.invalidateSize()
      })

      delete $scope.map.legend
      $scope.paths = new Array()

    // Draw leaflet markers
    // --------------------
      $scope.paths = new Array()
      $scope.geojson = {}
      $scope.markers = new Array()
      markerTrue = {
        lat: $scope.image.lat,
        lng: $scope.image.lng,
        icon: local_icons.green_icon
      }
      $scope.markers.push(markerTrue)
      markerComp = {
        lat: $scope.computedCamera.latitude,
        lng: $scope.computedCamera.longitude,
        icon: local_icons.red_icon
      }
      $scope.markers.push(markerComp)
    // Draw distance
      var distLine = {
        color: 'red',
        dashArray: '20,15',
        lineJoin: 'round',
        weight: 4,
        latlngs: [
          { lat: markerTrue.lat, lng: markerTrue.lng },
          { lat: markerComp.lat, lng: markerComp.lng}
        ]
      }
      $scope.paths.push(distLine)
    // Zoom on markers
      leafletData.getMap().then(function (map) {
        var m1 = L.latLng(markerTrue.lat, markerTrue.lng)
        var m2 = L.latLng(markerComp.lat, markerComp.lng)
        var bounds = L.latLngBounds(m1, m2)
        map.fitBounds(bounds)
      })
      $scope.poseComputed = true
    // Open modal
      $scope.openPoseComputed()
    }
  // Compute camera orientation
    $scope.computePose = function () {
    // Get virtual camera parameters
    // -----------------------------
    // Get camera location and orientation
      var camCartesian = $scope.cesiumObject.cesium.scene.camera.position
      var camCarto = Cesium.Cartographic.fromCartesian(camCartesian)
      var camLong = Cesium.Math.toDegrees(camCarto.longitude)
      var camLat = Cesium.Math.toDegrees(camCarto.latitude)

    // Get camera projected coordinates
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var projCoord = proj4(WGS84proj, ch1903plusproj, [camLong, camLat])
      var trueXY = proj4(WGS84proj, ch1903plusproj, [$scope.image.lng, $scope.image.lat])

    // Provide aprior values
      var camXYZ = [projCoord[0], projCoord[1], camCarto.height]
      var camPitch = 0
      var camHeading = $scope.providedCamera.heading
      var camRoll = 0

    // Set apriori parameters
    // ----------------------
      var imageWidth = $scope.mysd.viewer.source.Image.Size.Width
      var imageHeight = $scope.mysd.viewer.source.Image.Size.Height
    // focal in pixel, set to a normal focal: focal = diagonal
      var f = Math.sqrt(Math.pow(imageWidth, 2) + Math.pow(imageHeight, 2))
    // Principal point
      var cx = imageWidth / 2.0 // half column size
      var cy = imageHeight / 2.0 // half height size
    // location of the camera
      var X0 = camXYZ[0]
      var Y0 = camXYZ[1]
      var Z0 = camXYZ[2]
    // Camera angles
      angles = cesium_to_LM_angles(camHeading, camPitch, camRoll)
      var az = angles[0]
      var tilt = angles[1]
      var roll = angles[2]

    // Generate input of the LM algorithm
      var pApriori = [X0, Y0, Z0, az, tilt, roll, f, cx, cy] // Vector of the camera orientation
    // The orientation and focal are optimized
      var boolUnknowns = [true, true, true, true, true, true, true, false, false]
    // Get GCPs
      var GCPs = $scope.gcpArray
    // Compute camera orientation
      var p = estimatePoseLM(pApriori, boolUnknowns, GCPs)
      $scope.p = p

    // Check altitude
    // --------------
    // Coordinate transformation
      var WGS84proj = 'EPSG:4326'
      var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
      var longLat = proj4(ch1903plusproj, WGS84proj, [p[0], p[1]])

      var position = [Cesium.Cartographic.fromDegrees(longLat[0], longLat[1])]
      var promise = Cesium.sampleTerrain($scope.cesiumObject.cesium.terrainProvider, 11, position)
      Cesium.when(promise, function (updatedPosition) {
        var demZ = updatedPosition[0].height + 5
        var compZ = $scope.p[2]
        if (compZ < demZ) {
        // Generate input of the LM algorithm
          $scope.p[2] = demZ
          var boolUnknowns = [false, false, false, true, true, true, true, false, false]
        // Compute camera orientation force heiht
        // --------------------------
          $scope.p = estimatePoseLM($scope.p, boolUnknowns, GCPs)
        }

      // Compute error for the game
      // --------------------------
        Xcomputed = $scope.p[0]
        Ycomputed = $scope.p[1]
        Xtrue = trueXY[0]
        Ytrue = trueXY[1]
        dist = Math.sqrt((Xcomputed - Xtrue) * (Xcomputed - Xtrue) + (Ycomputed - Ytrue) * (Ycomputed - Ytrue))
        $scope.distance = dist.toFixed(0)
      // Compute p
        maxDist = 500
        maxScore = $scope.gameSteps.curStep.scoreMax
        score = Math.round(-maxScore / maxDist * dist + maxScore)
        if (score < 0) {
          score = 0
        }
      // Update score
        $scope.score.partial = score
        $scope.score.distancePenalty = $scope.gameSteps.curStep.scoreMax - score
        $scope.score.final = $scope.score.final + (-maxScore + score)
        if ($scope.score.final < 0) {
          $scope.score.final = 0
        }

      // Set camera location
      // -------------------
        var p = $scope.p
      // Coordinate transformation
        var WGS84proj = 'EPSG:4326'
        var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
        var longLat = proj4(ch1903plusproj, WGS84proj, [p[0], p[1]])
        $scope.computedCamera.longitude = longLat[0]
        $scope.computedCamera.latitude = longLat[1]
        $scope.computedCamera.height = p[2]

        var cartesian = Cesium.Cartesian3.fromDegrees(longLat[0], longLat[1], p[2])

        var angles = LM_to_cesium_angles(p[3], p[4], p[5])
        var newHeading = angles[0]
        var newTilt = angles[1]
        var newRoll = angles[2]
        $scope.computedCamera.heading = angles[0]
        $scope.computedCamera.tilt = angles[1]
        $scope.computedCamera.roll = angles[2]

      // Update camera
      // -------------
        var camera = $scope.cesiumObject.cesium.camera
        camera.frustum.fov = Cesium.Math.PI / 3
        camera.flyTo({
          destination: cartesian,
          orientation: {
            heading: Cesium.Math.toRadians($scope.computedCamera.heading),
            pitch: Cesium.Math.toRadians($scope.computedCamera.tilt),
            roll: Cesium.Math.toRadians($scope.computedCamera.roll)
          }
        })

      // Compute error in image coordinate
      // ---------------------------------
        var res = computeError(p, GCPs)
        var dxy = res[1]
        var xy_reproj = res[0]
        var x_reproj = math.squeeze(xy_reproj[0])
        var y_reproj = math.squeeze(xy_reproj[1])

      // Draw reprojection
        var nGCP = $scope.gcpArray.length
        for (var i = 0; i < nGCP; i++) {
          var id = $scope.gcpArray[i].id + 200
        // Get image coordinates
          var xCur = math.subset(x_reproj, math.index(i))
          var yCur = math.subset(y_reproj, math.index(i))
          var viewportPoint = $scope.mysd.viewer.viewport.imageToViewportCoordinates(xCur, yCur)
        // Remove existing reproj
          $scope.mysd.viewer.removeOverlay(id.toString())
        // Draw reprojected points
        // osdService.drawReprojections($scope.mysd, xy_reproj, $scope.gcpArray)
        };
        $scope.switchToSolution()
      })// Promise
    } // Compute pose

  // Open modal when canton is found
    $scope.openCantonFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'cantonFound.html',
        bindToController: true,
        controller: 'cantonFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })

    // Promise is closed
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[2]

      // Update score
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax

        var abrev = $scope.selectedCanton.properties.abrev.toLowerCase()
      // Get geojsons
        $.getJSON('../geodata/grid_' + abrev + '.geojson', function (json) {
          $scope.grid = {
            name: 'Grid',
            type: 'geoJSONShape',
            data: json,
            style: {
              color: 'black',
              fillColor: 'yellow',
              weight: 4.0,
              opacity: 1,
              fillOpacity: 0.1
            }
          }
          $scope.geojson.cantons = {}
          $scope.geojson.grid = $scope.grid
        })
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

  // Open modal when cell is found
    $scope.openCellFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'cellFound.html',
        bindToController: true,
        controller: 'cellFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[3]
      // Update score
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
      // Show only selected cell
        $scope.geojson.grid.data = $scope.selectedCell
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

  // Open modal when location is found
    $scope.openLocationFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'locationFound.html',
        bindToController: true,
        controller: 'locationFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
      // Update step
        $scope.gameSteps.curStep = $scope.gameSteps.list[4]
      // Update score
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
        $scope.geojson = {}
        $scope.markers = new Array()
      // Draw marker
        $scope.markers.push($scope.lastMarker)
      // Create path for direction
        $scope.paths = new Array()
        var direction = {
          color: 'red',
          weight: 8,
          latlngs: [
            { lat: $scope.lastMarker.lat, lng: $scope.lastMarker.lng },
            { lat: $scope.lastMarker.lat - 0.03, lng: $scope.lastMarker.lng}
          ]
        }
        $scope.paths.push(direction)
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

  // Open modal when direction is found
    $scope.openDirectionFound = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'directionFound.html',
        bindToController: true,
        controller: 'directionFoundCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })
    // Hide leaflet, show cesium
      $scope.cesiumBool = true
      $scope.showStuck = true
      $scope.leafletBool = false
    // // Set camera
    //   var position = [Cesium.Cartographic.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude)]
    // // Get camera altitude
    //   var promise = Cesium.sampleTerrain($scope.cesiumObject.cesium.terrainProvider, 11, position)
    //   Cesium.when(promise, function (updatedPosition) {
    //     $scope.providedCamera.altitude = $scope.image.height + 100// updatedPosition[0].height+100;
    //     cesiumService.relaxCollisionDetection($scope.cesiumObject.cesium)
    //   // Set camera location
    //     var camera = $scope.cesiumObject.cesium.camera
    //     camera.setView({
    //       destination: Cesium.Cartesian3.fromDegrees($scope.providedCamera.longitude, $scope.providedCamera.latitude, $scope.providedCamera.altitude),
    //       orientation: {
    //         heading: Cesium.Math.toRadians($scope.providedCamera.heading),
    //         pitch: 0,
    //         roll: 0
    //       }
    //     })
    //   })
    // // Fix camera location
    //   var scene = $scope.cesiumObject.cesium.scene
    //   scene.screenSpaceCameraController.enableRotate = false
    //   scene.screenSpaceCameraController.enableTranslate = false
    //   scene.screenSpaceCameraController.enableTilt = false
    //   scene.screenSpaceCameraController.enableZoom = false
    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
        $scope.gameSteps.curStep = $scope.gameSteps.list[5]
      // Update score
        $scope.score.partial = $scope.gameSteps.curStep.scoreMax
      // Create empty GCP
        var GCP = {
          id: $scope.myGCPs.curGCPid,
          x: null,
          y: null,
          X: null,
          Y: null,
          Z: null,
          eM: null,
          ePix: null
        }
      // Push it in the dictionary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      // Select the current GCP
        $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

  // Open modal when camera orientation is computed
    $scope.openPoseComputed = function () {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: 'poseComputed.html',
        bindToController: true,
        controller: 'poseComputedCtrl',
        resolve: {
          distance: function () {
            return $scope.distance
          },
          nErrors: function () {
            return $scope.gameSteps.curStep.scoreMax - $scope.score.partial
          },
          final: function () {
            return $scope.score.final
          },
          distancePenalty: function () {
            return $scope.score.distancePenalty
          },
          congratulation: function () {
            return $scope.congratulation
          },
          texts: function () {
            return $scope.texts
          },
          lang: function () {
            return $scope.lang
          }
        }
      })

    // Promise
      modalInstance.closed.then(function () {
      // Reinitialize find
        $scope.gameSteps.find = false
        $scope.isCollapsed = true
        $scope.message = ''
        $scope.cssMessage.current = $scope.cssMessage.neutral
        $scope.gameSteps.curStep = $scope.gameSteps.list[5]
      // Update score
      // $scope.score.partial = $scope.gameSteps.curStep.scoreMax;
        $scope.poseComputed = true
      // Create empty GCP
        var GCP = {
          id: $scope.myGCPs.curGCPid,
          x: null,
          y: null,
          X: null,
          Y: null,
          Z: null,
          eM: null,
          ePix: null
        }
      // Push it in the dictionary
        $scope.myGCPs.dictGCPs[$scope.myGCPs.curGCPid] = GCP
      // Select the current GCP
        $scope.myGCPs.selectedGCPid = $scope.myGCPs.curGCPid
      }, function () {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

  // If skip step is clicked
    $scope.skipStep = function () {
      var idStep = $scope.gameSteps.curStep.id
      if (idStep == 1) {
      // find the right canton
      // ---------------------
      // loop over the cantons
        console.log('cantons', $scope.geojson.cantons)
        var isInside = false
        var id = 0
        while (isInside == false) {
          var shape = $scope.geojson.cantons.data.features[id]
          console.log('s', shape)
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var multiPoly = turf.multiPolygon(shape.geometry.coordinates)
          isInside = turf.inside(pt, multiPoly)
          id += 1
        }
      // Reduce score
        $scope.score.partial = $scope.score.partial - 10
        $scope.score.final = $scope.score.final - 10

        $scope.switchToGrid(shape, null)
      } else if (idStep == 2) {
      // find the right cell
      // -------------------
      // loop over the cells
        var isInside = false
        var id = 0
        while (isInside == false) {
          var shape = $scope.grid.data.features[id]
          var pt = turf.point([ $scope.image.lng, $scope.image.lat])
          var multiPoly = turf.multiPolygon(shape.geometry.coordinates)
          isInside = turf.inside(pt, multiPoly)
          id += 1
        }
      // Reduce score
        $scope.score.partial = $scope.score.partial - 10
        $scope.score.final = $scope.score.final - 10

        $scope.switchToLocation(shape)
      } else if (idStep == 3) {
      // Reduce score
        $scope.score.partial = $scope.score.partial - 10
        $scope.score.final = $scope.score.final - 10

      // Provide the right location
        var loc = {
          lat: $scope.image.lat,
          lng: $scope.image.lng
        }
        $scope.distance = 0
        $scope.switchToDirection(loc)
      } else if (idStep == 4) {
      // Reduce score
        $scope.score.partial = $scope.score.partial - 10
        $scope.score.final = $scope.score.final - 10
      // Provide the right direction
        $scope.switchToGCP($scope.image.heading, null)
      } else if (idStep == 5) {
      // Go to visit
        location.href = '/mono/?imageId=' + $scope.image.id
      }
    }
  // Monoplotter link
    $scope.goToMono = function () {
      window.open('/map/?imageId=' + $scope.image.id, '_blank')
    }
  // Game link
    $scope.goToGame = function () {
      location.href = '/game/?collectionId=' + $scope.image.collectionId
    }
  // Georeferencer link
    $scope.goToGeoref = function () {
      location.href = '/map/?geolocalized=false'//georef/?collectionId=' + $scope.image.collectionId
    }
  // Facebook sharing
    $scope.shareFb = function () {
      FB.ui(
        {
          method: 'share',
          href: 'https://smapshot.heig-vd.ch/game/?imageId=' + $scope.image.id
        }, function (response) {
        console.log('response fb', response)
      })
    }
  }])

// Modal controllers
// -----------------
gameApp.controller('initialCtrl', function ($scope, $uibModalInstance, texts, lang) {
  $scope.texts = texts
  $scope.lang = lang
  $scope.closeInitial = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('cantonFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, texts, lang) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.texts = texts
  $scope.lang = lang
  $scope.closeCantonFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('cellFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, texts, lang) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.texts = texts
  $scope.lang = lang
  $scope.closeCellFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('locationFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, texts, lang) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.texts = texts
  $scope.lang = lang
  $scope.closeLocationFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('directionFoundCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, texts, lang) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.texts = texts
  $scope.lang = lang
  $scope.closeDirectionFound = function () {
    $uibModalInstance.close()
  }
})

gameApp.controller('poseComputedCtrl', function ($scope, $uibModalInstance, distance, nErrors, final, distancePenalty, congratulation, texts, lang) {
  $scope.final = final
  $scope.nErrors = nErrors
  $scope.distance = distance
  $scope.distancePenalty = distancePenalty
  $scope.congratulation = congratulation
  $scope.texts = texts
  $scope.lang = lang
  $scope.closePoseComputed = function () {
    $uibModalInstance.close()
  }
})
