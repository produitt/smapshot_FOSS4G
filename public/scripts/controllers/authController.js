angular.module('authApp')

.controller('loginCtrl', function ($scope, $http, $auth, $window, $q, AuthService) { //, $state
  // texts
  // -----
  // Set initial language
  var lang = $window.navigator.language || $window.navigator.userLanguage
  lang = lang.slice(0, 2)
  if (lang === 'fr') {
    $scope.lang = 'fr'
  } else if (lang === 'it') {
    $scope.lang = 'it'
  } else if (lang === 'de') {
    $scope.lang = 'de'
  } else {
    $scope.lang = 'en'
  }

  // Language switchers
  $scope.switchToFrench = function () {
    $scope.lang = 'fr'
  }
  $scope.switchToGerman = function () {
    $scope.lang = 'de'
  }
  $scope.switchToEnglish = function () {
    $scope.lang = 'en'
  }
  $scope.switchToItalian = function () {
    $scope.lang = 'it'
  }
  // Load the text for the app
  // Load text as a promise
  $scope.loadText = function () {
    return $q(function (resolve, reject) {
      $http.get('../texts/login.json', {
        headers: {
          'Accept': 'application/json;charset=utf-8'
        }
      })
      .then(
        function (res) {
          resolve(res.data.texts)
        },
        function (res) {
          reject('Error during the loading of the text')
        }
      )
    })
  }

  $scope.textPromise = $scope.loadText()

  // Initialize volunteer model
  $scope.volunteer = {
    id: '',
    password: '',
    username: '',
    email: '',
    googleId: '',
    facebookId: '',
    letter: false,
    password: '',
    password_repeat: '',
    gRecaptchaResponse: null
  }
  // Initialize booleans
  $scope.showSocialLogin = true
  $scope.localAuthentication = false
  $scope.formIsValid = false
  $scope.showRegister = false
  $scope.showLogin = false

  // If volunteer values change
  $scope.textPromise.then(function (texts) {
    $scope.texts = texts
    $scope.$watch('volunteer', function () {
      $scope.verifyVolunteer()
    }, true)
  })

  // Error message for the form
  $scope.verifyVolunteer = function () {
    $scope.formIsValid = true
    $scope.rejectMessage = ''
    if ($scope.volunteer.password !== ''){

      if ($scope.volunteer.password.length < 6) {
        $scope.formIsValid = false
        $scope.rejectMessage = $scope.texts.pwdShort[$scope.lang]

      }else if ($scope.volunteer.password !== $scope.volunteer.password_repeat) {

        $scope.formIsValid = false
        $scope.rejectMessage = $scope.texts.pwdDiff[$scope.lang]

      }
      else if ($scope.volunteer.gRecaptchaResponse === null){

        $scope.formIsValid = false
        $scope.rejectMessage = $scope.texts.emptyCaptcha[$scope.lang]

      }else{
        $scope.formIsValid = true
      }

    }else{
      $scope.formIsValid = false
    }
  }

  // Local login is clicked
  $scope.enableLocalAuthentication = function () {
    $scope.localAuthentication = true
    $scope.showSocialLogin = false
  }

  // The user register a new account
  $scope.enableRegister = function () {
    // $scope.localAuthentication = false;
    $scope.showRegister = true
    $scope.localAuthentication = false
  }

  // The user already have a login
  $scope.enableLogin = function () {
    $scope.showLogin = true
    $scope.localAuthentication = false
  }

  // Authentication with google or facebook
  $scope.authenticateSocial = function (provider, userData) {
    $auth.setStorageType('sessionStorage')// trick to avoid that the token created by satellizer corrupts the one send by the server.
    $auth.authenticate(provider).then(function (response) {
      // Signed in with Google.
      if (!response.data.success){
        alert($scope.texts.emailExists[$scope.lang])
      }else{
        AuthService.storeUserCredentials(response.data.token)
        $window.close()
      }
    })
    .catch(function (response) {
      // Something went wrong.
      console.log('not signed in with google', response, userData)
    })
  }

  // Local login
  $scope.login = function () {
    AuthService.login($scope.volunteer).then(function (msg) {
      $window.open('/mysmapshot', '_blank')
      $window.close()
    }, function (errMsg) {
      alert($scope.texts.loginFail[$scope.lang])
    })
  }

  $scope.resetPassword = function () {
    var promise = AuthService.resetPassword($scope.volunteer)
    promise.then(function (response) {
      alert($scope.texts.resetSuccess[$scope.lang])
    }, function () {
      alert($scope.texts.resetFails[$scope.lang])
    })
  }

  // Local registration
  $scope.register = function () {
    $scope.verifyVolunteer()
    if ($scope.formIsValid) {
      AuthService.register($scope.volunteer).then(function (response) {
        AuthService.storeUserCredentials(response.token)
        $window.close()
        // window.history.back()
      }, function (errMsg) {
        if (errMsg === 'Email already exists.') {
          alert($scope.texts.emailExists[$scope.lang])
          $window.location.reload()
        } else if (errMsg === 'Email is not valid') {
          alert($scope.texts.emailNotValid[$scope.lang])
          $window.location.reload()
        }
      })
    }
  }

  $scope.back = function () {
    $scope.showSocialLogin = true
    $scope.localAuthentication = false
    $scope.formIsValid = false
    $scope.showRegister = false
    $scope.showLogin = false
  }

  // if recaptcha change
  $scope.$watch('volunteer.gRecaptchaResponse', function () {
    $scope.expired = false
    if (($scope.volunteer.gRecaptchaResponse !== undefined) && ($scope.volunteer.gRecaptchaResponse !== '')) {
      $scope.formIsValid = true
    }
  })
})

.controller('registerCtrl', function ($scope, AuthService) { //, $state
  $scope.volunteer = {
    id: '',
    password: ''
  }
})

.controller('insideCtrl', function ($scope, AuthService, API_ENDPOINT, $http) { //, $state
  $scope.destroySession = function () {
    AuthService.logout()
  }

  $scope.getInfo = function () {
    if (AuthService.isAuthenticated()) {
      $http.get(API_ENDPOINT.url + '/memberinfo').then(function (result) {
        $scope.memberinfo = result.data.msg
      })
    } else {
      console.log('not logged')
    }
  }

  // Logout
  $scope.logout = function () {
    AuthService.logout()
  }
})

.controller('AppCtrl', function ($scope, $state, AuthService, AUTH_EVENTS) {
  $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
    AuthService.logout()
    $state.go('outside.login')
    var alertPopup = alert({
      title: 'Session Lost!',
      template: 'Sorry, You have to login again.'
    })
  })
})
