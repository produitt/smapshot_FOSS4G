contactApp.controller('contactCtrl', ['$scope', '$http', '$window', 'AuthService', function ($scope, $http, $window, AuthService) {
  // texts
  // -----
  // Set initial language
  var lang = $window.navigator.language || $window.navigator.userLanguage
  lang = lang.slice(0, 2)
  if (lang == 'fr') {
    $scope.lang = 'fr'
  } else if (lang == 'it') {
    $scope.lang = 'it'
  } else if (lang == 'de') {
    $scope.lang = 'de'
  } else {
    $scope.lang = 'en'
  }
  // $scope.lang = 'fr'
  $scope.hTexts = {
    'login': {
      'fr': "M'identifier"
    }
  }
  $scope.myName = $scope.hTexts.login[$scope.lang]

  // Language switchers
  $scope.switchToFrench = function () {
    $scope.lang = 'fr'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToGerman = function () {
    $scope.lang = 'de'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToEnglish = function () {
    $scope.lang = 'en'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  $scope.switchToItalian = function () {
    $scope.lang = 'it'
    if (AuthService.isAuthenticated() == false) {
      $scope.myName = $scope.hTexts.login[$scope.lang]
    }
  }
  //Load the text for the app
  $http.get("../texts/contact.json",{
    headers : {
      "Accept":"application/json;charset=utf-8",
    }
  })
  .success(function (data, headers) {
    $scope.texts = data.texts
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  });
  //Load text for the header
  $http.get("../texts/header.json",{
    headers : {
      "Accept":"application/json;charset=utf-8",
    }
  })
  .success(function (data, headers) {
    $scope.hTexts = data.texts
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })

  // Functions for the navbar (require AuthService)
  // ------------------------
  $scope.getMyName = function () {
    if (AuthService.isAuthenticated()) {
      $http.get('/volunteers/memberinfo').then(function (result) {
        $scope.volunteer = result.data.volunteer
        $scope.myName = $scope.volunteer.username
        if ($scope.volunteer.lang){
          $scope.lang = $scope.volunteer.lang
        }
      })
    } else {
      $scope.myName = "M'identifier"
    }
  }
  $scope.getMyName()

  $scope.clickMy = function () {
    if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
      location.href = '/mysmapshot/'
    } else {
      // Go to login
      var win = $window.open('/login', '_blank')
    }
  }
}])
