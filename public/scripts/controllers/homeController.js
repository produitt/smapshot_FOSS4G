homeApp.controller('homeCtrl', ['$scope', '$http', '$window', '$sce',
'AuthService', 'imageQueryService', 'volunteersService', 'ownerQueryService',
'collectionsQueryService', 'langService',
  function ($scope, $http, $window, $sce, AuthService, imageQueryService,
    volunteersService, ownerQueryService, collectionsQueryService, langService
  ){

  $scope.owner = {
    id : 4
  }

  // lang
  // ----
  // Set initial language
  $scope.lang = langService.getBrowserLang()
  // Switch the lang
  $scope.switchLang = function (lang) {
    $scope.lang = lang
  }
  // Get languages model
  $scope.langs = langService.getLangs()

  // $scope.hTexts = {
  //   'login': {
  //     'fr': "M'identifier"
  //   }
  // }

  // Load the text for the app
  $http.get('../texts/home.json', {
    headers: {
      'Accept': 'application/json;charset=utf-8'
    }
    })
  .success(function (data, headers) {
    $scope.texts = data.texts
    $scope.getMyName()
  })
  .error(function (data, status, headers, config) {
    console.log('error loading json')
  })
  // Get current owner and collection
  // ---------------------------------
  $scope.collection = {
    id: null,
    description: null,
    name: null
  }
  $scope.owner = {
    id: null,
    name: null,
    currentCollectionId: 7
  }
  // Functions for the navbar (require AuthService)
  // ------------------------
    $scope.getMyName = function () {
      if (AuthService.isAuthenticated()) {
        $http.get('/volunteers/memberinfo').then(function (result) {
          $scope.volunteer = result.data.volunteer
          $scope.myName = $scope.volunteer.username
          if ($scope.volunteer.lang){
            $scope.lang = $scope.volunteer.lang
          }
        })
      } else {
        $scope.myName = $scope.texts.login[$scope.lang]
      }
    }
    // $scope.getMyName()

    $scope.clickMy = function () {
      if (AuthService.isAuthenticated()) {
      // Go to mysMapShot
        location.href = '/mysmapshot/'
      } else {
      // Go to login
        var win = $window.open('/login', '_blank')
        var timer = setInterval(checkChild, 500)
        function checkChild () {
          if (win.closed) {
            clearInterval(timer)
            AuthService.loadUserCredentials()
            $scope.getMyName()
            //$window.location.reload()
          }
        }
      }
    }

    // Queries send at initialization
    // ------------------------------
    $scope.startQueries = function () {

      ownerQueryService.getCurrent().then(function (res){
        console.log('res get current', res)
        $scope.owner.id = res.id
        $scope.owner.name = res.name
        $scope.selectedOwnerName = res.name
        $scope.owner.url_name = res.url_name
        $scope.owner.currentCollectionId = res.challenge_collection

        // Get the description and ratio of current collection
        collectionsQueryService.getDescription($scope.owner.currentCollectionId).then(function (res) {
          var descriptions = JSON.parse(res.long_descr)
          $scope.collection.descriptions = descriptions
          $scope.collection.description = descriptions[$scope.lang]
          $scope.collection.name = res.name
          $scope.collection.best3 = res.best3
          $scope.collection.ratioGeoref = Math.round(parseFloat(res.nGeoref)/parseFloat(res.nImages)*1000)/10

          // Display ratio infos
          var bar = document.getElementById('challenge-bar')
          if ($scope.collection.ratioGeoref < 5) {
            bar.style.width = '5%'
          } else {
            bar.style.width = $scope.collection.ratioGeoref + '%'
          }

          // Get statistics of current collection? owner?
          ownerQueryService.getStats($scope.owner.id) // )
          .then(function (res) {
            // Process and display stats
            $scope.ratioGeorefOwner = Math.round(res.ngeoref / res.nimages * 1000)/10
            $scope.nImagesOwner = res.nimages
            $scope.nVolunteers = res.nvolunteers
            // search data for the ranking
            // var searchData = {
            //   ownerId: $scope.ownerId,
            //   startDate: $scope.startDate,
            //   stopDate: $scope.endDate,
            //   photographerId: null,
            //   collectionId: null
            // }
            var ownerId = null
            collectionsQueryService.getCollections(ownerId).then(function (res){
              $scope.collectionsTab = res
            })
          }) // get owner stats
        }) // get description
      }) // get current owner
    } // start query
    $scope.startQueries()

    // Collection list
    // ----------------
    $scope.openCloseCollection = function (c) {
      c.showMore ^= true
    }
    $scope.nCollectionsToShow = 5
    $scope.moreCollections = false
    $scope.showMoreCollections = function () {
      $scope.moreCollections = true
      $scope.nCollectionsToShow = $scope.collectionsTab.length
    }
    $scope.showLessCollections = function () {
      $scope.moreCollections = false
      $scope.nCollectionsToShow = 5
    }
    // Go to owner
    $scope.goToSelectedOwner = function (c) {
      location.href = '/'+c.url_name
    }
    // Go to map
    $scope.goToMapCollection = function (c) {
      if (c.url_name){
        location.href = '/map/'+c.url_name+'/?collectionId='+c.id
      }else{
        location.href = '/map/?collectionId='+c.id
      }
    }
    // Go to georeferencing
    $scope.goToGeorefCollection = function (c) {
      if (c.url_name){
        location.href = '/map/'+c.url_name+'/?geolocalized=false&collectionId='+c.id
      }else{
        location.href = '/map/?geolocalized=false&collectionId='+c.id
      }
      //location.href = '/map/?geolocalized=false&collectionId='+c.id // /georef/'// +$scope.ownerUrlName
    }
    // Links
    $scope.goToOwner = function () {
      location.href = '/' + $scope.owner.url_name
    }
    $scope.goToContribute = function () {
      location.href = '/contact/'
    }
    $scope.goToVisit = function () {
      location.href = '/mono/?collectionId=' + $scope.challengeCollection.id
    }
    // Go to map
    $scope.goToChallengeMap = function () {
      location.href = '/map/'+$scope.owner.url_name+'/?collectionId='+$scope.owner.currentCollectionId
    }
    // Go to georeferencing
    $scope.goToChallengeGeoref = function () {
      location.href = '/map/'+$scope.owner.url_name + '/?geolocalized=false&collectionId='+$scope.owner.currentCollectionId // /georef/'// +$scope.ownerUrlName
    }
    $scope.goToMap = function () {
      location.href = '/map/'// '?collectionId='+$scope.challengeCollection.id;//'; //faire passer l'id de la collection?
    }
    $scope.goToMono = function () {
      location.href = '/mono/?collectionId=' + $scope.challengeCollection.id
    }
    $scope.goToGame = function () {
      location.href = '/game/'
    }
    $scope.goToGeoref = function () {
      location.href = '/map/?geolocalized=false'
    }
    $scope.goToOwnerGeoref = function () {
      location.href = '/map/'+$scope.owner.url_name+'/?geolocalized=false'
    }
    $scope.goToOwnerMap = function () {
      location.href = '/map/'+$scope.owner.url_name+'/'
    }
    $scope.goToNews = function () {
      location.href = '/news/'
    }
  }])
