var newsApp = angular.module('newsApp', ['authApp', 'ngSanitize'])

newsApp.config(function ($logProvider, $locationProvider) {
  $locationProvider.html5Mode(true)
})
