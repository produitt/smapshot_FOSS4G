var georefApp = angular.module('georefApp', ['ui-leaflet', 'ui.openseadragon',
'angularCesium', 'ngSanitize', 'ngCsv',
'ui.bootstrap', 'authApp', 'topoApp', 'cesiumApp', 'osdApp', 'dbApp', 'searchApp',
'moveApp', 'georefStepsApp', 'langApp', 'textApp', 'correctionsApp', 'remarksApp',
'observationsApp', 'problemsApp', 'errorsApp', 'validationsApp', 'imageQueryApp',
'angularResizable', 'ng.deviceDetector']) // ,, 'ngAnimate'"ngFileSaver" , "bootstrap.fileField", 'leaflet-directive'

// georefApp.directive('fileChange', function() {
//     return {
//      restrict: 'A',
//      scope: {
//        handler: '&'
//      },
//      link: function (scope, element) {
//       element.on('change', function (event) {
//         scope.$apply(function(){
//           scope.handler({files: event.target.files});
//         });
//       });
//      }
//     };
// });

georefApp.config(function ($logProvider, $locationProvider) {
  $logProvider.debugEnabled(false)
  $locationProvider.html5Mode(true)
})
