var helpApp = angular.module('helpApp', ["ngSanitize", "ui.bootstrap",
'authApp', 'searchApp', 'textApp', 'langApp']);

helpApp.config(function ($locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!')
})
