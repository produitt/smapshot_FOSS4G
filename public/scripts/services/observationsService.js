angular.module('observationsApp')

.service('observationsService', function ($q, $http) {

  // Push observation in the database
  var submitObservation = function (imageId, volunteerId, observation) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      observation: observation // contains: text, coord_x, coord_y, height, width
    }
    var postObservationPromise = $http.post('/observations/submit', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postObservationPromise
  }

  // Get observations which werent checked
  var getDashboard = function (sendData) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/observations/getDashboard/', sendData)
      httpPromise.then(function (res) {
        var listObservations = res.data
        var n = listObservations.length
        for (var i = 0; i < n; i++) {
          listObservations[i].thumbSrc = '../data/collections/'+listObservations[i].collection_id+'/images/thumbnails/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].src1500 = '../data/collections/'+listObservations[i].collection_id+'/images/1500/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].date_string = listObservations[i].date_observation.slice(0,16).replace('T', ' ')
          // listObservations[i].date_observation = new Date(listObservations[i].date_observation)
          // var day = listObservations[i].date_observation.getDate()
          // var month = listObservations[i].date_observation.getMonth() + 1
          // var year = listObservations[i].date_observation.getFullYear()
          // listObservations[i].date_string = day + '.' + month + '.' + year
          // if (listObservations[i].coord_x === null){
          //   listObservations[i].type = 'general'
          // }else{
          //   listObservations[i].type = 'local'
          // }
        }
        resolve(listObservations)
      })
    })
  }

  var getDataForDownload = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/observations/getForDownload/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        console.log('date for download', data)
        resolve(data)
      })
    })
  }
  // var getToValidateObservations = function (ownerId) {
  //   return $q(function (resolve, reject) {
  //     var sendData = {
  //       ownerId: ownerId
  //     }
  //     var httpPromise = $http.post('/observations/getToValidateObservations/', sendData)
  //     httpPromise.then(function (res) {
  //       var listObservations = res.data
  //       var n = listObservations.length
  //       for (var i = 0; i < n; i++) {
  //         listObservations[i].thumbSrc = '../data/collections/'+listObservations[i].collection_id+'/images/thumbnails/'.concat(listObservations[i].image_id).concat('.jpg')
  //         listObservations[i].src1500 = '../data/collections/'+listObservations[i].collection_id+'/images/1500/'.concat(listObservations[i].image_id).concat('.jpg')
  //         listObservations[i].date_observation = new Date(listObservations[i].date_observation)
  //         var day = listObservations[i].date_observation.getDate()
  //         var month = listObservations[i].date_observation.getMonth() + 1
  //         var year = listObservations[i].date_observation.getFullYear()
  //         listObservations[i].date_string = day + '.' + month + '.' + year
  //         if (listObservations[i].coord_x === null){
  //           listObservations[i].type = 'general'
  //         }else{
  //           listObservations[i].type = 'local'
  //         }
  //       }
  //       resolve(listObservations)
  //     })
  //   })
  // }

  // Get my observations
  var getMy = function (volunteerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        volunteerId: volunteerId
      }
      var httpPromise = $http.post('/observations/getMy/', sendData)
      httpPromise.then(function (res) {
        var listObservations = res.data
        var listCollections = []
        var listOwners = []

        var n = listObservations.length
        for (var i = 0; i < n; i++) {
          listObservations[i].thumbSrc = '../data/collections/'+listObservations[i].collection_id+'/images/thumbnails/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].src500 = '../data/collections/'+listObservations[i].collection_id+'/images/500/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].date = new Date(listObservations[i].date_observation)
          var day = listObservations[i].date.getDate()
          var month = listObservations[i].date.getMonth() + 1
          var year = listObservations[i].date.getFullYear()
          listObservations[i].date_string = day + '.' + month + '.' + year

          if (!listObservations[i].processed){
            listObservations[i].status = 'waiting'
          }else {
            if(listObservations[i].validated){
              listObservations[i].status = 'validated'
            }else{
              listObservations[i].status = 'rejected'
            }
          }


          if (i == 0) {
            var minDate = new Date(listObservations[i].date_observation)
            var maxDate = new Date(listObservations[i].date_observation)
          }
          if (new Date(listObservations[i].date_observation) > maxDate) {
            maxDate = new Date(listObservations[i].date_observation)
          }
          if (new Date(listObservations[i].date_observation) < minDate) {
            minDate = new Date(listObservations[i].date_observation)
          }

          if (listCollections.indexOf(listObservations[i].collectionname) == -1){
            listCollections.push(listObservations[i].collectionname)
          }
          if (listOwners.indexOf(listObservations[i].ownername) == -1){
            listOwners.push(listObservations[i].ownername)
          }
        }
        res = {
          listObservations: listObservations,
          listOwners: listOwners,
          listCollections: listCollections,
          minDate: minDate,
          maxDate: maxDate
        }
        resolve(res)
      })
    })
  }

  // Get acceptedd observations
  var getImageObservations = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: imageId
      }
      var httpPromise = $http.post('/observations/getImageObservations/', sendData)
      httpPromise.then(function (res) {
        var listObservations = res.data
        var n = listObservations.length
        for (var i = 0; i < n; i++) {
          listObservations[i].thumbSrc = '../data/collections/'+listObservations[i].collection_id+'/images/thumbnails/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].src1500 = '../data/collections/'+listObservations[i].collection_id+'/images/1500/'.concat(listObservations[i].image_id).concat('.jpg')
          listObservations[i].date_observation = new Date(listObservations[i].date_observation)
          var day = listObservations[i].date_observation.getDate()
          var month = listObservations[i].date_observation.getMonth() + 1
          var year = listObservations[i].date_observation.getFullYear()
          listObservations[i].date_string = day + '.' + month + '.' + year
        }
        resolve(listObservations)
      })
    })
  }

  var acceptObservation = function (observation, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        observation: observation,
        validatorId: validatorId
      }
      var httpPromise = $http.post('/observations/accept/', sendData)
      httpPromise.then(function (res) {
        resolve()
      })
    })
  }

  var rejectObservation = function (observation, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        observation: observation,
        validatorId: validatorId
      }
      var httpPromise = $http.post('/observations/reject/', sendData)
      httpPromise.then(function (res) {
        resolve()
      })
    })
  }

  return {
    submitObservation: submitObservation,
    // getToValidateObservations: getToValidateObservations,
    getDashboard: getDashboard,
    acceptObservation: acceptObservation,
    rejectObservation: rejectObservation,
    getImageObservations: getImageObservations,
    getMy: getMy,
    getDataForDownload: getDataForDownload
  }
})
