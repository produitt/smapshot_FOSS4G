angular.module('statsApp')

.service('statsService', function ($q, $http) {

  //Push correction in the database
  var submitProblem = function (imageId, volunteerId, problem) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      problem: problem // contains: title, dateShot, caption
    }
    var postProblemPromise = $http.post('/problems/submit', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postProblemPromise
  }

  return {
    submitProblem: submitProblem
  }
})
