angular.module('validationsApp')

.service('validationsService', function ($q, $http) {

  // push the validation in the database
  var submitValidation = function (imageId, volunteerId, validatorId, validation) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      validatorId: validatorId,
      validation: validation
    }
    var postErrorsPromise = $http.post('/validation/submitValidation', sendData)
    .then(function (result) {
      return result.data
    })
    .catch(function (result) {
      return result
    })
    return postErrorsPromise
  }

  // push the validation in the database
  var createValidation = function (imageId, volunteerId) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId
    }
    var postErrorsPromise = $http.post('/validation/createValidation', sendData)
    .then(function (result) {
      return result.data
    })
    .catch(function (result) {
      return result
    })
    return postErrorsPromise
  }

  var getDashboard = function (searchData) {
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/validations/getDashboard/', searchData)
      httpPromise.then(function (response) {
        console.log('response', response)
        for (var i = 0; i<response.data.length; i++){
          response.data[i].thumbSrc = '../data/collections/'+response.data[i].collection_id+'/images/thumbnails/'.concat(response.data[i].image_id).concat('.jpg')
          response.data[i].src1500 = '../data/collections/'+response.data[i].collection_id+'/images/1500/'.concat(response.data[i].image_id).concat('.jpg')
          // console.log(parseInt(response.data[i].stop_time.slice(0,2)))
          // console.log(parseInt(response.data[i].stop_time.slice(-2)))
          // console.log(parseInt(parseInt(response.data[i].stop_time.slice(0,2)) + parseInt(response.data[i].stop_time.slice(-2))))
          // var h = parseInt(response.data[i].stop_time.slice(0,2)) + parseInt(response.data[i].stop_time.slice(-2))
          // var hString = h.toString()
          response.data[i].date_georef = response.data[i].date_georef.slice(0,10) + ' ' +response.data[i].stop_time.slice(0,5)
        }
        resolve(response.data)
      })
    })
  }

  return {
    submitValidation: submitValidation,
    createValidation: createValidation,
    getDashboard: getDashboard
  }
})
