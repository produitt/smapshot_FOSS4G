angular.module('volunteersQueryApp')

.service('volunteersQueryService', function ($q, $http) {
  // var getRanking = function (searchData) {
  //   return $q(function (resolve, reject) {
  //     console.log('sendData', searchData)
  //     var httpPromise = $http.post('/search/getRanking/', searchData)
  //     httpPromise.then(function (response) {
  //       console.log('response ranking', response)
  //       resolve(response.data.table)
  //     })
  //   })
  // }

  var getVisitorInfo = function (){
    return $q(function (resolve, reject) {
      $http.get('/volunteers/memberinfo').then(function (result) {
        // $scope.visitor = result.data.volunteer
        // $scope.myName = $scope.visitor.username
        resolve(result.data.volunteer)
      })
    })
  }


  return {
    //getRanking: getRanking,
    // getVisitorInfo: getVisitorInfo
  }
})
