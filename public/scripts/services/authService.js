angular.module('authApp')

.service('AuthService', function ($q, $http, $window, API_ENDPOINT) {
  var LOCAL_TOKEN_KEY = 'auth_token'// 'satellizer_token';//sMapShotTokenKey;
  var isAuthenticated = false
  var authToken

  // function loadUserCredentials() {
  var loadUserCredentials = function () {
    try {
      var token = $window.localStorage.getItem(LOCAL_TOKEN_KEY)
      if (token) {
        useCredentials(token)
      } else {
        console.log('token not found')
      }
    } catch (exception) {
      return {}
    }
  }

  var storeUserCredentials = function (token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token)
    useCredentials(token)
  }

  function useCredentials (token) {
    isAuthenticated = true
    authToken = token

    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken
  }

  function destroyUserCredentials () {
    authToken = undefined
    isAuthenticated = false
    $http.defaults.headers.common.Authorization = undefined
    window.localStorage.removeItem(LOCAL_TOKEN_KEY)
  }

  var register = function (user) {
    return $q(function (resolve, reject) {
      $http.post('/auth/register', user).then(function (result) { // API_ENDPOINT.url +
        if (result.data.success) {
          resolve(result.data)
        } else {
          reject(result.data.msg)
        }
      })
    })
  }

  var updatePwd = function (pwds) {
    return $q(function (resolve, reject) {
      $http.post('/volunteers/updatePwd', pwds).then(function (result) {
        if (result.data.success) {
          resolve(result.data)
        } else {
          console.log('reject')
          reject(result.data.msg)
        }
      })
    })
  }

  var update = function (user) {
    return $q(function (resolve, reject) {
      $http.post('/auth/update', user).then(function (result) {
        if (result.data.success) {
          resolve(result.data)
        } else {
          reject(result.data.msg)
        }
      })
    })
  }

  var resetPassword = function (user) {
    return $q(function (resolve, reject) {
      $http.post('/auth/resetPassword', user).then(function (result) {
        if (result.data.success) {
          resolve(result.data)
        } else {
          reject(result.data.msg)
        }
      })
    })
  }

  var login = function (user) {
    return $q(function (resolve, reject) {
      $http.post('/auth/authenticate', user).then(function (result) { // API_ENDPOINT.url +
        if (result.data.success) {
          storeUserCredentials(result.data.token)
          resolve(result.data.msg)
        } else {
          reject(result.data.msg)
        }
      })
    })
  }

  // var isAuthenticatedFu = function () {
  //   return isAuthenticated
  // }

  var logout = function () {
    destroyUserCredentials()
  }

  var getMyName = function () {
    return $q(function (resolve, reject) {
      if (isAuthenticated) { //AuthService.
        $http.get('/volunteers/memberinfo').then(function (result) {
          // $scope.volunteer = result.data.volunteer
          // $scope.myName = $scope.volunteer.username
          // var result = {
          //   volunteer: result.data.volunteer,
          //
          //
          // }
          resolve(result.data.volunteer)
        })
      } else {
        // $scope.myName = "M'identifier"
        //resolve()
        reject()
      }
    })
  }

  var getUserInfo = function () {
    return $q(function (resolve, reject) {
      if (isAuthenticated) {
        $http.get('/volunteers/memberinfo').then(function (result) {
          resolve(result.data.volunteer)
        })
      } else {
      // Go to login
        var win = $window.open('/login', '_blank')
        reject()
      }
    })
  }

  // var submitNewsletters = function (newsletters) {
  //   var sendData = {
  //     newsletters: newsletters
  //   }
  //   var promise = $http.post('/volunteers/submitNewsletters', sendData).then(function (result) {
  //     return promise
  //   })
  // }
  //
  // var submitNotifications = function (notifications) {
  //   var sendData = {
  //     notifications: notifications
  //   }
  //   var promise = $http.post('/volunteers/submitNotifications', sendData).then(function (result) {
  //     return promise
  //   })
  // }
  //
  // var submitUsername = function (username) {
  //   var sendData = {
  //     username: username
  //   }
  //   var promise = $http.post('/volunteers/submitUsername', sendData).then(function (result) {
  //     return promise
  //   })
  // }

  loadUserCredentials()

  return {
    login: login,
    register: register,
    logout: logout,
    storeUserCredentials: storeUserCredentials,
    loadUserCredentials: loadUserCredentials,
    update: update,
    isAuthenticated: function () { return isAuthenticated },
    resetPassword: resetPassword,
    getMyName: getMyName,
    getUserInfo: getUserInfo,
    updatePwd: updatePwd
    // submitNewsletters: submitNewsletters,
    // submitNotifications: submitNotifications,
    // submitUsername: submitUsername
  }
})

.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated
      }[response.status], response)
      return $q.reject(response)
    }
  }
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor')
})
