angular.module('topoApp')

.service('topoService', function ($q, $http) {
  // var LOCAL_TOKEN_KEY = 'auth_token';//'satellizer_token';//sMapShotTokenKey;
  // var isAuthenticated = false;
  // var geojsonToponyms = '';
  var geoTags = []

  // function loadUserCredentials() {
  var getToponyms = function (footprint, latitude, longitude) {
    var sendData = {
      footprint: footprint,
      latitude: latitude,
      longitude: longitude
    }
    // return $q(function(resolve, reject) {
    var topoPromise = $http.post('/topo/getToponyms', sendData).then(function (result) { // API_ENDPOINT.url +
      if (result.data) {
        geojsonToponyms = result.data
        return geojsonToponyms
          // resolve();
      } else {
          // reject();
      }
    })
    return topoPromise
    // });
  }

  var filterToponyms = function (geojsonData, latitude, longitude) {
    var listFeatures = {
      type: 'FeatureCollection',
      features: []
    }
    for (var idFeat in geojsonData.features) {
      var feature = geojsonData.features[idFeat]
      if ((feature.properties.type != 'Hauptgipfel') && (feature.properties.type != 'Alpiner Gipfel') && (feature.properties.type != 'peak') && (feature.properties.type != 'Gipfel')) {
        var point = feature.geometry.coordinates
        var fromPt = turf.point([point[0], point[1]])
        var toPt = turf.point([longitude, latitude])
        var dist = turf.distance(fromPt, toPt) // KM
        if (dist < 6) {
          listFeatures.features.push(feature)
        }
      } else {
        listFeatures.features.push(feature)
      }
    }
    return listFeatures
  }

  var geojsonTo3D = function (geoJsonData, viewer, terrainProvider) {
    // Inputs
    // ------
    // var viewer = $scope.cesium;
    // var terrainProvider = $scope.terrainProvider;

    var positions = []
    var idGeojson = []
    var idPoints = []
    var idPolys = []
    for (var idFeat in geoJsonData.features) {
      // var idFeat = 0;
      var feature = geoJsonData.features[idFeat]

      if (feature.geometry.type == 'Point') {
        var point = feature.geometry.coordinates
        var p = Cesium.Cartographic.fromDegrees(point[0], point[1])
        positions.push(p)
        idGeojson.push(idFeat)
        idPoints.push(null)
        idPolys.push(null)
      } else if (feature.geometry.type == 'LineString') {
        for (idPoint in feature.geometry.coordinates) {
          var point = feature.geometry.coordinates[idPoint]
          var p = Cesium.Cartographic.fromDegrees(point[0], point[1])
          positions.push(p)
          idGeojson.push(idFeat)
          idPoints.push(idPoint)
          idPolys.push(null)
        };
      } else {
        for (idPoly in feature.geometry.coordinates) {
          var poly = feature.geometry.coordinates[idPoly]
          for (idPoint in poly) {
            var point = poly[idPoint]
            var p = Cesium.Cartographic.fromDegrees(point[0], point[1])
            positions.push(p)
            idGeojson.push(idFeat)
            idPoints.push(idPoint)
            idPolys.push(idPoly)
          };
        };
      };
    };

    var promise = Cesium.sampleTerrain(terrainProvider, 11, positions)
    var promise3d = Cesium.when(promise, function (updatedPositions) {
        // positions[0].height and positions[1].height have been updated.
        // updatedPositions is just a reference to positions.
      for (var idC in updatedPositions) {
        var idPoly = idPolys[idC]
        var idPoint = idPoints[idC]
        var idFeat = idGeojson[idC]
        if ((idPoint == null) && (idPoly == null)) {
            // Point geometry
          var feature = geoJsonData.features[idFeat]
          if ((feature.properties.type != 'Hauptgipfel') && (feature.properties.type != 'Alpiner Gipfel')) {
            feature.geometry.coordinates.push(updatedPositions[idC].height)
          } else {
            feature.geometry.coordinates.push(updatedPositions[idC].height - 50)
          }
        } else if (idPoly == null) {
            // Line geometry
          var feature = geoJsonData.features[idFeat]
          feature.geometry.coordinates[idPoint].push(updatedPositions[idC].height)
        } else {
            // Polygon geometry
          var feature = geoJsonData.features[idFeat]
          feature.geometry.coordinates[idPoly][idPoint].push(updatedPositions[idC].height)
        };
      };
        // geojsonToponmys = geoJsonData;
      return geoJsonData
    })
    return promise3d
  }

  var drawToponyms = function (geoJsonData, cesium, tilesLoadedPromise) {
    // Draw toponym on the windows
    var toponyms = {
      geotags: [],
      icons: [],
      geoJsonData: geoJsonData
    }
    // geotags = []

    // Inputs
    // geoJsonData = $scope.toponyms.data
    var promise = tilesLoadedPromise.then(function () {
      for (var idFeat in geoJsonData.features) {
        // var idFeat = 0;
        var feature = geoJsonData.features[idFeat]

        if (feature.geometry.type == 'Point') {
          var point = feature.geometry.coordinates

          var cart = Cesium.Cartesian3.fromDegrees(point[0], point[1], point[2])
          var pt2D = Cesium.SceneTransforms.wgs84ToWindowCoordinates(cesium.scene, cart)

          if ((typeof pt2D !== 'undefined') && (typeof feature.properties.name !== 'undefined')) {
            if ((pt2D.x > 0) && (pt2D.x < cesium.canvas.width) && (pt2D.y > 0) && (pt2D.y < cesium.canvas.height)) {
              var ray = cesium.camera.getPickRay(pt2D)
              if (typeof ray !== 'undefined') {
                var intersection = cesium.scene.globe.pick(ray, cesium.scene)
                if (typeof intersection !== 'undefined') {
                  var dist = Cesium.Cartesian3.distance(cart, intersection)
                  if (dist <= 200.0) {
                    // Push in scope
                    var curTopo = {
                      name: feature.properties.name,
                      latitude: point[1],
                      longitude: point[0],
                      height: point[2],
                      labelId: 'label_' + feature.properties.name,
                      imgId: 'icon_' + feature.properties.name,
                      elementId: feature.properties.name
                    }

                    // Draw toponym
                    var styles = drawCurTopo(curTopo, cesium)//, el);

                    curTopo.stylePoint = styles.point
                    curTopo.styleLabel = styles.label
                    toponyms.geotags.push(curTopo.name)
                    toponyms.icons.push(curTopo)
                  } // dist
                  else {

                  }
                }// inter undefined
              }// ray undefined
            }// within
          } // undefined
        }
      } // for loop
      return toponyms
    })// then
    return promise
  }

  var drawToponymsList = function (list, cesium) {
    var newList = []
    for (var id in list) {
      var curTopo = list[id]
      // Draw toponym
      var styles = drawCurTopo(curTopo, cesium)//, el);
      curTopo.stylePoint = styles.point
      curTopo.styleLabel = styles.label
      newList.push(curTopo)
      // geotags.push(curTopo)
    }
    return newList
  }

  var deleteToponyms = function (list, cesium) {
    for (var id in list) {
      eraseCurTopo(list[id], cesium)
    }
  }
  var eraseCurTopo = function (curTopo, cesium) {
    // Get icon div
    var element = document.getElementById('icon_' + curTopo.elementId)
    if (element != null) {
      element.parentNode.removeChild(element)
    }
    // Get label
    var element = document.getElementById('label_' + curTopo.elementId)
    if (element != null) {
      element.parentNode.removeChild(element)
    }
  }
  var drawCurTopo = function (curTopo, cesium) { //, el
    // Draw toponym on the windows
    var cart = Cesium.Cartesian3.fromDegrees(curTopo.longitude, curTopo.latitude, curTopo.height)
    var pt2D = Cesium.SceneTransforms.wgs84ToWindowCoordinates(cesium.scene, cart)

    if (typeof pt2D !== 'undefined') {
      if ((pt2D.x > 0) && (pt2D.x < cesium.canvas.width) && (pt2D.y > 0) && (pt2D.y < cesium.canvas.height)) {
        var el = document.getElementById('cesiumContainer')
        var rect = el.getBoundingClientRect()

        var yOff = rect.top// 0;//rect.top;
        var xOff = rect.left// padding;
        var heightIcon = 10
        var widthIcon = 10

        var xCoord = xOff + pt2D.x - widthIcon / 2// xOff + pt2D.x+widthIcon/2;
        var yCoord = yOff + pt2D.y - heightIcon// yOff + pt2D.y+heightIcon/2;

        styles = {}
        styles.point = {
          'position': 'fixed',
          'top': yCoord.toFixed(1).concat('px'),
          'left': xCoord.toFixed(1).concat('px'),
          'z-index': 10
        }

        var yLoc = yCoord - 20
        var xLoc = xCoord + 20
        styles.label = {
          'position': 'fixed',
          'top': yLoc.toFixed(1).concat('px'),
          'left': xLoc.toFixed(1).concat('px'),
          'z-index': 10,
          'opacity': 0.8,
          'backgroundColor': 'white'
        }

        return styles
        // var div = document.createElement('div');
        // div.id = 'icon_'+curTopo.elementId.toString();//'icon_'+curTopo.name;//
        // div.style.position = 'fixed'//'relative';//fixed';absolute
        // div.style.top = yCoord.toFixed(1).concat('px');
        // div.style.left = xCoord.toFixed(1).concat('px');
        // el.appendChild(div);
        //
        // var img = document.createElement('img');
        // img.setAttribute('src', '../icons/namePoint.png');
        // img.setAttribute('alt', '');
        // img.setAttribute('width', widthIcon);
        // img.setAttribute('height', heightIcon);
        // div.appendChild(img);
        //
        // var divLabel = document.createElement('div');
        // divLabel.id = 'label_'+curTopo.elementId.toString();//curTopo.name;//
        // divLabel.style.position = 'absolute';
        // divLabel.style.opacity = 0.8;
        // divLabel.style.backgroundColor = 'white';
        // var yLoc = yCoord-20;
        // var xLoc = xCoord+20;
        // divLabel.style.top = yLoc.toFixed(1).concat('px');
        // divLabel.style.left = xLoc.toFixed(1).concat('px');
        // divLabel.innerHTML = curTopo.name;
        // el.appendChild(divLabel);
      } // undefined
    }
  }

  var postTopo = function (toponym, volunteerId, imageId) {
    var sendData = {
      toponym: toponym,
      volunterId: volunteerId,
      imageId: imageId
    }
    var postTopoPromise = $http.post('/topo/createToponym', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postTopoPromise
  }

  var postTags = function (geotags, imageId) {
    var geotags_array = '{' + geotags.toString() + '}'
    var sendData = {
      geotags_array: geotags_array,
      imageId: imageId
    }
    var postTagsPromise = $http.post('/topo/saveToponyms', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postTagsPromise
  }

  var getOSMNamesPromiseFunction = function (footprint) {
    return $q(function (resolve, reject) {
      var footprintStr = geojson2Turbo(footprint)

      // Build a query for overpass (see overpass-turbo)
      var query = '[out:json];(node[natural]({{bbox}});node[place]({{bbox}}););out body;>;out;'
      var query = query.replace(/{{bbox}}/g, footprintStr)

      var data_url = 'http://overpass-api.de/api/interpreter?data='.concat(query)

      // Send the request
      var req = new XMLHttpRequest()
      req.open('GET', data_url)

      req.onload = function () {
        // If no error
        if (req.readyState == 4 && req.status == 200) {
          // Process the response
          var jsonData = JSON.parse(req.responseText)
          var geojsonToponyms = osmtogeojson(jsonData)
          // $scope.toponyms.data = geojsonData;
          resolve(geojsonToponyms)
        } else {
          // Reject and send error message
          reject(Error(req.statusText))
        }
      }
      // Manage network error
      req.onerror = function () {
        reject(Error('Network error'))
      }
      // send the request
      req.send()
    })
  }

  return {
    // getOSMNamesPromiseFunction: getOSMNamesPromiseFunction,
    getToponyms: getToponyms,
    geojsonTo3D: geojsonTo3D,
    geojsonToponyms: function () { return geojsonToponyms },
    drawToponyms: drawToponyms,
    drawToponymsList: drawToponymsList,
    drawCurTopo: drawCurTopo,
    eraseCurTopo: eraseCurTopo,
    getGeotags: function () { return geoTags },
    postTopo: postTopo,
    postTags: postTags,
    deleteToponyms: deleteToponyms,
    filterToponyms: filterToponyms
    // logout: logout,
    // storeUserCredentials: storeUserCredentials,
    // loadUserCredentials: loadUserCredentials,
    // update: update,
    // isAuthenticated: function() {return isAuthenticated;},
  }
})
