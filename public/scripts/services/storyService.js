angular.module('storyApp')

.service('storyService', function ($q, $http) {
  var drawStories = function (dicStory, cesium, tilesLoadedPromise) {
    // Draw toponym on the windows
    var dicStoryReady = {}
    var promise = tilesLoadedPromise.then(function () {
      for (var id in dicStory) {
        var story = dicStory[id]
        // Draw toponym
        var location = drawCurStory(story, cesium)
        if (location) {
          story.x = location.x
          story.y = location.y
          story.style = {
            'position': 'fixed',
            'top': location.y,
            'left': location.x,
            'z-index': 10
          }
          dicStoryReady[id] = story
        }
      } // for loop
      return dicStoryReady
    })// then
    return promise
  }

  var deleteStories = function (dicStory, cesium) {
    for (var id in dicStory) {
      var story = dicStory[id]
      eraseCurStory(story, cesium)
    }
  }
  var eraseCurStory = function (story, cesium) {
    // Get icon div
    var element = document.getElementById('icon_' + story.storyId)
    if (element != null) {
      element.parentNode.removeChild(element)
    }
  }
  var drawCurStory = function (curStory, cesium) { //, el
    // Draw toponym on the windows
    var cart = Cesium.Cartesian3.fromDegrees(curStory.longitude, curStory.latitude, curStory.height)
    var pt2D = Cesium.SceneTransforms.wgs84ToWindowCoordinates(cesium.scene, cart)

    if (typeof pt2D !== 'undefined') {
      if ((pt2D.x > 0) && (pt2D.x < cesium.canvas.width) && (pt2D.y > 0) && (pt2D.y < cesium.canvas.height)) {
        var el = document.getElementById('cesiumContainer')
        var rect = el.getBoundingClientRect()

        var yOff = rect.top// 0;//rect.top;
        var xOff = rect.left// padding;
        var heightIcon = 53
        var widthIcon = 53

        var xCoord = xOff + pt2D.x// +widthIcon;//xOff + pt2D.x+widthIcon/2;
        var yCoord = yOff + pt2D.y - heightIcon// yOff + pt2D.y+heightIcon/2;

        // var div = document.createElement('div');
        // div.id = 'icon_'+curStory.storyId.toString();//'icon_'+curTopo.name;//
        // div.style.position = 'fixed'//'relative';//fixed';absolute
        // div.style.top = yCoord.toFixed(1).concat('px');
        // div.style.left = xCoord.toFixed(1).concat('px');
        // div.setAttribute('uib-popover',"yahoo");
        // div.setAttribute('popover-title',"blabla");
        // div.setAttribute('popover-trigger','mouseenter')
        // el.appendChild(div);
        //
        // var img = document.createElement('img');
        // img.setAttribute('src', '../icons/story.png');
        // img.setAttribute('alt', '');
        // img.setAttribute('width', widthIcon);
        // img.setAttribute('height', heightIcon);
        // img.setAttribute('uib-popover',"yahoo");
        // img.setAttribute('popover-title',"blabla");
        // img.setAttribute('popover-trigger','mouseenter')
        // div.appendChild(img);

        var location = {
          x: xCoord.toFixed(1).concat('px'),
          y: yCoord.toFixed(1).concat('px')
        }
        return location
        // var divLabel = document.createElement('div');
        // divLabel.id = 'label_'+curTopo.elementId.toString();//curTopo.name;//
        // divLabel.style.position = 'absolute';
        // divLabel.style.opacity = 0.8;
        // divLabel.style.backgroundColor = 'white';
        // var yLoc = yCoord-20;
        // var xLoc = xCoord+20;
        // divLabel.style.top = yLoc.toFixed(1).concat('px');
        // divLabel.style.left = xLoc.toFixed(1).concat('px');
        // divLabel.innerHTML = curTopo.name;
        // el.appendChild(divLabel);
      } // undefined
    }
  }

  var getStories = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var getStoryPromise = $http.post('/story/getStory', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return getStoryPromise
  }

  var postStory = function (story, volunteerId, imageId) {
    var sendData = {
      story: story,
      volunteerId: volunteerId,
      imageId: imageId
    }
    var postTopoPromise = $http.post('/story/createStory', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postTopoPromise
  }

  return {
    drawStories: drawStories,
    drawCurStory: drawCurStory,
    eraseCurStory: eraseCurStory,
    postStory: postStory,
    deleteStories: deleteStories,
    getStories: getStories
  }
})
