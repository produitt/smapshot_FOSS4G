angular.module('ownerQueryApp')

.service('ownerQueryService', function ($q, $http) {

  var getCustomParameters = function (ownerUrlName) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerUrlName: ownerUrlName
      }
      var httpPromise = $http.post('/owners/getCustomParameters/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getCurrent = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/owners/getCurrent/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getCollections = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      var httpPromise = $http.post('/owners/getCollections/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getOwnersCollections = function (listOwners) {
    return $q(function (resolve, reject) {
      var listOwnersId = []
      for (var i = 0; i < listOwners.length; i++) {
        listOwnersId.push(listOwners[i].id)
      }
      var sendData = {
        listOwnersId: listOwnersId
      }
      var httpPromise = $http.post('/owners/getOwnersCollections/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getStats = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      var httpPromise = $http.post('/owners/getStats/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data[0])
      })
    })
  }

  var getList = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/owners/getList/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  return {
    getCustomParameters: getCustomParameters,
    getCollections: getCollections,
    getStats: getStats,
    getList: getList,
    getOwnersCollections: getOwnersCollections,
    getCurrent: getCurrent
  }
})
