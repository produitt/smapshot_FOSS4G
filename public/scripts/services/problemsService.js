angular.module('problemsApp')

.service('problemsService', function ($q, $http) {


  //Push correction in the database
  var deleteProblem = function (problemId) {
    var sendData = {
      problemId: problemId
    }
    var postProblemPromise = $http.post('/problems/delete/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postProblemPromise
  }

  //Push correction in the database
  var submitProblem = function (imageId, volunteerId, problem) {
    console.log('in submit service')
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      problem: problem // contains: title, dateShot, caption
    }
    var postProblemPromise = $http.post('/problems/submit/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postProblemPromise
  }

  // Get problems which werent processed
  var getDashboard = function (sendData) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/problems/getDashboard/', sendData)
      httpPromise.then(function (res) {
        var listProblems = res.data
        for (var i = 0; i<listProblems.length; i++){
          listProblems[i].date = listProblems[i].date.slice(0,16).replace('T', ' ')
        }
        resolve(listProblems)
      })
    })
  }

  // The owner has processed the problem
  var ownerProcessed = function (problemId) {
    var sendData = {
      problemId: problemId
    }
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/problems/ownerProcessed/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  // The smapshot has processed the problem
  var smapshotProcessed = function (problemId) {
    var sendData = {
      problemId: problemId
    }
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/problems/smapshotProcessed/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  return {
    submitProblem: submitProblem,
    getDashboard: getDashboard,
    ownerProcessed: ownerProcessed,
    smapshotProcessed: smapshotProcessed,
    deleteProblem: deleteProblem
  }
})
