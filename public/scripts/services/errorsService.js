angular.module('errorsApp')

.service('errorsService', function ($q, $http) {

  // get the error list from the database
  var getAllErrors = function () {

    var postErrorsPromise = $http.post('/errors/getErrorsList')
    .then(function (result) {
      return result.data
    })
    .catch(function (result) {
      return result
    })
    return postErrorsPromise
  }

  var getErrors = function (errorsList) {
    var sendData = {
      list: errorsList
    }

    var postErrorsPromise = $http.post('/errors/getErrors', sendData)
    .then(function (result) {
      var nErrors = result.data.length
      for (var i=0; i < nErrors; i++){
        result.data[i].translations = JSON.parse(result.data[i].translations)
      }
      return result.data
    })
    .catch(function (result) {
      return result
    })
    return postErrorsPromise
  }


  return {
    getAllErrors: getAllErrors,
    getErrors: getErrors
  }
})
