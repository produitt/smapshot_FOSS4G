angular.module('textApp')

.service('textService', function($q, $http, $window) {

  var loadText = function (fileName) {
    return $q(function (resolve, reject) {
      $http.get('../texts/' + fileName + '.json', {
        headers: {
          'Accept': 'application/json;charset=utf-8'
        // "Accept-Charset":"charset=utf-8"
        }
      })
    .then(function (res) {
      resolve(res.data.texts)
    })
    })
  }

  return {
    loadText: loadText
  }
})
