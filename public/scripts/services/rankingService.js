angular.module('rankingApp')

.service('rankingService', function ($q, $http, volunteersQueryService) {

  // Date selection
  // Initialize start date
  var startDate = new Date() // Today
  // Get previous month
  startDate.setMonth(startDate.getMonth() - 1)
  var startYear = startDate.getFullYear()
  var startMonth = startDate.getMonth() + 1

  // Initialize end date
  var endDate = new Date()
  var endYear = endDate.getFullYear()
  var endMonth = endDate.getMonth() + 1

  // list of years
  var listYears = [endYear - 1, endYear]
  //Push correction in the database
  var listMonths = {
    1: {fr: 'Janvier', en: 'January', de: 'Januar', it: 'Gennaio', mid: 1, short: 'Jan'},
    2: {fr: 'Février', en: 'February', de: 'Februar', it: 'Febbraio', mid: 2, short: 'Feb'},
    3: {fr: 'Mars', en: 'March', de: 'März', it: 'Marzo', mid: 3, short: 'Mar'},
    4: {fr: 'Avril', en: 'April', de: 'April', it: 'Aprile', mid: 4, short: 'Apr'},
    5: {fr: 'Mai', en: 'May', de: 'Mai', it: 'Maggio', mid: 5, short: 'Mai'},
    6: {fr: 'Juin', en: 'June', de: 'Juni', it: 'Giugno', mid: 6, short: 'Jun'},
    7: {fr: 'Juillet', en: 'July', de: 'Juli', it: 'Luglio', mid: 7, short: 'Jul'},
    8: {fr: 'Août', en: 'August', de: 'August', it: 'Agosto', mid: 8, short: 'Aug'},
    9: {fr: 'Septembre', en: 'September', de: 'September', it: 'Settembre', mid: 9, short: 'Sep'},
    10: {fr: 'Octobre', en: 'October', de: 'Oktober', it: 'Ottobre', mid: 10, short: 'Oct'},
    11: {fr: 'Novembre', en: 'November', de: 'November', it: 'Novembre', mid: 11, short: 'Nov'},
    12: {fr: 'Décembre', en: 'December', de: 'Dezember', it: 'Dicembre', mid: 12, short: 'Dec'}
  }

  // Change start month for ranking
  var selectStartMonth = function (month, ownerId, collectionId) {
    startMonth = month.mid
    startDate = new Date(startYear, startMonth, 1)
    // Check if the end month is after start date
    checkUpperLimit()
    // Generate limit dates
    var limitDates = generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    // Query data for ranking
    var searchData = {
      ownerId: ownerId,
      collectionId: collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null
    }
    // Get the ranking of the volunteers
    volunteersQueryService.getRanking(searchData)
    .then(function (response) {
      generateDataForRanking(response)
    })
  }
  // Change start year
  var selectStartYear = function (year, ownerId, collectionId) {
    startYear = year
    startDate = new Date(startYear, startMonth, 1)
    checkUpperLimit()
    var limitDates = generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    var searchData = {
      ownerId: ownerId,
      collectionId: collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null
    }
    // Get the ranking of the volunteers
    volunteersQueryService.getRanking(searchData)
    .then(function (response) {
      generateDataForRanking(response)
    })
  }
  // Change end month
  var selectEndMonth = function (month, ownerId, collectionId) {
    endMonth = month.mid
    endDate = new Date(endYear, endMonth, 0)
    checkLowerLimit()
    var limitDates = generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    var searchData = {
      ownerId: ownerId,
      collectionId: collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null
    }
    // Get the ranking of the volunteers
    volunteersQueryService.getRanking(searchData)
    .then(function (response) {
      generateDataForRanking(response)
    })
  }
  var selectEndYear = function (year, ownerId, collectionId) {
    endYear = year
    endDate = new Date(endYear, endMonth, 0)
    checkLowerLimit()

    var limitDates = generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]

    var searchData = {
      ownerId: ownerId,
      collectionId: collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      collectionId: null
    }
    // Get the ranking of the volunteers
    volunteersQueryService.getRanking(searchData)
    .then(function (response) {
      generateDataForRanking(response)
    })
  }
  var checkLowerLimit = function () {

    if (endDate < startDate) {
      startDate.setMonth(endDate.getMonth() - 1)
      startDate.setYear(endDate.getFullYear())

      startMonth = startDate.getMonth() + 1
      startYear = startDate.getFullYear()
    }
  }
  var checkUpperLimit = function () {
    if (endDate < startDate) {
      endDate.setMonth(startDate.getMonth() + 1)
      endDate.setYear(startDate.getFullYear())

      endMonth = endDate.getMonth() + 1
      endYear = endDate.getFullYear()
    }
  }
  var generateLimitDates = function () {
    // Generate start and end dates string
    if (endMonth.toString().length < 2) {
      var endMonth = '0' + endMonth
    } else {
      var endMonth = endMonth
    }
    if (startMonth.toString().length < 2) {
      var startMonth = '0' + startMonth
    } else {
      var startMonth = startMonth
    }
    var stopDate = [endYear, endMonth, '31'].join('-')
    var startDate = [startYear, startMonth, '01'].join('-')
    return [startDate, stopDate]
  }
  // Click to change the collection
  var selectCollection = function (collection, ownerId, collectionId) {
    collectionId = collection.id

    var limitDates = generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]

    var searchData = {
      ownerId: ownerId,
      collectionId: collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null
    }
    // Get the ranking of the volunteers
    volunteersQueryService.getRanking(searchData)
    .then(function (response) {
      generateDataForRanking(response)
    })
  }
  // Create data for the ranking
  var generateDataForRanking = function (response) {
    var dataRanking = []
    // Fill the data with the users
    for (var i = 0; i < response.length; i++) {
      // Get current user in the ranking
      var curUser = response[i]
      var d = {
        username: curUser.username,
        nImages: parseInt(curUser.nImages)
      }
      dataRanking.push(d)
    }
    dataRankingEnd = dataRanking.splice(3, dataRanking.length - 3)
  }

  return {
    listMonths: function () { return listMonths },
    listYears: function () { return listYears },
    startMonth: function () { return startMonth },
    startYear: function () { return startYear },
    endMonth: function () { return endMonth },
    endYear: function () { return endYear },
    selectStartMonth: selectStartMonth,
    selectStartYear: selectStartYear,
    selectEndMonth: selectEndMonth,
    selectEndYear: selectEndYear,
    selectCollection: selectCollection,
  }
})
