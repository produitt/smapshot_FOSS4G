angular.module('cesiumApp')

.service('cesiumService', function ($q, $http) {
  // var modelGlobal = null;
  var transparency = 1

  var drawToponyms = function (geoJsonData, cesium) {
    for (var idFeat in geoJsonData.features) {
      // var idFeat = 0;
      var feature = geoJsonData.features[idFeat]

      if (feature.geometry.type == 'Point') {
        var point = feature.geometry.coordinates

        // var cart = Cesium.Cartesian3.fromRadians($scope.positions[i].longitude, $scope.positions[i].latitude, $scope.positions[i].height);
        var cartesian = Cesium.Cartesian3.fromDegrees(point[0], point[1], point[2])

        // Create camera entity
        var curEntity = cesium.entities.add({
          id: feature.properties.name,
          position: cartesian,
          point: {
            pixelSize: 7,
            outlineColor: Cesium.Color.WHITE,
            outlineWidth: 2
          },
          label: {
            show: true,
            text: feature.properties.name,
            font: '14pt monospace',
            style: Cesium.LabelStyle.FILL_AND_OUTLINE,
            outlineWidth: 2,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            pixelOffset: new Cesium.Cartesian2(0, -9)
          }
        })
        var selectedColor = new Cesium.Color(0.12, 0.8, 10.68)
        curEntity.point.color = new Cesium.ConstantProperty(selectedColor)//Cesium.Color.GREEN)
      }
    }
  }

  // Draw the image in Cesium
  // ------------------------
  var drawMarker = function (feature, cesium, terrainProvider) {
    if (feature.georef_3d_bool == true) {
      // Diplay marker
      var cartesian = Cesium.Cartesian3.fromDegrees(feature.longitude, feature.latitude, feature.height)
      var curEntity = cesium.entities.add({
        id: feature.id,
        position: cartesian,
        point: {
          pixelSize: 15,
          outlineColor: Cesium.Color.WHITE,
          outlineWidth: 2
        }
      })
      curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.GREEN)
      // Link the model to the image objects
      // $scope.images.dicImages[feature.properties.id].pointModel = curEntity
      // if (feature.properties.id == $scope.images.selectedImage.id){
      //   $scope.images.selectedImage.pointModel = curEntity;
      // }
    } else {
      // Image is not georeferenced
      var position = [Cesium.Cartographic.fromDegrees(feature.longitude, feature.latitude)]
      var promise = Cesium.sampleTerrain(terrainProvider, 17, position)
      Cesium.when(promise, function (updatedPosition) {
        // Diplay marker
        var cartesian = Cesium.Cartesian3.fromRadians(updatedPosition[0].longitude, updatedPosition[0].latitude, updatedPosition[0].height + 20)
        var curEntity = cesium.entities.add({
          id: feature.id,
          position: cartesian,
          point: {
            pixelSize: 15,
            outlineColor: Cesium.Color.WHITE,
            outlineWidth: 2
          }
        })
        curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.RED)
        // Link the model to the image objects
        // $scope.images.dicImages[feature.properties.id].pointModel = curEntity
        // if (feature.properties.id == $scope.images.selectedImage.id){
        //   $scope.images.selectedImage.pointModel = curEntity;
        // }
      })
    }
    return curEntity
  }
  var drawCylinder = function (feature, cesium) {
    // Display cylinder
    var circleInstance = new Cesium.GeometryInstance({
      geometry: new Cesium.CircleGeometry({
        center: Cesium.Cartesian3.fromDegrees(feature.longitude, feature.latitude),
        radius: 2.5,
            // vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT,
        height: 0,
        extrudedHeight: feature.height[2] - 10
      }),
      attributes: {
        color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.GREY)
      },
      id: feature.id
    })
    var primitive = new Cesium.Primitive({
      id: feature.id,
      geometryInstances: circleInstance,
      appearance: new Cesium.PerInstanceColorAppearance({
        translucent: false,
        closed: true
      })
    })
    // Link the model to the image objects
    // $scope.images.dicImages[feature.properties.id].cylinderModel = primitive
    // if (feature.properties.id == $scope.images.selectedImage.id){
    //   $scope.images.selectedImage.cylinderModel = primitive
    // }
    // Add in cesium
    cesium.scene.primitives.add(primitive)
    return primitive
  }

  var deleteModel = function (cesium, model) {
    cesium.scene.primitives.remove(model)
  }

  var drawModel = function (image, cesium, transparency, modelGlobal, tempBool) {
    var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(
        Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height)
    )
    // Get model
    if (tempBool === true) {
      var path2model = '../data/collections/' + image.collection_id + '/temp_collada/'
    }else{
      var path2model = '../data/collections/' + image.collection_id + '/gltf/'
    }

    var url = path2model + image.id + '.gltf'
    var prim = Cesium.Model.fromGltf({
      id: image.id,
      basePath: '',
      url: url,
      modelMatrix: modelMatrix,
      scale: 0.5,
      incrementallyLoadTextures: false,
      show: true,
      color: Cesium.Color.fromAlpha(Cesium.Color.WHITE, 1)
    })
    // Add in cesium
    if (modelGlobal != null) {
      cesium.scene.primitives.remove(modelGlobal)
    }
    // cesium.scene.primitives.removeAll()

    var modelGlobal = cesium.scene.primitives.add(prim)
    modelGlobal.readyPromise.then(function(){
      //Set initial transparency
      var mat = model.getMaterial("material_0");
      var curT = mat.getValue('transparency');
      mat.setValue('transparency', 1);
      return model
     //return modelGlobal
    })
    return modelGlobal
  }

  // hide or show buildings
  var hideShowBuildings = function (buildings, boolShow) {
    // Get
    // if (buildings.tileset1.show == true) {
    //   buildings.tileset1.show = false
    //   buildings.tileset2.show = false
    // } else {
    buildings.tileset1.show = boolShow
    buildings.tileset2.show = boolShow
    // }
    return buildings
  }

  var hideShowModel = function (model) {
    // Get
    if (model.show == true) {
      model.show = false
    } else {
      model.show = true
    }
    return model
  }

  var changeModelTransparency = function (model, transparency) {
    var mat = model.getMaterial('material_0')
    model.color = Cesium.Color.fromAlpha(Cesium.Color.WHITE, transparency)
  }

  var flyToModel = function (orientationArray, camera, model) {
    var cartesian = Cesium.Cartesian3.fromDegrees(orientationArray[0].long, orientationArray[0].lat, orientationArray[0].Z)
    camera.flyTo({
      destination: cartesian,
      orientation: {
        heading: Cesium.Math.toRadians(orientationArray[0].azimuth),
        pitch: Cesium.Math.toRadians(orientationArray[0].tilt),
        roll: Cesium.Math.toRadians(orientationArray[0].roll)
      }
    })
  }

  var flyHome = function (cameraOrientation, camera) {
    var cartesian = Cesium.Cartesian3.fromDegrees(cameraOrientation.longitude, cameraOrientation.latitude, cameraOrientation.height)
    camera.flyTo({
      destination: cartesian,
      orientation: {
        heading: Cesium.Math.toRadians(cameraOrientation.azimuth),
        pitch: Cesium.Math.toRadians(cameraOrientation.tilt),
        roll: Cesium.Math.toRadians(cameraOrientation.roll)
      }
    })
  }

  var goToModel = function (orientationArray, camera) {
    // camera.frustum.fov = Cesium.Math.PI_OVER_TWO
    camera.setView({
      destination: Cesium.Cartesian3.fromDegrees(orientationArray[0].long, orientationArray[0].lat, orientationArray[0].Z),
      orientation: {
        heading: Cesium.Math.toRadians(orientationArray[0].azimuth),
        pitch: Cesium.Math.toRadians(orientationArray[0].tilt),
        roll: Cesium.Math.toRadians(orientationArray[0].roll)
      }
    })
  }

  var goToModelPromise = function (orientationArray, camera) {
    return $q(function (resolve, reject) {
        // Trick to avoid bug in getting the footprint
      camera.setView({
        destination: Cesium.Cartesian3.fromDegrees(orientationArray[0].long, orientationArray[0].lat, orientationArray[0].Z + 50),
        orientation: {
          heading: Cesium.Math.toRadians(orientationArray[0].azimuth),
          pitch: Cesium.Math.toRadians(orientationArray[0].tilt),
          roll: Cesium.Math.toRadians(orientationArray[0].roll)
        }
      })
      camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(orientationArray[0].long, orientationArray[0].lat, orientationArray[0].Z),
        orientation: {
          heading: Cesium.Math.toRadians(orientationArray[0].azimuth),
          pitch: Cesium.Math.toRadians(orientationArray[0].tilt),
          roll: Cesium.Math.toRadians(orientationArray[0].roll)
        },
        duration: 3,
        complete: function () { resolve() }
      })
    })
  }

  var deleteAllGCP = function (cesium, myGCPs) {
    // Delete element in the globe
    for (id in myGCPs.dictGCPs) {
      var objectToDelete = cesium.entities.getById(id)
      cesium.entities.remove(objectToDelete)
    }
  }

  var lockCamera = function (cesium) {
    var scene = cesium.scene
    scene.screenSpaceCameraController.enableRotate = false
    scene.screenSpaceCameraController.enableTranslate = false
    scene.screenSpaceCameraController.enableTilt = false
    scene.screenSpaceCameraController.enableZoom = false
    // Enable collision detection
    // scene.screenSpaceCameraController.enableCollisionDetection = true;
    // scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000;
  }

  var unlockCamera = function (cesium) {
    var scene = cesium.scene
    scene.screenSpaceCameraController.enableRotate = true
    scene.screenSpaceCameraController.enableTranslate = false
    scene.screenSpaceCameraController.enableTilt = true
    scene.screenSpaceCameraController.enableZoom = true
    // Enable collision detection
    // scene.screenSpaceCameraController.enableCollisionDetection = true;
    // scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000;
  }

  var goToNavigationMode = function (cesium) {
    var scene = cesium.scene
    var camera = cesium.camera

    camera.frustum.fov = Cesium.Math.PI_OVER_TWO
    scene.screenSpaceCameraController.enableRotate = true
    scene.screenSpaceCameraController.enableTranslate = true
    scene.screenSpaceCameraController.enableTilt = true
    scene.screenSpaceCameraController.enableZoom = true
    // Enable collision detection
    scene.screenSpaceCameraController.enableCollisionDetection = true
    scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000

    // Change viewpoint
    camera.flyTo({
      destination: Cesium.Cartesian3.fromRadians(camera.positionCartographic.longitude, camera.positionCartographic.latitude, camera.positionCartographic.height + 300),
      orientation: {
        heading: camera.heading,
        pitch: camera.pitch - Cesium.Math.PI / 6,
        roll: 0
      }
    })
  }

  // Draw camera position
  var drawCameraPosition = function (cesium, orientationArray) {
    // Create an indicator for the camera
    var cartesian = Cesium.Cartesian3.fromDegrees(orientationArray[0].long, orientationArray[0].lat, orientationArray[0].Z)
    var camEntity = cesium.entities.getById('cameraPosition')
    if (typeof camEntity !== 'undefined') {
      // Move camera entity
      camEntity.position = cartesian
    } else {
      // Create camera entity
      var curEntity = cesium.entities.add({
        id: 'cameraPosition',
        position: cartesian,
        point: {
          pixelSize: 7,
          outlineColor: Cesium.Color.WHITE,
          outlineWidth: 2
        }
        // label : {
        //   show : true,
        //   text : 'Position du photographe',
        //   font : '14pt monospace',
        //   style: Cesium.LabelStyle.FILL_AND_OUTLINE,
        //   outlineWidth : 2,
        //   verticalOrigin : Cesium.VerticalOrigin.BOTTOM,
        //   pixelOffset : new Cesium.Cartesian2(0, -9)
        // }
      })
      curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.GREEN)
    }
  }

  // Create matrix of 3D coordinate (slow so better make a promise)
  var cartMatrixPromiseFunction = function (cesium, matWidth, matHeight, widthOffset, heightOffset, scaleMat) {
    return $q(function (resolve, reject) {
      // Compute time for logging the coordinates
      // var tIni = new Date().getTime();
      try {
        var cartMatrix = math.zeros(matWidth, matHeight, 3)
        for (var ix = 0; ix < matWidth; ix++) {
          for (var iy = 0; iy < matHeight; iy++) {
            // Compute canvas coordinates
            var x = widthOffset + ix / scaleMat
            var y = heightOffset + iy / scaleMat
            // Get coordinates
            var windowPosition = new Cesium.Cartesian2(x, y)
            var ray = cesium.scene.camera.getPickRay(windowPosition)
            var cartesianLoc = cesium.scene.globe.pick(ray, cesium.scene)
            // Record coordinates
            if (typeof cartesianLoc !== 'undefined') {
              cartMatrix.subset(math.index(ix, iy, 0), cartesianLoc.x)
              cartMatrix.subset(math.index(ix, iy, 1), cartesianLoc.y)
              cartMatrix.subset(math.index(ix, iy, 2), cartesianLoc.z)
            };
          };
        };
        // Store in the scope
        var locationMatrix = {
          cartMatrix: cartMatrix,
          scale: scaleMat,
          widthOffset: widthOffset,
          heightOffset: heightOffset
        }
        resolve(locationMatrix)
      } catch (err) {
        reject()
      };
    })
  }

  var setCollisionDetection = function (cesium) {
    var scene = cesium.scene
    scene.screenSpaceCameraController.enableCollisionDetection = true
    scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000
  }

  var relaxCollisionDetection = function (cesium) {
    var scene = cesium.scene
    scene.screenSpaceCameraController.enableCollisionDetection = false
  }

  var drawGCP = function (cesium, gcp) {
    var cartesian = Cesium.Cartesian3.fromDegrees(gcp.longitude, gcp.latitude, gcp.Z + 1)
    var curEntity = cesium.entities.add({
      id: (gcp.id).toString(),
      position: cartesian,
      point: {
        pixelSize: 15,
        outlineColor: Cesium.Color.WHITE,
        outlineWidth: 2
      }
      // label : {
      //   show : true,
      //   text : (gcp.id).toString(),
      //   font : '14pt monospace',
      //   style: Cesium.LabelStyle.FILL_AND_OUTLINE,
      //   outlineWidth : 2,
      //   verticalOrigin : Cesium.VerticalOrigin.BOTTOM,
      //   pixelOffset : new Cesium.Cartesian2(0, -9)
      // }
    })
    var selectedColor = new Cesium.Color(0.12, 0.8, 10.68)
    curEntity.point.color = new Cesium.ConstantProperty(selectedColor)//Cesium.Color.YELLOW)
  }

  var drawGCPLabel = function (cesium, gcp) {
    var cartesian = Cesium.Cartesian3.fromDegrees(gcp.longitude, gcp.latitude, gcp.Z + 1)
    var curEntity = cesium.entities.add({
      id: (gcp.id).toString(),
      position: cartesian,
      point: {
        pixelSize: 15,
        outlineColor: Cesium.Color.WHITE,
        outlineWidth: 2
      },
      label: {
        show: true,
        text: (gcp.id + 1).toString(),
        font: '14pt monospace',
        style: Cesium.LabelStyle.FILL_AND_OUTLINE,
        outlineWidth: 2,
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        pixelOffset: new Cesium.Cartesian2(0, -9)
      }
    })
    var selectedColor = new Cesium.Color(0.12, 0.8, 10.68)
    curEntity.point.color = new Cesium.ConstantProperty(selectedColor)//Cesium.Color.YELLOW)
  }

  var unselectAll = function (cesium, myGCPs) {
    for (curGCP in myGCPs.dictGCPs) {
      if (myGCPs.dictGCPs[curGCP].X != null) {
        var entitySelected = cesium.entities.getById(curGCP.toString())
        var selectedColor = new Cesium.Color(0.12, 0.8, 10.68)
        entitySelected.point.color.setValue(selectedColor)//Cesium.Color.YELLOW)
      };
    };
  }

  var selectGCP = function (cesium, idClicked) {
    var entitySelected = cesium.entities.getById(idClicked)
    var selectedColor = Cesium.Color.YELLOW //new Cesium.Color(1, 0.69, 0.15)
    entitySelected.point.color.setValue(selectedColor)//Cesium.Color.RED)
  }

  var deleteGCP = function (cesium, myGCPs, idClicked) {
    if (myGCPs.dictGCPs[idClicked].X != null) {
      var objectToDelete = cesium.entities.getById(idClicked)
      cesium.entities.remove(objectToDelete)
    }
  }

  var getHeight = function (cesium) {
    return $q(function (resolve, reject) {
      var cameraHeight = cesium.camera.positionCartographic.height
      var position = [cesium.camera.positionCartographic]
      position[0].height = 0
      // var position = [cesium.camera.position[0], cesium.camera.position[1] ];
      var promise = Cesium.sampleTerrain(cesium.terrainProvider, 17, position)
      Cesium.when(promise, function (updatedPosition) {
        var groundHeight = updatedPosition[0].height
        var delta = (cameraHeight - groundHeight) / 5
        resolve(delta)
      })
    })
  }
  var moveUp = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveUp(delta)
    })
  }
  var moveDown = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveDown(delta)
    })
  }
  var moveLeft = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveLeft(delta)
    })
  }
  var moveRight = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveRight(delta)
    })
  }
  var moveBack = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveBackward(delta)
    })
  }
  var moveFront = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveForward(delta)
    })
  }
  var moveOut = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveBackward(delta)
    })
  }
  var moveIn = function (cesium) {
    var deltaPromise = getHeight(cesium)
    deltaPromise.then(function (delta) {
      cesium.camera.moveForward(delta)
    })
  }
  var turn = function (cesium, direction) {
    return $q(function (resolve, reject) {
      if (direction == 'Up') {
        var camera = cesium.camera
        if (camera.pitch < Cesium.Math.PI / 2 - 2 * Cesium.Math.PI / 20) {
          camera.flyTo({
            destination: camera.position,
            orientation: {
              heading: camera.heading,
              pitch: camera.pitch + camera.frustum.fov / 6, // +Cesium.Math.PI/20,
              roll: camera.roll
            },
            duration: 0.25,
            complete: function () {
              resolve()
            }
          })
        }
      } else if (direction == 'Down') {
        var camera = cesium.camera
        if (camera.pitch > -Cesium.Math.PI / 2 + 2 * Cesium.Math.PI / 20) {
          camera.flyTo({
            destination: camera.position,
            orientation: {
              heading: camera.heading,
              pitch: camera.pitch - camera.frustum.fov / 6, // -Cesium.Math.PI/20,
              roll: camera.roll
            },
            duration: 0.25,
            complete: function () {
              resolve()
            }
          })
        }
      } else if (direction == 'Right') {
        var camera = cesium.camera
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading + camera.frustum.fov / 6, // +Cesium.Math.PI/20,
            pitch: camera.pitch,
            roll: camera.roll
          },
          duration: 0.25,
          complete: function () {
            resolve()
          }

        })
      } else if (direction == 'Left') {
        var camera = cesium.camera
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading - camera.frustum.fov / 6, // Cesium.Math.PI/20,
            pitch: camera.pitch,
            roll: camera.roll
          },
          duration: 0.25,
          complete: function () {
            resolve()
          }
        })
      }
    })
  }
  var turnUp = function (cesium) {
    return $q(function (resolve, reject) {
      var camera = cesium.camera
      if (camera.pitch < Cesium.Math.PI / 2 - 2 * Cesium.Math.PI / 20) {
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch + camera.frustum.fov / 6, // +Cesium.Math.PI/20,
            roll: camera.roll
          },
          duration: 0.25,
          complete: function () {
            resolve()
          }
        })
      }
    })
    // cesium.camera.moveUp(100);
  }
  var turnDown = function (cesium) {
    return $q(function (resolve, reject) {
      var camera = cesium.camera
      if (camera.pitch > -Cesium.Math.PI / 2 + 2 * Cesium.Math.PI / 20) {
        camera.flyTo({
          destination: camera.position,
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch - camera.frustum.fov / 6, // -Cesium.Math.PI/20,
            roll: camera.roll
          },
          duration: 0.25,
          complete: function () {
            resolve()
          }
        })
      }
    })
    // cesium.camera.moveDown(100);
  }
  var turnLeft = function (cesium) {
    return $q(function (resolve, reject) {
      var camera = cesium.camera
      camera.flyTo({
        destination: camera.position,
        orientation: {
          heading: camera.heading - camera.frustum.fov / 6, // Cesium.Math.PI/20,
          pitch: camera.pitch,
          roll: camera.roll
        },
        duration: 0.25,
        complete: function () {
          resolve()
        }
      })
    })
    // cesium.camera.moveLeft(250);
  }
  var turnRight = function (cesium) {
    return $q(function (resolve, reject) {
      var camera = cesium.camera
      camera.flyTo({
        destination: camera.position,
        orientation: {
          heading: camera.heading + camera.frustum.fov / 6, // +Cesium.Math.PI/20,
          pitch: camera.pitch,
          roll: camera.roll
        },
        duration: 0.25,
        complete: function () {
          resolve()
        }
      })
    })
    // cesium.camera.turnRight(250);
  }

  var zoom = function (cesium, direction) {
    var camera = cesium.camera
    var fov = camera.frustum.fov
    if (direction == 'In') {
      if (fov > Cesium.Math.PI / 20) {
        camera.frustum.fov = fov - 0.05
      };
    } else {
      if (fov < Cesium.Math.PI*0.55) {
        camera.frustum.fov = fov + 0.05
      };
    }
  }
  var zoomIn = function (cesium) {
    var camera = cesium.camera
    var fov = camera.frustum.fov
    if (fov > Cesium.Math.PI / 20) {
      camera.frustum.fov = fov - 0.05
    };
  }

  var zoomOut = function (cesium) {
    var camera = cesium.camera
    var fov = camera.frustum.fov
    if (fov < Cesium.Math.PI*0.55) {
      camera.frustum.fov = fov + 0.05
    };
  }

  var changeLatLng = function (lat, lng, cesium, terrainProvider) {
    var camera = cesium.camera
    // Check if the new position is in the ground

    var position = [Cesium.Cartographic.fromDegrees(lng, lat)]
    var promise = Cesium.sampleTerrain(terrainProvider, 17, position)
    Cesium.when(promise, function (updatedPosition) {

      if (updatedPosition[0].height > camera.positionCartographic.height){
        camera.flyTo({
          destination: Cesium.Cartesian3.fromDegrees(lng, lat, updatedPosition[0].height + 5),
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch,
            roll: camera.roll
          },
          // maximumHeight: 4500,
          duration: 1,
        })
      }else{
        camera.flyTo({
          destination: Cesium.Cartesian3.fromDegrees(lng, lat, camera.positionCartographic.height),
          orientation: {
            heading: camera.heading,
            pitch: camera.pitch,
            roll: camera.roll
          },
          // maximumHeight: 4500,
          duration: 1,
        })
      }
    })
  }

  var changeAzimuth = function (azimuth, cesium) {
    var camera = cesium.camera
    //camera.position = new Cesium.Cartesian3();
    camera.flyTo({
      destination: Cesium.Cartesian3.fromRadians(camera.positionCartographic.longitude, camera.positionCartographic.latitude, camera.positionCartographic.height),
      orientation: {
        heading: azimuth*Cesium.Math.PI/180,
        pitch: camera.pitch,
        roll: camera.roll
      },
      // maximumHeight: 4500,
      duration: 1,
    })
  }

  var flyToImage = function (image, cesium) {
    cesium.scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 1
    cesium.scene.screenSpaceCameraController.enableCollisionDetection = true

    var camera = cesium.camera
    camera.flyTo({
      destination: Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height),
      orientation: {
        heading: image.azimuth * Cesium.Math.PI / 180,
        pitch: image.tilt * Cesium.Math.PI / 180,
        roll: image.roll * Cesium.Math.PI / 180
      },
      // maximumHeight: 4500,
      duration: 3,
      complete: function () {
        // Lock the camera
        var scene = cesium.scene
        scene.screenSpaceCameraController.enableRotate = false
        scene.screenSpaceCameraController.enableTranslate = false
        scene.screenSpaceCameraController.enableTilt = false
        scene.screenSpaceCameraController.enableZoom = false
      }
    })
    // cesium.camera.turnRight(250);
  }

  return {
    // getOSMNamesPromiseFunction: getOSMNamesPromiseFunction,
    drawModel: drawModel,
    deleteModel: deleteModel,
    drawMarker: drawMarker,
    drawCylinder: drawCylinder,
    changeModelTransparency: changeModelTransparency,
    hideShowModel: hideShowModel,
    flyToModel: flyToModel,
    goToModel: goToModel,
    goToModelPromise: goToModelPromise,
    deleteAllGCP: deleteAllGCP,
    lockCamera: lockCamera,
    goToNavigationMode: goToNavigationMode,
    drawCameraPosition: drawCameraPosition,
    setCollisionDetection: setCollisionDetection,
    relaxCollisionDetection: relaxCollisionDetection,
    drawGCP: drawGCP,
    drawGCPLabel: drawGCPLabel,
    unselectAll: unselectAll,
    selectGCP: selectGCP,
    deleteGCP: deleteGCP,
    turnUp: turnUp,
    turnDown: turnDown,
    turnLeft: turnLeft,
    turnRight: turnRight,
    moveUp: moveUp,
    moveDown: moveDown,
    moveLeft: moveLeft,
    moveRight: moveRight,
    moveFront: moveFront,
    moveBack: moveBack,
    moveIn: moveIn,
    moveOut: moveOut,
    drawToponyms: drawToponyms,
    cartMatrixPromiseFunction: cartMatrixPromiseFunction,
    getModel: function () { return modelGlobal },
    flyToImage: flyToImage,
    zoomIn: zoomIn,
    zoomOut: zoomOut,
    turn: turn,
    zoom: zoom,
    hideShowBuildings: hideShowBuildings,
    flyHome: flyHome,
    changeLatLng: changeLatLng,
    changeAzimuth: changeAzimuth
  }
})
