angular.module('markerApp')

.service('markerService', function ($q, $http) {
  // function loadUserCredentials() {
  var getMarkersImage = function (imageId) {
    var sendData = {
      imageId: imageId
    }

    var markerPromise = $http.post('/markers/getMarkersImage', sendData).then(function (result) { // API_ENDPOINT.url +
      if (result.data) {
        geojsonMarkers = result.data
        return result.data
      };
    })
    return markerPromise
  }

  var drawCurMarker = function (curMarker, cesium) { //, el
    // Draw toponym on the windows
    var cart = Cesium.Cartesian3.fromDegrees(curMarker.longitude, curMarker.latitude, curMarker.height)
    var pt2D = Cesium.SceneTransforms.wgs84ToWindowCoordinates(cesium.scene, cart)

    if (typeof pt2D !== 'undefined') {
      if ((pt2D.x > 0) && (pt2D.x < cesium.canvas.width) && (pt2D.y > 0) && (pt2D.y < cesium.canvas.height)) {
        var el = document.getElementById('cesiumContainer')
        var rect = el.getBoundingClientRect()

        var yOff = rect.top// 0;//rect.top;
        var xOff = rect.left// padding;
        var heightIcon = 53
        var widthIcon = 53

        var xCoord = xOff + pt2D.x// +widthIcon;//xOff + pt2D.x+widthIcon/2;
        var yCoord = yOff + pt2D.y - heightIcon// yOff + pt2D.y+heightIcon/2;

        var location = {
          x: xCoord.toFixed(1).concat('px'),
          y: yCoord.toFixed(1).concat('px')
        }
        return location
      } // undefined
    }
  }

  var drawMarkers = function (dicMarker, cesium, tilesLoadedPromise) {
    // Draw toponym on the windows

    var promise = tilesLoadedPromise.then(function () {
      for (var id in dicMarker) {
        var marker = dicMarker[id]
        // Draw toponym
        var location = drawCurMarker(marker, cesium)
        marker.x = location.x
        marker.y = location.y
        marker.style = {
          'position': 'fixed',
          'top': location.y,
          'left': location.x,
          'z-index': 10
        }
      } // for loop
      return dicMarker
    })// then
    return promise
  }

  var postMarker = function (marker, volunteerId, imageId) {
    var sendData = {
      marker: marker,
      volunteerId: volunteerId,
      imageId: imageId
    }
    var postMarkerPromise = $http.post('/markers/createMarker', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postMarkerPromise
  }

  return {
    getMarkersImage: getMarkersImage,
    drawMarkers: drawMarkers,
    drawCurMarker: drawCurMarker,
    postMarker: postMarker
  }
})
