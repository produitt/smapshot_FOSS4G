angular.module('forumApp')

.service('forumService', function ($q, $http) {
  var getComments = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var getForumPromise = $http.post('/forum/getForum', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return getForumPromise
  }

  var postComment = function (comment, volunteerId, imageId) {
    var sendData = {
      comment: comment,
      volunteerId: volunteerId,
      imageId: imageId
    }
    var postCommentPromise = $http.post('/forum/createComment', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postCommentPromise
  }

  return {
    postComment: postComment,
    getComments: getComments
  }
})
