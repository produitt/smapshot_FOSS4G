angular.module('collectionsQueryApp')

.service('collectionsQueryService', function ($q, $http) {


  var getList = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      var httpPromise = $http.post('/collections/getList/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getDescription = function (collectionId) {
    return $q(function (resolve, reject) {
      var sendData = {
        collectionId: collectionId
      }
      var httpPromise = $http.post('/collections/getDescription/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data[0])
      })
    })
  }

  var getMy = function (volunteerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        volunteerId: volunteerId
      }
      var httpPromise = $http.post('/collections/getMy/', sendData)
      httpPromise.then(function (response) {
        myCollections = {
          list: response.data,
          selected: response.data[0]
        }
        resolve(myCollections)
      })
    })
  }

  var getCollections = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      var httpPromise = $http.post('/collections/getCollections/', sendData)
      httpPromise.then(function (response) {
        var collections = response.data
        for (var i=0;i<collections.length;i++){
          collections[i].long_descr = JSON.parse(collections[i].long_descr)
          collections[i].showMore = false
          collections[i].selected = false
        }
        resolve(collections)
      })
    })
  }

  return {
    getList: getList,
    getDescription: getDescription,
    getMy: getMy,
    getCollections: getCollections
  }

})
