angular.module('badGeolocApp')

.service('badGeolocService', function ($q, $http) {

  //Reject bad geolocalization
  var remove = function (id) {
    var sendData = {
      imageId: id
    }
    var postPromise = $http.post('/badGeoloc/remove/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postPromise
  }

  //Reject bad geolocalization
  var reject = function (id, validatorId) {
    var sendData = {
      imageId: id,
      validatorId: validatorId
    }
    var postPromise = $http.post('/badGeoloc/reject/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postPromise
  }

  //Validate bad geolocalization
  var validate = function (id, validatorId) {
    var sendData = {
      imageId: id,
      validatorId: validatorId
    }
    var postPromise = $http.post('/badGeoloc/validate/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postPromise
  }

  //Submit bad geolocalization
  var submit = function (imageId, volunteerId) {
    var sendData = {
      imageId: imageId
      // volunteerId: volunteerId,
    }
    var postPromise = $http.post('/badGeoloc/submit/', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postPromise
  }

  // Get bad geolocalizations which werent processed
  var getDashboard = function (sendData) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/badGeoloc/getDashboard/', sendData)
      httpPromise.then(function (res) {
        var list = res.data
        resolve(list)
      })
    })
  }

  return {
    remove: remove,
    reject: reject,
    validate: validate,
    submit: submit,
    getDashboard: getDashboard
  }
})
