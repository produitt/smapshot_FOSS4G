angular.module('osdApp')

.service('osdService', function ($q, $http) {

  var disableSelection = function(osd){
     osd.viewer.selectionInstance.disable()
  }
  var enableSelection = function(osd, selectionValidation){
    // Add selection to the viewer
    var rect = new OpenSeadragon.SelectionRect(
      parseFloat(0.1),
      parseFloat(0.1),
      parseFloat(0.1),
      parseFloat(0.1)
    )
    // Draw rectangle
    osd.viewer.selectionInstance.rect = rect
    osd.viewer.selectionInstance.enable()
    osd.viewer.forceRedraw()
  }

  var initializeSelection = function(osd, selectionValidation){
    // Add selection to the viewer
    var rect = new OpenSeadragon.SelectionRect(
      parseFloat(0.1),
      parseFloat(0.1),
      parseFloat(0.1),
      parseFloat(0.1)
    )
    // Draw rectangle
    var selection = osd.viewer.selection({
      element: null, // html element to use for overlay
      showSelectionControl: false, // show button to toggle selection mode
      toggleButton: null, // dom element to use as toggle button
      showConfirmDenyButtons: true,
      styleConfirmDenyButtons: true,
      returnPixelCoordinates: false, // We want relative coordinates
      keyboardShortcut: null, //'c', // key to toggle selection mode
      rect: rect, // initial selection as an OpenSeadragon.SelectionRect object
      allowRotation: false, // turn selection rotation on or off as needed
      startRotated: false, // alternative method for drawing the selection; useful for rotated crops
      startRotatedHeight: 0.1, // only used if startRotated=true; value is relative to image height
      restrictToImage: true, // true = do not allow any part of the selection to be outside the image
      onSelection: function(rect) {
        selectionValidation(rect)
      }, // callback
      onCancel: function(){
        console.log('cancel')
      },
      prefixUrl: null, // overwrites OpenSeadragon's option
      navImages: { // overwrites OpenSeadragon's options
        selection: {
            REST:   'selection_rest.png',
            GROUP:  'selection_grouphover.png',
            HOVER:  'selection_hover.png',
            DOWN:   'selection_pressed.png'
        },
        selectionConfirm: {
            REST:   'selection_confirm_rest.png',
            GROUP:  'selection_confirm_grouphover.png',
            HOVER:  'selection_confirm_hover.png',
            DOWN:   'selection_confirm_pressed.png'
        },
        selectionCancel: {
            REST:   'selection_cancel_rest.png',
            GROUP:  'selection_cancel_grouphover.png',
            HOVER:  'selection_cancel_hover.png',
            DOWN:   'selection_cancel_pressed.png'
        },
      }
    })
    selection.enable()
  }

  // var startRectangle = function(osd){
  //   var rect = new OpenSeadragon.SelectionRect(
  //     parseFloat(0.1),
  //     parseFloat(0.1),
  //     parseFloat(0.1),
  //     parseFloat(0.1)
  //   )
  //   osd.viewer.selectionInstance.rect = rect
  //   var zoomLevel = osd.viewer.viewport.getZoom(true)
  //   var event = osd.viewer.viewport.zoomTo(zoomLevel - 0.01, rect, true)
  // }

  var drawRectangleObservation = function (osd, observation, withText){
    var rect = new OpenSeadragon.Rect(
      parseFloat(observation.coord_x),
      parseFloat(observation.coord_y),
      parseFloat(observation.width),
      parseFloat(observation.height)
    )
    // Draw rectangle
    var div = document.createElement("div")
    div.className = 'rect-observation'
    div.innerHTML = ''
    div.id = 'obsRect-' + observation.id
    osd.viewer.addOverlay(div, rect)
    // Draw text
    var point = {
      x: parseFloat(observation.coord_x)+parseFloat(observation.width),//observation.coord_x,
      y: parseFloat(observation.coord_y)+parseFloat(observation.height),//observation.coord_y
    }
    var div = document.createElement("div")
    div.className = 'highlight'
    div.id = 'obsText-'+ observation.id
    if (withText){
      var innerHtml = '"'+observation.observation + '"'
      if (observation.username){
        innerHtml = innerHtml + '<br>' + observation.username
      }
      if (observation.date_string){
        innerHtml = innerHtml + '<br>' + observation.date_string
      }
      div.innerHTML = innerHtml
    }else{
      div.innerHTML = '<i class="fa fa-plus" aria-hidden="true"></i>'
    }

    // div.id = 'obs-'+ observation.obs_id

    div.onclick = function () {
      var innerHtml = '"'+observation.observation + '"'
      if (observation.username){
        innerHtml = innerHtml + '<br>' + observation.username
      }
      if (observation.date_string){
        innerHtml = innerHtml + '<br>' + observation.date_string
      }
      if (div.innerHTML === '<i class="fa fa-plus" aria-hidden="true"></i>') {
        div.innerHTML = innerHtml //'"'+observation.observation + '"<br>' + observation.username + '<br>' + observation.date_string
      }else{
        div.innerHTML = '<i class="fa fa-plus" aria-hidden="true"></i>'

      }
    }
    var ov = osd.viewer.addOverlay(div, point, 'TOP-LEFT')
  }

  var disableMouseNav = function (osd){
    osd.viewer.setMouseNavEnabled(false)
  }

  var enableMouseNav = function (osd){
    osd.viewer.setMouseNavEnabled(true)
  }

  var drawRectangleInput = function (osd, observation, validate, cancel){
    console.log('draw rectangle', osd)


    // Location of the textarea
    var rect = new OpenSeadragon.Rect(
      parseFloat(observation.coord_x),
      parseFloat(observation.coord_y),
      parseFloat(observation.width),
      parseFloat(observation.height)
    )
    // Zoom on the area
    osd.viewer.viewport.fitBoundsWithConstraints(rect)
    // Draw rectangle
    var ta = document.createElement("textarea")
    ta.setAttribute('cols',40);
    ta.setAttribute('rows', 4);
    ta.setAttribute('placeholder', 'Write your observation')
    ta.className = 'rect-observation-input'
    ta.id = 'obsTextArea'
    ta.onclick = function (event) {
      // Disable the selection
      osd.viewer.selectionInstance.disable()
      // Set the focus on the textarea (enable writing)
      event.srcElement.focus()
      // Disable osd keyboard navigation
      osd.viewer.innerTracker.keyDownHandler = null;
      osd.viewer.innerTracker.keyPressHandler = null;
      osd.viewer.innerTracker.keyHandler = null;
    }
    osd.viewer.addOverlay(ta, rect)


    // Location of the buttons
    var point = {
      x: parseFloat(observation.coord_x),//observation.coord_x,
      y: parseFloat(observation.coord_y)+parseFloat(observation.height),//observation.coord_y
    }
    // Create div
    var divB = document.createElement('div')
    divB.id = 'obsButtons'
    // Create cancel button
    var cancelButton = document.createElement('button')
    cancelButton.innerHTML = 'Cancel'
    cancelButton.className = 'btn yellow'
    cancelButton.onclick = function (event){
      // Delete div
      osd.viewer.removeOverlay('obsTextArea')
      osd.viewer.removeOverlay('obsButtons')
      osd.viewer.viewport.goHome()
      disableSelection(osd)
      cancel()
    }
    divB.appendChild(cancelButton)
    // Create submit button
    var submitButton = document.createElement('button')
    submitButton.innerHTML = 'Submit'
    submitButton.className = 'btn green'
    submitButton.onclick = function(event){
      // Get and record observation text
      observation.observation = document.getElementById('obsTextArea').value
      // Delete div
      osd.viewer.removeOverlay('obsTextArea')
      osd.viewer.removeOverlay('obsButtons')
      osd.viewer.viewport.goHome()
      disableSelection(osd)
      // Validate observation
      validate(observation)
    }
    divB.appendChild(submitButton)
    osd.viewer.addOverlay(divB, point)
  }

  // Draw the image in Cesium
  // ------------------------
  var drawCursor = function (osd, xy) {
    var x = math.squeeze(xy[0])
    var y = math.squeeze(xy[1])
    // Get image coordinates
    var xCur = math.subset(x, math.index(0))
    var yCur = math.subset(y, math.index(0))
    var viewportPoint = osd.viewer.viewport.imageToViewportCoordinates(xCur, yCur)
    // Get image parameters
    var imageWidth = osd.viewer.source.Image.Size.Width
    var imageHeight = osd.viewer.source.Image.Size.Height
    var ratio = imageHeight / imageWidth
    // The viewport limits are 1 and 1 for a square images, else 1 is equal to the width
    osd.viewer.removeOverlay('cursor')
    if ((viewportPoint.x > 0) && (viewportPoint.x < 1) && (viewportPoint.y > 0) && (viewportPoint.y < ratio)) {
      // Draw reprojected points
      var img = document.createElement('img')
      img.src = '../icons/cursor.png'
      img.id = 'cursor'
      osd.viewer.addOverlay(img, viewportPoint, 'TOP_LEFT')
    } else {
      // Remove existing reproj
      osd.viewer.removeOverlay('cursor')
    }
  }

  var deleteObservation = function (osd, observation) {
    osd.viewer.removeOverlay('obsText-'+observation.id)
    osd.viewer.removeOverlay('obsRect-'+observation.id)
  }

  var deleteAllObservations = function (osd, observations) {
    for (var i=0; i<observations.length; i++) {
      var observation = observations[i]
      osd.viewer.removeOverlay('obsText-'+observation.id)
      osd.viewer.removeOverlay('obsRect-'+observation.id)
    }
  }

  var drawGCP = function (osd, gcp, options) {
    var img = document.createElement('img')
    img.src = options.gcpIconSelected
    img.id = gcp.id// $scope.myGCPs.curGCPid;
    img.isReproj = false
    var viewportPoint = osd.viewer.viewport.imageToViewportCoordinates(gcp.x, gcp.y)
    var ov = osd.viewer.addOverlay(img, viewportPoint, 'CENTER')//, 'CENTER'
    var zoomLevel = osd.viewer.viewport.getZoom(true)
    var event = osd.viewer.viewport.zoomTo(zoomLevel - 0.01, viewportPoint, true)
    osd.viewer.viewport.goHome()
  }

  var drawGCPLabel = function (osd, gcp, options) {
    var img = document.createElement('img')
    img.src = options.gcpIcon
    img.id = gcp.id.toString()
    img.isReproj = false
    img.className = 'gcp'

    try {
      osd.viewer.removeOverlay(gcp.id.toString())
      osd.viewer.removeOverlay(gcp.id+'_label')
    }catch(err){

    }

    //var viewportPoint = osd.viewer.TiledImage.imageToViewportCoordinates(gcp.x, gcp.y);
    var viewportPoint = osd.viewer.viewport.imageToViewportCoordinates(parseFloat(gcp.x), parseFloat(gcp.y))

    var ov = osd.viewer.addOverlay(img, viewportPoint, 'CENTER')// CENTER, var ov =

    var label = document.createElement('p')
    label.id = gcp.id + '_label'
    label.innerHTML = (gcp.id + 1).toString()
    label.className = 'label'
    // var viewportPoint = $scope.mysd.viewer.TiledImage.imageToViewportCoordinates(xy[0], xy[1]);
    var viewportPoint = osd.viewer.viewport.imageToViewportCoordinates(gcp.x, gcp.y)
    var ov = osd.viewer.addOverlay(label, viewportPoint, 'CENTER')
    //Trick for the placement
    var zoomLevel = osd.viewer.viewport.getZoom(true)
    var event = osd.viewer.viewport.zoomTo(zoomLevel - 0.01, viewportPoint, true)
    osd.viewer.viewport.goHome()
  }

  var unselectAll = function (osd, optionsViewer) {
    var nOv = osd.viewer.currentOverlays.length
    for (var i = 0; i < nOv; i++) {
      if (osd.viewer.currentOverlays[i].element.isReproj == false) {
        osd.viewer.currentOverlays[i].element.src = optionsViewer.gcpIcon
      };
    };
  }

  var selectGCP = function (osd, optionsViewer, idClicked) {
    var nOv = osd.viewer.currentOverlays.length
    for (var i = 0; i < nOv; i++) {
      if (osd.viewer.currentOverlays[i].element.id == idClicked) {
        if (osd.viewer.currentOverlays[i].element.isReproj == false) {
          osd.viewer.currentOverlays[i].element.src = optionsViewer.gcpIconSelected
        } else {

        };
      };
    };
  }

  var deleteGCP = function (osd, idClicked) {
    var nOv = osd.viewer.currentOverlays.length
    for (var i = 0; i < nOv; i++) {
      if ((typeof osd.viewer.currentOverlays[i] !== 'undefined') && (osd.viewer.currentOverlays[i].element.id == idClicked)) {
        // Remove gcp
        osd.viewer.removeOverlay(osd.viewer.currentOverlays[i].element)
        // Remove label
        var idString = idClicked.toString()
        osd.viewer.removeOverlay(idString + '_label')
        // Remove reprojection
        var id = parseInt(idClicked) + 200
        osd.viewer.removeOverlay(id.toString())
      };
    };
  }

  var drawReprojections = function (osd, xy_reproj, gcpArray, myGCPs) {
    var x_reproj = math.squeeze(xy_reproj[0])
    var y_reproj = math.squeeze(xy_reproj[1])
    var nGCP = gcpArray.length

    for (var i = 0; i < nGCP; i++) {
      var id = gcpArray[i].id + 200
      // Get image coordinates
      var xCur = math.subset(x_reproj, math.index(i))
      var yCur = math.subset(y_reproj, math.index(i))
      var viewportPoint = osd.viewer.viewport.imageToViewportCoordinates(xCur, yCur)
      // Remove existing reproj
      osd.viewer.removeOverlay(id.toString())
      // Draw reprojected points
      var img = document.createElement('img')
      if (myGCPs.dictGCPs[gcpArray[i].id].eClass == 'error-low') {
        img.src = '../icons/georef/reproj_green.png'
      } else if (myGCPs.dictGCPs[gcpArray[i].id].eClass == 'error-medium') {
        img.src = '../icons/georef/reproj_orange.png'
      } else {
        img.src = '../icons/georef/reproj_red.png'
      }
      img.id = id.toString()
      img.isReproj = true
      var ov = osd.viewer.addOverlay(img, viewportPoint, 'CENTER')//, 'CENTER'
    };
    var zoomLevel = osd.viewer.viewport.getZoom(true)
    var event = osd.viewer.viewport.zoomTo(zoomLevel - 0.01, viewportPoint, true)
    osd.viewer.viewport.goHome()
  }

  return {

    drawCursor: drawCursor,
    drawGCP: drawGCP,
    drawGCPLabel: drawGCPLabel,
    unselectAll: unselectAll,
    selectGCP: selectGCP,
    deleteGCP: deleteGCP,
    drawReprojections: drawReprojections,
    // drawObservation: drawObservation,
    deleteObservation: deleteObservation,
    // drawImageObservation: drawImageObservation,
    // createImageObservation: createImageObservation,
    // fillImageObservation: fillImageObservation,
    enableSelection: enableSelection,
    disableSelection: disableSelection,
    // startRectangle: startRectangle,
    drawRectangleObservation: drawRectangleObservation,
    drawRectangleInput: drawRectangleInput,
    deleteAllObservations: deleteAllObservations,
    initializeSelection: initializeSelection,
    disableMouseNav: disableMouseNav,
    enableMouseNav: enableMouseNav
    // getModel: function() {return modelGlobal;},

  }
})
