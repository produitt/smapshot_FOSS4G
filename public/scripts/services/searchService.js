angular.module('searchApp')
.service('searchService', function ($q, $http) {
  var getImage = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: imageId
      }
      var httpPromise = $http.post('/search/getImage/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data.table[0])
      })
    })
  }

  var getNotGeolocatedBbox = function (bbox) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/search/getNotGeolocatedBbox', bbox)
      httpPromise.then(function (response) {
        resolve(response.data.table[0])
      }).catch(function (error) {
        console.log('error nearby', error)
      })
    })
  }
  var getNotGeolocatedNearby = function (lat, lng) {
    return $q(function (resolve, reject) {
      var sendData = {
        lat: lat,
        lng: lng
      }
      var httpPromise = $http.post('/search/getNotGeolocatedNearby', sendData)
      httpPromise.then(function (response) {
        resolve(response.data.table[0])
      }).catch(function (error) {
        console.log('error nearby', error)
      })
    })
  }

  var getVisits = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getVisits', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      }).catch(function (error) {
        console.log('error get visits', error)
      })
    })
  }

  var getImagesForSlider = function (listImageId, collectionId, imageId, latitude, longitude) {
    return $q(function (resolve, reject) {
      var sendData = {
        listImageId: listImageId,
        collectionId: collectionId,
        imageId: imageId,
        latitude: latitude,
        longitude: longitude
      }
      var httpPromise = $http.post('/search/getImagesForSlider/', sendData)
      httpPromise.then(function (response) {
        resolve(response)
      }, function (error) {
        reject()
      })
    })
  }

  var getImagesForMono = function (listImageId, collectionId, imageId, visitId) {
    return $q(function (resolve, reject) {
      var sendData = {
        listImageId: listImageId,
        collectionId: collectionId,
        visitId: visitId,
        imageId: imageId
      }
      var httpPromise = $http.post('/search/getImagesForMono/', sendData)
      httpPromise.then(function (response) {
        resolve(response)
      })
    })
  }

  var getMyImages = function (volunteerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        volunteerId: volunteerId
      }
      var httpPromise = $http.post('/search/getMyImages/', sendData)
      httpPromise.then(function (res) {
        var listImages = res.data.table

        var myImages = {
          list: [],
          owners: [],
          collections: [],
          maxDate: null,
          minDate: null
        }

        var myOwners = []
        var myCollections = []
        var myMinDate = new Date()
        var myMaxDate = new Date()

        for (var index in listImages) {
          // Get unique owners
          if (!myOwners.includes(listImages[index].ownername)) {
            myOwners.push(listImages[index].ownername)
          }
          // Get unique collections
          if (!myCollections.includes(listImages[index].collectionname)) {
            myCollections.push(listImages[index].collectionname)
          }
          // Get min date
          if (new Date(listImages[index].date_georef) < myMinDate) {
            myMinDate = new Date(listImages[index].date_georef)
          }
          // Get max date
          if (new Date(listImages[index].date_georef) > myMaxDate) {
            myMaxDate = new Date(listImages[index].date_georef)
          }
          // Get validation result
          listImages[index].validationResult = {
            errors_list: listImages[index].errors_list,
            remark: listImages[index].remark,
            other_desc: listImages[index].other_desc
          }
          delete listImages[index].errors_list
          delete listImages[index].remark
          delete listImages[index].other_desc
          // Create paths
          listImages[index].thumbSrc = '../images/thumbnails/'.concat(listImages[index].id).concat('.jpg')
          listImages[index].src500 = '../images/500/'.concat(listImages[index].id).concat('.jpg')
          // Create readable date
          listImages[index].date_georef = new Date(listImages[index].date_georef)
          var day = listImages[index].date_georef.getDate()
          if (day <= 9){
            day = '0'+day
          }
          var month = listImages[index].date_georef.getMonth() + 1
          if (month <= 9){
            month = '0'+month
          }
          var year = listImages[index].date_georef.getFullYear()
          listImages[index].date_string = day + '.' + month + '.' + year
          // Get owner
          listImages[index].ownerName = listImages[index].ownername
          delete listImages[index].ownername
          // Get collection
          listImages[index].collectionName = listImages[index].collectionname
          delete listImages[index].collectionname
          // Create status
          if (listImages[index].is_validated) {
            listImages[index].status = 'validated'
          } else if (listImages[index].improved) {
            listImages[index].status = 'improved'
          } else if (listImages[index].rejected) {
            listImages[index].status = 'rejected'
          } else {
            listImages[index].status = 'waiting'
          }
        }
        myImages.list = listImages
        myImages.owners = myOwners
        myImages.collections = myCollections
        myImages.maxDate = myMaxDate
        myImages.minDate = myMinDate
        resolve(myImages)
      })
    })
  }

  var getRandomImage = function (collectionId, imageId) {
    return $q(function (resolve, reject) {
      var sendData = {
        collectionId: collectionId,
        imageId: imageId
      }
      var httpPromise = $http.post('/game/randomImage/', sendData)
      httpPromise.then(function (res) {
        var data = res.data
        var image = {
          lat: data.latitude,
          lng: data.longitude,
          height: data.height,
          tilt: data.tilt,
          heading: data.azimuth,
          pitch: data.roll,
          title: data.title,
          id: data.id,
          collectionId: data.collection_id
        }
        resolve(image)
      })
    })
  }

  var getVolunteersImages = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {}
      var httpPromise = $http.post('/search/getVolunteersImages/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data.table)
      })
    })
  }

  var getToValidateImages = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {}
      var httpPromise = $http.post('/search/getToValidateImages/', sendData)
      httpPromise.then(function (res) {
        var listImages = res.data.table
        for (var index in listImages) {
          listImages[index].thumbSrc = '../images/thumbnails/'.concat(listImages[index].id).concat('.jpg')
          listImages[index].date_georef = new Date(listImages[index].date_georef)
          var day = listImages[index].date_georef.getDate()
          var month = listImages[index].date_georef.getMonth() + 1
          var year = listImages[index].date_georef.getFullYear()
          listImages[index].date_string = day + '.' + month + '.' + year
        }
        resolve(listImages)
      })
    })
  }

  var getToponymsLike = function (userInput) {
    return $q(function (resolve, reject) {
      var sendData = {
        input: userInput
      }
      var httpPromise = $http.post('/search/getToponymsLike/', sendData)
      httpPromise.then(function (response) {
        var propositions = []
        for (id in response.data.table) {
          var prop = {
            name: response.data.table[id].name,
            lat: response.data.table[id].lat,
            lng: response.data.table[id].lng
          }
          propositions.push(prop)
        }
        resolve(propositions)
      })
    })
  }

  var getToponymsSoundLike = function (userInput) {
    return $q(function (resolve, reject) {
      var sendData = {
        input: userInput
      }
      var httpPromise = $http.post('/search/getToponymsSoundLike/', sendData)
      httpPromise.then(function (response) {
        var propositions = []
        for (id in response.data.table) {
          var prop = {
            name: response.data.table[id].name,
            lat: response.data.table[id].lat,
            lng: response.data.table[id].lng
          }
          propositions.push(prop)
        }
        resolve(propositions)
      })
    })
  }

  var getToponymLocation = function (userInput) {
    return $q(function (resolve, reject) {
      var sendData = {
        input: userInput
      }
      var httpPromise = $http.post('/search/getToponymLocation/', sendData)
      httpPromise.then(function (response) {
        var prop = {
          name: response.data.table[0].name,
          lat: response.data.table[0].lat,
          lng: response.data.table[0].lng
        }
        resolve(prop)
      })
    })
  }

  var getCollections = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getCollections/', sendData)
      httpPromise.then(function (response) {
        var collections = response.data.table
        resolve(collections)
      })
    })
  }

  var getCurrentChallenge = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getCurrentChallenge/', sendData)
      httpPromise.then(function (response) {
        var collections = response.data.table
        resolve(collections)
      })
    })
  }

  var getFootprint = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: id
      }
      var httpPromise = $http.post('/search/getFootprint/', sendData)
      httpPromise.then(function (response) {
        var footprint = response.data
        resolve(footprint)
      })
    })
  }

  var getPhotographerLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        photographer_id: id
      }
      var httpPromise = $http.post('/search/getPhotographerLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getOwnerLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        owner_id: id
      }
      var httpPromise = $http.post('/search/getOwnerLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getCollectionLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        collection_id: id
      }
      var httpPromise = $http.post('/search/getCollectionLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getImagesFromCollection = function (collectionId) {
    return $q(function (resolve, reject) {
      var searchData = {
        collectionId: collectionId,
        georef: 'no' // in 'no', 'yes', 'all'
      }
      // Submit query
      var httpPromise = $http.post('/search/getImagesFromCollection/', searchData)
      httpPromise.then(function (response) {
        resolve(response)
      })
    })
  }

  var submitForm = function (search) {
    return $q(function (resolve, reject) {
      var searchData = {
        listCollectionId: [],
        dates: {
          modified: search.dates.modified,
          from: search.dates.from,
          to: search.dates.to
        },
        georef: {
          georef_3d_bool: false

        },
        roi: search.roi,
        toponyms: search.toponyms.selected
        // tags: search.tags.selected,
      }
      // Process collection selection
      for (id in search.collections.selected) {
        searchData.listCollectionId.push(search.collections.selected[id].id)
      }
      // Process georef
      if (search.georef.all == true) {
        searchData.georef.georef_3d_bool = null
      }
      if (search.georef.with == true) {
        searchData.georef.georef_3d_bool = true
      }
      if (search.georef.without == true) {
        searchData.georef.georef_3d_bool = false
      }
      // Process roi geojson
      if (search.roi.shape) {
        var roiGeoJson = {
          type: 'Polygon',
          coordinates: search.roi.shape.geometry.coordinates,
          crs: {
            type: 'name',
            properties: {
              name: 'EPSG:4326'
            }
          }
        }
      }
      searchData.roi.shapeString = JSON.stringify(roiGeoJson)
      // Submit query
      var httpPromise = $http.post('/search/submitForm/', searchData)
      httpPromise.then(function (response) {
        resolve(response)
      })
    })
  }




  return {
    getToponymsLike: getToponymsLike,
    getToponymsSoundLike: getToponymsSoundLike,
    getCollections: getCollections,
    // submitForm: submitForm,
    // getFootprint: getFootprint,
    getCurrentChallenge: getCurrentChallenge,
    // getImage: getImage,
    // getVolunteersImages: getVolunteersImages,
    // getImagesFromCollection: getImagesFromCollection,
    // getRandomImage: getRandomImage,
    // getMyImages: getMyImages,
    // getImagesForMono: getImagesForMono,
    getToponymLocation: getToponymLocation,
    getNotGeolocatedNearby: getNotGeolocatedNearby,
    getNotGeolocatedBbox: getNotGeolocatedBbox,
    getImagesForSlider: getImagesForSlider,
    getToValidateImages: getToValidateImages,
    // getPhotographerLink: getPhotographerLink,
    getCollectionLink: getCollectionLink,
    getOwnerLink: getOwnerLink,
    getVisits: getVisits,
    // getMapImages: getMapImages,
    // getMapImagesInBbox: getMapImagesInBbox
  }
})
