angular.module('dateApp')

.service('dateService', function($q, $http, $window) {

  // list of years

  // list of months
  var listMonths = {
    1: {fr: 'Janvier', en: 'January', de: 'Januar', it: 'Gennaio', mid: 1, short: 'Jan'},
    2: {fr: 'Février', en: 'February', de: 'Februar', it: 'Febbraio', mid: 2, short: 'Feb'},
    3: {fr: 'Mars', en: 'March', de: 'März', it: 'Marzo', mid: 3, short: 'Mar'},
    4: {fr: 'Avril', en: 'April', de: 'April', it: 'Aprile', mid: 4, short: 'Apr'},
    5: {fr: 'Mai', en: 'May', de: 'Mai', it: 'Maggio', mid: 5, short: 'Mai'},
    6: {fr: 'Juin', en: 'June', de: 'Juni', it: 'Giugno', mid: 6, short: 'Jun'},
    7: {fr: 'Juillet', en: 'July', de: 'Juli', it: 'Luglio', mid: 7, short: 'Jul'},
    8: {fr: 'Août', en: 'August', de: 'August', it: 'Agosto', mid: 8, short: 'Aug'},
    9: {fr: 'Septembre', en: 'September', de: 'September', it: 'Settembre', mid: 9, short: 'Sep'},
    10: {fr: 'Octobre', en: 'October', de: 'Oktober', it: 'Ottobre', mid: 10, short: 'Oct'},
    11: {fr: 'Novembre', en: 'November', de: 'November', it: 'Novembre', mid: 11, short: 'Nov'},
    12: {fr: 'Décembre', en: 'December', de: 'Dezember', it: 'Dicembre', mid: 12, short: 'Dec'}
  }
  // Change start month for ranking
  var selectStartMonth = function (month) {
    $scope.startMonth = month.mid
    $scope.startDate = new Date($scope.startYear, $scope.startMonth, 1)
    // Check if the end month is after start date
    $scope.checkUpperLimit()
    // Generate limit dates
    var limitDates = $scope.generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    // Query data for ranking
    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  // Change start year
  var selectStartYear = function (year) {
    $scope.startYear = year
    $scope.startDate = new Date($scope.startYear, $scope.startMonth, 1)
    $scope.checkUpperLimit()
    var limitDates = $scope.generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  // Change end month
  var selectEndMonth = function (month) {
    $scope.endMonth = month.mid
    $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0)
    $scope.checkLowerLimit()
    var limitDates = $scope.generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]
    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  var selectEndYear = function (year) {
    $scope.endYear = year
    $scope.endDate = new Date($scope.endYear, $scope.endMonth, 0)

    $scope.checkLowerLimit()

    var limitDates = $scope.generateLimitDates()
    var startDate = limitDates[0]
    var stopDate = limitDates[1]

    var searchData = {
      ownerId: $scope.ownerId,
      collectionId: $scope.collectionId,
      startDate: startDate,
      stopDate: stopDate,
      photographerId: null,
      collectionId: null,
      type: 'geoloc'
    }
    // Get the ranking of the volunteers
    volunteersService.getRanking(searchData)
    .then(function (response) {
      $scope.generateDataForRanking(response)
    })
  }
  var checkLowerLimit = function () {
    if ($scope.endDate < $scope.startDate) {
      $scope.startDate.setMonth($scope.endDate.getMonth() - 1)
      $scope.startDate.setYear($scope.endDate.getFullYear())

      $scope.startMonth = $scope.startDate.getMonth() + 1
      $scope.startYear = $scope.startDate.getFullYear()
    }
  }
  var checkUpperLimit = function () {
    if ($scope.endDate < $scope.startDate) {
      $scope.endDate.setMonth($scope.startDate.getMonth() + 1)
      $scope.endDate.setYear($scope.startDate.getFullYear())

      $scope.endMonth = $scope.endDate.getMonth() + 1
      $scope.endYear = $scope.endDate.getFullYear()
    }
  }
  var generateLimitDates = function () {
    // Generate start and end dates string
    if ($scope.endMonth.toString().length < 2) {
      var endMonth = '0' + $scope.endMonth
    } else {
      var endMonth = $scope.endMonth
    }
    if ($scope.startMonth.toString().length < 2) {
      var startMonth = '0' + $scope.startMonth
    } else {
      var startMonth = $scope.startMonth
    }
    var stopDate = [$scope.endYear, endMonth, '31'].join('-')
    var startDate = [$scope.startYear, startMonth, '01'].join('-')
    return [startDate, stopDate]
  }

  return {
    generateLimitDates: generateLimitDates,
    checkUpperLimit: checkUpperLimit,
    checkLowerLimit: checkLowerLimit,
    selectEndYear: selectEndYear,
    selectEndMonth: selectEndMonth,
    selectStartYear: selectStartYear,
    selectStartMonth: selectStartMonth,
    listMonths: function () { return listMonths }
  }
})
