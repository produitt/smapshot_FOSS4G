angular.module('remarksApp')

.service('remarksService', function ($q, $http) {

  //Push remark in the database
  var submitRemark = function (imageId, volunteerId, remark) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      remark: remark // contains: title, dateShot, caption
    }
    var postRemarkPromise = $http.post('/remarks/submit', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postRemarkPromise
  }

  // Get corrections which werent checked
  var getToValidateRemarks = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {}
      var httpPromise = $http.post('/remarks/getToValidateRemarks/', sendData)
      httpPromise.then(function (res) {
        var listRemarks = res.data
        var n = listRemarks.length
        // .table
        for (var i = 0; i < n; i++) {
          listRemarks[i].thumbSrc = '../data/collections/'+collection_id+'/images/thumbnails/'.concat(listRemarks[i].image_id).concat('.jpg')
          listRemarks[i].date_remark = new Date(listRemarks[i].date_remark)
          var day = listRemarks[i].date_remark.getDate()
          var month = listRemarks[i].date_remark.getMonth() + 1
          var year = listRemarks[i].date_remark.getFullYear()
          listRemarks[i].hasReply = false
          // listRemarks[i].date_string = day + '.' + month + '.' + year
          // // Check if the title is modified
          // if (listRemarks[i].sub_title) {
          //   listRemarks[i].hasTitle = true
          // } else {
          //   listRemarks[i].hasTitle = false
          // }
          // if (listRemarks[i].title_processed) {
          //   listRemarks[i].hasTitle = false
          // }
          // // Check if the caption is modified
          // if (listRemarks[i].sub_caption) {
          //   listRemarks[i].hasCaption = true
          // } else {
          //   listRemarks[i].hasCaption = false
          // }
          // if (listRemarks[i].caption_processed) {
          //   listRemarks[i].hasCaption = false
          // }
          // // Set is updated to false
          // listRemarks[i].titleIsUpdated = false
          // listRemarks[i].captionIsUpdated = false
        }
        resolve(listRemarks)
      })
    })
  }

  //
  var checked = function (remark) {
    return $q(function (resolve, reject) {
      if (!remark.hasReply) {
        remark.reply = null
      }
      var sendData = {
        remark: remark
      }
      var httpPromise = $http.post('/remarks/process/', sendData)
      httpPromise.then(function (res) {
        resolve()
      })
    })
  }

  return {
    submitRemark: submitRemark,
    getToValidateRemarks: getToValidateRemarks,
    checked: checked
  }
})
