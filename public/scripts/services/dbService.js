angular.module('dbApp')

.service('dbService', function ($q, $http) {

  var createGltf = function (cameraParameters, imageId, collectionId) {
    var url = '/georef/saveGltf/'// +$scope.imageId;
    var sendData = {
      imageId: imageId,
      cameraParameters: cameraParameters,
      collectionId: collectionId
    }
    var saveGltfPromise = $http.post(url, sendData)
    .then(function successCallback (response) {
      return response
    })
    return saveGltfPromise
  }

  // Reject geolocation
  var rejectGeoloc = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var promise = $http.post('/georef/reject/', sendData).then(function (result) {
      console.log('result reject', result)
    })
    return promise
  }

  // Reject geolocation
  var validateGeoloc = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var promise = $http.post('/georef/validate/', sendData).then(function (result) {
      console.log('result validate', result)
    })
    return promise
  }

  // Remove image from the game
  var removeFromGame = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var promise = $http.post('/game/remove/', sendData).then(function (result) {
      console.log('result remove from game', result)
    })
    return promise
  }

  // Add image in the game
  var addToGame = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    var promise = $http.post('/game/add/', sendData).then(function (result) {
      console.log('result add to game', result)
    })
    return promise
  }

  // Update position
  var postMapLocation = function (imageId, location, volunteerId) {
    var url = '/georef/saveMapLocation/'// +$scope.imageId;
    var sendData = {
      imageId: imageId,
      location: location,
      volunteerId: volunteerId
    }
    var postMapLocPromise = $http.post(url, sendData)
    .then(function (res) {
      return res
    })

    return postMapLocPromise
  }

  var postAzi = function (imageId, azimuth) {
    var url = '/georef/saveMapAzimuth/'// +$scope.imageId;
    var sendData = {
      imageId: imageId,
      azimuth: azimuth
    }
    var postAziPromise = $http.post(url, sendData)
    .then(function successCallback (response) {
      return response
    })
    return postAziPromise
  }

  var postFootprint = function (imageId, footprintGeojson, latitude, longitude) {

    //Send geojson to the database
    var url = '/georef/saveFootprint/'

    var sendData = {
      imageId: imageId,
      footprintGeojson: footprintGeojson,
      latitude: latitude,
      longitude: longitude
    }
    var postFootprintPromise = $http.post(url, sendData)
    .then(function (response) {
      return response
    })
    return postFootprintPromise
  }

  var copyGltf = function (imageId, collectionId) {
    var url = '/georef/copyGltf/'
    var sendData = {
      imageId: imageId,
      collectionId: collectionId
    }
    var gltfPromise = $http.post(url, sendData).then(function (response) {
      return response
    }, function (response) {
      return response
    })
    return gltfPromise
  }

  var updateOrientation = function (imageId, orientationArray, volunteerId, gcpJson, score, surface, validationMode, validatorId) {
    return $q(function (resolve, reject) {
      var url = '/georef/updateOrientation/'// +$scope.imageId;
      var sendData = {
        imageId: imageId,
        orientation: orientationArray[0],
        volunteerId: volunteerId,
        gcpArray: JSON.stringify(gcpJson),
        score: score,
        surfaceRatio: surface,
        validationMode: validationMode,
        validatorId: validatorId
      }
      $http.post(url, sendData)
        .then(function successCallback (response) {
          // this callback will be called asynchronously
          // when the response is available
          resolve()
        }, function errorCallback (response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          reject()
        })
      })
  }

  var saveOrientation = function (imageId, orientationArray, volunteerId, gcpJson, score, surface) {
    return $q(function (resolve, reject) {
      var url = '/georef/saveOrientation/'
      var sendData = {
        imageId: imageId,
        orientation: orientationArray[0],
        volunteerId: volunteerId,
        gcpArray: JSON.stringify(gcpJson),
        score: score,
        surfaceRatio: surface
      }
      $http.post(url, sendData)
        .then(function successCallback (response) {
          // this callback will be called asynchronously
          // when the response is available
          var url = '/validation/recordStopTime'
          var sendData = {
            imageId: imageId,
            volunteerId: volunteerId
          }
          $http.post(url, sendData)
            .then(function successCallback (response) {
              resolve()
            })
          //resolve()

        }, function errorCallback (response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          reject()
        })
      })
  }

  var saveCollada = function (xmlData) {
    var url = '/georef/save3Dmodel/'
    var gltfPromise = $http.post(url, xmlData).then(function (response) {
      return response
    }, function (response) {
      return response
    })
    return gltfPromise
  }

  var saveVolunteerId = function (imageId, volunteerId) {
    var url = '/georef/saveVolunteerId/'
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId
    }
    var saveVolunteerPromise = $http.post(url, sendData).then(function (response) {
      return response
    }, function (response) {
      return response
    })
    return saveVolunteerPromise
  }

  return {
    createGltf: createGltf,
    //getImage: getImage,
    postMapLocation: postMapLocation,
    postAzi: postAzi,
    postFootprint: postFootprint,
    saveCollada: saveCollada,
    rejectGeoloc: rejectGeoloc,
    validateGeoloc: validateGeoloc,
    addToGame: addToGame,
    removeFromGame: removeFromGame,
    saveVolunteerId: saveVolunteerId,
    copyGltf: copyGltf,
    saveOrientation: saveOrientation,
    updateOrientation: updateOrientation
  }
})
