angular.module('imageQueryApp')
.service('imageQueryService', function ($q, $http) {

  var getImage = function (imageId) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: imageId
      }
      var httpPromise = $http.post('/search/getImage/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data[0])
      })
    })
  }

  // Create a new entry in images_views to record the number of views
  var incrementViews = function (imageId, type) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: imageId,
        type: type
      }
      var httpPromise = $http.post('/images/incrementViews', sendData)
      httpPromise.then(function (response) {
        console.log('increment view resolve')
        resolve()
      })
    })
  }

  // Create a new entry in images_downloads to record the number of downloads
  var incrementDownloads = function (imageId, type) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: imageId,
        type: type
      }
      var httpPromise = $http.post('/images/incrementDownloads', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }

  var getNumberNotGeoref = function (ownerId, collectionId) {
    var searchData = {
      collectionId: collectionId,
      ownerId: ownerId,
    }
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/images/numberNotGeoref', searchData)
      httpPromise.then(function (response) {
        resolve(response.data)
      }).catch(function (error) {
        console.log('error get number', error)
      })
    })
  }
  var getNotGeolocatedBbox = function (bbox) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/search/getNotGeolocatedBbox', bbox)
      httpPromise.then(function (response) {
        resolve(response.data.table[0])
      }).catch(function (error) {
        console.log('error nearby', error)
      })
    })
  }
  var getNotGeolocatedNearby = function (lat, lng) {
    return $q(function (resolve, reject) {
      var sendData = {
        lat: lat,
        lng: lng
      }
      var httpPromise = $http.post('/search/getNotGeolocatedNearby', sendData)
      httpPromise.then(function (response) {
        resolve(response.data.table[0])
      }).catch(function (error) {
        console.log('error nearby', error)
      })
    })
  }

  var getVisits = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getVisits', sendData)
      httpPromise.then(function (response) {
        resolve(response.data)
      }).catch(function (error) {
        console.log('error get visits', error)
      })
    })
  }

  var getImagesForSlider = function (listImageId, collectionId, imageId, latitude, longitude) {
    return $q(function (resolve, reject) {
      var sendData = {
        listImageId: listImageId,
        collectionId: collectionId,
        imageId: imageId,
        latitude: latitude,
        longitude: longitude
      }
      var httpPromise = $http.post('/search/getImagesForSlider/', sendData)
      httpPromise.then(function (response) {
        resolve(response)
      }, function (error) {
        reject()
      })
    })
  }

  var getImagesForMono = function (listImageId, collectionId, imageId, visitId) {
    return $q(function (resolve, reject) {
      var sendData = {
        listImageId: listImageId,
        collectionId: collectionId,
        visitId: visitId,
        imageId: imageId
      }
      var httpPromise = $http.post('/search/getImagesForMono/', sendData)
      httpPromise.then(function (response) {
        resolve(response)
      })
    })
  }

  var getMyImages = function (volunteerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        volunteerId: volunteerId
      }
      var httpPromise = $http.post('/search/getMyImages/', sendData)
      httpPromise.then(function (res) {
        var listImages = res.data.table
        var listCollections = []
        var listOwners = []

        for (var index in listImages) {
          listImages[index].thumbSrc = '../data/collections/' + listImages[index].collection_id + '/images/thumbnails/'.concat(listImages[index].id).concat('.jpg')
          listImages[index].src500 = '../data/collections/' + listImages[index].collection_id + '/images/500/'.concat(listImages[index].id).concat('.jpg')
          listImages[index].date = new Date(listImages[index].date_georef)
          if (index == 0) {
            var minDate = new Date(listImages[index].date_georef)
            var maxDate = new Date(listImages[index].date_georef)
          }
          if (new Date(listImages[index].date) > maxDate) {
            maxDate = new Date(listImages[index].date)
          }
          if (new Date(listImages[index].date) < minDate) {
            minDate = new Date(listImages[index].date)
          }
          var day = listImages[index].date.getDate()
          var month = listImages[index].date.getMonth() + 1
          var year = listImages[index].date.getFullYear()
          listImages[index].date_string = day + '.' + month + '.' + year
          if (listCollections.indexOf(listImages[index].collectionname) == -1){
            listCollections.push(listImages[index].collectionname)
          }
          if (listOwners.indexOf(listImages[index].ownername) == -1){
            listOwners.push(listImages[index].ownername)
          }

          if (listImages[index].validated) {
            listImages[index].status = 'validated'
          } else if (listImages[index].improved) {
            listImages[index].status = 'improved'
          } else if (listImages[index].rejected) {
            listImages[index].status = 'rejected'
          }else {
            listImages[index].status = 'waiting'
          }

        }
        res = {
          listImages: listImages,
          listOwners: listOwners,
          listCollections: listCollections,
          minDate: minDate,
          maxDate: maxDate
        }
        resolve(res)
      })
    })
  }

  var getRandomImage = function (collectionId, imageId) {
    return $q(function (resolve, reject) {
      var sendData = {
        collectionId: collectionId,
        imageId: imageId
      }
      var httpPromise = $http.post('/game/randomImage/', sendData)
      httpPromise.then(function (res) {
        var data = res.data
        var image = {
          lat: data.latitude,
          lng: data.longitude,
          height: data.height,
          tilt: data.tilt,
          heading: data.azimuth,
          pitch: data.roll,
          title: data.title,
          id: data.id,
          collectionId: data.collection_id
        }
        resolve(image)
      })
    })
  }

  var getVolunteersImages = function () {
    return $q(function (resolve, reject) {
      var sendData = {}
      var httpPromise = $http.post('/search/getVolunteersImages/', sendData)
      httpPromise.then(function (response) {
        resolve(response.data.table)
      })
    })
  }

  var getToValidateImages = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      var httpPromise = $http.post('/search/getToValidateImages/', sendData)
      httpPromise.then(function (res) {
        var listImages = res.data.table
        for (var index in listImages) {
          listImages[index].thumbSrc = '../data/collections/' + listImages[index].collection_id + '/images/thumbnails/'.concat(listImages[index].id).concat('.jpg')
          listImages[index].date_georef = new Date(listImages[index].date_georef)
          var day = listImages[index].date_georef.getDate()
          var month = listImages[index].date_georef.getMonth() + 1
          var year = listImages[index].date_georef.getFullYear()
          listImages[index].date_string = day + '.' + month + '.' + year
        }
        resolve(listImages)
      })
    })
  }

  // var getToponymsLike = function (userInput) {
  //   return $q(function (resolve, reject) {
  //     var sendData = {
  //       input: userInput
  //     }
  //     var httpPromise = $http.post('/search/getToponymsLike/', sendData)
  //     httpPromise.then(function (response) {
  //       var propositions = []
  //       for (id in response.data.table) {
  //         var prop = {
  //           name: response.data.table[id].name,
  //           lat: response.data.table[id].lat,
  //           lng: response.data.table[id].lng
  //         }
  //         propositions.push(prop)
  //       }
  //       resolve(propositions)
  //     })
  //   })
  // }
  //
  // var getToponymsSoundLike = function (userInput) {
  //   return $q(function (resolve, reject) {
  //     var sendData = {
  //       input: userInput
  //     }
  //     var httpPromise = $http.post('/search/getToponymsSoundLike/', sendData)
  //     httpPromise.then(function (response) {
  //       var propositions = []
  //       for (id in response.data.table) {
  //         var prop = {
  //           name: response.data.table[id].name,
  //           lat: response.data.table[id].lat,
  //           lng: response.data.table[id].lng
  //         }
  //         propositions.push(prop)
  //       }
  //       resolve(propositions)
  //     })
  //   })
  // }
  //
  // var getToponymLocation = function (userInput) {
  //   return $q(function (resolve, reject) {
  //     var sendData = {
  //       input: userInput
  //     }
  //     var httpPromise = $http.post('/search/getToponymLocation/', sendData)
  //     httpPromise.then(function (response) {
  //       var prop = {
  //         name: response.data.table[0].name,
  //         lat: response.data.table[0].lat,
  //         lng: response.data.table[0].lng
  //       }
  //       resolve(prop)
  //     })
  //   })
  // }

  var getCollections = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getCollections/', sendData)
      httpPromise.then(function (response) {
        var collections = response.data.table
        resolve(collections)
      })
    })
  }

  var getCurrentChallenge = function () {
    return $q(function (resolve, reject) {
      var sendData = {
      }
      var httpPromise = $http.post('/search/getCurrentChallenge/', sendData)
      httpPromise.then(function (response) {
        var collections = response.data.table
        resolve(collections)
      })
    })
  }

  var getFootprint = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: id
      }
      var httpPromise = $http.post('/search/getFootprint/', sendData)
      httpPromise.then(function (response) {
        var footprint = response.data
        resolve(footprint)
      })
    })
  }

  var getPhotographerLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        photographer_id: id
      }
      var httpPromise = $http.post('/search/getPhotographerLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getOwnerLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        owner_id: id
      }
      var httpPromise = $http.post('/search/getOwnerLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getCollectionLink = function (id) {
    return $q(function (resolve, reject) {
      var sendData = {
        collection_id: id
      }
      var httpPromise = $http.post('/search/getCollectionLink/', sendData)
      httpPromise.then(function (response) {
        var link = response.data
        resolve(link)
      })
    })
  }

  var getImagesFromCollection = function (collectionId) {
    return $q(function (resolve, reject) {
      var searchData = {
        collectionId: collectionId,
        georef: 'no' // in 'no', 'yes', 'all'
      }
      // Submit query
      var httpPromise = $http.post('/search/getImagesFromCollection/', searchData)
      httpPromise.then(function (response) {
        resolve(response)
      })
    })
  }

  var processFilter = function (search) {
    var searchData = {
      listCollectionId: [],
      listOwnersId: [],
      dates: {
        // modified: search.dates.modified,
        from: search.dates.from,
        to: search.dates.to
      },
      georef: {
        georef_3d_bool: false

      },
      roi: search.roi,
      toponyms: search.toponyms.selected,
      keyword: search.keyword
      // tags: search.tags.selected,
    }
    // Process collection selection
    for (id in search.collections.selected) {
      searchData.listCollectionId.push(search.collections.selected[id].id)
    }
    // Process owner selection
    for (id in search.owners.selected) {
      searchData.listOwnersId.push(search.owners.selected[id].id)
    }
    // Process georef
    if (search.georef.all == true) {
      searchData.georef.georef_3d_bool = null
    }
    if (search.georef.with == true) {
      searchData.georef.georef_3d_bool = true
    }
    if (search.georef.without == true) {
      searchData.georef.georef_3d_bool = false
    }
    // Process roi geojson
    if (search.roi.shape) {
      var roiGeoJson = {
        type: 'Polygon',
        coordinates: search.roi.shape.geometry.coordinates,
        crs: {
          type: 'name',
          properties: {
            name: 'EPSG:4326'
          }
        }
      }
    }
    searchData.roi.shapeString = JSON.stringify(roiGeoJson)
    return searchData
  }
  var submitForm = function (search) {
    return $q(function (resolve, reject) {
      var searchData = processFilter(search)
      // Submit query
      var httpPromise = $http.post('/search/submitForm/', searchData)
      httpPromise.then(function (response) {
        var geojson = response.data
        var nFeat = geojson.features.length
        var images = []

        for (id=0; id<nFeat;id++){
          var feature = geojson.features[id]
          var keys = Object.keys(feature.properties)
          console.log('keys', keys)
          var image = {}
          for (var i=0; i<keys.length; i++){
            image[keys[i]] = feature.properties[keys[i]]
          }
          image.thumbSrc = "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg')
          image.tileSrc = "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi'
          image.latitude = feature.geometry.coordinates[1]
          image.longitude = feature.geometry.coordinates[0]
          //create images
          // var image = {
          //   id: feature.properties.id,
          //   name: feature.properties.name,
          //   thumbSrc: "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg'),
          //   georef_3d_bool: feature.properties.georef_3d_bool,
          //   owner_id: feature.properties.owner_id,
          //   volunteer_id: feature.properties.volunteer_id,
          //   collection_id: feature.properties.collection_id,
          //   photographer_id: feature.properties.photographer_id,
          //   license: feature.properties.license,
          //   date: feature.properties.date_shot,
          //   orig_date: feature.properties.orig_date,
          //   link: feature.properties.link,
          //   title: feature.properties.title,
          //   latitude: feature.geometry.coordinates[1],
          //   longitude: feature.geometry.coordinates[0],
          //   owner: feature.properties.owner,
          //   photographer: feature.properties.photographer,
          //   collection: feature.properties.collection,
          //   username: feature.properties.username,
          //   tileSrc: "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi',
          //   caption: feature.properties.caption,
          //   azimuth: feature.properties.azimuth,
          //   tilt: feature.properties.tilt,
          //   roll: feature.properties.roll,
          //   height: feature.properties.height,
          //   original_id: feature.properties.original_id,
          //   correction_enabled: feature.properties.correction_enabled,
          //   observation_enabled: feature.properties.observation_enabled,
          //   download_link: feature.properties.download_link,
          // }
          // images[feature.properties.id] = image
          images.push(image)
        }
        console.log('images submit form', images)
        var response = {
          geojson: geojson,
          images: images
        }
        resolve(response)
      })
    })
  }

  var getRatio = function (searchData) {
    return $q(function (resolve, reject) {
      // Get the number of geolocalised and not geolocalised images
      // search data (ownerId, startDate, stopDate, collectionId, photographerId)
      var httpPromise = $http.post('/search/getRatio', searchData)
      httpPromise.then(function (response) {
        resolve(response.data)
      })
    })
  }

  var getNearest = function (imageId, geolocalized, collectionId, ownerId, filter) {
    return $q(function (resolve, reject) {
      var searchData = {}
      if (filter) {
        searchData.collectionId = null
        searchData.ownerId = null
        searchData.filter = processFilter(filter)
      }else{
        searchData.collectionId = collectionId
        searchData.ownerId = ownerId
        searchData.filter = null
      }
      searchData.geolocalized = geolocalized
      searchData.imageId = imageId

      var httpPromise = $http.post('/images/getNearest/', searchData)
      httpPromise.then(function (response) {
        var images = response.data
        for (id=0; id<images.length;id++){
          var curIm = images[id]
          curIm.thumbSrc = "../data/collections/" + curIm.collection_id + "/images/thumbnails/".concat(curIm.id).concat('.jpg')
          curIm.date = curIm.date_shot
          curIm.tileSrc = "../data/collections/" + curIm.collection_id + "/images/tiles/" + curIm.id + '.dzi'
        }
        console.log('images back', images)
        resolve(images)
      })
    })
  }

  var getNoApriori = function (collectionId, ownerId) {
    return $q(function (resolve, reject) {
      var searchData = {
        collectionId: collectionId,
        ownerId: ownerId,
      }
      var httpPromise = $http.post('/map/noApriori/', searchData)
      httpPromise.then(function (response) {
        var images = response.data
        for (id=0; id<images.length;id++){
          var curIm = images[id]
          curIm.thumbSrc = "../data/collections/" + curIm.collection_id + "/images/thumbnails/".concat(curIm.id).concat('.jpg')
          curIm.date = curIm.date_shot
          curIm.tileSrc = "../data/collections/" + curIm.collection_id + "/images/tiles/" + curIm.id + '.dzi'
        }
        resolve(images)
      })
    })
  }

  var getMapImages = function (collectionId, ownerId, geolocalized, filter) {
    return $q(function (resolve, reject) {
      // var searchData = {
      //   collectionId: collectionId,
      //   ownerId: ownerId,
      //   geolocalized: geolocalized
      // }
      var searchData = {}
      if (filter) {
        searchData.collectionId = null
        searchData.ownerId = null
        searchData.filter = processFilter(filter)
      }else{
        searchData.collectionId = collectionId
        searchData.ownerId = ownerId
        searchData.filter = null
      }
      searchData.geolocalized = geolocalized

      console.log('get all search data', searchData)

      // Submit query
      var httpPromise = $http.post('/map/allImages/', searchData)
      httpPromise.then(function (response) {
        var geojson = response.data
        var nFeat = geojson.features.length
        var images = []//{}


        //for (var id in data.features){
        for (id=0; id<nFeat;id++){
          var feature = geojson.features[id]
          var keys = Object.keys(feature.properties)
          var image = {}
          for (var i=0; i<keys.length; i++){
            image[keys[i]] = feature.properties[keys[i]]
          }
          image.thumbSrc = "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg'),
          image.tileSrc = "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi'
          image.latitude = feature.geometry.coordinates[1]
          image.longitude = feature.geometry.coordinates[0]
          //create images
          // var image = {
          //   id: feature.properties.id,
          //   name: feature.properties.name,
          //   thumbSrc: "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg'),
          //   georef_3d_bool: feature.properties.georef_3d_bool,
          //   owner_id: feature.properties.owner_id,
          //   volunteer_id: feature.properties.volunteer_id,
          //   collection_id: feature.properties.collection_id,
          //   photographer_id: feature.properties.photographer_id,
          //   license: feature.properties.license,
          //   date: feature.properties.date_shot,
          //   orig_date: feature.properties.orig_date,
          //   link: feature.properties.link,
          //   title: feature.properties.title,
          //   latitude: feature.geometry.coordinates[1],
          //   longitude: feature.geometry.coordinates[0],
          //   owner: feature.properties.owner,
          //   photographer: feature.properties.photographer,
          //   collection: feature.properties.collection,
          //   username: feature.properties.username,
          //   tileSrc: "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi',
          //   caption: feature.properties.caption,
          //   azimuth: feature.properties.azimuth,
          //   tilt: feature.properties.tilt,
          //   roll: feature.properties.roll,
          //   height: feature.properties.height,
          //   original_id: feature.properties.original_id,
          //   correction_enabled: feature.properties.correction_enabled,
          //   observation_enabled: feature.properties.observation_enabled
          // }
          // images[feature.properties.id] = image
          images.push(image)
        }
        var response = {
          geojson: geojson,
          images: images
        }
        resolve(response)
      })
    })
  }

  var getMapImagesInBbox = function (collectionId, ownerId, geolocalized, bbox, filter) {
    return $q(function (resolve, reject) {
      // var searchData = {
      //   collectionId: collectionId,
      //   ownerId: ownerId,
      //   geolocalized: geolocalized,
      //   bbox: bbox
      // }
      var searchData = {}
      if (filter) {
        searchData.collectionId = null
        searchData.ownerId = null
        searchData.filter = processFilter(filter)
      }else{
        searchData.collectionId = collectionId
        searchData.ownerId = ownerId
        searchData.filter = null
      }
      searchData.geolocalized = geolocalized
      searchData.bbox = bbox

      // Submit query
      var httpPromise = $http.post('/map/imagesInBbox/', searchData)
      httpPromise.then(function (response) {
        var geojson = response.data
        var nFeat = geojson.features.length
        var images = []//{}
        for (id=0; id<nFeat;id++){
          var feature = geojson.features[id]
          var keys = Object.keys(feature.properties)
          var image = {}
          for (var i=0; i<keys.length; i++){
            image[keys[i]] = feature.properties[keys[i]]
          }
          image.thumbSrc = "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg'),
          image.tileSrc = "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi'
          image.latitude = feature.geometry.coordinates[1]
          image.longitude = feature.geometry.coordinates[0]
          //create images
          // var image = {
          //   id: feature.properties.id,
          //   name: feature.properties.name,
          //   thumbSrc: "../data/collections/"+feature.properties.collection_id+"/images/thumbnails/".concat(feature.properties.id).concat('.jpg'),
          //   georef_3d_bool: feature.properties.georef_3d_bool,
          //   owner_id: feature.properties.owner_id,
          //   volunteer_id: feature.properties.volunteer_id,
          //   collection_id: feature.properties.collection_id,
          //   photographer_id: feature.properties.photographer_id,
          //   license: feature.properties.license,
          //   date: feature.properties.date_shot,
          //   link: feature.properties.link,
          //   title: feature.properties.title,
          //   latitude: feature.geometry.coordinates[1],
          //   longitude: feature.geometry.coordinates[0],
          //   owner: feature.properties.owner,
          //   photographer: feature.properties.photographer,
          //   collection: feature.properties.collection,
          //   username: feature.properties.username,
          //   tileSrc: "../data/collections/" + feature.properties.collection_id + "/images/tiles/"+ feature.properties.id + '.dzi',
          //   caption: feature.properties.caption,
          //   azimuth: feature.properties.azimuth,
          //   tilt: feature.properties.tilt,
          //   roll: feature.properties.roll,
          //   height: feature.properties.height,
          //   original_id: feature.properties.original_id,
          //   correction_enabled: feature.properties.correction_enabled,
          //   observation_enabled: feature.properties.observation_enabled
          // }
          images.push(image)
        }
        var response = {
          geojson: geojson,
          images: images
        }
        resolve(response)
      })
    })
  }
  var getDashboard = function (searchData) {
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/getDashboard/', searchData)
      httpPromise.then(function (response) {
        var images = response.data
        console.log('images', images)
        var n = images.length
        for (var i=0; i<n; i++){
          images[i].count = parseInt(images[i].count)
          if (images[i].date_georef){
            images[i].date_string = images[i].date_georef.slice(0,16).replace('T', ' ')
          }

        }
        resolve(response.data)
      })
    })
  }
  var unlock = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/unlock/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }
  var lock = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/lock/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }
  var publish = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/publish/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }
  var unpublish = function (imageId) {
    var sendData = {
      imageId: imageId
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/unpublish/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }
  var publishList = function (imageIds) {
    var sendData = {
      imageIds: imageIds
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/publishList/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }
  var unpublishList = function (imageIds) {
    var sendData = {
      imageIds: imageIds
    }
    return $q(function (resolve, reject) {
      // Submit query
      var httpPromise = $http.post('/images/unpublishList/', sendData)
      httpPromise.then(function (response) {
        resolve()
      })
    })
  }

  var getDataForDownload = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/images/getForDownload/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        resolve(data)
      })
    })
  }

  var fullDownload = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/images/fullDownload/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        resolve(data)
      })
    })
  }

  var getSmapshotAddresses = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/images/getSmapshotAddresses/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        resolve(data)
      })
    })
  }
  // var getDashboard = function (ownerId, collectionId, dates, geolocalized, observed, corrected, originalId) {
  //   return $q(function (resolve, reject) {
  //     var searchData = {
  //       ownerId: ownerId,
  //       collectionId: collectionId,
  //       dates: dates,
  //       geolocalized: geolocalized,
  //       observed: observed,
  //       corrected: corrected,
  //       originalId: originalId
  //     }
  //     // Submit query
  //     var httpPromise = $http.post('/images/getDashboard/', searchData)
  //     httpPromise.then(function (response) {
  //       var images = response.data
  //       var n = images.length
  //       for (var i=0; i<n; i++){
  //         images[i].count = parseInt(images[i].count)
  //       }
  //       resolve(response.data)
  //     })
  //   })
  // }
  return {
    submitForm: submitForm,
    getFootprint: getFootprint,
    getImage: getImage,
    getVolunteersImages: getVolunteersImages,
    getImagesFromCollection: getImagesFromCollection,
    getRandomImage: getRandomImage,
    getMyImages: getMyImages,
    getImagesForMono: getImagesForMono,
    getNumberNotGeoref: getNumberNotGeoref,
    getNotGeolocatedNearby: getNotGeolocatedNearby,
    getNotGeolocatedBbox: getNotGeolocatedBbox,
    getImagesForSlider: getImagesForSlider,
    getToValidateImages: getToValidateImages,
    getPhotographerLink: getPhotographerLink,
    getCollectionLink: getCollectionLink,
    getOwnerLink: getOwnerLink,
    getRatio: getRatio,
    getMapImages: getMapImages,
    getMapImagesInBbox: getMapImagesInBbox,
    getNoApriori: getNoApriori,
    getNearest: getNearest,
    incrementViews: incrementViews,
    incrementDownloads: incrementDownloads,
    getDashboard: getDashboard,
    publish: publish,
    unpublish: unpublish,
    publishList: publishList,
    unpublishList: unpublishList,
    getDataForDownload: getDataForDownload,
    getSmapshotAddresses: getSmapshotAddresses,
    unlock: unlock,
    lock: lock,
    fullDownload: fullDownload
  }
})
