angular.module('volunteersApp')

.service('volunteersService', function ($q, $http) {

  var submitNewsletters = function (newsletters) {
    var sendData = {
      newsletters: newsletters
    }
    var promise = $http.post('/volunteers/submitNewsletters', sendData).then(function (result) {
      return promise
    })
  }

  var submitNotifications = function (notifications) {
    var sendData = {
      notifications: notifications
    }
    var promise = $http.post('/volunteers/submitNotifications', sendData).then(function (result) {
      return promise
    })
  }

  var submitUsername = function (username) {
    var sendData = {
      username: username
    }
    var promise = $http.post('/volunteers/submitUsername', sendData).then(function (result) {
      return promise
    })
  }

  var submitLang = function (lang) {
    var sendData = {
      lang: lang
    }
    var promise = $http.post('/volunteers/submitLang', sendData).then(function (result) {
      return promise
    })
  }

  var getRanking = function (searchData) {
    console.log('search data', searchData)
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/search/getRanking/', searchData)
      httpPromise.then(function (response) {
        if (searchData.type === 'geoloc'){
          resolve(response.data.table)
        } else if (searchData.type === 'obs'){
          var ranking = response.data.table
          // allocate the number of observation to the number of images
          for (var i=0; i< ranking.length; i++){
            ranking[i].nImages = ranking[i].nobs
          }
          resolve(ranking)
        }else if (searchData.type === 'modif'){
          var ranking = response.data.table
          // allocate the number of observation to the number of images
          for (var i=0; i< ranking.length; i++){
            ranking[i].nImages = ranking[i].ncorr
          }
          resolve(ranking)
        }
      })
    })
  }

  var getVisitorInfo = function (){
    return $q(function (resolve, reject) {
      $http.get('/volunteers/memberinfo').then(function (result) {
        // $scope.visitor = result.data.volunteer
        // $scope.myName = $scope.visitor.username
        resolve(result.data.volunteer)
      })
    })
  }

  var getValidatorsListByOwner = function (ownerId) {
    return $q(function (resolve, reject) {
      var sendData = {
        ownerId: ownerId
      }
      $http.get('/volunteers/validatorsListByOwner', sendData).then(function (result) {
        resolve(result.data)
      })
    })
  }

  var getEmails = function (filter) {
    return $q(function (resolve, reject) {
      $http.post('/volunteers/getEmails', filter).then(function (result) {
        res = {
          volunteers:result.data.table,
          emails: []
        }
        for (var i=0; i<result.data.table.length; i++){
          if (result.data.table[i].email){
            res.emails.push({email:result.data.table[i].email})
          }else{
            // res.emails[0].push('')
          }

        }
        resolve(res)
      })
    })
  }

  var getUsersByEmail = function (email) {
    console.log('email',email)
    return $q(function (resolve, reject) {
      $http.post('/volunteers/getByEmail', {email: email}).then(function (result) {
        console.log('res by email', result)
        resolve(result.data)
      })
    })
  }

  var giveSuperAdminRights = function (volunteerId) {
    var sendData = {
      volunteerId: volunteerId
    }
    var promise = $http.post('/volunteers/setSuperAdmin', sendData).then(function (result) {
      console.log('result admin', result)
      return promise
    })
  }

  var giveOwnerAdminRights = function (volunteerId, ownerId) {
    var sendData = {
      volunteerId: volunteerId,
      ownerId: ownerId
    }
    var promise = $http.post('/volunteers/setOwnerAdmin', sendData).then(function (result) {
      return promise
    })
  }

  var giveValidatorRights = function (volunteerId, ownerId) {
    var sendData = {
      volunteerId: volunteerId,
      ownerId: ownerId
    }
    var promise = $http.post('/volunteers/setValidator', sendData).then(function (result) {
      return promise
    })
  }

  var removeRights = function (volunteerId) {
    var sendData = {
      volunteerId: volunteerId
    }
    var promise = $http.post('/volunteers/removeRights', sendData).then(function (result) {
      return promise
    })
  }

  return {
    submitNewsletters: submitNewsletters,
    submitNotifications: submitNotifications,
    submitUsername: submitUsername,
    submitLang: submitLang,
    getRanking: getRanking,
    getVisitorInfo: getVisitorInfo,
    getValidatorsListByOwner: getValidatorsListByOwner,
    getEmails: getEmails,
    getUsersByEmail: getUsersByEmail,
    giveSuperAdminRights: giveSuperAdminRights,
    giveOwnerAdminRights: giveOwnerAdminRights,
    giveValidatorRights: giveValidatorRights,
    removeRights: removeRights
  }
})
