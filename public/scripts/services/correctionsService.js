angular.module('correctionsApp')

.service('correctionsService', function ($q, $http) {

  //Push correction in the database
  var submitCorrection = function (imageId, volunteerId, correction) {
    var sendData = {
      imageId: imageId,
      volunteerId: volunteerId,
      correction: correction // contains: title, dateShot, caption
    }
    var postCorrectionPromise = $http.post('/corrections/submit', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return postCorrectionPromise
  }
  var getDashboard = function (sendData) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/corrections/getDashboard/', sendData)
      httpPromise.then(function (res) {
        var listCorrections = res.data
        var n = listCorrections.length
        // .table
        for (var i = 0; i < n; i++) {
          listCorrections[i].thumbSrc = '../data/collections/'+listCorrections[i].collection_id+'/images/thumbnails/'.concat(listCorrections[i].image_id).concat('.jpg')
          // listCorrections[i].date_correction = new Date(listCorrections[i].date_correction)
          // var day = listCorrections[i].date_correction.getDate()
          // var month = listCorrections[i].date_correction.getMonth() + 1
          // var year = listCorrections[i].date_correction.getFullYear()
          listCorrections[i].date_string = listCorrections[i].date_correction.slice(0,10) //.replace('T', ' ')
          // Check if the title is modified
          if (listCorrections[i].sub_title) {
            listCorrections[i].hasTitle = true
          } else {
            listCorrections[i].hasTitle = false
          }
          // if (listCorrections[i].title_processed) {
          //   listCorrections[i].hasTitle = false
          // }
          // Check if the caption is modified
          if (listCorrections[i].sub_caption) {
            listCorrections[i].hasCaption = true
          } else {
            listCorrections[i].hasCaption = false
          }
          // if (listCorrections[i].caption_processed) {
          //   listCorrections[i].hasCaption = false
          // }
          // Set is updated to false
          listCorrections[i].titleIsUpdated = false
          listCorrections[i].captionIsUpdated = false
        }
        resolve(listCorrections)
      })
    })
  }
  // Get corrections which werent checked
  // var getToValidateCorrections = function (ownerId) {
  //   return $q(function (resolve, reject) {
  //     var sendData = {
  //       ownerId: ownerId
  //     }
  //     var httpPromise = $http.post('/corrections/getToValidateCorrections/', sendData)
  //     httpPromise.then(function (res) {
  //       var listCorrections = res.data
  //       var n = listCorrections.length
  //       // .table
  //       for (var i = 0; i < n; i++) {
  //         listCorrections[i].thumbSrc = '../data/collections/'+listCorrections[i].collection_id+'/images/thumbnails/'.concat(listCorrections[i].image_id).concat('.jpg')
  //         listCorrections[i].date_correction = new Date(listCorrections[i].date_correction)
  //         var day = listCorrections[i].date_correction.getDate()
  //         var month = listCorrections[i].date_correction.getMonth() + 1
  //         var year = listCorrections[i].date_correction.getFullYear()
  //         listCorrections[i].date_string = day + '.' + month + '.' + year
  //         // Check if the title is modified
  //         if (listCorrections[i].sub_title) {
  //           listCorrections[i].hasTitle = true
  //         } else {
  //           listCorrections[i].hasTitle = false
  //         }
  //         if (listCorrections[i].title_processed) {
  //           listCorrections[i].hasTitle = false
  //         }
  //         // Check if the caption is modified
  //         if (listCorrections[i].sub_caption) {
  //           listCorrections[i].hasCaption = true
  //         } else {
  //           listCorrections[i].hasCaption = false
  //         }
  //         if (listCorrections[i].caption_processed) {
  //           listCorrections[i].hasCaption = false
  //         }
  //         // Set is updated to false
  //         listCorrections[i].titleIsUpdated = false
  //         listCorrections[i].captionIsUpdated = false
  //       }
  //       resolve(listCorrections)
  //     })
  //   })
  // }

  var updateTitle = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: correction.image_id,
        valTitle: correction.newTitle,
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        volTitle: correction.sub_title,
        titleRemark: correction.title_remark
      }
      var httpPromise = $http.post('/corrections/updateTitle/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var updateCaption = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        imageId: correction.image_id,
        volCaption: correction.sub_caption,
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        valCaption: correction.newCaption,
        captionRemark: correction.caption_remark
      }
      var httpPromise = $http.post('/corrections/updateCaption/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var rejectCaption = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        volCaption: correction.sub_caption,
        imageId: correction.image_id,
        captionRemark: correction.caption_remark
      }
      var httpPromise = $http.post('/corrections/rejectCaption/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var rejectTitle = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        volTitle: correction.sub_title,
        imageId: correction.image_id,
        titleRemark: correction.title_remark,
        captionRemark: correction.caption_remark
      }
      var httpPromise = $http.post('/corrections/rejectTitle/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var acceptCaption = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        newCaption: correction.sub_caption,
        imageId: correction.image_id,
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        captionRemark: correction.caption_remark
      }
      var httpPromise = $http.post('/corrections/acceptCaption/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var acceptTitle = function (correction, validatorId) {
    return $q(function (resolve, reject) {
      var sendData = {
        newTitle: correction.sub_title,
        imageId: correction.image_id,
        corrId: correction.corr_id,
        validatorId: validatorId,
        volunteerId: correction.volunteer_id,
        titleRemark: correction.title_remark
      }
      var httpPromise = $http.post('/corrections/acceptTitle/', sendData)
      httpPromise.then(function (res) {
        resolve(res)
      })
    })
  }

  var getMy = function (volunteerId){
    return $q(function (resolve, reject) {
      var sendData = {
        volunteerId: volunteerId,
      }
      var httpPromise = $http.post('/corrections/getMy/', sendData)
      httpPromise.then(function (res) {
        var listCorrections = res.data
        var corrections = []
        var listCollections = []
        var listOwners = []

        for (var index in listCorrections) {
          listCorrections[index].date_correction = new Date(listCorrections[index].date_correction)
          var day = listCorrections[index].date_correction.getDate()
          var month = listCorrections[index].date_correction.getMonth() + 1
          var year = listCorrections[index].date_correction.getFullYear()

          // Create one entry for title
          if (listCorrections[index].sub_title) {
            var correction = {}
            correction.type = 'title'
            correction.thumbSrc = '../data/collections/' + listCorrections[index].collection_id + '/images/thumbnails/'.concat(listCorrections[index].image_id).concat('.jpg')
            correction.src500 = '../data/collections/' + listCorrections[index].collection_id + '/images/500/'.concat(listCorrections[index].image_id).concat('.jpg')
            //Get titles
            correction.original = listCorrections[index].orig_title
            correction.submited = listCorrections[index].sub_title
            correction.updated = listCorrections[index].title_update

            correction.remark = listCorrections[index].title_remark
            correction.original_id = listCorrections[index].original_id

            correction.ownername = listCorrections[index].ownername
            correction.collectionname = listCorrections[index].collectionname



            if (!listCorrections[index].title_processed) {
              correction.status = 'waiting'
            }else {
              if (listCorrections[index].title_result === 'accepted') {
                correction.status = 'validated'
              } else if (listCorrections[index].title_result === 'rejected') {
                correction.status = 'rejected'
              } else if (listCorrections[index].title_result === 'updated') {
                correction.status = 'improved'
              }
            }
            correction.date_string = day + '.' + month + '.' + year
            correction.date = listCorrections[index].date_correction
            corrections.push(correction)
          }

          // Create one entry for caption
          if (listCorrections[index].sub_caption) {
            var correction = {}
            correction.type = 'caption'
            correction.thumbSrc = '../data/collections/' + listCorrections[index].collection_id + '/images/thumbnails/'.concat(listCorrections[index].image_id).concat('.jpg')
            correction.src500 = '../data/collections/' + listCorrections[index].collection_id + '/images/500/'.concat(listCorrections[index].image_id).concat('.jpg')
            //Get captions
            correction.original = listCorrections[index].orig_caption
            correction.submited = listCorrections[index].sub_caption
            correction.updated = listCorrections[index].caption_update

            correction.remark = listCorrections[index].caption_remark
            correction.original_id = listCorrections[index].original_id

            correction.ownername = listCorrections[index].ownername
            correction.collectionname = listCorrections[index].collectionname
            if (!listCorrections[index].caption_processed) {
              correction.status = 'waiting'
            }else {
              if (listCorrections[index].caption_result === 'accepted') {
                correction.status = 'validated'
              } else if (listCorrections[index].caption_result === 'rejected') {
                correction.status = 'rejected'
              } else if (listCorrections[index].caption_result === 'updated') {
                correction.status = 'improved'
              }
            }
            correction.date_string = day + '.' + month + '.' + year
            correction.date = listCorrections[index].date_correction
            corrections.push(correction)
          }

          if (index == 0) {
            var minDate = new Date(listCorrections[index].date_correction)
            var maxDate = new Date(listCorrections[index].date_correction)
          }
          if (new Date(listCorrections[index].date_correction) > maxDate) {
            maxDate = new Date(listCorrections[index].date_correction)
          }
          if (new Date(listCorrections[index].date_correction) < minDate) {
            minDate = new Date(listCorrections[index].date_correction)
          }

          if (listCollections.indexOf(listCorrections[index].collectionname) == -1){
            listCollections.push(listCorrections[index].collectionname)
          }
          if (listOwners.indexOf(listCorrections[index].ownername) == -1){
            listOwners.push(listCorrections[index].ownername)
          }
        }
        res = {
          corrections: corrections,
          listOwners: listOwners,
          listCollections: listCollections,
          minDate: minDate,
          maxDate: maxDate
        }
        resolve(res)
      })
    })
  }

  var getTitleDataForDownload = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/corrections/getTitleForDownload/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        console.log('titles', data)
        resolve(data)
      })
    })
  }

  var getCaptionDataForDownload = function (downloadFilter) {
    return $q(function (resolve, reject) {
      var httpPromise = $http.post('/corrections/getCaptionForDownload/', downloadFilter)
      httpPromise.then(function (res) {
        var data = res.data
        console.log('captions', data)
        resolve(data)
      })
    })
  }

  return {
    submitCorrection: submitCorrection,
    // getToValidateCorrections: getToValidateCorrections,
    getDashboard: getDashboard,
    updateCaption: updateCaption,
    updateTitle: updateTitle,
    rejectCaption: rejectCaption,
    rejectTitle: rejectTitle,
    acceptCaption: acceptCaption,
    acceptTitle: acceptTitle,
    getMy: getMy,
    getCaptionDataForDownload: getCaptionDataForDownload,
    getTitleDataForDownload: getTitleDataForDownload
  }
})
