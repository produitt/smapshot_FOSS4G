angular.module('leafApp')

.service('leafService', function ($q, $http) {
  // function loadUserCredentials() {
  var drawImages = function (data, geolocalized, mouseOverMarker, clickMarker) { // , markers, images, local_icons, select
    images = {}
    // markers = {}
    // Get map
    // leafletData.getMap().then(function(map) {

    if (geolocalized) {
      // Create cluster lay for georeferenced layer
      var markers = L.markerClusterGroup({
        singleMarkerMode: true,
        showCoverageOnHover: false,
        zoomToBoundsOnClick: true,
      })
      // Create icons
      var iconGreen = L.icon({
        iconUrl: '../icons/camera_green.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's location
      })

      // Loop on the images
      var nFeat = data.features.length
      for (id = 0; id < nFeat; id++) {
        var feature = data.features[id]

        var marker = L.marker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
          icon: iconGreen,
          draggable: false,
          id: feature.properties.id
        })
        marker.on('click', function (e) {
          clickMarker(e.target.options.id)
        })
        marker.on('mouseover', function (e) {
          mouseOverMarker(e.target.options.id)
        })
        markers.addLayer(marker)// L.marker(
      }
    } else {
      // Create cluster layers for non georeferenced layer
      var markers = L.markerClusterGroup({
        iconCreateFunction: function (cluster) {
          return L.divIcon({className: 'marker-cluster yellow',  iconSize: new L.Point(160, 160) }) //  html: '<div><span>' + cluster.getChildCount() + '</span></div>',
        },
        singleMarkerMode: true,
        showCoverageOnHover: false,
        zoomToBoundsOnClick: true,
        maxClusterRadius: 160
      })
      // Create icons
      var iconRed = L.icon({
        iconUrl: '../icons/camera_red.png',
        iconSize: [48, 48], // size of the icon
        iconAnchor: [24, 48] // point of the icon which will correspond to marker's locatio
      })

      // Loop on the images
      var nFeat = data.features.length
      for (id = 0; id < nFeat; id++) {
        var feature = data.features[id]
        var marker = L.marker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
          icon: iconRed,
          draggable: false,
          id: feature.properties.id
        })
        marker.on('click', function (e) {
          clickMarker(e.target.options.id)
        })
        marker.on('mouseover', function (e) {
          mouseOverMarker(e.target.options.id)
        })
        markers.addLayer(marker)
      }
    }
    return markers
  }

  var drawImages_bkp = function (data, markers, images, local_icons) {
    images = {}
    markers = {}
    // Loop on the images
    for (var id in data.features) {
      var feature = data.features[id]
      // Create markers
      if (feature.properties.georef_3d_bool == true) {
        var marker = {
          group: 'imagesGreen',
          id: feature.properties.id,
          lat: feature.geometry.coordinates[1],
          lng: feature.geometry.coordinates[0],
          icon: local_icons.green_icon,
          draggable: false
        }
      } else {
        var marker = {
          group: 'imagesRed',
          id: feature.properties.id,
          lat: feature.geometry.coordinates[1],
          lng: feature.geometry.coordinates[0],
          icon: local_icons.red_icon,
          draggable: false
        }
      }
      markers[feature.properties.id] = marker

      // create images
      image = {
        id: feature.properties.id,
        name: feature.properties.name,
        thumbSrc: '../images/thumbnails/'.concat(feature.properties.id).concat('.jpg'),
        georef_3d_bool: feature.properties.georef_3d_bool,
        owner_id: feature.properties.owner_id,
        volunteer_id: feature.properties.volunteer_id,
        collection_id: feature.properties.collection_id,
        photographer_id: feature.properties.photographer_id,
        license: feature.properties.license,
        date: new Date(feature.properties.date_shot),
        link: feature.properties.link,
        title: feature.properties.title,
        latitude: feature.geometry.coordinates[1],
        longitude: feature.geometry.coordinates[0],
        owner: feature.properties.owner,
        photographer: feature.properties.photographer,
        collection: feature.properties.collection,
        username: feature.properties.username
      }
      images[feature.properties.id] = image
    };
    res = {
      markers: markers,
      images: images
    }
    return res
  }

  return {
    drawImages: drawImages
  }
})
