angular.module('newsApp')

.service('newsService', function ($q, $http) {
  var getNews = function () {
    var sendData = {
    }
    var getNewsPromise = $http.post('/news/getNews', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return getNewsPromise
  }

  var getLastNew = function () {
    var sendData = {
    }

    var getLastNewPromise = $http.post('/news/getLastNew', sendData)
    .then(function (result) {
      return result
    })
    .catch(function (result) {
      return result
    })
    return getLastNewPromise
  }

  return {
    getNews: getNews,
    getLastNew: getLastNew
  }
})
