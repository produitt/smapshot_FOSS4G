angular.module('langApp')

.service('langService', function($q, $http, $window) {

  var getBrowserLang = function () {
    var lang = $window.navigator.language || $window.navigator.userLanguage
    lang = lang.slice(0, 2)
    return lang
  }
  var getLangs = function () {
    var langs = {
      fr: {
        abr: 'fr',
        short: 'Fr',
        long: 'Français'
      },
      de: {
        abr: 'de',
        short: 'De',
        long: 'Deutsch'
      },
      it: {
        abr: 'it',
        short: 'It',
        long: 'Italiano'
      },
      en: {
        abr: 'en',
        short: 'En',
        long: 'English'
      }
    }
    return langs
  }

  return {
    getBrowserLang: getBrowserLang,
    getLangs: getLangs
  }
})
