var contactApp = angular.module('contactApp', ["ngSanitize", "ui.bootstrap",
'authApp', 'textApp', 'langApp']);

contactApp.config(function ($locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!')
})
