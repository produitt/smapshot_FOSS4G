'use strict';

(function () {
  var module = angular.module('ui.openseadragon', [])
  module.directive('seadragon', [function () {
    return {
      restrict: 'AE', // how the parameters have to be specified in the html page, Attribute, Element
      scope: { // elements which are transfered from the parents
        options: '=',
        name: '=', // = means data binding
        tilesource: '@', // @ means string
        prefixUrl: '@',
        gcpIconSelected: '@', /// /
        gcpIcon: '@',
        myGCPs: '=gcp', // Correspond to gcp = "myGCPs" in the html file
        selectGCP: '&selectionFunction',
        deleteGCP: '&deletionFunction',
        httpPromise: '=httpPromise',
        osdPromise: '=osdPromise',
        drawGCP: '=drawGcp',
        osdGCP: '&osdGcpFunction'
      },

      controller: ['$scope', function ($scope) {
        $scope.osd = null
      }],

      link: function (scope, element, attrs) {
        scope.updateViewportCount = 0

                    // Create options object
        var opts = angular.extend({}, scope.options, {
          id: 'openseadragon-' + Math.random(),
          element: element[0]
        })

                    // opts.tileSources = scope.imagePath;
        if (attrs.prefixUrl) {
          opts.prefixUrl = attrs.prefixUrl
        }

        opts.overlays = [
                      // {
                      //   id: 'overlay', //Rectangle position and size is updated later
                      //   x: 0.,
                      //   y: 0.5,
                      //   width: 1,
                      //   height: 0.5,
                      // }
        ]
                    // Create the viewer
        scope.osd = OpenSeadragon(opts)

                    // Create a wrapper
        var wrapper = {
          setFullScreen: function (fullScreen) {
            scope.osd.setFullScreen(fullScreen)
          },
          forceRedraw: function () {
            scope.osd.forceRedraw()
          },
          mouse: {
            position: null,
            imageCoord: null,
            viewportCoord: null
          },
          zoom: 0,
          viewport: {
            bounds: null,
            center: null,
            rotation: 0,
            zoom: 0
          },
          viewer: scope.osd

        }
                    // if @name is set, put the wrapper in the scope and handle the events
                    // var zoomHandler = null;
                    // var updateViewportHandler = null;
        var clickHandler = null
        var dragHandler = null
        var openHandler = null

        if (attrs.name) {
                        // Make the OSD available to parent scope
          scope.$parent[attrs.name] = wrapper

                        // //Define event handlers
                        // zoomHandler = function (e) {
                        //     scope.$apply(function () {
                        //         wrapper.zoom = e.zoom;
                        //     });
                        // }

          clickHandler = function (e) {
            scope.$apply(function () {
                            // reset paning
              e.eventSource.panHorizontal = true
              e.eventSource.panVertical = true

              var viewportPoint = scope.osd.viewport.pointFromPixel(e.position)
              var imagePoint = scope.osd.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y)

                            // Check if an overlay is present at this location
              var clickOnPt = false
              if (e.originalEvent.target.constructor.name == 'HTMLImageElement') {
                clickOnPt = true
              };

                            // Check if click is rapid
              var quickClick = e.quick
                            // A new point is created if the click is not over an
                            // existing point and the click is quick (not a drag)
                            // and a GCP is selected and the GCP is empty
              if ((scope.myGCPs.selectedGCPid != null) && (clickOnPt == false) && (quickClick) && (scope.myGCPs.fullBool == false)) {
                var xy = [(imagePoint.x).toFixed(1), (imagePoint.y).toFixed(1)]
                scope.osdGCP()(xy, viewportPoint)

                              // if (scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].x == null){
                              //   var img = document.createElement("img");
                              //   img.src = scope.options.gcpIconSelected;
                              //   img.id = scope.myGCPs.curGCPid-1;
                              //   img.isReproj = false;
                              //
                              // 	var ov = scope.osd.addOverlay(img, viewportPoint, 'CENTER');//, 'CENTER'
                              //
                              //   //Fill the GCP
                              //   scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].x = (imagePoint.x).toFixed(1);
                              //   scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].y = (imagePoint.y).toFixed(1);
                              //
                              // }
              } else if (clickOnPt && quickClick) {
                                // Rapid click on existing point
                var idToSelect = e.originalEvent.target.id

                                // scope.selectGCP()(idToSelect);
              };
            })
          }

          dragHandler = function (e) { ///
            scope.$apply(function () {
              var viewportPoint = scope.osd.viewport.pointFromPixel(e.position)
              var imagePoint = scope.osd.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y)

              var idSelected = null

                            // Check if an overlay is present at this location
              var clickOnPt = false
              if (e.originalEvent.target.constructor.name == 'HTMLImageElement') {
                clickOnPt = true
              };
              idSelected = e.originalEvent.target.id

              if (clickOnPt) {
                              // block paning
                e.eventSource.panHorizontal = false
                e.eventSource.panVertical = false

                              // update GCP
                scope.myGCPs.dictGCPs[idSelected].x = (imagePoint.x).toFixed(1)
                scope.myGCPs.dictGCPs[idSelected].y = (imagePoint.y).toFixed(1)

                              // update overlay

                              // find the appropriate element
                var nOv = e.eventSource.currentOverlays.length
                var elementToMove = null
                var idOverlayToMove = null
                var labelToMove = null
                var idLabelToMove = null
                for (var i = 0; i < nOv; i++) {
                  if (e.eventSource.currentOverlays[i].element.id == idSelected) {
                    idOverlayToMove = i
                  };
                  if (e.eventSource.currentOverlays[i].element.id === idSelected + '_label') {
                    idLabelToMove = i
                  };
                };
                elementToMove = e.eventSource.currentOverlays[idOverlayToMove].element
                e.eventSource.updateOverlay(elementToMove, viewportPoint, 'CENTER')

                labelToMove = e.eventSource.currentOverlays[idLabelToMove].element
                e.eventSource.updateOverlay(labelToMove, viewportPoint, 'CENTER')
              } else {

              };
            })
          }
          openHandler = function (e) { ///
            scope.$apply(function () {
                            // var bounds = scope.osd.viewport.getHomeBounds();
                            // var height = bounds.height;
                            // var width = bounds.width;
                            // var overlay = scope.osd.currentOverlays[0];
                            // var rect = new OpenSeadragon.Rect(0., height-(0.35*height), 1, 0.1*height)
                            // overlay.update(rect)
            })
          }

                        // Assign event handlers
                        // scope.osd.addHandler("zoom", zoomHandler);
                        // scope.osd.addHandler("update-viewport", updateViewportHandler);
          scope.osd.addHandler('canvas-click', clickHandler)
          scope.osd.addHandler('canvas-drag', dragHandler)
          scope.osd.addHandler('open', openHandler)
                        // scope.osd.addHandler("canvas-enter", enterHandler);
                        // scope.osd.addHandler("canvas-quit", quitHandler);

                        // Add a mouse handler
          scope.mouse = new OpenSeadragon.MouseTracker({
            element: scope.osd.canvas,
            enterHandler: function (e) {
              if (scope.osd.viewport) {
                var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
                var pos = e.position.plus(coord)
                var mouse = {
                  position: pos,
                  imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                  viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
                }
                scope.$apply(function () {
                  wrapper.mouse = mouse
                })
              }
            },
            moveHandler: function (e) {
              if (scope.osd.viewport) {
                var webPoint = e.position
                var viewportPoint = scope.osd.viewport.pointFromPixel(webPoint)

                var imageWidth = scope.osd.source.Image.Size.Width
                var imageHeight = scope.osd.source.Image.Size.Height
                var ratio = imageHeight / imageWidth

                                  // The viewport limits are 1 and 1 for a square images, else 1 is equal to the width
                                  // if ((viewportPoint.x > 0) && (viewportPoint.x < 1) && (viewportPoint.y > 0) && (viewportPoint.y < ratio)){
                                  //   scope.drawCursor()(viewportPoint);
                                  // }

                var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
                var pos = e.position// .plus(coord);

                var mouse = {
                  position: pos,
                  viewportPoint: scope.osd.viewport.pointFromPixel(webPoint),
                  imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                  viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
                }

                scope.$apply(function () {
                  wrapper.mouse = mouse
                })
              }
            },
            exitHandler: function (e) {
              scope.$apply(function () {
                wrapper.mouse.position = null
                wrapper.mouse.imageCoord = null
                wrapper.mouse.viewportCoord = null

                var existCursor = !!document.getElementById('cursorIcon')
                if (existCursor) {
                  var div = document.getElementById('cursorIcon')
                  div.parentNode.removeChild(div)
                }
              })
            }
          })
          scope.mouse.setTracking(true)
        }

                    // When element is destroyed, destroy the viewer
        element.on('$destroy', function () {
                        // if @nam eis set, remove it from parent scope, and remove event handlers
          if (attrs.name) {
                            // Remove from parent scope
            scope.$parent[attrs.name] = null
            scope.$parent[viewer] = null // me

                            // Destroy mouse handler
            scope.mouse.destroy()

                            // Remove event handlers
                            // scope.osd.removeHandler("zoom", zoomHandler);
                            // scope.osd.removeHandler("update-viewport", updateViewportHandler);
            scope.osd.removeHandler('canvas-click', clickHandler)
            scope.osd.removeHandler('canvas-drag', dragHandler)
          }

                        // Destroy the viewer
          scope.osd.destroy()
        })
                    // Draw the gcps if any
                    // scope.drawGCP();
                     // Promise
      }

    }
  }])
})()
