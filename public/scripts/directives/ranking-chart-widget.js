angular.module('rankingChart', []).directive('rankingChartWidget', function () {
  // constants
  // var margin = 20,
  //   width = 500,
  //   height = 200 - .5 - margin,
  //   color = d3.interpolateRgb("#f77", "#77f");

  return {
    restrict: 'E',
    scope: {
      val: '=',
      grouped: '=',
      me: '=',
      user: '=',
      rankingReady: '=rankingReady',
      size: '=',
      rankingChart: '='
    },
    link: function (scope, element, attrs) {
      var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = scope.size.width - margin.left - margin.right,
        height = scope.size.height - margin.top - margin.bottom

        // set up initial svg object
        var chart = d3.select(element[0])
          .append('svg')
            // .attr("width", width)
            // .attr("height", height + margin + 100);
            .attr('width', width)
            .attr('height', height)

        // var chart = scope.rankingChart

      // console.log('chart', chart)

      scope.$watch('val', function (newVal, oldVal) {
        // Delete previous graphic
        chart.selectAll('.bar').remove()
        chart.selectAll('.text').remove()
        chart.selectAll('g').remove()

        if (scope.rankingReady === true) {
          console.log('mimi', scope.me)
          var data = scope.val

          var x = d3.scaleLinear()
                    .range([0, width])

          var y = d3.scaleLinear()
                    .range([height, 0])

          // Scale the range of the data in the domains
          var maxRank = d3.max(data, function (d) { return d.rank }) + 1
          x.domain([0, d3.max(data, function (d) { return d.rank }) + 1])
          y.domain([0, d3.max(data, function (d) { return d.value })])

          var zoom = d3.zoom()
            .scaleExtent([1, 40])
            // .translateExtent([[0, 0], [width, height]])
            .on('zoom', zoomed)

          //Display tip on mouseover
          var tip = d3.tip()
            .attr('class', 'd3-tip')
            .direction('n')

          // append the rectangles for the bar chart
          var gBar = chart.selectAll('.bar')
              .data(data)
              .enter()
              .append('rect')
              .attr('class', 'bar')
              .attr('x', function (d) {
                return x(d.rank - 1)
              })
              .attr('width', (width / maxRank) - 1)
              .attr('y', function (d) {
                if (d.value === 0) {
                  return y(0.5)
                } else {
                  return y(d.value)
                }
              })
              .attr('height', function (d) {
                console.log('height', height - y(d.value))
                if (d.value === 0) {
                  return height - y(0.5)
                } else {
                  return height - y(d.value)
                }
              })
              .style('fill', function (d) { return (d.color) })
              .on('mouseover', function (d) {
                scope.$apply(function () {
                  scope.user.username = d.username
                  scope.user.nImages = d.value
                  scope.user.rank = d.rank

                  tip
                  .html('<h4>' + d.username + '</h4>' +
                      '<ul>' +
                      '<li> Classement: ' + d.rank +
                        '</li>' +
                      '<li>Images géoréférencées: ' + scope.user.nImages + '</li>' +
                      '</ul>'
                    )
                  .show()
                })

                var tr = d3.event// d3.zoomTransform(zoom);
                d3.event.target.style.fill = 'yellow'
              })
              .on('mouseout', function (d) {
                d3.event.target.style.fill = d.color
                tip.hide()
              })

          // generates labels (volunteers usernames)
          var namesText = chart.selectAll('.text')
            .data(data)
            .enter()
            .append('text')
            .attr('class', 'text')
            .style('text-anchor', 'start')

            // .attr('x', function (d) {
            //   return x(d.rank)
            // })
            // .attr('y', function (d) {
            //   return 0.0// y(d.value);
            // })
            .attr('dy', '.35em')
            .attr('dx', '.35em')
            .text(function (d) { return d.username })
            .attr('transform', function (d) {
              // Compute x coordinate
              var newx = x(d.rank) - 0.5 * (width / maxRank)
              return 'translate(' + newx + ' ' + height + ') rotate (-90)'
            })

          // add the y Axis
          var yAxisLeft = d3.axisLeft(y)
            .ticks(10)
            .tickSize(width)
            .tickPadding(8 - width)
          var gYLeft = chart.append('g')
              .attr('class', 'axis')
              .attr('transform', 'translate(' + (0 + width) + ', ' + 0 + ')')
              .call(yAxisLeft)

          var yAxisRight = d3.axisRight(y)
          var gYRight = chart.append('g')
              .attr('class', 'axis')
              .attr('transform', 'translate(' + 0 + ', ' + 0 + ')')
              .call(yAxisRight)

          chart.call(zoom).call(tip)

          // Compute translation
          // var scale = width / maxRank // Scale between the pixel space and the rank space
          // console.log('scope.me.rank', scope.me.rank, maxRank)
          // var xTranslation = (maxRank / 2 - scope.me.rank) * scale
          // console.log('xTranslation', xTranslation)
          // zoom.translateBy(chart, xTranslation, 0)
          // var scale = maxRank / 11
          // zoom.scaleTo(chart, scale)

          // Set max extent
          zoom.translateExtent([[0, 0], [width, height]])

          function zoomed () {
            var bars = chart.selectAll('.bar')
            var strTrans = 'translate(' + (d3.event.transform.x) + ' ' + 0 + ') scale(' + d3.event.transform.k + ' 1)'
            bars.attr('transform', strTrans)

            var namesText = chart.selectAll('.text')
              .attr('x', function (d) {
                return 0
              })
              .attr('y', function (d) {
                return 0
              })
              .attr('transform', function (d) {
                // Compute x coordinate
                var newx = width / maxRank * d3.event.transform.k * (d.rank - 0.5) + d3.event.transform.x
                return 'translate(' + newx + ' ' + height + ') rotate (-90)'
              })
              .style('opacity', function (d) {
                var widthBar = width / maxRank * d3.event.transform.k
                if (d.color === 'green') {
                  if (widthBar < 60) {
                    return '0'
                  } else {
                    return '1'
                  }
                }
              })
          }
        }
      })
    }
  }
})
