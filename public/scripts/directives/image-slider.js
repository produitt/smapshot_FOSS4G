angular.module('imageSliderApp', []).directive('imageSlider', function () {
  return {
    restrict: 'EA',
    scope: {
      imagesInSlider: '=imagesInSlider',
      imageSelected: '=imageSelected',
      mouseOverSlider: '&overFunction',
      clickSlider: '&clickFunction',
    },
    templateUrl: '../views/slider-template.html'
  }
})
