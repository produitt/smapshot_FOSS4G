angular.module('moveApp', []).directive('moveButtons', ['$compile', function ($compile) {
  return {
    restrict: 'AE',
    replace: true,
    transclude: true,
    template: function (elem, attr) {
      if (attr.navMode == 'game') {
        return "<object id='mbId' type='image/svg+xml' data='../icons/moveButtonsGame.svg'></object>"
      } else if (attr.navMode == 'georef') {
        return "<object id='mbId' type='image/svg+xml' data='../icons/moveButtonsPlain.svg'></object>"
      } else if (attr.navMode == 'monoNav') {
        return "<object id='mbId' type='image/svg+xml' data='../icons/moveButtonsPlain.svg'></object>"
      } else if (attr.navMode == 'monoImage') {
        return "<object id='mbId' type='image/svg+xml' data='../icons/moveButtonsGame.svg'></object>"
      }
    },
    scope: {
      moveFront: '&moveFrontFunction',
      moveBack: '&moveBackFunction',
      moveLeft: '&moveLeftFunction',
      moveRight: '&moveRightFunction',
      moveUp: '&moveUpFunction',
      moveDown: '&moveDownFunction',
      turnLeft: '&turnLeftFunction',
      turnRight: '&turnRightFunction',
      turnUp: '&turnUpFunction',
      turnDown: '&turnDownFunction',
      zoomIn: '&zoomInFunction',
      zoomOut: '&zoomOutFunction'
    },
    link: function (scope, element, attrs) {
      // var statusChanged = function(newValue, oldValue) {
      // scope.keyClick=function(){
      //   console.log('braaaaaaaa')
      //   scope.moveUp()()
      // }
      var init = function () {
        var upElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('up'))
        upElm.attr('ng-click', 'moveUp()()')
        $compile(upElm)(scope)

        var downElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('down'))
        downElm.attr('ng-click', 'moveDown()()')
        $compile(downElm)(scope)

        var leftElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('left'))
        leftElm.attr('ng-click', 'moveLeft()()')
        $compile(leftElm)(scope)

        var rightElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('right'))
        rightElm.attr('ng-click', 'moveRight()()')
        $compile(rightElm)(scope)

        var frontElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('front'))
        frontElm.attr('ng-click', 'moveFront()()')
        $compile(frontElm)(scope)

        var backElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('back'))
        backElm.attr('ng-click', 'moveBack()()')
        $compile(backElm)(scope)

        var turnUpElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnUp'))
        turnUpElm.attr('ng-click', 'turnUp()()')
        $compile(turnUpElm)(scope)

        var turnDownElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnDown'))
        turnDownElm.attr('ng-click', 'turnDown()()')
        $compile(turnDownElm)(scope)

        var turnLeftElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnLeft'))
        turnLeftElm.attr('ng-click', 'turnLeft()()')
        $compile(turnLeftElm)(scope)

        var turnRightElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnRight'))
        turnRightElm.attr('ng-click', 'turnRight()()')
        $compile(turnRightElm)(scope)

        var zoomInElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('zoomIn'))
        zoomInElm.attr('ng-click', 'zoomIn()()')
        $compile(zoomInElm)(scope)

        var zoomOutElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('zoomOut'))
        zoomOutElm.attr('ng-click', 'zoomOut()()')
        $compile(zoomOutElm)(scope)
        //   console.log('yahoo')
        // }
        // console.log('upel', upElm)
        // upElm[0].attr("ng-click", "keyClick()");
      }

      try {
        if (element[0].getSVGDocument()) {
          init()
        } else {
          element.on('load', init)
        }
      } catch (exception) {
        // For ie
        console.log('in the catch')
        try {
          angular.element(document).ready(init)
        } catch (ex) {
          console.log('prout')
        }
      }

      // $compile(element)(scope);
    }
  }
}])
