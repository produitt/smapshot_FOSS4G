'use strict';

angular.module('angularCesium',[]).directive('acMap', ['$http', '$q', function($http, $q) {
  return {
    restrict : 'AE',
    template : '<div> <ng-transclude></ng-transclude> <div class="map-container"></div> </div>',
    transclude : true,
    scope : {
      //dimensions : '@',
      //imageryProvider : '@',
      //terrain: '=?',//'=',
      cesium: "=?",
      cameraPosition: '=?',
      cameraOrientation: '=?',
      cameraOrientationIni: '=?',
      providedCamera: '=?',
      myGCPs: "=gcp",
      selectGCP: '&selectionFunction',
      cesiumGCP: '&cesiumGcpFunction',
      //deleteGCP: '&deletionFunction',
      //getWorldCoordinates: '&getworldFunction',
      drawCursorImage: '&drawCursorImageFunction',
      //httpPromise: "=httpPromise",
      //imageName: "=?imageName",
      imageModel: "=?imageModel",
      modelIsDrawn: "=?modelIsDrawn",
      toponyms: "=?toponyms",
      mode: "=?mode",
      tilesLoadedPromise:"=?tilesLoadedPromise",
      unselectAllGCP: "=?unselectAllGcp",
      showGlobe: "=showGlobe",
      image: "=image",
      alertClickImage: '&alertClickImage',
      alertClickBuilding: '&alertClickBuilding',
      buildings: "=buildings",
      // switchImagery: "&switchImagery"
      //model: "=model"
    },

    controller : function($scope, $element) {
      this.getCesiumWidget = function() {
        return $scope.cesium;
      }
    },

    link : {
      pre: function (scope, element) {
        // scope.$watch('showGlobe', function(){
        //   if(scope.showGlobe == true){
        console.log('initialise cesium')
            //Initialise Cesium
            //Cesium.BingMapsApi.defaultKey = 'Ai1ve4rnjQSwI9BpNNF0cv23lthgOcFuL6EJiwHQu1xf7qxHcRu82xjNJnWDpa5F'
            var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741);
            scope.cesium = new Cesium.Viewer('cesiumContainer',{
              animation: false,
              baseLayerPicker: false,
              vrButton: false,
              geocoder: false,
              homeButton: false,
              fullscreenButton: false,
              infoBox: false,
              sceneModePicker: false,
              selectionIndicator: false,
              timeline: false,
              navigationHelpButton: false,
              scene3DOnly: true,
              orderIndependentTranslucency: true,
              useDefaultRenderLoop: true,
              imageryProvider: new Cesium.UrlTemplateImageryProvider({
                url: "//wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/{z}/{x}/{y}.jpeg",
                subdomains: '56789',
                availableLevels: [8, 10, 12, 14, 15, 16, 17, 18],
                minimumRetrievingLevel: 8,
                maximumLevel: 17,
                tilingScheme: new Cesium.GeographicTilingScheme({
                  numberOfLevelZeroTilesX: 2,
                  numberOfLevelZeroTilesY: 1
                }),
                rectangle: rectangle
                }),
            });

            // var tileset = new Cesium.Cesium3DTileset({
            //     url : 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20161217/tileset.json'
            // })
            var tileset1 = new Cesium.Cesium3DTileset({
              // url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20161217/tileset.json'
              url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20170425/tileset.json'
            })
            var tileset2 = new Cesium.Cesium3DTileset({
              url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20170814/tileset.json'
            })


            var canvas = scope.cesium.canvas;
            var scene = scope.cesium.scene;
            var globe = scope.cesium.scene.globe;

            globe.depthTestAgainstTerrain = true;
            //scene.screenSpaceCameraController.enableCollisionDetection = false;
            scene.fog.enabled = false;


            scene.screenSpaceCameraController.maximumMovementRatio = 0.05;
            //Block camera move with drag
            // scene.screenSpaceCameraController.enableRotate = true;
            // scene.screenSpaceCameraController.enableTilt = true;
            // scene.screenSpaceCameraController.enableZoom = false;
            // scene.screenSpaceCameraController.enableLook = true;//
            // scene.screenSpaceCameraController.lookEventTypes = {
            //   eventType : Cesium.CameraEventType.LEFT_DRAG,
            // }
            scene.screenSpaceCameraController.enableRotate = false;
            scene.screenSpaceCameraController.enableTilt = false;
            scene.screenSpaceCameraController.enableZoom = false;
            scene.screenSpaceCameraController.enableLook = false;//
            // scene.screenSpaceCameraController.lookEventTypes = {
            //   eventType : Cesium.CameraEventType.LEFT_DRAG,
            // }


            //terrainProvider
            var terrainProvider = new Cesium.CesiumTerrainProvider({
              url : '//3d.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/default/20160115/4326/',
              availableLevels:[8,9,10,12,14,16,17],
              rectangle: rectangle // Doesn't work without
            });
            scope.cesium.terrainProvider = terrainProvider;

            scope.tilesLoadedPromise = $q(function (resolve, reject) {
              var eve = scope.cesium.scene.globe.tileLoadProgressEvent.addEventListener(function (event) {
                if (event == 0) {
                  resolve()
                }
              })
            })
            scope.tilesLoadedPromise.then(function () {
              // add buildings
              // console.log('primise', scope.cesium.imageryLayers._layers[0])
              // scope.globe.cesium.imageryLayers._layers[0]._maximumTerrainLevel = 10
              scene.fog.density = 0.2e-3// 0.2e-3;
              // scope.cesium.imageryProvider.maximumLevel = 16

              camera.flyTo({
                destination: Cesium.Cartesian3.fromDegrees(scope.cameraPosition.longitude, scope.cameraPosition.latitude, scope.cameraPosition.height),
                orientation: {
                  heading: scope.cameraOrientation.heading,
                  pitch: scope.cameraOrientation.pitch,
                  roll: scope.cameraOrientation.roll
                },
                duration: 10
              })

              // scope.buildings = scope.cesium.scene.primitives.add(tileset)
              scope.buildings = {
                tileset1: scope.cesium.scene.primitives.add(tileset1),
                tileset2: scope.cesium.scene.primitives.add(tileset2)
              }

              //scope.drawMarkers()
            })

            scope.$parent["cesium"] = scope.cesium;

            // Initialise view
            var camera = scope.cesium.camera
            if (scope.cameraPosition.height != null) {
              camera.setView({
                destination: Cesium.Cartesian3.fromDegrees(scope.cameraPosition.longitude, scope.cameraPosition.latitude, scope.cameraPosition.height + 1000),
                orientation: {
                  heading: scope.cameraOrientation.heading,
                  pitch: -Cesium.Math.PI_OVER_TWO,
                  roll: scope.cameraOrientation.roll
                }
              })

            } else {
              var position = [Cesium.Cartographic.fromDegrees(scope.cameraPosition.longitude, scope.cameraPosition.latitude)]
              var promise = Cesium.sampleTerrain(scope.cesium.terrainProvider, 17, position)
              Cesium.when(promise, function (updatedPosition) {
                if (scope.image.apriori_height != null){
                  scope.image.apriori_height = parseFloat(scope.image.apriori_height)
                }else{
                  scope.image.apriori_height = 300
                }
                if (scope.image.view_type == 'terrestrial'){
                  scope.image.apriori_height = 50
                }
                scope.cameraPosition.height = updatedPosition[0].height + scope.image.apriori_height
                scope.cameraOrientationIni.height = updatedPosition[0].height + scope.image.apriori_height
                camera.setView({
                  destination: Cesium.Cartesian3.fromDegrees(scope.cameraPosition.longitude, scope.cameraPosition.latitude, scope.cameraPosition.height + 1000),
                  orientation: {
                    heading: scope.cameraOrientation.heading,
                    pitch: -Cesium.Math.PI_OVER_TWO,
                    roll: scope.cameraOrientation.roll
                  }
                })
              })
            }
      },

      post: function (scope, element){
        console.log('post POST')
        scope.$watch('showGlobe', function(){
          if(scope.showGlobe == true){
            //Global variables
            var dragging = false;

            var scene = scope.cesium.scene;
            var globe =  scene.globe;

            //Initialise handlers
            var leftClickFunction = null;
            var leftDownFunction = null;
            var mouseMoveFunction = null;
            var leftUpFunction = null;
            var rightDownFunction = null;
            var middleDownFunction = null;
            var wheelFunction = null;
            var lockCamera = null;
            var unlockCamera = null;

            lockCamera = function(){
              scene.screenSpaceCameraController.enableRotate = false;
              scene.screenSpaceCameraController.enableLook = false;
            };

            unlockCamera = function(){
              scene.screenSpaceCameraController.enableLook = true;
            };
            //Create GCP
            //----------
            leftClickFunction = function(movement){
              scope.$apply(function(){
                  dragging = false;
                  if (scope.mode == 'toponym'){
                    //Ray between the clicked windows location and the camera
                    var ray = scope.cesium.camera.getPickRay(movement.position);
                    //Intersection between the ray and the 3D model
                    var cartesian = globe.pick(ray, scene);
                    if (cartesian) {
                      var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                      var longitude = Cesium.Math.toDegrees(cartographic.longitude);
                      var latitude =  Cesium.Math.toDegrees(cartographic.latitude);

                      scope.toponyms.curTopo.latitude = latitude.toFixed(4);
                      scope.toponyms.curTopo.longitude = longitude.toFixed(4);
                      scope.toponyms.curTopo.height = cartographic.height.toFixed(0);
                    }

                  }else if (scope.mode === 'correspondences'){ //GCP mode
                    if ((scope.modelIsDrawn) && (scope.imageModel.show)){
                        scope.imageModel.show = false
                    }else{
                      //if an object is clicked: selection
                      var pickedObjects = scene.drillPick(movement.position);
                      console.log('pickedObjects', pickedObjects)
                      if (pickedObjects.length != 0) {
                        //Someting is clicked either a GCP or a building
                        if (pickedObjects[0].id){

                          if (pickedObjects[0].id.id != 'cameraPosition'){
                            //Object is a GCP
                            scope.selectGCP()(pickedObjects[0].id.id);
                          }else{

                          }

                        }else{
                          // Object is a building
                          console.log('is a building')
                          alert('You clicked on a building. You have to click at the ground level.')
                        }
                      }
                      //Create only if a point is selected and empty
                      else if (scope.myGCPs.curGCPid != null){ //selectedGCPid
                        if (scope.myGCPs.dictGCPs[scope.myGCPs.curGCPid].X == null){////selectedGCPid
                          //Ray between the clicked windows location and the camera
                          var ray = scope.cesium.camera.getPickRay(movement.position);
                          //Intersection between the ray and the 3D model
                          var cartesian = globe.pick(ray, scene);
                          if (cartesian) {
                              //Add a GCP in the table
                              //----------------------
                              //Coordinate conversion
                              var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                              var longitude = Cesium.Math.toDegrees(cartographic.longitude);
                              var latitude =  Cesium.Math.toDegrees(cartographic.latitude);
                              var WGS84proj = 'EPSG:4326';
                                // copied from http://spatialreference.org/ref/epsg/2056/proj4/
                              var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                              var projCoord = proj4(WGS84proj, ch1903plusproj, [longitude, latitude]);
                              //Fill the GCP
                              var XYZ = [projCoord[0], projCoord[1], cartographic.height]
                              //Create new object
                              //-----------------
                              var curEntity = scope.cesium.entities.add({
                                id:(scope.myGCPs.curGCPid).toString(), //-1
                                position: cartesian,
                                point : {
                                  pixelSize : 20,
                                  outlineColor : Cesium.Color.WHITE,
                                  outlineWidth : 2
                                },
                                label : {
                                  show : true,
                                  text : (scope.myGCPs.curGCPid+1).toString(), //-1
                                  font : '14pt monospace',
                                  style: Cesium.LabelStyle.FILL_AND_OUTLINE,
                                  outlineWidth : 2,
                                  verticalOrigin : Cesium.VerticalOrigin.BOTTOM,
                                  pixelOffset : new Cesium.Cartesian2(0, -9)
                                }
                              });
                              var selectedColor = new Cesium.Color(1, 0.69, 0.15)
                              curEntity.point.color = new Cesium.ConstantProperty(selectedColor)//Cesium.Color.RED);
                              //scope.unselectAllGCP()
                              scope.cesiumGCP()(XYZ);


                          } else {
                            scope.unselectAllGCP()
                            unlockCamera()
                            //scene.screenSpaceCameraController.enableLook = true;
                            //scene.screenSpaceCameraController.enableRotate = true;
                            //scene.screenSpaceCameraController.enableTilt = true;
                          }
                        }else{
                          //If the GCP doesnt have image coordinates : alert
                          if (scope.myGCPs.dictGCPs[scope.myGCPs.curGCPid].x == null){
                            //window.alert('alerte cobra')
                            scope.alertClickImage()()
                          }
                          scope.unselectAllGCP()
                          unlockCamera()
                          //scene.screenSpaceCameraController.enableLook = true;
                          //scene.screenSpaceCameraController.enableRotate = true;
                          //scene.screenSpaceCameraController.enableTilt = true;
                        }
                      }else{
                        //Deselect
                        scope.unselectAllGCP()
                        unlockCamera()
                        //scene.screenSpaceCameraController.enableLook = true;
                        //scene.screenSpaceCameraController.enableRotate = true;
                        //scene.screenSpaceCameraController.enableTilt = true;
                      }
                    }
                  }
              })
            };

            //Move GCP with drag
            //------------------
            leftDownFunction = function(click){
                scope.$apply(function() {
                  // if ((scope.mode == 'poseEstimation') && (scope.imageModel)){
                  //   scope.imageModel.show = false;
                  // }
                  var pickedObject = scene.drillPick(click.position);
                  //if nothing is selected we are in dragging mode
                  if (Cesium.defined(pickedObject) && (pickedObject.length != 0)){
                    if (pickedObject[0].id){
                      if (pickedObject[0].id.id == scope.myGCPs.selectedGCPid){
                        dragging = true;
                        //scene.screenSpaceCameraController.enableRotate = false;
                        //scene.screenSpaceCameraController.enableLook = false;
                        lockCamera()
                      }else if (pickedObject[0].id.id == 'cameraPosition') {
                        dragging = true;
                        //scene.screenSpaceCameraController.enableRotate = false;
                        //scene.screenSpaceCameraController.enableLook = false;
                        lockCamera()
                      }
                    }
                  }

                  // } && (pickedObject[0].id.id == scope.myGCPs.selectedGCPid)) {
                  //     dragging = true;
                  //     //scene.screenSpaceCameraController.enableRotate = false;
                  //     //scene.screenSpaceCameraController.enableLook = false;
                  //     lockCamera()
                  // }else if (Cesium.defined(pickedObject) && (pickedObject.length != 0) && (pickedObject[0].id.id == 'cameraPosition')) {
                  //   dragging = true;
                  //   //scene.screenSpaceCameraController.enableRotate = false;
                  //   //scene.screenSpaceCameraController.enableLook = false;
                  //   lockCamera()
                  // }
                  else{
                    //scene.screenSpaceCameraController.enableLook = true;
                    unlockCamera()
                    //scene.screenSpaceCameraController.enableRotate = true;
                    //scene.screenSpaceCameraController.enableTilt = true;
                  }
                })
            };
            mouseMoveFunction = function(movement){
                scope.$apply(function() {
                    //Ugly method to fix the collision detection after the view being set
                    scene.screenSpaceCameraController.enableCollisionDetection = true;
                    scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000;


                    if (dragging) {
                      scene.screenSpaceCameraController.enableRotate = false;
                      scene.screenSpaceCameraController.enableLook = false;
                      var pickedObject = scene.drillPick(movement.startPosition);
                      if (Cesium.defined(pickedObject) && (pickedObject.length != 0) && (Cesium.defined(pickedObject[0].id.id))){
                        var idSelected = pickedObject[0].id.id;
                        //console.log('idSelected', idSelected)
                        //Ray between the clicked windows location and the camera
                        var ray = scope.cesium.camera.getPickRay(movement.endPosition);
                        //Intersection between the ray and the 3D model
                        var cartesian = globe.pick(ray, scene);
                        if (Cesium.defined(cartesian)){
                          //Update table
                          //------------
                          //coordinate conversion
                          var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                          var longitude = Cesium.Math.toDegrees(cartographic.longitude);
                          var latitude =  Cesium.Math.toDegrees(cartographic.latitude);
                          var WGS84proj = 'EPSG:4326';
                          var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                          var projCoord = proj4(WGS84proj, ch1903plusproj, [longitude, latitude]);
                          if (idSelected == 'cameraPosition'){
                            //Update the camera position
                            scope.providedCamera.location = {
                              X: (projCoord[0]).toFixed(1),
                              Y: (projCoord[1]).toFixed(1),
                              Z: (cartographic.height).toFixed(1)+2,
                              longitude: longitude,
                              latitude: latitude
                            };
                          }else{
                            //Update the GCP
                            scope.myGCPs.dictGCPs[idSelected].X = projCoord[0]// ).toFixed(1);
                            scope.myGCPs.dictGCPs[idSelected].Y = projCoord[1]// ).toFixed(1);
                            scope.myGCPs.dictGCPs[idSelected].Z = cartographic.height// ).toFixed(1);
                          }
                          //Move cesium object
                          //------------------
                          var selEntity = pickedObject[0].id
                          selEntity.position = cartesian;
                        }
                      }
                    }
                })
            };

            leftUpFunction = function(click){
                scope.$apply(function() {
                    if(dragging) {
                      dragging = false;
                      //scene.screenSpaceCameraController.enableRotate = true;
                      var pickedObject = scene.drillPick(click.position);
                      if (Cesium.defined(pickedObject) && (pickedObject.length != 0) && (Cesium.defined(pickedObject[0].id.id))){
                        //scene.screenSpaceCameraController.enableLook = true;
                        unlockCamera()
                        //scene.screenSpaceCameraController.enableRotate = true;
                        //scene.screenSpaceCameraController.enableTilt = true;

                        var idSelected = pickedObject[0].id.id;
                        //console.log('idSelected 2', idSelected)
                        //Ray between the clicked windows location and the camera
                        var ray = scope.cesium.camera.getPickRay(click.position);
                        //Intersection between the ray and the 3D model
                        var cartesian = globe.pick(ray, scene);
                        var selEntity = pickedObject[0].id;
                        selEntity.position = cartesian;
                      }
                    }
                })
            };

            rightDownFunction = function(click){
                scope.$apply(function() {
                  if ((scope.mode == 'correspondences') && (scope.imageModel)){
                    scope.imageModel.show = false;
                  }
                })
            };

            middleDownFunction = function(click){
                scope.$apply(function() {
                  if ((scope.mode == 'correspondences') && (scope.imageModel)){
                    scope.imageModel.show = false;
                  }
                })
            };

            wheelFunction = function(click){
              console.log('wheel', click)
              var camera = scope.cesium.camera;
              console.log('fov', camera.frustum.fov)
              if (click > 0){
                camera.frustum.fov = camera.frustum.fov-camera.frustum.fov*0.1;
              }else{
                if (camera.frustum.fov<1.5){
                  camera.frustum.fov = camera.frustum.fov+camera.frustum.fov*0.1;
                }

              }
              //camera.frustum.fov = Cesium.Math.PI_OVER_THREE;
                scope.$apply(function() {
                  if ((scope.mode == 'correspondences') && (scope.imageModel)){
                    scope.imageModel.show = false;
                  }
                })
            };

            // switchImagery = function (){
            //   var layers = viewer.imageryLayers;
            //   var baseLayer = layers.get(0)
            //   layers.remove(baseLayer);
            //   layers.addImageryProvider(new Cesium.ArcGisMapServerImageryProvider({
            //       url : '//server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer'
            //   }))
            // }

            //Left click handler: create GCP
            var leftClickHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            leftClickHandler.setInputAction(leftClickFunction, Cesium.ScreenSpaceEventType.LEFT_CLICK);

            var leftDownHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            leftDownHandler.setInputAction(leftDownFunction, Cesium.ScreenSpaceEventType.LEFT_DOWN);

            var mouseMoveHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            mouseMoveHandler.setInputAction(mouseMoveFunction, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

            var leftUpHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            leftUpHandler.setInputAction(leftUpFunction, Cesium.ScreenSpaceEventType.LEFT_UP);

            var rightDownHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            rightDownHandler.setInputAction(rightDownFunction, Cesium.ScreenSpaceEventType.RIGHT_DOWN);

            var wheelHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            wheelHandler.setInputAction(wheelFunction, Cesium.ScreenSpaceEventType.WHEEL);

            var middleDownHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            middleDownHandler.setInputAction(middleDownFunction, Cesium.ScreenSpaceEventType.MIDDLE_DOWN);

            var dbleClickHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas);
            dbleClickHandler.setInputAction(function(movement) {
                scope.cesium.trackedEntity = undefined;
            }, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);

            //When element is destroyed, destroy the viewer
            element.on('$destroy', function () {
              leftClickHandler.destroy();
              leftDownHandler.destroy();
              mouseMoveHandler.destroy();
              rightDownHandler.destroy();
              wheelHandler.destroy();
              middleDownHandler.destroy();
              dbleClickHandler.destroy();
              leftUpHandler.destroy();
              scope.cesium.destroy();
            });

          }
        })
        // scope.httpPromise.then(
        //   function(data){
        //
        // });
      }
    }
  };
}]);
