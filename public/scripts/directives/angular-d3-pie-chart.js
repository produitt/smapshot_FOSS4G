homeApp.directive('ghVisualization', function () {
  // constants
  // var margin = 20,
  //   width = 500,
  //   height = 200 - .5 - margin,
  //   color = d3.interpolateRgb("#f77", "#77f");

  return {
    restrict: 'E',
    scope: {
      data: '='
      // grouped: '=',
      // me: '=',
      // user: '='
    },
    link: function (scope, element, attrs) {
      var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = 500 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom,
        radius = Math.min(width, height) / 2

      // set up initial svg object
      var chart = d3.select(element[0])
        .append('svg')
          // .attr("width", width)
          // .attr("height", height + margin + 100);
        	.attr('width', width)
        	.attr('height', height)
          .append('g')
          .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')')

      // console.log('chart', chart)

      scope.$watch('data', function (newVal, oldVal) {
        // Delete previous graphic
        d3.selectAll('g > *').remove()

        data = scope.data

        var arc = d3.arc()
          .outerRadius(radius - margin.top)
          .innerRadius(0)

        var pie = d3.pie()
          .sort(null)
          .value(function (d) {
            return d.value
          })

        var g = chart.selectAll('.arc')
          .data(pie(data))
          .enter().append('g')

        g.append('path')
          .attr('d', arc)
          .style('fill', function (d, i) {
            return d.data.color
          })
        var g = chart.selectAll('.arc')
          .data(pie(data))
          .enter().append('g')

        g.append('text')
          .attr('transform', function (d) {
            var _d = arc.centroid(d)
            // _d[0] *= 2.2;	//multiply by a constant factor
            // _d[1] *= 2.2;	//multiply by a constant factor
            return 'translate(' + _d + ')'
          })
          .attr('dy', '.50em')
          .style('text-anchor', 'middle')
          .style('fill', 'white')
          .text(function (d) {
            return d.value + '%'
          })

        var legend = g.selectAll('.legend')
          .data(data)
          .enter().append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) { return 'translate(' + (-width / 2) + ',' + (-height / 2 + i * 28) + ')' })
            .style('font', '16px sans-serif')

        legend.append('rect')
            .attr('x', 0)
            .attr('width', 24)
            .attr('height', 24)
            .attr('fill', function (d, i) {
              return d.color
            })

        legend.append('text')
            .attr('x', 24)
            .attr('y', 9)
            .attr('dy', '.35em')
            .attr('text-anchor', 'start')
            .text(function (d) { return d.name })
      }, true)
    }
  }
})
