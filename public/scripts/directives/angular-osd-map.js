'use strict';
(function () {
  var module = angular.module('ui.openseadragon', [])
  module.directive('seadragon', [function () {
    return {
      restrict: 'AE',
      transclude: true,
      scope: {
        options: '=',
        mysd: '=mysd', // two way binding
                // name: "=",
        tilesource: '@',
        prefixUrl: '@',
        imagebool: '=a',
        transferViewer: '&c',
        osdClick: '&osdClick',
        obsCreationBool: '=obsCreationBool',
        selectionValidation: '&selectionValidation'
      },
            // controller: ["$scope", function ($scope) {
            //     $scope.osd = null;
            // }],
      link: function (scope, element, attrs) {
              // scope.$watch('imageIsSet', function (newVal, oldVal) {

                // Create options object
        var opts = angular.extend({}, scope.options, {
          id: 'openseadragon-' + Math.random(),
          element: element[0]
        })
        if (attrs.tilesource) {
          opts.tileSources = [attrs.tilesource]
        }
        if (attrs.prefixUrl) {
          opts.prefixUrl = attrs.prefixUrl
        }
        console.log('opts osd', opts)
        // Create the viewer
        scope.osd = OpenSeadragon(opts)
        // // Add selection to the viewer
        // var selection = scope.osd.selection({
        //   element: null, // html element to use for overlay
        //   showSelectionControl: false, // show button to toggle selection mode
        //   toggleButton: null, // dom element to use as toggle button
        //   showConfirmDenyButtons: true,
        //   styleConfirmDenyButtons: true,
        //   returnPixelCoordinates: false, // We want relative coordinates
        //   keyboardShortcut: 'c', // key to toggle selection mode
        //   rect: null, // initial selection as an OpenSeadragon.SelectionRect object
        //   allowRotation: false, // turn selection rotation on or off as needed
        //   startRotated: false, // alternative method for drawing the selection; useful for rotated crops
        //   startRotatedHeight: 0.1, // only used if startRotated=true; value is relative to image height
        //   restrictToImage: true, // true = do not allow any part of the selection to be outside the image
        //   onSelection: function(rect) {
        //     console.log('in on selection')
        //     scope.selectionValidation()(rect)
        //   }, // callback
        //   prefixUrl: null, // overwrites OpenSeadragon's option
        //   navImages: { // overwrites OpenSeadragon's options
        //     selection: {
        //         REST:   'selection_rest.png',
        //         GROUP:  'selection_grouphover.png',
        //         HOVER:  'selection_hover.png',
        //         DOWN:   'selection_pressed.png'
        //     },
        //     selectionConfirm: {
        //         REST:   'selection_confirm_rest.png',
        //         GROUP:  'selection_confirm_grouphover.png',
        //         HOVER:  'selection_confirm_hover.png',
        //         DOWN:   'selection_confirm_pressed.png'
        //     },
        //     selectionCancel: {
        //         REST:   'selection_cancel_rest.png',
        //         GROUP:  'selection_cancel_grouphover.png',
        //         HOVER:  'selection_cancel_hover.png',
        //         DOWN:   'selection_cancel_pressed.png'
        //     },
        //   }
        // })
        // selection.disable()


        scope.mysd.viewer = scope.osd

        // Create a wrapper
        var wrapper = {
          setFullScreen: function (fullScreen) {
            scope.osd.setFullScreen(fullScreen)
          },
          forceRedraw: function () {
            scope.osd.forceRedraw()
          },
          mouse: {
            position: null,
            imageCoord: null,
            viewportCoord: null
          },
          zoom: 0,
          viewport: {
            bounds: null,
            center: null,
            rotation: 0,
            zoom: 0
          },
          viewer: scope.osd
        }
                // if @name is set, put the wrapper in the scope and handle the events
        var zoomHandler = null
        var updateViewportHandler = null
        var clickHandler = null

        clickHandler = function (e) {
          scope.$apply(function () {
            // console.log('click dans osd')
            //         // reset paning
            // e.eventSource.panHorizontal = true
            // e.eventSource.panVertical = true
            //
            // var viewportPoint = scope.osd.viewport.pointFromPixel(e.position)
            // var imagePoint = scope.osd.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y)
            //
            //         // Check if an overlay is present at this location
            // var clickOnPt = false
            // // if (e.originalEvent.target.constructor.name == 'HTMLImageElement') {
            // //     clickOnPt = true
            // // };
            //
            // // Check if click is rapid
            // var quickClick = e.quick
            //
            // if (scope.obsCreationBool) {
            //   scope.osdClick()(viewportPoint)
            //
            //
            // }
          })
        }


                    // Define event handlers
        zoomHandler = function (e) {
          scope.$apply(function () {
            wrapper.zoom = e.zoom
          })
        }
        updateViewportHandler = function (e) {
          scope.$apply(function () {
            wrapper.viewport = {
              bounds: scope.osd.viewport.getBounds(false),
              center: scope.osd.viewport.getCenter(false),
              rotation: scope.osd.viewport.getRotation(),
              zoom: scope.osd.viewport.getZoom(false)
            }
          })
        }


                    // Assign event handlers
        scope.osd.addHandler('zoom', zoomHandler)
        scope.osd.addHandler('update-viewport', updateViewportHandler)
        scope.osd.addHandler('canvas-click', clickHandler)

                    // Add a mouse handler
        scope.mouse = new OpenSeadragon.MouseTracker({
          element: scope.osd.canvas,
          enterHandler: function (e) {
            if (scope.osd.viewport) {
              var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
              var pos = e.position.plus(coord)
              var mouse = {
                position: pos,
                imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
              }
              scope.$apply(function () {
                wrapper.mouse = mouse
              })
            }
          },
          moveHandler: function (e) {
            if (scope.osd.viewport) {
              var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
              var pos = e.position.plus(coord)
              var mouse = {
                position: pos,
                imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
              }
              scope.$apply(function () {
                wrapper.mouse = mouse
              })
            }
          },
          exitHandler: function (e) {
            scope.$apply(function () {
              wrapper.mouse.position = null
              wrapper.mouse.imageCoord = null
              wrapper.mouse.viewportCoord = null
            })
          }
        })

        scope.mouse.setTracking(true)
//                } //If attrs name

                // When element is destroyed, destroy the viewer
        element.on('$destroy', function () {
          console.log('Destroy YYYY')
                    // if @nam eis set, remove it from parent scope, and remove event handlers
                    // if (attrs.name) {
                        // Remove from parent scope
          scope.$parent[attrs.name] = null

                        // Destroy mouse handler
          scope.mouse.destroy()

                        // Remove event handlers
          scope.osd.removeHandler('zoom', zoomHandler)
          scope.osd.removeHandler('update-viewport', updateViewportHandler)
          scope.osd.removeHandler('canvas-click', clickHandler)
                    // }

                    // Destroy the viewer
          scope.osd.destroy()
        }) // Destroy
          //  })//watch
      }
    }
  }])
})()
