'use strict'

angular.module('angularCesium', []).directive('acMap', ['$http', '$q', function ($http, $q) {
  return {
    restrict: 'AE',
    template: '<div> <ng-transclude></ng-transclude> <div class="map-container"></div> </div>',
    transclude: true,
    scope: {
      dimensions: '@',
      imageryProvider: '@',
      terrain: '=?', // '=',
      providedCamera: '=providedCamera',
      myGCPs: '=gcp',
      image: '=image',
      message: '=message',
      gameSteps: '=gameSteps',
      cesium: '=?',
      selectGCP: '&selectionFunction',
      deleteGCP: '&deletionFunction',
      cesiumGCP: '&cesiumGcpFunction'
      // imageId: '@'
    },

    controller: function ($scope, $element) {
      this.getCesiumWidget = function () {
        return $scope.cesium
      }
    },

    link: {
      pre: function (scope, element) {
        // Initialise Cesium
        // Cesium.BingMapsApi.defaultKey = 'Ai1ve4rnjQSwI9BpNNF0cv23lthgOcFuL6EJiwHQu1xf7qxHcRu82xjNJnWDpa5F'
        // var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741);
        // scope.cesium = new Cesium.Viewer('cesiumContainer',{
        //   animation: false,
        //   baseLayerPicker: false,
        //   vrButton: false,
        //   geocoder: false,
        //   homeButton: false,
        //   //sceneModePicker: true,
        //   selectionIndicator: false,
        //   timeline: false,
        //   navigationInstructionsInitiallyVisible: false,
        //   scene3DOnly: true,
        //   fullscreenButton: false,
        //   infoBox: false,
        // });
        var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741)
        scope.cesium = new Cesium.Viewer('cesiumContainer', {
          animation: false,
          baseLayerPicker: false,
          vrButton: false,
          geocoder: false,
          homeButton: false,
          // sceneModePicker: true,
          selectionIndicator: false,
          timeline: false,
          navigationHelpButton: false,
          // navigationInstructionsInitiallyVisible: false,
          scene3DOnly: true,
          fullscreenButton: false,
          infoBox: false,
          imageryProvider: new Cesium.UrlTemplateImageryProvider({
            url: '//wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/{z}/{x}/{y}.jpeg',
            subdomains: '56789',
            // url: "//wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/20150101/4326/{z}/{x}/{y}.png",
            // subdomains: ['10', '11', '12', '13', '14'],
            // metadataUrl: '//terrain3.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/20150101/4326/'
            availableLevels: [8, 10, 12, 14, 15, 16, 17, 18],
            minimumRetrievingLevel: 8,
            maximumLevel: 17,
            tilingScheme: new Cesium.GeographicTilingScheme({
              numberOfLevelZeroTilesX: 2,
              numberOfLevelZeroTilesY: 1

            }),
            rectangle: rectangle
          })
        })

        scope.$parent['cesium'] = scope.cesium
        var canvas = scope.cesium.canvas
        var scene = scope.cesium.scene
        var globe = scope.cesium.scene.globe
        globe.depthTestAgainstTerrain = true
        scene.screenSpaceCameraController.enableCollisionDetection = true
        scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 10000

        // terrainProvider
        // var terrainProvider = new Cesium.CesiumTerrainProvider({
        //     url : 'https://assets.agi.com/stk-terrain/world',
        //     //requestVertexNormals : true
        // });
        var terrainProvider = new Cesium.CesiumTerrainProvider({
          url: '//3d.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/default/20160115/4326/',
          availableLevels: [8, 9, 10, 12, 14, 16, 17],
          rectangle: rectangle // Doesn't work without
        })
        scope.cesium.terrainProvider = terrainProvider

        // Set camera
        var position = [Cesium.Cartographic.fromDegrees(scope.providedCamera.longitude, scope.providedCamera.latitude)]
        // Get camera altitude
        var promise = Cesium.sampleTerrain(scope.cesium.terrainProvider, 11, position)
        Cesium.when(promise, function (updatedPosition) {
          scope.providedCamera.altitude = scope.image.height + 100// updatedPosition[0].height+100;
          cesiumService.relaxCollisionDetection(scope.cesium)
        // Set camera location
          // var camera = scope.cesium.camera
          // console.log('set view', scope.providedCamera)
          // camera.setView({
          //   destination: Cesium.Cartesian3.fromDegrees(scope.providedCamera.longitude, scope.providedCamera.latitude, scope.providedCamera.altitude+500),
          //   orientation: {
          //     heading: Cesium.Math.toRadians(scope.providedCamera.heading),
          //     pitch: -Cesium.Math.PI_OVER_FOUR,
          //     roll: 0
          //   }
          // })
        })
        scope.tilesLoadedPromise = $q(function (resolve, reject) {
          var eve = scope.cesium.scene.globe.tileLoadProgressEvent.addEventListener(function (event) {
            if (event == 0) {
              resolve()
            }
          })
        })
        scope.tilesLoadedPromise.then(function () {
          scope.cesium.camera.flyTo({
            destination: Cesium.Cartesian3.fromDegrees(scope.providedCamera.longitude, scope.providedCamera.latitude, scope.providedCamera.altitude+200),
            orientation: {
              heading: Cesium.Math.toRadians(scope.providedCamera.heading),
              pitch: 0,
              roll: 0
            },
            duration: 10
          })
        })
      // Fix camera location
        var scene = scope.cesium.scene
        scene.screenSpaceCameraController.enableRotate = false
        scene.screenSpaceCameraController.enableTranslate = false
        scene.screenSpaceCameraController.enableTilt = false
        scene.screenSpaceCameraController.enableZoom = false
      },

      post: function (scope, element) {
        // Global variables
        var dragging = false

        var camera = scope.cesium.camera
        // camera.frustum.fov = Cesium.Math.PI_OVER_THREE;;
        camera.setView({
          destination: Cesium.Cartesian3.fromDegrees(scope.providedCamera.longitude, scope.providedCamera.latitude, scope.providedCamera.altitude),
          orientation: {
            heading: scope.providedCamera.heading,
            pitch: -Cesium.Math.PI_OVER_FOUR,
            roll: 0
          }
        })

        var scene = scope.cesium.scene
        scene.fog.enabled = false
        scene.screenSpaceCameraController.enableRotate = false
        scene.screenSpaceCameraController.enableTranslate = false
        scene.screenSpaceCameraController.enableTilt = false
        scene.screenSpaceCameraController.enableZoom = false
        scene.screenSpaceCameraController.enableLook = false
        var globe = scene.globe

        // Initialise handlers
        var leftClickFunction = null
        var leftDownFunction = null
        var mouseMoveFunction = null
        var leftUpFunction = null
        var wheelFunction = null

        // Create GCP
        // ----------
        leftClickFunction = function (movement) {
          scope.$apply(function () {
            dragging = false

            var pickedObject = scene.drillPick(movement.position)
            // if nothing is selected we are in dragging mode
            if (Cesium.defined(pickedObject) && (pickedObject.length != 0)) {
              console.log('obj')
            } else {
              // Ray between the clicked windows location and the camera
              var ray = scope.cesium.camera.getPickRay(movement.position)
              // Intersection between the ray and the 3D model
              var cartesian = globe.pick(ray, scene)

              if (cartesian) {
                // Coordinate conversion
                var cartographic = Cesium.Cartographic.fromCartesian(cartesian)
                // Increase height
                cartographic.height = cartographic.height + 5
                var longitude = Cesium.Math.toDegrees(cartographic.longitude)
                var latitude = Cesium.Math.toDegrees(cartographic.latitude)
                var cartesian = Cesium.Cartesian3.fromDegrees(longitude, latitude, cartographic.height)

                var WGS84proj = 'EPSG:4326'
                // copied from http://spatialreference.org/ref/epsg/2056/proj4/
                var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                var projCoord = proj4(WGS84proj, ch1903plusproj, [longitude, latitude])

                if ((scope.gameSteps.curStep.id == 5) && (scope.gameSteps.find == false) && (scope.myGCPs.fullBool == false)) {
                  var XYZ = [(projCoord[0]).toFixed(1), (projCoord[1]).toFixed(1), (cartographic.height).toFixed(1)]
                  var curEntity = scope.cesium.entities.add({
                    id: (scope.myGCPs.curGCPid).toString(),
                    position: cartesian,
                    point: {
                      pixelSize: 15,
                      outlineColor: Cesium.Color.WHITE,
                      outlineWidth: 2
                    },
                    label: {
                      show: true,
                      text: (scope.myGCPs.curGCPid + 1).toString(),
                      font: '14pt monospace',
                      style: Cesium.LabelStyle.FILL_AND_OUTLINE,
                      outlineWidth: 2,
                      verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                      pixelOffset: new Cesium.Cartesian2(10, -10)
                    }
                  })
                  curEntity.point.color = new Cesium.ConstantProperty(Cesium.Color.RED)
                  scope.cesiumGCP()(XYZ)
                };
              };// Cartesian
            };// pickedObject
          })
        }

        // Move GCP with drag
        // ------------------
        leftDownFunction = function (click) {
          scope.$apply(function () {
            var pickedObject = scene.drillPick(click.position)
                // if nothing is selected we are in dragging mode
            if (Cesium.defined(pickedObject) && (pickedObject.length != 0)) { // && (pickedObject.id === entity)
              dragging = true
              scene.screenSpaceCameraController.enableRotate = false
            };
          })
        }

        mouseMoveFunction = function (movement) {
          scope.$apply(function () {
                // //Ray between the clicked windows location and the camera
                // var ray = scope.cesium.camera.getPickRay(movement.endPosition);
                // //Intersection between the ray and the 3D model
                // var targetCartesian = globe.pick(ray, scene);
                // if (Cesium.defined(targetCartesian)){
                //
                //   var camCartesian = scope.cesium.scene.camera.position;
                //   console.log('t', targetCartesian, camCartesian)
                //   var offsetCartesian = new Cesium.Cartesian3()
                //   Cesium.Cartesian3.subtract(targetCartesian,  camCartesian,   offsetCartesian);
                //   scope.cesium.scene.camera.direction= offsetCartesian
                //   //console.log(offsetCartesian)
                //   //scope.cesium.camera.lookAt(targetCartesian, offsetCartesian);
                // };

            if (dragging) {
              var pickedObject = scene.drillPick(movement.startPosition)
              if (Cesium.defined(pickedObject) && (pickedObject.length != 0)) {
                var idSelected = pickedObject[0].id.id

                    // Ray between the clicked windows location and the camera
                var ray = scope.cesium.camera.getPickRay(movement.endPosition)
                    // Intersection between the ray and the 3D model
                var cartesian = globe.pick(ray, scene)

                if (Cesium.defined(cartesian)) {
                      // Update table
                      // ------------
                      // coordinate conversion
                  var cartographic = Cesium.Cartographic.fromCartesian(cartesian)

                  var longitude = Cesium.Math.toDegrees(cartographic.longitude)
                  var latitude = Cesium.Math.toDegrees(cartographic.latitude)

                  var WGS84proj = 'EPSG:4326'
                  var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                  var projCoord = proj4(WGS84proj, ch1903plusproj, [longitude, latitude])

                      // Update the GCP
                  scope.myGCPs.dictGCPs[idSelected].X = (projCoord[0]).toFixed(1)
                  scope.myGCPs.dictGCPs[idSelected].Y = (projCoord[1]).toFixed(1)
                  scope.myGCPs.dictGCPs[idSelected].Z = (cartographic.height).toFixed(1)

                      // Move cesium object
                      // ------------------
                  var selEntity = pickedObject[0].id
                  selEntity.position = cartesian
                }
              }
            } else {
                  // Ray between the clicked windows location and the camera
              var ray = scope.cesium.camera.getPickRay(movement.endPosition)
                  // console.log('ray', ray)
                  // Intersection between the ray and the 3D model
              var cartesian = globe.pick(ray, scene)
                  // console.log('cartesian', cartesian)

              if (typeof cartesian !== 'undefined') {
                    // coordinate conversion
                var cartographic = Cesium.Cartographic.fromCartesian(cartesian)
                var longitude = Cesium.Math.toDegrees(cartographic.longitude)
                var latitude = Cesium.Math.toDegrees(cartographic.latitude)
                var WGS84proj = 'EPSG:4326'
                var ch1903plusproj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
                var projCoord = proj4(WGS84proj, ch1903plusproj, [longitude, latitude])
                var XYZ = [projCoord[0], projCoord[1], cartographic.height]
              }
            }
          })
        }

        leftUpFunction = function (click) {
          scope.$apply(function () {
            if (dragging) {
              dragging = false
                  // scene.screenSpaceCameraController.enableRotate = true;

              var pickedObject = scene.drillPick(click.position)
              if (Cesium.defined(pickedObject) && (pickedObject.length != 0)) {
                var idSelected = pickedObject[0].id.id

                    // Ray between the clicked windows location and the camera
                var ray = scope.cesium.camera.getPickRay(click.position)
                    // Intersection between the ray and the 3D model
                var cartesian = globe.pick(ray, scene)

                var selEntity = pickedObject[0].id
                selEntity.position = cartesian
              }
            }
          })
        }
        wheelFunction = function (event) {
          scope.$apply(function () {
            if (event > 0) {
              var camera = scope.cesium.camera
              var fov = camera.frustum.fov
              if (fov > Cesium.Math.PI / 20) {
                camera.frustum.fov = fov - 0.05
              };
            } else {
              var camera = scope.cesium.camera
              var fov = camera.frustum.fov
              if (fov < Cesium.Math.PI / 3) {
                camera.frustum.fov = fov + 0.05
              };
            }
                // if(dragging) {
                //   dragging = false;
                //   scene.screenSpaceCameraController.enableRotate = true;
                //
                //   var pickedObject = scene.drillPick(click.position);
                //   if (Cesium.defined(pickedObject) && (pickedObject.length != 0)){
                //     var idSelected = pickedObject[0].id.id;
                //
                //     //Ray between the clicked windows location and the camera
                //     var ray = scope.cesium.camera.getPickRay(click.position);
                //     //Intersection between the ray and the 3D model
                //     var cartesian = globe.pick(ray, scene);
                //
                //     var selEntity = pickedObject[0].id;
                //     selEntity.position = cartesian;
                //   }
                // }
          })
        }

        // Left click handler: create GCP
        var leftClickHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas)
        leftClickHandler.setInputAction(leftClickFunction, Cesium.ScreenSpaceEventType.LEFT_CLICK)

        var leftDownHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas)
        leftDownHandler.setInputAction(leftDownFunction, Cesium.ScreenSpaceEventType.LEFT_DOWN)

        var mouseMoveHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas)
        mouseMoveHandler.setInputAction(mouseMoveFunction, Cesium.ScreenSpaceEventType.MOUSE_MOVE)

        var leftUpHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas)
        leftUpHandler.setInputAction(leftUpFunction, Cesium.ScreenSpaceEventType.LEFT_UP)

        var wheelHandler = new Cesium.ScreenSpaceEventHandler(scope.cesium.scene.canvas)
        wheelHandler.setInputAction(wheelFunction, Cesium.ScreenSpaceEventType.WHEEL)

        // When element is destroyed, destroy the viewer
        element.on('$destroy', function () {
          leftClickHandler.destroy()
          leftDownHandler.destroy()
          mouseMoveHandler.destroy()
          leftUpHandler.destroy()
          wheelHandler.destroy()
          scope.cesium.destroy()
        })
      }// end post
    }
  }
}])
