angular.module('rApp', []).directive('ranking', function () {
  return {
    restrict: 'EA',
    scope: {
      title: '=title',
      listMonths: '=listMonths',
      listYears: '=listYears',
      startMonth: '=startMonth',
      startYear: '=startYear',
      endMonth: '=endMonth',
      endYear: '=endYear',
      collections: '=collections',
      dataRanking: '=dataRanking',
      showMoreVolunteers: '=showMoreVolunteers',
      selectStartMonth: '&selectStartMonth',
      selectStartYear: '&selectStartYear',
      selectEndMonth: '&selectEndMonth',
      selectEndYear: '&selectEndYear',
      selectCollection: '&selectCollection',
      moreVolunteers: '&moreVolunteers'
    },
    template: function (scope, element, attr) {
      return `
      <h2>{{title}} <img src = '../icons/trophy.png'></h2>
      <div class = "rank-choices-box">
        <div class = "rank-choices-item">
            <div uib-dropdown class='date-choice'>
              <p uib-dropdown-toggle class = 'hover'>
                {{listMonths[startMonth].short}}
              </p>
              <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
               <li>
                 <p ng-repeat = 'm in listMonths' ng-click = 'selectStartMonth(m)'>{{m.short}}</p>
               </li>
             </ul>
           </div>
            <div uib-dropdown class='date-choice'>
              <p uib-dropdown-toggle class = 'hover'>
                {{startYear}} -
              </p>
              <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
               <li>
                 <p ng-repeat = 'y in listYears'  ng-click = 'selectStartYear(y)'>
                   {{y}}
                 </p>
               </li>
             </ul>
           </div>
            <div uib-dropdown class='date-choice'>
              <p uib-dropdown-toggle  class = 'hover'>
                {{listMonths[endMonth].short}}
              </p>
              <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
               <li>
                 <p ng-repeat = 'm in listMonths'  ng-click = 'selectEndMonth(m)'>{{m.short}}</p>
               </li>
             </ul>
           </div>
            <div uib-dropdown class='date-choice'>
              <p uib-dropdown-toggle  class = 'hover'>
                {{endYear}} <span class="caret"></span>
              </p>
              <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
               <li>
                 <p ng-repeat = 'y in listYears'  ng-click = 'selectEndYear(y)'>
                   {{y}}
                 </p>
               </li>
             </ul>
           </div>
         </div>
         <div class = "rank-choices-item">
            <div uib-dropdown>
              <p uib-dropdown-toggle  class = 'hover'>
                Alle Bildersammlungen <span class="caret"></span>
              </p>
              <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
               <li>
                 <p ng-repeat = 'c in collections' ng-click= 'selectCollection(c)'>{{c.name}}</p>
               </li>
             </ul>
           </div>
         </div>
       </div>
      <div class = 'table-box'>
      <table>
        <tr>
          <td>
            <div class= 'medal gold'>
                <p class= 'medal-text'>{{dataRanking[0].nImages}}</p>
            </div>
          </td>
          <td class = "ranking">{{dataRanking[0].username}}</td>
        </tr>
        <tr>
          <td>
            <div class= 'medal silver'>
                <p class= 'medal-text'>{{dataRanking[1].nImages}}</p>
            </div>
          </td>
          <td  class = "ranking">{{dataRanking[1].username}}</td>
        </tr>
        <tr>
          <td>
            <div class= 'medal bronze'>
                <p class= 'medal-text'>{{dataRanking[2].nImages}}</p>
            </div>
          </td>
          <td class = "ranking">{{dataRanking[2].username}}</td>
        </tr>
        <tr ng-show = 'showMoreVolunteers' ng-repeat='volunteer in dataRankingEnd'>
          <td>
            <div class= 'medal chocolate'>
                <p class= 'medal-text'>{{volunteer.nImages}}</p>
            </div>
          </td>
          <td class = "ranking">{{volunteer.username}}</td>
      </table>
    </div>
      <p class='center hover' ng-click = 'moreVolunteers()' ng-hide = 'showMoreVolunteers'>+ Mehr Teilnehmer zeigen</p>
      <p class='center hover' ng-click = 'moreVolunteers()' ng-show = 'showMoreVolunteers'>- Weniger Teilnehmer zeigen</p>
    </div>
      `
    }
  }
})
