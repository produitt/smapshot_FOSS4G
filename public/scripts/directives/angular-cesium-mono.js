'use strict'

angular.module('angularCesium', ['ui.bootstrap', 'cesiumApp'])
.directive('acMap', ['$http', '$q', 'cesiumService', function ($http, $q, cesiumService) {
  return {
    restrict: 'AE',
    template: '<div> <ng-transclude></ng-transclude> <div class="map-container"></div> </div>',
    transclude: true,
    scope: {
      dimensions: '@',
      imageryProvider: '=?', // '@',
      terrain: '=?', // '=',
      cameraOrientation: '=?',
      globe: '=?globe',
      httpPromise: '=httpPromise',
      imageId: '=?imageId',
      toponyms: '=?toponyms',
      markers: '=?markers',
      stories: '=?stories',
      images: '=?images',
      selectedImage: '=?selectedImage',
      imageMode: '=?imageMode',
      toolMode: '=toolMode',
      httpPromiseReadImages: '=httpPromiseReadImages',
      // drawImages: "=drawImages",
      drawImage: '=drawImage',
      drawModel: '=drawModel',
      tilesLoadedPromise: '=?tilesLoadedPromise',
      showGlobe: '=showGlobe',
      drawMarkers: '=drawMarkers'
      // imageId: '@'
    },

    controller: function ($scope, $element) {
      this.getCesiumWidget = function () {
        console.log('yipa')
        return $scope.cesium
      }
    },

    link: {

      pre: function (scope, element) {
        console.log('in pre')
        scope.$watch('showGlobe', function () {
          if (scope.showGlobe == true) {
            //var image = scope.images.selectedImage
            //var image = scope.images[scope.imageId]
            var image = scope.selectedImage
            //console.log('images', scope.images)


            // Initialise Cesium
            var rectangle = Cesium.Rectangle.fromDegrees(5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741)
            scope.globe.cesium = new Cesium.Viewer('cesiumContainer', {
              animation: false,
              baseLayerPicker: false,
              vrButton: false,
              geocoder: false,
              homeButton: false,
              fullscreenButton: false,
              infoBox: false,
              sceneModePicker: false,
              selectionIndicator: false,
              timeline: false,
              navigationHelpButton: false,
              scene3DOnly: true,
              orderIndependentTranslucency: true,
              useDefaultRenderLoop: true,
              imageryProvider: new Cesium.UrlTemplateImageryProvider({
                url: '//wmts{s}.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/{z}/{x}/{y}.jpeg',
                subdomains: '56789',
                availableLevels: [8, 10, 12, 14, 15, 16],
                tilingScheme: new Cesium.GeographicTilingScheme({
                  numberOfLevelZeroTilesX: 2,
                  numberOfLevelZeroTilesY: 1
                }),
                rectangle: rectangle
              }),
              terrainProvider: scope.terrain
            })

            // Add buildings url
            var tileset1 = new Cesium.Cesium3DTileset({
              // url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20161217/tileset.json'
              url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20170425/tileset.json'
            })
            var tileset2 = new Cesium.Cesium3DTileset({
              url: 'https://vectortiles.geo.admin.ch/ch.swisstopo.swisstlm3d.3d/20170814/tileset.json'
            })

            var canvas = scope.globe.cesium.canvas
            var scene = scope.globe.cesium.scene
            var globe = scope.globe.cesium.scene.globe
            globe.depthTestAgainstTerrain = true
            // var ellipsoid = scope.cesium.globe.ellipsoid;

            // Create a promise which is solve once that the tiles are loaded
            // scope.$parent["loadPromise"] = $q(function(resolve, reject) {
            //   var eve = scope.cesium.scene.globe.tileLoadProgressEvent.addEventListener(function(event){
            //     if (event == 0){ //(camsetBool == true) &&
            //       resolve()
            //     }
            //   });
            // });

            scope.tilesLoadedPromise = $q(function (resolve, reject) {
              var eve = scope.globe.cesium.scene.globe.tileLoadProgressEvent.addEventListener(function (event) {
                if (event == 0) {
                  resolve()
                }
              })
            })
            scope.tilesLoadedPromise.then(function () {
              // add buildings
              // console.log('primise', scope.cesium.imageryLayers._layers[0])
              // scope.globe.cesium.imageryLayers._layers[0]._maximumTerrainLevel = 10
              scene.fog.density = 0.2e-3// 0.2e-3;
              // scope.cesium.imageryProvider.maximumLevel = 16
              camera.flyTo({
                destination: Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height),
                orientation: {
                  heading: Cesium.Math.toRadians(image.azimuth),
                  pitch: Cesium.Math.toRadians(image.tilt),
                      // pitch : -Cesium.Math.PI_OVER_TWO,
                  roll: Cesium.Math.toRadians(image.roll)
                },
                duration: 7
                  //
              })

              scope.buildings = {
                tileset1: scope.globe.cesium.scene.primitives.add(tileset1),
                tileset2: scope.globe.cesium.scene.primitives.add(tileset2)
              }
              // scope.buildings = scope.globe.cesium.scene.primitives.add(tileset1)

              //scope.drawMarkers()
            })

            //scope.$parent['cesium'] = scope.cesium

            var credit = new Cesium.Credit('© data: swisstopo', '', 'http://swisstopo.admin.ch/')
            scene.frameState.creditDisplay.addDefaultCredit(credit)

            // Set terrain
            // var terrainProvider = new Cesium.CesiumTerrainProvider({
            //     url : scope.terrain
            // });
            // new Cesium.ImageryLayer(scope.imageryProvider)
            // scope.cesium.imageryLayers.addImageryProvider(scope.imageryProvider);
            // scope.cesium.terrainProvider = scope.terrain;
            // console.log('scope.cesium.terrainProvider', scope.cesium.terrainProvider)

            // console.log('scene', scene)
            scene.fog.enabled = true
            // scene.fog.density = 4.0e-3;//0.2e-3;
            scene.useDepthPicking = true
            scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 0
            scene.screenSpaceCameraController.enableCollisionDetection = false

            scene.screenSpaceCameraController.enableRotate = false
            scene.screenSpaceCameraController.enableTranslate = false
            scene.screenSpaceCameraController.enableTilt = false
            scene.screenSpaceCameraController.enableZoom = false

            var camera = scope.globe.cesium.camera
            camera.frustum.fov = Cesium.Math.PI_OVER_TWO
            camera.setView({
              destination: Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height + 4000),
              orientation: {
                heading: Cesium.Math.toRadians(image.azimuth),
                    // pitch : Cesium.Math.toRadians(res.data.tilt),
                pitch: -Cesium.Math.PI_OVER_TWO,
                roll: Cesium.Math.toRadians(image.roll)
              }
            })
            // camera.flyTo({
            //     destination : Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height),
            //     orientation: {
            //         heading : Cesium.Math.toRadians(image.azimuth),
            //         pitch : Cesium.Math.toRadians(image.tilt),
            //         //pitch : -Cesium.Math.PI_OVER_TWO,
            //         roll : Cesium.Math.toRadians(image.roll)
            //     },
            //     duration:15,
            //     //
            // });
            //scope.drawModel()
            image.gltfModel = cesiumService.drawModel(image, scope.globe.cesium, 1, null, false)
            image.gltfLoaded = true

            // scope.images[image.id].gltfModel = image.gltfModel
            // scope.images[image.id].gltfLoaded = true

            // var feature = {
            //   properties: {
            //     id:image.id
            //   },
            //   geometry: {
            //     coordinates: [image.longitude, image.latitude, image.height]
            //   }
            // }

            // var ellipsoid = scope.cesium.globe.ellipsoid;

            // //Set camera location
            // console.log('scope.images.selectedImage', scope.images.selectedImage)
            // var camera = scope.cesium.camera;
            // camera.setView({
            //     destination : Cesium.Cartesian3.fromDegrees(scope.images.selectedImage.longitude, scope.images.selectedImage.latitude, scope.images.selectedImage.height),
            //     orientation: {
            //         heading : Cesium.Math.toRadians(scope.images.selectedImage.heading),
            //         pitch : Cesium.Math.toRadians(scope.images.selectedImage.tilt),
            //         roll : Cesium.Math.toRadians(scope.images.selectedImage.roll)
            //     }
            // });
            // var camera = scope.cesium.camera;
            // camera.setView({
            //     destination : Cesium.Cartesian3.fromDegrees(scope.cameraOrientation.long, scope.cameraOrientation.lat, scope.cameraOrientation.Z),
            //     orientation: {
            //         heading : Cesium.Math.toRadians(scope.cameraOrientation.azimuth),
            //         pitch : Cesium.Math.toRadians(scope.cameraOrientation.tilt),
            //         roll : Cesium.Math.toRadians(scope.cameraOrientation.roll)
            //     }
            // });

            // Set 3D model
            // var path2model = "../collada/"
            // var url = path2model+scope.imageId+'.gltf';
            // addModel(scope, scene, url);

            scope.globe.cesium.scene.preRender.addEventListener(function () {
              // Get camera position
              var camera = scope.globe.cesium.scene.camera
              var camPosition = camera.position
              // var direction = camera.direction
              var camHeading = camera.heading

              // console.log('scope.imageMode', scope.imageMode)
              // console.log('direction', camHeading)
              // console.log('scope.images.data', scope.images)

              // if (scope.imageMode == true) {
              //   for (var id in scope.images.dicImages) {
              //     var image = scope.images.dicImages[id]
              //     // console.log('image', image.georef_3d_bool)
              //     if (image.georef_3d_bool == true) {
              //       if (image.id !== scope.images.selectedImage.id) {
              //         if (scope.images.dicImages[image.id].gltfModel) {
              //           scope.images.dicImages[image.id].gltfModel.show = false
              //         }
              //         if (scope.images.dicImages[image.id].pointModel) {
              //           scope.images.dicImages[image.id].pointModel.show = false
              //         }
              //         if (scope.images.dicImages[image.id].cylinderModel) {
              //           scope.images.dicImages[image.id].cylinderModel.show = false
              //         }
              //       } else {
              //         // scope.images.dicImages[feature.properties.id].gltfModel.show = true;
              //       }
              //     }
              //   }
              //   // Mode image only the selected image is visible, the pillars and dots are hidden as well
              //   // var geojson = scope.images.geojson;
              //   // if (geojson !== 'undefined'){
              //   //   for (var idFeat in geojson.features){
              //   //     var feature = geojson.features[idFeat];
              //   //     //console.log('feature', feature)
              //   //     if (feature.properties.georeferenced == true){
              //   //       if (feature.properties.id !== scope.images.selectedImage.id){
              //   //         scope.images.dicImages[feature.properties.id].gltfModel.show = false;
              //   //         scope.images.dicImages[feature.properties.id].pointModel.show = false;
              //   //         scope.images.dicImages[feature.properties.id].cylinderModel.show = false;
              //   //       }else{
              //   //         //scope.images.dicImages[feature.properties.id].gltfModel.show = true;
              //   //       }
              //   //     }
              //   //   }
              //   // }
              // } else {
              //   // images are visible if they are sufficiently close and have an appropriate angle
              //   for (var id in scope.images.dicImages) {
              //     var image = scope.images.dicImages[id]
              //
              //     // Show image points
              //     if (scope.images.dicImages[image.id].pointModel) {
              //       scope.images.dicImages[image.id].pointModel.show = true
              //     }
              //
              //     // Show cylinders
              //     if (scope.images.dicImages[image.id].cylinderModel) {
              //       scope.images.dicImages[image.id].cylinderModel.show = true
              //     }
              //
              //     // if (image.gltfLoaded == false) {
              //     //   //Compute distance, if close enough load the model
              //     //   var fromPt = turf.point([image.longitude, image.latitude]);
              //     //   var toPt = turf.point([ shape.properties.longitude, shape.properties.latitude]);
              //     //   var dist = turf.distance(fromPt, toPt); //KM
              //     //   scope.drawImage()
              //     // }
              //
              //     if (image.georef_3d_bool == true) {
              //       // Check distances and angles
              //       // var modelPosition = Cesium.Cartesian3.fromDegrees(feature.geometry.coordinates[0], feature.geometry.coordinates[1], feature.geometry.coordinates[2]);
              //       // if (image.gltfLoaded == false) {
              //       //  scope.drawImage(image)
              //       // }
              //       var modelPosition = Cesium.Cartesian3.fromDegrees(image.longitude, image.latitude, image.height)
              //       var dist = Cesium.Cartesian3.distance(camPosition, modelPosition)
              //       // Comparison of the distance: if a picture is too close or to far
              //       // it is invisible
              //       var minT = 0
              //       var maxT = 5000
              //       if ((dist != 0) && (dist < maxT)) {
              //         // console.log('dist', dist, image)
              //         if (image.gltfLoaded == false) {
              //           scope.drawImage(image)
              //         }
              //
              //         scope.images.dicImages[image.id].gltfModel.show = true
              //         // Comparison of the azimuths: if the directions are bigger than 45degree the images are invisible
              //         var modelAz = image.azimuth
              //         if (modelAz > 360) {
              //           var modelAz = modelAz - 360
              //         } else if (modelAz < 0) {
              //           var modelAz = modelAz + 360
              //         }
              //         var delta = Math.abs(camHeading * 180 / Cesium.Math.PI - modelAz)
              //         if ((delta > 55) && (delta < 305)) {
              //           scope.images.dicImages[image.id].gltfModel.show = false
              //         }
              //       } else {
              //         if (image.gltfLoaded) {
              //           scope.images.dicImages[image.id].gltfModel.show = false
              //         }
              //       }
              //     }
              //   }
              //   // var geojson = scope.images.geojson;
              //   // if (geojson !== 'undefined'){
              //   //   for (var idFeat in geojson.features){
              //   //     var feature = geojson.features[idFeat];
              //   //     //Show image points
              //   //     scope.images.dicImages[feature.properties.id].pointModel.show = true;
              //   //     if ((feature.properties.georeferenced == true)){
              //   //       //Show cylinders
              //   //       scope.images.dicImages[feature.properties.id].cylinderModel.show = true;
              //   //       //Check distances and angles
              //   //       var modelPosition = Cesium.Cartesian3.fromDegrees(feature.geometry.coordinates[0], feature.geometry.coordinates[1], feature.geometry.coordinates[2]);
              //   //       var dist = Cesium.Cartesian3.distance(camPosition, modelPosition);
              //   //       //Comparison of the distance: if a picture is too close or to far
              //   //       //it is invisible
              //   //       var minT = 0;
              //   //       var maxT = 5000;
              //   //       if (dist != 0){
              //   //         if (dist < minT){
              //   //           scope.images.dicImages[feature.properties.id].gltfModel.show = false;
              //   //         }
              //   //         else if (dist > maxT){
              //   //           scope.images.dicImages[feature.properties.id].gltfModel.show = false;
              //   //         }
              //   //         else{
              //   //           scope.images.dicImages[feature.properties.id].gltfModel.show = true;
              //   //           //Comparison of the azimuths: if the directions are bigger than 45degree the images are invisible
              //   //           var modelAz = feature.properties.azimuth;
              //   //           if (modelAz > 360){
              //   //             var modelAz = modelAz-360;
              //   //           }
              //   //           else if (modelAz < 0){
              //   //             var modelAz = modelAz+360;
              //   //           }
              //   //           var delta = Math.abs(camHeading*180/Cesium.Math.PI - modelAz);
              //   //           if ((delta > 55) && (delta < 305)){
              //   //             scope.images.dicImages[feature.properties.id].gltfModel.show = false;
              //   //           }
              //   //         }
              //   //       }//dist 0
              //   //     }//georef
              //   //   }//for
              //   // }//if
              //   // else{
              //   //   console.log('undefined')
              //   // }
              // }// image mode
            })

            // scope.globe.cesium.camera.moveStart.addEventListener(function() {
            //   //console.log('camera start')
            // });
            // scope.globe.cesium.camera.moveEnd.addEventListener(function() {
            //   //console.log('camera stopo')
            // });
          }// if show globe
        })// watch
      }, // pre

      post: function (scope, element) {
        scope.$watch('showGlobe', function () {
          if (scope.showGlobe == true) {
          // Global variables
            var dragging = false

            var scene = scope.globe.cesium.scene
            var globe = scene.globe

          // Initialise handlers
            var leftClickFunction = null
            var leftDoubleClickFunction = null
            var wheelFunction = null
          // var leftDownFunction = null;
          // var mouseMoveFunction = null;
          // var leftUpFunction = null;

          // Double click = zoom in the images
          // ---------------------------------
            leftDoubleClickFunction = function (movement) {
              // scope.globe.cesium.trackedEntity = undefined
              // scope.$apply(function () {
              //   var pick = scene.pick(movement.position)
              //   if (Cesium.defined(pick)) {
              //     if (Cesium.defined(pick.mesh)) {
              //     // Picked object is a point
              //       var id = pick.id
              //     } else {
              //     // Picked object is an images
              //       var id = pick.id.id
              //     }
              //     scope.images.selectedImage = scope.images.dicImages[id]
              //     var selectedImage = scope.images.selectedImage
              //     if (selectedImage.georef_3d_bool == true) {
              //       scope.imageMode = true
              //
              //       if (selectedImage.gltfLoaded == false) {
              //         scope.drawImage(selectedImage)
              //       }
              //       scope.images.dicImages[selectedImage.id].gltfModel.show = true
              //
              //       scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 1
              //       scene.screenSpaceCameraController.enableCollisionDetection = true
              //
              //     // Change scale of the model
              //       selectedImage.gltfModel.scale = 1
              //
              //       var camera = scope.globe.cesium.camera
              //       camera.flyTo({
              //         destination: Cesium.Cartesian3.fromDegrees(selectedImage.longitude, selectedImage.latitude, selectedImage.height),
              //         orientation: {
              //           heading: selectedImage.azimuth * Cesium.Math.PI / 180,
              //           pitch: selectedImage.tilt * Cesium.Math.PI / 180,
              //           roll: selectedImage.roll * Cesium.Math.PI / 180
              //         },
              //         complete: function () {
              //         // Lock the camera
              //           var scene = scope.globe.cesium.scene
              //           scene.screenSpaceCameraController.enableRotate = false
              //           scene.screenSpaceCameraController.enableTranslate = false
              //           scene.screenSpaceCameraController.enableTilt = false
              //           scene.screenSpaceCameraController.enableZoom = false
              //         }
              //       })
              //     }
              //   }
              // })
            }

          // Get 3D point
          // ------------
            leftClickFunction = function (movement) {
              scope.$apply(function () {
                if (scope.imageMode == true) {
                  dragging = false

                // Ray between the clicked windows location and the camera
                  var ray = scope.globe.cesium.camera.getPickRay(movement.position)
                // Intersection between the ray and the 3D model
                  var cartesian = globe.pick(ray, scene)

                  if (cartesian) {
                    // Add the coordinates in the table
                    // ----------------------

                    // Coordinate conversion
                    var cartographic = Cesium.Cartographic.fromCartesian(cartesian)

                    var longitude = Cesium.Math.toDegrees(cartographic.longitude)
                    var latitude = Cesium.Math.toDegrees(cartographic.latitude)

                    if (scope.toolMode.addTopo == true) {
                      scope.toponyms.curTopo.latitude = latitude.toFixed(4)
                      scope.toponyms.curTopo.longitude = longitude.toFixed(4)
                      scope.toponyms.curTopo.height = cartographic.height.toFixed(0)
                    }
                    if (scope.toolMode.addStory == true) {
                      scope.stories.curStory.latitude = latitude.toFixed(4)
                      scope.stories.curStory.longitude = longitude.toFixed(4)
                      scope.stories.curStory.height = cartographic.height.toFixed(0)
                    }
                    if (scope.toolMode.addMarker == true) {
                      scope.markers.curMarker.latitude = latitude.toFixed(4)
                      scope.markers.curMarker.longitude = longitude.toFixed(4)
                      scope.markers.curMarker.height = cartographic.height.toFixed(0)
                    }
                  } else {
                    console.log('Pas de cartesian')
                  } // if cartesian
                } else {
                  var pick = scene.pick(movement.position)
                  if (Cesium.defined(pick)) {
                    if (Cesium.defined(pick.mesh)) {
                    // Picked object is a point

                      var id = pick.id
                    } else {
                    // Picked object is an images
                      var id = pick.id.id
                    }
                    scope.images.selectedImage = scope.images.dicImages[id]
                    var selectedImage = scope.images.selectedImage

                    if (selectedImage.georef_3d_bool == true) {
                      scope.imageMode = true

                      scene.screenSpaceCameraController.minimumCollisionTerrainHeight = 1
                      scene.screenSpaceCameraController.enableCollisionDetection = true

                    // Change scale of the model
                    // var globe = scope.globe.cesium.scene.globe;
                    // globe.depthTestAgainstTerrain = false;
                    // scope.images.selectedImage.gltfModel.scale = 1;
                      if (selectedImage.gltfModel) {
                        selectedImage.gltfModel.scale = 1
                      } else {
                        scope.drawModel()
                        selectedImage.gltfModel.scale = 1
                      }
                      selectedImage.gltfModel.scale = 1
                      var camera = scope.globe.cesium.camera
                      camera.flyTo({
                        destination: Cesium.Cartesian3.fromDegrees(selectedImage.longitude, selectedImage.latitude, selectedImage.height),
                        orientation: {
                          heading: selectedImage.azimuth * Cesium.Math.PI / 180,
                          pitch: selectedImage.tilt * Cesium.Math.PI / 180,
                          roll: selectedImage.roll * Cesium.Math.PI / 180
                        },
                        complete: function () {
                        // Lock the camera
                          var scene = scope.globe.cesium.scene
                          scene.screenSpaceCameraController.enableRotate = false
                          scene.screenSpaceCameraController.enableTranslate = false
                          scene.screenSpaceCameraController.enableTilt = false
                          scene.screenSpaceCameraController.enableZoom = false
                        },
                        duration: 7
                      })
                    }
                  }
                }
              }) // apply
            } // leftclick function

            wheelFunction = function (movement) {
              scope.$apply(function () {
                if (movement < 0){

                  var camera = scope.globe.cesium.camera
                  var fov = camera.frustum.fov
                  if (fov > Cesium.Math.PI / 20) {
                    camera.frustum.fov = fov + 0.05
                  }

                }else{
                  var camera = scope.globe.cesium.camera
                  var fov = camera.frustum.fov
                  if (fov > Cesium.Math.PI / 20) {
                    camera.frustum.fov = fov - 0.05
                  }

                }
              })
            }

          // Left click handler: get 3D coordinates
            var leftClickHandler = new Cesium.ScreenSpaceEventHandler(scope.globe.cesium.scene.canvas)
            leftClickHandler.setInputAction(leftClickFunction, Cesium.ScreenSpaceEventType.LEFT_CLICK)

          // Left double click handler: zoom in the image
            var leftDoubleClickHandler = new Cesium.ScreenSpaceEventHandler(scope.globe.cesium.scene.canvas)
            leftDoubleClickHandler.setInputAction(leftDoubleClickFunction, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK)

            // Left double click handler: zoom in the image
            var wheelHandler = new Cesium.ScreenSpaceEventHandler(scope.globe.cesium.scene.canvas)
            wheelHandler.setInputAction(wheelFunction, Cesium.ScreenSpaceEventType.WHEEL )


          // When element is destroyed, destroy the viewer
            element.on('$destroy', function () {
              leftClickHandler.destroy()
              leftDoubleClickHandler.destroy()
              wheelHandler.destroy()
            // leftDownHandler.destroy();
            // mouseMoveHandler.destroy();
            // leftUpHandler.destroy();

              scope.globe.cesium.destroy()
            })
          }// if showGlobe
        }) // watch
      } // post
    } // link
  }
}]) // return

// function addModel(scope, scene, url){
//  console.log('scope.cameraOrientation.long add model' ,scope.cameraOrientation.long, url)
//   //Set offset
//   var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(
//       Cesium.Cartesian3.fromDegrees(scope.cameraOrientation.long, scope.cameraOrientation.lat, scope.cameraOrientation.Z));//3712.9 8.0945, 46.4315, 0.0));
//
//   //Get model
//   scope.$parent["model"] = Cesium.Model.fromGltf({
//     url : url,//path2model+modelName,
//     modelMatrix : modelMatrix,
//     scale : 1,//0.05,
//     incrementallyLoadTextures: false,
//     show: true,
//   })
//   //console.log('mod', mod, scene.primitives)
//   var model = scene.primitives.add(scope.$parent["model"])//add(mod);
//
// };

// function changeVisibility(){
//   //Get
//   if (mod.show == true){
//     mod.show=false;
//   }else{
//     mod.show=true;
//   }
// }
// function increaseTransparency(){
//
//   var mat = mod.getMaterial("material0");//("material0");
//   var curT = mat.getValue('transparency');
//   if (curT<=0.9){
//     mat.setValue('transparency', curT+0.1);
//   }
// };
// function decreaseTransparency(){
//   var mat = mod.getMaterial("material0");//("material0");
//   var curT = mat.getValue('transparency');
//   if (curT>=0.1){
//     mat.setValue('transparency', curT-0.1);
//   }
// }
