angular.module('moveApp', []).directive('moveButtonsGame', ['$compile', function ($compile) {
  return {
    restrict: 'AE',
    replace: true,
    transclude: true,
    template: function (elem, attr) {
      console.log('elem', elem)
      console.log('attr', attr)
      return "<object type='image/svg+xml' data='../icons/moveButtonsGame.svg'></object>"
    },
    scope: {
      turnLeft: '&turnLeftFunction',
      turnRight: '&turnRightFunction',
      turnUp: '&turnUpFunction',
      turnDown: '&turnDownFunction',
      zoomIn: '&zoomInFunction',
      zoomOut: '&zoomOutFunction'
    },
    link: function (scope, element, attrs) {
      console.log('yahhhhhhhhhhhhhhhhhhhh')
      // var statusChanged = function(newValue, oldValue) {
      // scope.keyClick=function(){
      //   console.log('braaaaaaaa')
      //   scope.moveUp()()
      // }
      var init = function () {
        var turnUpElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnUp'))
        console.log('turupelm', turnUpElm)
        turnUpElm.attr('ng-click', 'turnUp()()')
        $compile(turnUpElm)(scope)

        var turnDownElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnDown'))
        turnDownElm.attr('ng-click', 'turnDown()()')
        $compile(turnDownElm)(scope)

        var turnLeftElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnLeft'))
        turnLeftElm.attr('ng-click', 'turnLeft()()')
        $compile(turnLeftElm)(scope)

        var turnRightElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('turnRight'))
        turnRightElm.attr('ng-click', 'turnRight()()')
        $compile(turnRightElm)(scope)

        var zoomInElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('zoomIn'))
        console.log('zoomInElm', zoomInElm)
        zoomInElm.attr('ng-click', 'zoomIn()()')
        $compile(zoomInElm)(scope)

        var zoomOutElm = angular.element(element[0]
          .getSVGDocument()
          .getElementById('zoomOut'))
        zoomOutElm.attr('ng-click', 'zoomOut()()')
        $compile(zoomOutElm)(scope)
        //   console.log('yahoo')
        // }
        // console.log('upel', upElm)
        // upElm[0].attr("ng-click", "keyClick()");
      }
      // if (element[0].getSVGDocument()) {
      //   init();
      // } else {
      //   element.on("load", init);
      // }
      try {
        if (element[0].getSVGDocument()) {
          init()
        } else {
          element.on('load', init)
        }
      } catch (exception) {
        // For ie
        angular.element(document).ready(init)
      }

      // $compile(element)(scope);
    }
  }
}])
