'use strict';

(function () {
  var module = angular.module('ui.openseadragon', [])
  module.directive('seadragon', [function () {
    return {
      restrict: 'AE', // how the parameters have to be specified in the html page, Attribute, Element
      scope: { // elements which are transfered from the parents
        options: '=',
        wrapper: '=?',
        tilesource: '@', // @ means string
        prefixUrl: '@',
        gcpIconSelected: '@', /// /
        gcpIcon: '@',
        myGCPs: '=gcp', // Correspond to gcp = "myGCPs" in the html file
        selectGCP: '&selectionFunction',
        deleteGCP: '&deletionFunction',
        drawCursor: '&drawCursorFunction',
        locationMatrix: '=locmat',
        httpPromise: '=httpPromise',
        imageName: '=imageName',
        osdPromise: '=osdPromise',
        drawGCP: '=drawGcp',
        image: '=image',
        osdGCP: '&osdGcpFunction',
        alertClickGlobe: '&alertClickGlobeFunction',
        mode: '=mode',
        //osdObservation: '&osdObservationFunction',
        selectionValidation: '&selectionValidationFunction'
      },

      controller: ['$scope', function ($scope) {
        $scope.osd = null
      }],

      link: function (scope, element, attrs) {
        scope.$watch('image', function () {
          if (scope.image != null) {

            scope.updateViewportCount = 0
            scope.imageName = scope.image.id

            scope.imagePath = '../data/collections/'+scope.image.collection_id+'/images/tiles/' + scope.image.id + '.dzi'

                  // Create options object
            var opts = angular.extend({}, scope.options, {
              id: 'openseadragon-' + Math.random(),
              element: element[0]
            })
                  // if (attrs.tilesource) {
                  //     //opts.tileSources = [attrs.tilesource];
                  //     opts.tileSources = [[scope.imagePath]];
                  // }
            opts.tileSources = scope.imagePath
            if (attrs.prefixUrl) {
              opts.prefixUrl = attrs.prefixUrl
            }

            // Create the viewer
            scope.osd = OpenSeadragon(opts)

            // // Add selection to the viewer
            // var selection = scope.osd.selection({
            //   element: null, // html element to use for overlay
            //   showSelectionControl: false, // show button to toggle selection mode
            //   toggleButton: null, // dom element to use as toggle button
            //   showConfirmDenyButtons: true,
            //   styleConfirmDenyButtons: true,
            //   returnPixelCoordinates: false, // We want relative coordinates
            //   keyboardShortcut: 'c', // key to toggle selection mode
            //   rect: null, // initial selection as an OpenSeadragon.SelectionRect object
            //   allowRotation: false, // turn selection rotation on or off as needed
            //   startRotated: false, // alternative method for drawing the selection; useful for rotated crops
            //   startRotatedHeight: 0.1, // only used if startRotated=true; value is relative to image height
            //   restrictToImage: true, // true = do not allow any part of the selection to be outside the image
            //   onSelection: function(rect) {
            //     console.log('in on selection')
            //     scope.selectionValidation()(rect)
            //   }, // callback
            //   prefixUrl: null, // overwrites OpenSeadragon's option
            //   navImages: { // overwrites OpenSeadragon's options
            //     selection: {
            //         REST:   'selection_rest.png',
            //         GROUP:  'selection_grouphover.png',
            //         HOVER:  'selection_hover.png',
            //         DOWN:   'selection_pressed.png'
            //     },
            //     selectionConfirm: {
            //         REST:   'selection_confirm_rest.png',
            //         GROUP:  'selection_confirm_grouphover.png',
            //         HOVER:  'selection_confirm_hover.png',
            //         DOWN:   'selection_confirm_pressed.png'
            //     },
            //     selectionCancel: {
            //         REST:   'selection_cancel_rest.png',
            //         GROUP:  'selection_cancel_grouphover.png',
            //         HOVER:  'selection_cancel_hover.png',
            //         DOWN:   'selection_cancel_pressed.png'
            //     },
            //   }
            // })
            // selection.disable()

            // console.log('scope.gcpIcon', scope.options.gcpIcon)
            // //scope.drawGCP();
            // for (var i in scope.myGCPs.dictGCPs) {
            //   var gcp = scope.myGCPs.dictGCPs[i]
            //   if ((gcp.x != null) && (gcp.X != null)) {
            //     console.log('gcp', gcp)
            //     var img = document.createElement('img')
            //     img.src = scope.options.gcpIcon
            //     img.id = gcp.id.toString()
            //     img.isReproj = false
            //     img.className = 'gcp'
            //     // var viewportPoint = $scope.mysd.viewer.TiledImage.imageToViewportCoordinates(xy[0], xy[1]);
            //     var viewportPoint = scope.osd.viewport.imageToViewportCoordinates(gcp.x, gcp.y)
            //     console.log('viewportPoint', viewportPoint, gcp.x)
            //     var ov = scope.osd.addOverlay(img, viewportPoint, 'CENTER')// CENTER, var ov =
            //
            //     var label = document.createElement('p')
            //     label.id = gcp.id + '_label'
            //     label.innerHTML = (gcp.id + 1).toString()
            //     label.className = 'label'
            //     // var viewportPoint = $scope.mysd.viewer.TiledImage.imageToViewportCoordinates(xy[0], xy[1]);
            //     var viewportPoint = scope.osd.viewport.imageToViewportCoordinates(gcp.x, gcp.y)
            //     var ov = scope.osd.addOverlay(label, viewportPoint, 'CENTER')
            //   }
            // }
            // //Trick for the placement
            // var zoomLevel = scope.osd.viewport.getZoom(true)
            // var event = scope.osd.viewport.zoomTo(zoomLevel - 0.01, viewportPoint, true)
            // scope.osd.viewport.goHome()

                  // Create a wrapper
            var wrapper = {
              setFullScreen: function (fullScreen) {
                scope.osd.setFullScreen(fullScreen)
              },
              forceRedraw: function () {
                scope.osd.forceRedraw()
              },
              mouse: {
                position: null,
                imageCoord: null,
                viewportCoord: null
              },
              zoom: 0,
              viewport: {
                bounds: null,
                center: null,
                rotation: 0,
                zoom: 0
              },
              viewer: scope.osd

            }
                  // if @name is set, put the wrapper in the scope and handle the events
                  // var zoomHandler = null;
            var updateViewportHandler = null
            var clickHandler = null
            var dragHandler = null

            scope.wrapper = wrapper
            // scope.$parent['osdObject.mysd'] = wrapper

            // if (attrs.name) {
                      // Make the OSD available to parent scope
            //  scope.$parent[attrs.name] = wrapper

                      // Define event handlers
                      // zoomHandler = function (e) {
                      //     scope.$apply(function () {
                      //         wrapper.zoom = e.zoom;
                      //     });
                      // }

              updateViewportHandler = function (e) {
                if (scope.updateViewportCount == 0) {
                  if (scope.mode === 'correspondences' || scope.mode === 'validation_viz') { //scope.mode === 'correspondences' ||
                    scope.drawGCP() // is here because the viewer needs to be loaded to render overlays
                    scope.updateViewportCount += 1
                  }

                }

                scope.$apply(function () {
                  wrapper.viewport = {
                      bounds: scope.osd.viewport.getBounds(false),
                      center: scope.osd.viewport.getCenter(false),
                      rotation: scope.osd.viewport.getRotation(),
                      zoom: scope.osd.viewport.getZoom(false)
                    }
                })
              }

              clickHandler = function (e) {
                scope.$apply(function () {
                          // reset paning
                  e.eventSource.panHorizontal = true
                  e.eventSource.panVertical = true

                  var viewportPoint = scope.osd.viewport.pointFromPixel(e.position)
                  var imagePoint = scope.osd.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y)

                          // Check if an overlay is present at this location
                  var clickOnPt = false
                  if (e.originalEvent.target.constructor.name == 'HTMLImageElement') {
                      clickOnPt = true
                    };

                          // Check if click is rapid
                  var quickClick = e.quick
                          // A new point is created if the click is not over an
                          // existing point and the click is quick (not a drag)
                          // and a GCP is selected and the GCP is empty
                          // (scope.myGCPs.selectedGCPid != null) &&
                  if (scope.mode == 'correspondences') {
                      if ((scope.myGCPs.dictGCPs[scope.myGCPs.curGCPid].x == null) && (clickOnPt == false) && (quickClick)) {

                        var xy = [imagePoint.x, imagePoint.y]
                        // var xy = [imagePoint.x.toFixed(1), (imagePoint.y).toFixed(1)]
                        scope.osdGCP()(xy, viewportPoint)

                              // if (scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].x == null){
                              //   var img = document.createElement("img");
                              //   img.src = scope.options.gcpIconSelected;
                              //   img.id = scope.myGCPs.curGCPid-1;
                              //   img.isReproj = false;
                              //
                              //   var ov = scope.osd.addOverlay(img, viewportPoint, 'CENTER');//, 'CENTER'
                              //
                              //   //Fill the GCP
                              //   scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].x = (imagePoint.x).toFixed(1);
                              //   scope.myGCPs.dictGCPs[scope.myGCPs.selectedGCPid].y = (imagePoint.y).toFixed(1);

                              // }
                      } else if (clickOnPt && quickClick) {
                                // Rapid click on existing point
                          var idToSelect = e.originalEvent.target.id

                          scope.selectGCP()(idToSelect)
                        } else if (quickClick) {
                              // If the GCP doesnt have image coordinates : alert
                            if (scope.myGCPs.dictGCPs[scope.myGCPs.curGCPid].X == null) {
                                // window.alert('alerte cobra')
                              console.log('alert alert')
                              scope.alertClickGlobe()()
                            }
                          };
                    }
                    // else if (scope.mode === 'correction') {
                    //   if ((clickOnPt == false) && (quickClick)){
                    //     // send image coordinate expressed as ratio of the size
                    //     console.log('imagePoint', imagePoint)
                    //     var xy = [(imagePoint.x).toFixed(1), (imagePoint.y).toFixed(1)]
                    //     scope.osdObservation()(xy, viewportPoint)
                    //   }
                    //
                    // }
                })
              }

              dragHandler = function (e) { ///
                scope.$apply(function () {
                  var viewportPoint = scope.osd.viewport.pointFromPixel(e.position)
                  var imagePoint = scope.osd.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y)

                  var idSelected = null

                          // Check if an overlay is present at this location
                  var clickOnPt = false
                  if (e.originalEvent.target.constructor.name == 'HTMLImageElement') {
                      clickOnPt = true
                    };
                  idSelected = e.originalEvent.target.id

                  if (clickOnPt && (scope.myGCPs.selectedGCPid == idSelected)) {
                            // block paning
                      e.eventSource.panHorizontal = false
                      e.eventSource.panVertical = false

                            // update GCP
                      scope.myGCPs.dictGCPs[idSelected].x = imagePoint.x // ).toFixed(1)
                      scope.myGCPs.dictGCPs[idSelected].y = imagePoint.y // ).toFixed(1)

                            // update overlay

                      // find the appropriate element (target)
                      var nOv = e.eventSource.currentOverlays.length
                      var elementToMove = null
                      var idOverlayToMove = null
                      for (var i = 0; i < nOv; i++) {
                        if (e.eventSource.currentOverlays[i].element.id == idSelected) {
                            idOverlayToMove = i
                          };
                      };
                      elementToMove = e.eventSource.currentOverlays[idOverlayToMove].element
                      e.eventSource.updateOverlay(elementToMove, viewportPoint, 'CENTER')

                      // find the appropriate element (label)
                      var nOv = e.eventSource.currentOverlays.length
                      var elementToMove = null
                      var idOverlayToMove = null
                      for (var i = 0; i < nOv; i++) {
                        if (e.eventSource.currentOverlays[i].element.id == idSelected+'_label') {
                            idOverlayToMove = i
                          };
                      };
                      elementToMove = e.eventSource.currentOverlays[idOverlayToMove].element
                      e.eventSource.updateOverlay(elementToMove, viewportPoint, 'CENTER')
                    } else {

                    };
                })
              }

                      // Assign event handlers
                      // scope.osd.addHandler("zoom", zoomHandler);
              scope.osd.addHandler('update-viewport', updateViewportHandler)
              scope.osd.addHandler('canvas-click', clickHandler)
              scope.osd.addHandler('canvas-drag', dragHandler)
                      // scope.osd.addHandler("canvas-enter", enterHandler);
                      // scope.osd.addHandler("canvas-quit", quitHandler);

                      // Add a mouse handler
              scope.mouse = new OpenSeadragon.MouseTracker({
                element: scope.osd.canvas,
                enterHandler: function (e) {
                  if (scope.osd.viewport) {
                      var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
                      var pos = e.position.plus(coord)
                      var mouse = {
                        position: pos,
                        imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                        viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
                      }
                      scope.$apply(function () {
                        wrapper.mouse = mouse
                      })
                    }
                },
                moveHandler: function (e) {
                  if (scope.osd.viewport) {
                      var webPoint = e.position
                      var viewportPoint = scope.osd.viewport.pointFromPixel(webPoint)

                      var imageWidth = scope.osd.source.Image.Size.Width
                      var imageHeight = scope.osd.source.Image.Size.Height
                      var ratio = imageHeight / imageWidth

                                // The viewport limits are 1 and 1 for a square images, else 1 is equal to the width
                                // if ((viewportPoint.x > 0) && (viewportPoint.x < 1) && (viewportPoint.y > 0) && (viewportPoint.y < ratio)){
                                //   scope.drawCursor()(viewportPoint);
                                // }

                      var coord = OpenSeadragon.getElementPosition(scope.osd.canvas)
                      var pos = e.position// .plus(coord);

                      var mouse = {
                        position: pos,
                        viewportPoint: scope.osd.viewport.pointFromPixel(webPoint),
                        imageCoord: scope.osd.viewport.windowToImageCoordinates(pos),
                        viewportCoord: scope.osd.viewport.windowToViewportCoordinates(pos)
                      }

                      scope.$apply(function () {
                        wrapper.mouse = mouse
                      })
                    }
                },
                exitHandler: function (e) {
                  scope.$apply(function () {
                      wrapper.mouse.position = null
                      wrapper.mouse.imageCoord = null
                      wrapper.mouse.viewportCoord = null

                      var existCursor = !!document.getElementById('cursorIcon')
                      if (existCursor) {
                        var div = document.getElementById('cursorIcon')
                        div.parentNode.removeChild(div)
                      }
                    })
                }
              })
              scope.mouse.setTracking(true)
            // }

                  // When element is destroyed, destroy the viewer
            element.on('$destroy', function () {
              // Commented to avoid that the viewer is detroyed with ng-switch
              //         // if @nam eis set, remove it from parent scope, and remove event handlers
              // if (attrs.name) {
              //             // Remove from parent scope
              //   scope.$parent[attrs.name] = null
              //   scope.$parent[viewer] = null // me
              //
              //             // Destroy mouse handler
              //   scope.mouse.destroy()
              //
              //             // Remove event handlers
              //             // scope.osd.removeHandler("zoom", zoomHandler);
              //   scope.osd.removeHandler('update-viewport', updateViewportHandler)
              //   scope.osd.removeHandler('canvas-click', clickHandler)
              //   scope.osd.removeHandler('canvas-drag', dragHandler)
              // }
              //
              //         // Destroy the viewer
              // scope.osd.destroy()
            })
          }
        })
      }

    }
  }])
})()
