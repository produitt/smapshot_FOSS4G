mysmapApp.directive('ghVisualization', function () {
  // constants
  // var margin = 20,
  //   width = 500,
  //   height = 200 - .5 - margin,
  //   color = d3.interpolateRgb("#f77", "#77f");

  return {
    restrict: 'E',
    scope: {
      val: '=',
      grouped: '=',
      me: '=',
      user: '=',
      rankingReady: '=rankingReady',
      size: '='
    },
    link: function (scope, element, attrs) {
      var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = scope.size.width - margin.left - margin.right,
        height = scope.size.height - margin.top - margin.bottom

      // set up initial svg object
      var chart = d3.select(element[0])
        .append('svg')
          // .attr("width", width)
          // .attr("height", height + margin + 100);
        	.attr('width', width)
        	.attr('height', height)

      // console.log('chart', chart)

      scope.$watch('rankingReady', function (newVal, oldVal) {
        // console.log('bibi')
        // var data = [
        // 	{name: "Locke",    value:  4},
        // 	{name: "Reyes",    value:  8},
        // 	{name: "Ford",     value: 15},
        // 	{name: "Jarrah",   value: 16},
        // 	{name: "Shephard", value: 23},
        // 	{name: "Kwon",     value: 42}
        // ];
        // console.log('data', data)
      //   for (id in response){
      //     var curUser = response[id];
      //     var d = {
      //       rank = id+1,
      //       nImages = curUser.nImages,
      //       name = curUser.username,
      //       value = curUser.nImages,
      //       color: ""
      //     }
      //     if (curUser.volunteer_id == me.volunteer_id){
      //       d.color = 'red'
      //       $scope.volunteer.nImages = curUser.nImages;
      //       $scope.volunteer.rank = id+1;
      //       $scope.user.diffImages = $scope.user.nImages-$scope.volunteer.nImages;
      //       $scope.user.diffRank = $scope.user.rank-$scope.volunteer.rank;
      //
      //     }else{
      //       d.color = 'green'
      //     }
      //     data.push(d)
      //   }
      // })
        // var meRank = 1
        if (scope.rankingReady == true) {
          console.log('mimi', scope.me)
          var data = scope.val
          // var data = []
          // scope.me.rank = 5
          // scope.me.nImages = 20
          // for (i = 0; i < 10; i++){
          //   if (i == scope.me.rank){
          //     data.push({"username": "name"+i, "value": i, "color": "red", "rank": i})
          //   }else{
          //     data.push({"username": "name"+i, "value": i, "color": "green", "rank": i})
          //   }
          //   //vals.push((i,i))
          // }

          // set the ranges
          // var x = d3.scaleBand()
          //           .range([0, width])
          //           .padding(0.1);
          var x = d3.scaleLinear()
                    .range([0, width])

          var y = d3.scaleLinear()
                    .range([height, 0])

          // Scale the range of the data in the domains
          // x.domain(data.map(function(d) { return d.name; }));
          console.log('data', data)
          var maxRank = d3.max(data, function (d) { return d.rank }) + 1
          console.log('maxRank', maxRank)
          x.domain([0, d3.max(data, function (d) { return d.rank }) + 1])
          y.domain([0, d3.max(data, function (d) { return d.value })])

          var zoom = d3.zoom()
            .scaleExtent([1, 40])
            // .translateExtent([[0, 0], [width, height]])
            .on('zoom', zoomed)

          // var div = d3.select(element[0]).append("div")
          //     .attr("class", "tooltip")
          //     .style("opacity", 0)
          //     .style("position", "relative")

          var tip = d3.tip()
            .attr('class', 'd3-tip')
            .direction('n')

          // append the rectangles for the bar chart
          var gBar = chart.selectAll('.bar')
              .data(data)
              .enter()
              .append('rect')
              .attr('class', 'bar')
              .attr('x', function (d) {
                console.log('x', d.rank, x(d.rank))
                return x(d.rank - 1)
              })
              .attr('width', (width / maxRank) - 1)
              .attr('y', function (d) {
                console.log('y', d.value, y(d.value))
                if (d.value == 0) {
                  return y(0.5)
                } else {
                  return y(d.value)
                }
              })
              .attr('height', function (d) {
                console.log('height', height - y(d.value))
                if (d.value == 0) {
                  return height - y(0.5)
                } else {
                  return height - y(d.value)
                }
              })
              .style('fill', function (d) { return (d.color) })
              .on('mouseover', function (d) {
                scope.$apply(function () {
                  scope.user.username = d.username
                  scope.user.nImages = d.value
                  scope.user.rank = d.rank

                  // scope.me.rank = 20
                  // scope.me.nImages = 20
                  // console.log(scope.user.nImages, scope.me.nImages)
                  // Update compared user values
                  scope.user.diffImages = scope.user.nImages - scope.me.nImages
                  scope.user.diffRank = scope.user.rank - scope.me.rank
                  tip
                  .html('<h4>' + d.username + '</h4>' +
                      '<ul>' +
                      '<li> Classement: ' + d.rank +// +" ("+ scope.user.diffRank +")"
                        '</li>' +
                      '<li>Images géoréférencées: ' + scope.user.nImages + '</li>' + // +" ("+ scope.user.diffRank +")"
                      '</ul>'
                    )
                  .show()
                })

                // this.style("fill", "yellow")
                var tr = d3.event// d3.zoomTransform(zoom);
                d3.event.target.style.fill = 'yellow'
                  // var tr = d3.zoomTransform(d3.event.target.node())
                  // console.log('tr', tr)
                  // var xPosition = xAxis.scale(tr.rescaleX(x(d.rank))) //d3.mouse(this)[0] - 15;//
                  // var yPosition = -height+y(d.value);//d3.mouse(this)[1] - 25;
                  // div.transition()
                  //     .duration(200)
                  //     .style("opacity", .9);
                  // div.html(d.name + "<br/>"  + d.value)
                  //     .style("left", (xPosition) + "px")
                  //     .style("top", (yPosition) + "px");
              })
              .on('mouseout', function (d) {
                d3.event.target.style.fill = d.color
                tip.hide()
                  // div.transition()
                  //     .duration(500)
                  //     .style("opacity", 0);
              })
            //  .append("text")
            //  .attr("x", function(d) { return x(d.rank); })
            //  .attr("y", function(d) { return y(d.value); })
            //  //.attr("dy", ".35em")
            //  .text(function(d) { return d.username; });

          var namesText = chart.selectAll('.text')
            .data(data)
            .enter()
            .append('text')
            .attr('class', 'text')
            .style('text-anchor', 'start')// "end")

            .attr('x', function (d) {
              return x(d.rank)
            })
            .attr('y', function (d) {
              return 0.0// y(d.value);
            })
            .attr('dy', '.35em')
            .attr('dx', '.35em')
            .text(function (d) { return d.username })

          // add the x Axis
          // var xAxisBottom = d3.axisBottom(x) //The ticks are on the bottom
          //   .tickFormat(d3.format('.0f'))
          // var gX = chart.append("g")
          //     .attr("transform", "translate("+0+", " + 0 + ")")
          //     .attr("class", "axis")
          //     .call(xAxisBottom);

          // add the x Axis
          // var xAxisTop = d3.axisTop(x) //The ticks are above the line
          //   .tickFormat(d3.format('.0f'))
          // var gXTop = chart.append("g")
          //     .attr("transform", "translate("+0+", " + (0+height) + ")")
          //     //.attr("class", "axis axis--x")
          //     .attr("class", "axis")
          //     .call(xAxisTop);

          // add the y Axis
          var yAxisLeft = d3.axisLeft(y)
            .ticks(10)
            .tickSize(width)
            .tickPadding(8 - width)
          var gYLeft = chart.append('g')
              .attr('class', 'axis')
              .attr('transform', 'translate(' + (0 + width) + ', ' + 0 + ')')
              .call(yAxisLeft)

          var yAxisRight = d3.axisRight(y)
          var gYRight = chart.append('g')
              .attr('class', 'axis')
              .attr('transform', 'translate(' + 0 + ', ' + 0 + ')')
              .call(yAxisRight)

          chart.call(zoom).call(tip)

          // Compute translation
          var scale = width / maxRank // Scale between the pixel space and the rank space
          console.log('scope.me.rank', scope.me.rank, maxRank)
          var xTranslation = (maxRank / 2 - scope.me.rank) * scale
          console.log('xTranslation', xTranslation)
          zoom.translateBy(chart, xTranslation, 0)
          var scale = maxRank / 11
          zoom.scaleTo(chart, scale)

          // Set max extent
          zoom.translateExtent([[0, 0], [width, height]])

          // var iniZoom = d3.zoom()
          //  .translateBy(100, 0);
          // chart.call.transition().duration(750)(iniZoom)

          function zoomed () {
            var bars = chart.selectAll('.bar')
            var strTrans = 'translate(' + (d3.event.transform.x) + ' ' + 0 + ') scale(' + d3.event.transform.k + ' 1)'
            bars.attr('transform', strTrans)
            // gXTop.call(xAxisTop.scale(d3.event.transform.rescaleX(x)));

            var namesText = chart.selectAll('.text')
              .attr('x', function (d) {
                return 0
              })
              .attr('y', function (d) {
                return 0
              })
              .attr('transform', function (d) {
                // Compute x coordinate
                // var x = (d.rank*width/maxRank*d3.event.transform.k)+d3.event.transform.x + width/maxRank/2*d3.event.transform.k
                var newx = width / maxRank * d3.event.transform.k * (d.rank - 0.5) + d3.event.transform.x
                // var newy = y(d.value)
                // if (d.value == 0){
                //   var newy = y(0.5)
                // }else{
                //   var newy = y(d.value)
                // }

                // return "translate("+newx+" " +newy+") rotate (-90)"
                return 'translate(' + newx + ' ' + height + ') rotate (-90)'
              })
              .style('opacity', function (d) {
                var widthBar = width / maxRank * d3.event.transform.k
                if (d.color == 'green') {
                  if (widthBar < 60) {
                    return '0'
                  } else {
                    return '1'
                  }
                }
              })
          }
        }

        // var margin = {top: 20, right: 30, bottom: 30, left: 40},
        // 	width = 500 - margin.left - margin.right,
        // 	height = 300 - margin.top - margin.bottom;

        // var x = d3.scale.ordinal()
        // 	.domain(data.map(function(d) { return d.name; }))
        // 	.rangeRoundBands([0, width], .1);
        //
        // var y = d3.scale.linear()
        // 	.domain([0, d3.max(data, function(d) { return d.value; })])
        // 	.range([height, 0])
        //   .nice();
        //
        // var xAxis = d3.svg.axis()
        // 	.scale(x)
        // 	.orient("bottom");
        // //console.log('xAxis', xAxis)
        // //xAxis.ticks(10)
        // //xAxis.tickValues(d3.range(20, 80, 4));
        //
        // var yAxis = d3.svg.axis()
        // 	.scale(y)
        // 	.orient("left")
        //   .innerTickSize(-width)
        //   .outerTickSize(0)
        //   .tickPadding(10);
        // //yAxis.tickValues(d3.range(0, 100, 5));
        //
        // var line = d3.svg.line()
        //   //.x(function(d) { return xScale(d.x); })
        //   .y(function(d) { return y(d.value); });
        //
        // //var chart = d3.select(".chart")
        // // var chart = d3.select(element[0])
        // // 	.attr("width", width + margin.left + margin.right)
        // // 	.attr("height", height + margin.top + margin.bottom)
        // var d3zoom = d3.behavior.zoom()
        // .scaleExtent([1, 20])
        // .on("zoom", zoom)
        // .scale(5);
        //
        // chart.append("g")
        // 	.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        // 	.call(d3zoom);
        //
        // //Add a clip path, so the content outside the domain should be hidden
        //
        // var mSvg = d3.select(".chart");
        //
        //
        //
        // //Add a "defs" element to the svg
        // var defs = mSvg.append("defs");
        //
        // //Append a clipPath element to the defs element, and a Shape
        // // to define the cliping area
        // defs.append("clipPath").attr('id','my-clip-path').append('rect')
        //     .attr('width',width) //Set the width of the clipping area
        //     .attr('height',height); // set the height of the clipping area
        //
        // //clip path for x axis
        // defs.append("clipPath").attr('id','x-clip-path').append('rect')
        //     .attr('width',width) //Set the width of the clipping area
        //     .attr('height',height + margin.bottom); // set the height of the clipping area
        //
        // //add a group that will be clipped (this will contain the bars)
        // var barsGroup = chart.append('g');
        //
        // //Set the clipping path to the group (g element) that you want to clip
        // barsGroup.attr('clip-path','url(#my-clip-path)');
        //
        // var xAxisGroup = chart.append("g").attr('class','x-axis')
        //
        // xAxisGroup.append('g')
        // 	.attr("class", "x axis")
        // 	.attr("transform", "translate(0," + 0 + ")") //height
        // 	.call(xAxis)
        //   .selectAll("text")
        //   .attr("y", 0)
        //   .attr("x", 0)
        //   // .attr("x", function(d) { return x(d.name); })
        //   // .attr("y", function(d) { return y(d.value); })
        //   //.attr("dy", ".35em")
        //   .attr("transform", "rotate(-90)")
        //   .style("text-anchor", "start");
        //
        // //The xAxis is scalled on zoom, so we need to clip it to
        //
        // xAxisGroup.attr('clip-path','url(#x-clip-path)');
        //
        // // draw y axis with labels and move in from the size by the amount of padding
        // chart.append("g")
        // 	.attr("class", "y axis")
        //   .attr("transform", "translate("+20+",0)")
        // 	.call(yAxis)
        //   .selectAll("text")
        //   .attr("y", 0)
        //   .attr("x", 0)
        //   .style("text-anchor", "start");
        //
        //
        // // chart.append("path")
        // //     .data([data])
        // //     .attr("class", "line")
        // //     .attr("d", line)
        //
        //   // Define the div for the tooltip
        // var div = d3.select("body").append("div")
        //     .attr("class", "tooltip")
        //     .style("opacity", 0)
        //     .style("position", "absolute")
        //
        // var bars = barsGroup.selectAll(".bar")
        // 	.data(data)
        // 	.enter().append("rect")
        // 	.attr("class", "bar")
        // 	.attr("x", function(d) { return x(d.name); })
        // 	.attr("y", function(d) { return y(d.value); })
        // 	.attr("height", function(d) { return height - y(d.value); })
        // 	.attr("width", x.rangeBand())
        //   .style("fill", function(d) { return (d.color); })
        //   .on("mouseover", function(d) {
        //     var xPosition = d3.mouse(this)[0] - 15;
        //     var yPosition = d3.mouse(this)[1] - 25;
        //     div.transition()
        //         .duration(200)
        //         .style("opacity", .9);
        //     div.html(d.name + "<br/>"  + d.value)
        //         .style("left", (xPosition) + "px")
        //         .style("top", (yPosition) + "px");
        //     })
        //   .on("mouseout", function(d) {
        //       div.transition()
        //           .duration(500)
        //           .style("opacity", 0);
        //   })
        //   //.attr("transform", "translate(" + -200+",0)scale(" + 5 + ",1)");
        //
        // function zoom() {
        //   console.log('t', d3.event.translate[0])
        //   console.log('s', d3.event.scale)
        //   console.log('b', x.rangeBand())
        //   var t = d3.event.translate[0]
        //   console.log('t*s*b', d3.event.translate[0]/d3.event.scale)
        //   var s = d3.event.scale
        //   var ts = t*s;
        //   if (ts>0){
        //     t = 0;
        //     d3zoom.translate([0, 0]);
        //   }
        //   bars.attr("transform", "translate(" + t +",0)scale(" + d3.event.scale + ",1)");
        //     chart.select(".x.axis").attr("transform", "translate(" + t +","+(height)+")")
        //         .call(xAxis.scale(x.rangeRoundBands([0, width * d3.event.scale],.1 * d3.event.scale)));
        //   chart.select(".y.axis").call(yAxis);
        // }
        // chart.call(d3zoom)

        // return

        // // clear the elements inside of the directive
        // vis.selectAll('*').remove();
        //
        // // if 'val' is undefined, exit
        // if (!newVal) {
        //   return;
        // }
        //
        // // Based on: http://mbostock.github.com/d3/ex/stack.html
        // var n = newVal.length, // number of layers
        //     m = newVal[0].length, // number of samples per layer
        //     data = d3.layout.stack()(newVal);
        //
        // var mx = m,
        //     my = d3.max(data, function(d) {
        //       return d3.max(d, function(d) {
        //         return d.y0 + d.y;
        //       });
        //     }),
        //     mz = d3.max(data, function(d) {
        //       return d3.max(d, function(d) {
        //         return d.y;
        //       });
        //     }),
        //     x = function(d) { return d.x * width / mx; },
        //     y0 = function(d) { return height - d.y0 * height / my; },
        //     y1 = function(d) { return height - (d.y + d.y0) * height / my; },
        //     y2 = function(d) { return d.y * height / mz; }; // or `my` not rescale
        //
        // // Layers for each color
        // // =====================
        //
        // var layers = vis.selectAll("g.layer")
        //     .data(data)
        //   .enter().append("g")
        //     .style("fill", function(d, i) {
        //       return color(i / (n - 1));
        //     })
        //     .attr("class", "layer");
        //
        // // Bars
        // // ====
        //
        // var bars = layers.selectAll("g.bar")
        //     .data(function(d) { return d; })
        //   .enter().append("g")
        //     .attr("class", "bar")
        //     .attr("transform", function(d) {
        //       return "translate(" + x(d) + ",0)";
        //     });
        //
        // bars.append("rect")
        //     .attr("width", x({x: .9}))
        //     .attr("x", 0)
        //     .attr("y", height)
        //     .attr("height", 0)
        //   .transition()
        //     .delay(function(d, i) { return i * 10; })
        //     .attr("y", y1)
        //     .attr("height", function(d) {
        //       return y0(d) - y1(d);
        //     });
        //
        // // X-axis labels
        // // =============
        //
        // var labels = vis.selectAll("text.label")
        //     .data(data[0])
        //   .enter().append("text")
        //     .attr("class", "label")
        //     .attr("x", x)
        //     .attr("y", height + 6)
        //     .attr("dx", x({x: .45}))
        //     .attr("dy", ".71em")
        //     .attr("text-anchor", "middle")
        //     .text(function(d, i) {
        //       return d.date;
        //     });
        //
        // // Chart Key
        // // =========
        //
        // var keyText = vis.selectAll("text.key")
        //     .data(data)
        //   .enter().append("text")
        //     .attr("class", "key")
        //     .attr("y", function (d, i) {
        //       return height + 42 + 30*(i%3);
        //     })
        //     .attr("x", function (d, i) {
        //       return 155 * Math.floor(i/3) + 15;
        //     })
        //     .attr("dx", x({x: .45}))
        //     .attr("dy", ".71em")
        //     .attr("text-anchor", "left")
        //     .text(function(d, i) {
        //       return d[0].user;
        //     });
        //
        // var keySwatches = vis.selectAll("rect.swatch")
        //     .data(data)
        //   .enter().append("rect")
        //     .attr("class", "swatch")
        //     .attr("width", 20)
        //     .attr("height", 20)
        //     .style("fill", function(d, i) {
        //       return color(i / (n - 1));
        //     })
        //     .attr("y", function (d, i) {
        //       return height + 36 + 30*(i%3);
        //     })
        //     .attr("x", function (d, i) {
        //       return 155 * Math.floor(i/3);
        //     });
        //
        //
        // // Animate between grouped and stacked
        // // ===================================
        //
        // function transitionGroup() {
        //   vis.selectAll("g.layer rect")
        //     .transition()
        //       .duration(500)
        //       .delay(function(d, i) { return (i % m) * 10; })
        //       .attr("x", function(d, i) { return x({x: .9 * ~~(i / m) / n}); })
        //       .attr("width", x({x: .9 / n}))
        //       .each("end", transitionEnd);
        //
        //   function transitionEnd() {
        //     d3.select(this)
        //       .transition()
        //         .duration(500)
        //         .attr("y", function(d) { return height - y2(d); })
        //         .attr("height", y2);
        //   }
        // }
        //
        // function transitionStack() {
        //   vis.selectAll("g.layer rect")
        //     .transition()
        //       .duration(500)
        //       .delay(function(d, i) { return (i % m) * 10; })
        //       .attr("y", y1)
        //       .attr("height", function(d) {
        //         return y0(d) - y1(d);
        //       })
        //       .each("end", transitionEnd);
        //
        //   function transitionEnd() {
        //     d3.select(this)
        //       .transition()
        //         .duration(500)
        //         .attr("x", 0)
        //         .attr("width", x({x: .9}));
        //   }
        // }
        //
        // // reset grouped state to false
        // scope.grouped = false;
        //
        // // setup a watch on 'grouped' to switch between views
        // scope.$watch('grouped', function (newVal, oldVal) {
        //   // ignore first call which happens before we even have data from the Github API
        //   if (newVal === oldVal) {
        //     return;
        //   }
        //   if (newVal) {
        //     transitionGroup();
        //   } else {
        //     transitionStack();
        //   }
        // });
      })
    }
  }
})
