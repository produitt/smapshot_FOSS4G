angular.module('challengeImage', []).directive('challengeImageDirective', function () {
  return function (scope, element, attrs) {
    console.log('element', element)
    console.log('attrs', attrs)
    attrs.$observe('challengeImageDirective', function (value) {
      element.css({
        'background-image': 'url(../data/collections/' + value + '/example/example.jpg)'
      })
    })
  }
})
