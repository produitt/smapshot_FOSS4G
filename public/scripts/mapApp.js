var mapApp = angular.module('mapApp', ['ui-leaflet', 'ui.openseadragon',
'ui.bootstrap', 'leafApp', 'authApp', 'imageSliderApp', 'langApp', 'searchApp',
'textApp', 'imageQueryApp', 'dateApp', 'ownerQueryApp', 'collectionsQueryApp',
'angularCesium', 'cesiumApp', 'correctionsApp', 'osdApp', 'observationsApp',
'volunteersApp', 'badGeolocApp']); //, 'ngMaterial', ''leaflet-directive''
mapApp.config(function($logProvider, $locationProvider){
  $logProvider.debugEnabled(false);
  $locationProvider.html5Mode(true);

  // $routeProvider
  //
  //     // route for the home page
  //     .when('/map', {
  //         templateUrl : 'map/mapContent.html',
  //         controller  : 'mapController'
  //     })
  //
  //     // route for the about page
  //     .when('{ownerId}/map', {
  //         templateUrl : 'map/mapContent.html',
  //         controller  : 'mapController'
  //     })
})
