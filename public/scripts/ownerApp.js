var ownerApp = angular.module('ownerApp', ['ngSanitize', 'ui.bootstrap',
  'ownerQueryApp', 'imageQueryApp', 'volunteersApp', 'authApp',
  'challengeImage', 'langApp', 'collectionsQueryApp'])
