
var dashboardApp = angular.module('dashboardApp', ['ui.bootstrap', 'ui.openseadragon',
'ngSanitize', 'ngCsv',
'authApp', 'searchApp', 'dbApp', 'validationsApp', 'volunteersApp',
'correctionsApp', 'remarksApp', 'observationsApp', 'imageQueryApp',
'collectionsQueryApp', 'ownerQueryApp', 'problemsApp', 'badGeolocApp']) // 'errorsApp',
