//Settings for Passport = authentication

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
var models = require('../app/model');

module.exports = function(passport) {
  var opts = {};
  opts.secretOrKey = 'YourKey'; //Key use to encode and decode JWT
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader()
  //opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  //opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt')

  //Find an user with provided id
  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    models.volunteers.findOne({
      where: {
        id: jwt_payload.id//'id'
      }
    }
  ).then(function (volunteer){
      if (volunteer) {
          return done(null, volunteer);
      } else {
          return done(null, false);
      }
    }).catch(function(err){
      return done(err, false)
    });
  }));
};
